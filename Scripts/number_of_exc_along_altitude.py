from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st
import math
import sys, os

src_dir = sys.argv[1]
dest_dir = sys.argv[2]
date = sys.argv[3]
time = sys.argv[4]

ncid = Dataset(src_dir+"/p3_000_"+date+"_t"+time+".nc")
var_nc = ncid.variables

nproc = var_nc["nproc"][:][0] # number of procs
s_cen = var_nc["s_centr"][:]
r_planet = var_nc["r_planet"][:][0]
x0 = var_nc["phys_length"][:][0]
print(x0)

altitude = np.arange(0,6*x0*0.5,x0*0.5)
alt_exc  = np.zeros(len(altitude))
alt_nb_parts = np.zeros(len(altitude))

for iproc in range(26,37):#len(nproc)):
    if iproc < 10:
        ncid = Dataset(src_dir+"/p3_00"+str(iproc)+"_"+date+"_t"+time+".nc")
    elif iproc < 100:
        ncid = Dataset(src_dir+"/p3_0"+str(iproc)+"_"+date+"_t"+time+".nc")
    else:
        ncid = Dataset(src_dir+"/p3_"+str(iproc)+"_"+date+"_t"+time+".nc")
    var_nc = ncid.variables

    nptot = var_nc["nptot"][:][0] # total number of particules of proc 0
    print("proc : ",iproc," nptot : ",nptot)
    pos_x = var_nc["particule_x"][:] # contains all values of vx of all procs
    pos_y = var_nc["particule_y"][:] # contains all values of vx of all procs
    pos_z = var_nc["particule_z"][:] # contains all values of vx of all procs
    exc   = var_nc["particule_exc"][:]
    

    for i in range(0, nptot):
        # print(pos_x[i],pos_y[i],pos_z[i])
        # print(pos_x[i]-s_cen[0],pos_y[i]-s_cen[1],pos_z[i]-s_cen[2],(pos_x[i]-s_cen[0])**2 + (pos_y[i]-s_cen[1])**2 + (pos_z[i]-s_cen[2])**2)
        rb = np.sqrt((pos_x[i]-s_cen[0])**2 + (pos_y[i]-s_cen[1])**2 + (pos_z[i]-s_cen[2])**2)
        radius = (rb-r_planet)*x0
        
        j = int(radius/40)
        if 0 <= j and j < len(altitude):
            # print(j,radius,rb,pos_x[i],pos_y[i],pos_z[i],s_cen[0],s_cen[1],s_cen[2])
            # print(radius,j,altitude[j],altitude[j+1])
            alt_exc[j] += exc[i]/100
            alt_nb_parts[j] += 1

alt_exc_moy = alt_exc/alt_nb_parts
plt.plot(alt_exc,altitude)
plt.xlabel("number of charge exchanges")
plt.ylabel("Altitude")
plt.title("Total number of charge exchanges during 100 $\Omega ^{-1}$")
plt.grid(True,'both','both',ls=":",color="lightgrey")
plt.savefig(dest_dir+"/number_of_charge_exchange_along_altitude_100_inv_gyroperiod.png")
plt.close("all")
plt.plot(alt_exc_moy,altitude)
plt.xlabel("number of charge exchanges")
plt.ylabel("Altitude")
plt.title("Mean number of charge exchanges during 100 $\Omega ^{-1}$")
plt.grid(True,'both','both',ls=":",color="lightgrey")
plt.savefig(dest_dir+"/mean_number_of_charge_exchange_along_altitude_100_inv_gyroperiod.png")