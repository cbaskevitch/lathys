'''
Basic implementation of Cooley-Tukey FFT algorithm in Python
Reference:
https://en.wikipedia.org/wiki/Fast_Fourier_transform
'''
import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset
import random
import sys, os

ncid = Dataset(sys.argv[1])
var_nc = ncid.variables
centr  = var_nc['s_centr'][:]
radius = var_nc['r_planet'][:]
gs     = var_nc['gstep'][:]
Ux     = var_nc['Ux'][:]
Uy         = var_nc['Uy'][:]
Uz         = var_nc['Uz'][:]
nc = [len(Ux[0][0]), len(Ux[0]), len(Ux)]

# SAMPLE_RATE = 8192
# N = 128 # Windowing

N = nc[0]-1 # Windowing
SAMPLE_RATE = N*N/2


def fft(x):
	X = list()
	for k in range(0, N):
		window = 1 # np.sin(np.pi * (k+0.5)/N)**2
		X.append(np.complex(x[k] * window, 0))

	fft_rec(X)
	return X

def fft_rec(X):
	N = len(X)

	if N <= 1:
		return

	even = np.array(X[0:N:2])
	odd = np.array(X[1:N:2])

	fft_rec(even)
	fft_rec(odd)

	for k in range(0, int(N/2)):
		t = np.exp(np.complex(0, -2 * np.pi * k / N)) * odd[k]
		X[k] = even[k] + t
		X[int(N/2) + k] = even[k] - t




# x_values = np.arange(0, N, 1)

# x = np.sin((2*np.pi*x_values / 32.0)) # 32 - 256Hz
# x += np.sin((2*np.pi*x_values / 64.0)) # 64 - 128Hz


x = -5.0 #float(sys.argv[2])#4.0
z = 0.0

xi = 0
zi = int(np.fix(centr[2]/gs[2]))

test = (xi*gs[0] - (centr[0]+0.5*gs[0]))/radius
while not (x-0.05 < test and test < x+0.05):
    xi += 1
    test = (xi*gs[0] - (centr[0]+0.5*gs[0]))/radius

# Y = np.arange(0,nc[1]*gs[1],gs[1])
# Y = (Y - (centr[1]+0.5*gs[1]))/radius
# Z = np.arange(0,nc[2]*gs[2],gs[2])
# Z = (Z - (centr[2]+0.5*gs[2]))/radius
# print(xi)
# print(zi)
# print(centr[2]/gs[2])
# print(Y[xi])
# print(Z[zi])

# Ux_p = Ux[zi,xi,0:N]
modU = np.sqrt(Ux[zi,0:N,xi]*Ux[zi,0:N,xi] + Uy[zi,0:N,xi]*Uy[zi,0:N,xi] + Uz[zi,0:N,xi]*Uz[zi,0:N,xi])

# X = fft(Ux_p)
X = fft(modU)


# Plotting 
_, plots = plt.subplots(2)

## Plot in time domain
plots[0].plot(modU)

vs = [var_nc["vxs"][:], var_nc["vys"][:], var_nc["vzs"][:]]
modvs = np.sqrt(vs[0]*vs[0] + vs[1]*vs[1] + vs[2]*vs[2])*var_nc["phys_speed"][:]
## Plot in frequent domain
powers_all = np.abs(np.divide(X,1))#, modvs))
# powers_all = np.abs(X)
powers = powers_all[0:int(N/2)]
# frequencies = np.divide(np.multiply(SAMPLE_RATE, np.arange(0, int(N/2))), N)
# plots[1].plot(frequencies, powers)
plots[1].plot(powers_all)
print(powers)
print (powers_all)

## Show plots
# plt.show()
plt.savefig("fft_x_-5_z_0.png")