######################
# Plot_E_field_absolute_diff.py
# ---------------------
# This routine reads the magnetic
# field file of 2 simulations and plot 
# a 2D plan of the absolute difference
# between the 2 simulations
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# April 2021
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

def plot_E_field(ncfile1,ncfile2,dest_dir,rundate1,rundate2):
	ncid1 = Dataset(ncfile1)
	var_nc1 = ncid1.variables
	ncid2 = Dataset(ncfile2)
	var_nc2 = ncid2.variables
    
	centr1      = var_nc1['s_centr'][:]
	radius1     = var_nc1['r_planet'][:]
	gs1         = var_nc1['gstep'][:]
	Ex1         = var_nc1['Ex'][:]
	Ey1         = var_nc1['Ey'][:]
	Ez1         = var_nc1['Ez'][:]

	centr      = var_nc2['s_centr'][:]
	radius     = var_nc2['r_planet'][:]
	gs         = var_nc2['gstep'][:]
	Ex2         = var_nc2['Ex'][:]
	Ey2         = var_nc2['Ey'][:]
	Ez2         = var_nc2['Ez'][:]

	# radius=1
	nc = [len(Ex2[0][0]), len(Ex2[0]), len(Ex2)]

    # -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)

	# planet center in cell number (NE: cell number start at 0
	icentr = int(np.fix(centr[0]/gs[0]))-1
	jcentr = int(np.fix(centr[1]/gs[1]))-1
	kcentr = int(np.fix(centr[2]/gs[2]))-1
	iwake = int(icentr + np.fix(1.5*radius/gs[0]))

    #### On recupere les numeros de cellule pour matcher la taille de la petite boite dans la grande
	X = np.arange(0,nc[0]*gs[0],gs[0])
	X = X/radius - (centr[0]+0.5*gs[0])*np.ones(nc[0])/ radius
	XT = np.arange(0,len(Ex1[0][0])*gs1[0],gs1[0])
	XT = XT/radius1 - (centr1[0]+0.5*gs1[0])*np.ones(len(Ex1[0][0]))/ radius1
	i_t = 0
	while XT[i_t] < X[0]:
		i_t += 1
    
	lenY = len(Ex2[0])
	Y = np.arange(0,lenY*gs[1],gs[1])
	Y = Y/radius - (centr[1]+0.5*gs[1])*np.ones(lenY)/ radius

	lenYT = len(Ex1[0])
	YT = np.arange(0,lenYT*gs1[1],gs1[1])
	YT = YT/radius - (centr1[1]+0.5*gs1[1])*np.ones(lenYT)/ radius
	j_t = 0
	while YT[j_t] < Y[0]:
		j_t += 1

	Ex_diff = np.fabs(Ex1[j_t:j_t+nc[2],j_t:j_t+nc[1],i_t:i_t+nc[0]] - Ex2[:,:,:])
	Ey_diff = np.fabs(Ey1[j_t:j_t+nc[2],j_t:j_t+nc[1],i_t:i_t+nc[0]] - Ey2[:,:,:])
	Ez_diff = np.fabs(Ez1[j_t:j_t+nc[2],j_t:j_t+nc[1],i_t:i_t+nc[0]] - Ez2[:,:,:])

	Ex_XY        = np.zeros((nc[0],nc[1]))
	Ex_XY[:,:]   = np.matrix.transpose(Ex_diff[kcentr,:,:])
	Ey_XY        = np.zeros((nc[0],nc[1]))
	Ey_XY[:,:]   = np.matrix.transpose(Ey_diff[kcentr,:,:])
	Ez_XY        = np.zeros((nc[0],nc[1]))
	Ez_XY[:,:]   = np.matrix.transpose(Ez_diff[kcentr,:,:])
	
	Ex_XZ        = np.zeros((nc[0],nc[2]))
	Ex_XZ[:,:]   = np.matrix.transpose(Ex_diff[:,jcentr,:])
	Ey_XZ        = np.zeros((nc[0],nc[2]))
	Ey_XZ[:,:]   = np.matrix.transpose(Ey_diff[:,jcentr,:])
	Ez_XZ        = np.zeros((nc[0],nc[2]))
	Ez_XZ[:,:]   = np.matrix.transpose(Ez_diff[:,jcentr,:])
	
	Ex_YZ_term        = np.zeros((nc[1],nc[2]))
	Ex_YZ_term[:,:]   = np.matrix.transpose(Ex_diff[:,:,icentr])
	Ey_YZ_term        = np.zeros((nc[1],nc[2]))
	Ey_YZ_term[:,:]   = np.matrix.transpose(Ey_diff[:,:,icentr])
	Ez_YZ_term        = np.zeros((nc[1],nc[2]))
	Ez_YZ_term[:,:]   = np.matrix.transpose(Ez_diff[:,:,icentr])
	
	Ex_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ex_YZ_wake[:,:]   = np.matrix.transpose(Ex_diff[:,:,iwake])
	Ey_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ey_YZ_wake[:,:]   = np.matrix.transpose(Ey_diff[:,:,iwake])
	Ez_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ez_YZ_wake[:,:]   = np.matrix.transpose(Ez_diff[:,:,iwake])

	fig_size = [[6,9.5],[6,7.5],[8,6],[10,6]] #differentes tailles de fenetres
	figsize_Xnum = 1  #numero de la taille de la fenetre pour les plans XZ et XY
	figsize_Ynum = 2  #numero de la taille de la fenetre pour les plans YZ

	colmap = ["jet","Greens_r","GnEu_r"]
	ncol = 0  # numero de la colormap

	Xmin = X[0]
	Xmax = X[-1]
	Ymin = Y[0]
	Ymax = Y[-1]
	Zmin = Y[0]
	Zmax = Y[-1]

	min_val    = [0.0,0.0,0.0]
	max_val    = [100.0,100.0,100.0]

	theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
	xp = np.cos(theta)
	yp = np.sin(theta)

	# -- Figure 1 & 2 -- Ex
	# **************************************************************************

	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, Ex_XY, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Ex [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	# plt.show()
	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Ex_XY.png")

	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, Ex_XZ, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ex [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Ex_XZ.png")
	# plt.show()

	# -- Figure 3 & 4 -- Ey
	# **************************************************************************

	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, Ey_XY, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Ey [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Ey_XY.png")

	# --

	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, Ey_XZ, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ey [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Ey_XZ.png")

	# -- Figure 3 & 4 -- Ez
	# **************************************************************************

	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, Ez_XY, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Ez [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Ez_XY.png")

	# --

	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, Ez_XZ, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ez [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Ez_XZ.png")


	# =========== figure in YZ plane ==============================
	# Ex in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Ex_YZ_term, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ex [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Ex_YZ_terminator.png")

	# --

	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Ex_YZ_wake, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ex [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Ex_YZ_wake.png")

	# Ey in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Ey_YZ_term, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ey [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Ey_YZ_terminator.png")

	# --

	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Ey_YZ_wake, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ey [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Ey_YZ_wake.png")

	# Ez in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Ez_YZ_term, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ez [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Ez_YZ_terminator.png")

	# --

	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Ez_YZ_wake, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ez [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Ez_YZ_wake.png")

	
	plt.close('all')

	

    

