from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

### Ecart relatif des composantes de B, U et E entre la simulation de référence et le test
### @param dossier de la simulation de reference
### @param dossier de la simulation
### @param date de la simulation de reference
### @param date de la simulation

# dossier temoin
dir_T = sys.argv[1]
# dossier à comparer
dir = sys.argv[2]
date_T = sys.argv[3]
date = sys.argv[4]
# times = ["t00050","t00300"]
times = ["t00300"]

for t in times:
    ncfile_T = dir_T+"/"+t+"/Magw_"+date_T+"_"+t+".nc"
    ncfile = dir+"/Magw_"+date+"_t00300"+".nc"
    ncid_T = Dataset(ncfile_T)
    var_nc_T = ncid_T.variables
    ncid = Dataset(ncfile)
    var_nc = ncid.variables

    centr_T      = var_nc_T['s_centr'][:]
    radius     = var_nc_T['r_planet'][:]
    gs_T         = var_nc_T['gstep'][:]
    Bx_T         = var_nc_T['Bx'][:]
    By_T         = var_nc_T['By'][:]
    Bz_T         = var_nc_T['Bz'][:]
    s_min_T = var_nc_T['s_min'][:]
    s_max_T      = var_nc_T["s_max"][:]

#### Deference relative moyenne pour le champ magnetique ####
    centr      = var_nc['s_centr'][:]
    gs         = var_nc['gstep'][:]
    Bx         = var_nc['Bx'][:]
    By         = var_nc['By'][:]
    Bz         = var_nc['Bz'][:]
    s_min = var_nc['s_min'][:]
    s_max = var_nc['s_max'][:]

    # print(centr_T,"",centr)
    # print(len(Bx_T),len(Bx))
    # print(len(Bx_T[0]),len(Bx[0]))
    # print(len(Bx_T[0][0]),len(Bx[0][0]))
    # print(s_min_T,s_min)
    # print(s_max_T,s_max)
    # print("\n")
    
    # Recupere la taille de l'axe X et creation du tableau de l'axe X
    lenX = len(Bx[0][0])-1
    X = np.arange(0,lenX*gs[0],gs[0])
    X = X/radius - (centr[0]+0.5*gs[0])*np.ones(lenX)/ radius

    # Recupere la taille de l'axe X de la simulation temoin et creation du tableau de l'axe X (temoin)
    lenXT = len(Bx_T[0][0])
    XT = np.arange(0,lenXT*gs[0],gs_T[0])
    XT = XT/radius - (centr_T[0]+0.5*gs_T[0])*np.ones(lenXT)/ radius
    # calcule l'indice à partir duquel l'axe X commence dans la boite de la simulation temoin
    i_t = 0
    while XT[i_t] < X[0]:
        i_t += 1
    # print("Xmin",X[0],XT[i_t])
    # print("Xmax",X[-1],XT[i_t+lenX-1])

    # Recupere la taille de l'axe X et creation du tableau de l'axe Y
    lenY = len(Bx[0])
    Y = np.arange(0,lenY*gs[1],gs[1])
    Y = Y/radius - (centr[1]+0.5*gs[1])*np.ones(lenY)/ radius

    # Recupere la taille de l'axe X de la simulation temoin et creation du tableau de l'axe Y (temoin)
    lenYT = len(Bx_T[0])
    YT = np.arange(0,lenYT*gs_T[1],gs[1])
    YT = YT/radius - (centr_T[1]+0.5*gs_T[1])*np.ones(lenYT)/ radius
    # calcule l'indice à partir duquel l'axe X commence dans la boite de la simulation temoin
    j_t = 0
    while YT[j_t] < Y[0]:
        j_t += 1
    # print("Ymin",Y[10],YT[j_t+10])
    # print("Ymax",Y[-10],YT[j_t+lenY-10])

    # Recupere la taille de l'axe X et creation du tableau de l'axe Z
    lenZ = len(Bx)
    Z = np.arange(0,lenZ*gs[2],gs[2])
    Z = Z/radius - (centr[2]+0.5*gs[2])*np.ones(lenZ)/ radius
    # Recupere la taille de l'axe X de la simulation temoin et creation du tableau de l'axe Z (temoin)
    lenZT = len(Bx_T)
    ZT = np.arange(0,lenZT*gs_T[2],gs[2])
    ZT = ZT/radius - (centr_T[2]+0.5*gs_T[2])*np.ones(lenZT)/ radius
    # calcule l'indice à partir duquel l'axe X commence dans la boite de la simulation temoin
    k_t = 0
    while ZT[k_t] < Z[0]:
        k_t += 1
    # print("Zmin",Z[10],ZT[k_t+10])
    # print("Zmax",Z[-10],ZT[k_t+lenZ-10])

    # Calcule la difference relative moyenne de Bx plan par plan selon X sum(|Bx-BxT|/BxT)/n
    cpt = (len(Bx[0])-20)*(len(Bx)-20)
    sum_Bx = np.zeros(lenX)
    test = np.zeros(lenX)
    for i in range(0,lenX):
        kT_deb = k_t+10
        kT_fin = k_t+lenZ-10
        jT_deb = j_t+10
        jT_fin = j_t+lenY-10
        iT = i_t + i
        sum_Bx[i] = np.sum(np.fabs(np.fabs(Bx[10:-10,10:-10,i]-Bx_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/Bx_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]))
        # sum_Bx[i] = np.sum(np.fabs(Bx[10:-10,10:-10,i]-Bx_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/np.maximum(np.fabs(Bx_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]),np.fabs(Bx[10:-10,10:-10,i])))
    

    sum_Bx = (sum_Bx+test)/cpt


    plt.plot(X[:-1],sum_Bx[:-1],c="blue",label="Bx")
    # plt.show()

    # Calcule la difference relative moyenne de By plan par plan selon X sum(|By-ByT|/ByT)
    sum_By = np.zeros(lenX)
    for i in range(0,lenX):
        kT_deb = k_t+10
        kT_fin = k_t+lenZ-10
        jT_deb = j_t+10
        jT_fin = j_t+lenY-10
        iT = i_t + i
        sum_By[i] = np.sum(np.fabs(np.fabs(By[10:-10,10:-10,i]-By_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/By_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]))
        # sum_By[i] = np.sum((By[10:-10,10:-10,i]-By_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/By_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])
        # sum_By[i] = np.sum(np.fabs(By[10:-10,10:-10,i]-By_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/np.maximum(np.fabs(By_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]),np.fabs(By[10:-10,10:-10,i])))
    
    sum_By = sum_By/cpt
    #print(sum_By)

    plt.plot(X[:-1],sum_By[:-1],c="red",label="By")
    # plt.show()

    # Calcule la difference relative moyenne de Bz plan par plan selon X sum(|Bz-BzT|/BzT)
    sum_Bz = np.zeros(lenX)
    for i in range(0,lenX):
        kT_deb = k_t+10
        kT_fin = k_t+lenZ-10
        jT_deb = j_t+10
        jT_fin = j_t+lenY-10
        iT = i_t + i
        sum_Bz[i] = np.sum(np.fabs(np.fabs(Bz[10:-10,10:-10,i]-Bz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/Bz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]))
        # sum_Bz[i] = np.sum((Bz[10:-10,10:-10,i]-Bz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/Bz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])
        # sum_Bz[i] = np.sum(np.fabs(Bz[10:-10,10:-10,i]-Bz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/np.maximum(np.fabs(Bz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]),np.fabs(Bz[10:-10,10:-10,i])))
    
    sum_Bz = sum_Bz/cpt
    #print(sum_Bz)

    plt.plot(X[:-1],sum_Bz[:-1],c="green",label="Bz")
    plt.title("B field relative average deviation plane by plane along x\n between control and test simulation")
    plt.xlabel('X [R_G]')
    plt.legend(loc="upper right")
    plt.ylim(0,1)
    # plt.show()
    plt.savefig("profil_ecart_relatif_moyen_sur_X_BxByBz_temoin-"+date_T+"_"+date+".png")
    plt.close('all')

#### Deference relative moyenne pour la vitesse ####

    ncfile_T = dir_T+"/"+t+"/Thew_"+date_T+"_"+t+".nc"
    ncfile = dir+"/Thew_"+date+"_t00300"+".nc"
    ncid_T = Dataset(ncfile_T)
    var_nc_T = ncid_T.variables
    ncid = Dataset(ncfile)
    var_nc = ncid.variables

    Ux_T         = var_nc_T['Ux'][:]
    Uy_T         = var_nc_T['Uy'][:]
    Uz_T         = var_nc_T['Uz'][:]
    Ux         = var_nc['Ux'][:]
    Uy         = var_nc['Uy'][:]
    Uz         = var_nc['Uz'][:]

    sum_Ux = np.zeros(lenX)
    for i in range(0,lenX):
        kT_deb = k_t+10
        kT_fin = k_t+lenZ-10
        jT_deb = j_t+10
        jT_fin = j_t+lenY-10
        iT = i_t + i
        sum_Ux[i] = np.sum(np.fabs(np.fabs(Ux[10:-10,10:-10,i]-Ux_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/Ux_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]))
        # sum_Ux[i] = np.sum((Ux[10:-10,10:-10,i]-Ux_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/Ux_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])
        # sum_Ux[i] = np.sum(np.fabs(Ux[10:-10,10:-10,i]-Ux_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/np.maximum(np.fabs(Ux_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]),np.fabs(Ux[10:-10,10:-10,i])))
    
    sum_Ux = sum_Ux/cpt
    #print(sum_Ux)

    plt.plot(X[:-1],sum_Ux[:-1],c="blue",label="Ux")
    # plt.show()

    sum_Uy = np.zeros(lenX)
    for i in range(0,lenX):
        kT_deb = k_t+10
        kT_fin = k_t+lenZ-10
        jT_deb = j_t+10
        jT_fin = j_t+lenY-10
        iT = i_t + i
        sum_Uy[i] = np.sum(np.fabs(np.fabs(Uy[10:-10,10:-10,i]-Uy_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/Uy_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]))
        # sum_Uy[i] = np.sum((Uy[10:-10,10:-10,i]-Uy_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/Uy_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])
        # sum_Uy[i] = np.sum(np.fabs(Uy[10:-10,10:-10,i]-Uy_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/np.maximum(np.fabs(Uy_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]),np.fabs(Uy[10:-10,10:-10,i])))
    
    sum_Uy = sum_Uy/cpt
    #print(sum_Uy)

    plt.plot(X[:-1],sum_Uy[:-1],c="red",label="Uy")
    # plt.show()

    sum_Uz = np.zeros(lenX)
    for i in range(0,lenX):
        kT_deb = k_t+10
        kT_fin = k_t+lenZ-10
        jT_deb = j_t+10
        jT_fin = j_t+lenY-10
        iT = i_t + i
        sum_Uz[i] = np.sum(np.fabs(np.fabs(Uz[10:-10,10:-10,i]-Uz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/Uz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]))
        # sum_Uz[i] = np.sum((Uz[10:-10,10:-10,i]-Uz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/Uz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])
        # sum_Uz[i] = np.sum(np.fabs(Uz[10:-10,10:-10,i]-Uz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/np.maximum(np.fabs(Uz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]),np.fabs(Uz[10:-10,10:-10,i])))
    sum_Uz = sum_Uz/cpt

    plt.plot(X[:-1],sum_Uz[:-1],c="green",label="Uz")
    plt.title("U relative average deviation plane by plane along x\n between control and test simulation")
    plt.xlabel('X [R_G]')
    plt.legend(loc="upper right")
    plt.ylim(0,1)
    # plt.show()
    plt.savefig("profil_ecart_relatif_moyen_sur_X_UxUyUz_temoin-"+date_T+"_"+date+".png")
    plt.close('all')


#### Deference relative moyenne pour le champ électrique ####
    ncfile_T = dir_T+"/"+t+"/Elew_"+date_T+"_"+t+".nc"
    ncfile = dir+"/Elew_"+date+"_t00300"+".nc"
    ncid_T = Dataset(ncfile_T)
    var_nc_T = ncid_T.variables
    ncid = Dataset(ncfile)
    var_nc = ncid.variables

    Ex_T         = var_nc_T['Ex'][:]
    Ey_T         = var_nc_T['Ey'][:]
    Ez_T         = var_nc_T['Ez'][:]
    Ex         = var_nc['Ex'][:]
    Ey         = var_nc['Ey'][:]
    Ez         = var_nc['Ez'][:]

    sum_Ex = np.zeros(lenX)
    for i in range(0,lenX):
        kT_deb = k_t+10
        kT_fin = k_t+lenZ-10
        jT_deb = j_t+10
        jT_fin = j_t+lenY-10
        iT = i_t + i
        sum_Ex[i] = np.sum(np.fabs(np.fabs(Ex[10:-10,10:-10,i]-Ex_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/Ex_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]))
        # sum_Ex[i] = np.sum((Ex[10:-10,10:-10,i]-Ex_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/Ex_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])
        # sum_Ex[i] = np.sum(np.fabs(Ex[10:-10,10:-10,i]-Ex_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/np.maximum(np.fabs(Ex_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]),np.fabs(Ex[10:-10,10:-10,i])))
    
    sum_Ex = sum_Ex/cpt
    #print(sum_Ex)

    plt.plot(X[:-1],sum_Ex[:-1],c="blue",label="Ex")
    # plt.show()

    sum_Ey = np.zeros(lenX)
    for i in range(0,lenX):
        kT_deb = k_t+10
        kT_fin = k_t+lenZ-10
        jT_deb = j_t+10
        jT_fin = j_t+lenY-10
        iT = i_t + i
        sum_Ey[i] = np.sum(np.fabs(np.fabs(Ey[10:-10,10:-10,i]-Ey_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/Ey_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]))
        # sum_Ey[i] = np.sum((Ey[10:-10,10:-10,i]-Ey_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/Ey_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])
        # sum_Ey[i] = np.sum(np.fabs(Ey[10:-10,10:-10,i]-Ey_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/np.maximum(np.fabs(Ey_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]),np.fabs(Ey[10:-10,10:-10,i])))
    
    sum_Ey = sum_Ey/cpt
    #print(sum_Ey)

    plt.plot(X[:-1],sum_Ey[:-1],c="red",label="Ey")
    # plt.show()

    sum_Ez = np.zeros(lenX)
    for i in range(0,lenX):
        kT_deb = k_t+10
        kT_fin = k_t+lenZ-10
        jT_deb = j_t+10
        jT_fin = j_t+lenY-10
        iT = i_t + i
        sum_Ez[i] = np.sum(np.fabs(np.fabs(Ez[10:-10,10:-10,i]-Ez_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/Ez_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]))
        # sum_Ez[i] = np.sum((Ez[10:-10,10:-10,i]-Ez_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/Ez_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])
        # sum_Ez[i] = np.sum(np.fabs(Ez[10:-10,10:-10,i]-Ez_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])/np.maximum(np.fabs(Ez_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]),np.fabs(Ez[10:-10,10:-10,i])))
    
    sum_Ez = sum_Ez/cpt
    #print(sum_Ez)

    plt.plot(X[:-1],sum_Ez[:-1],c="green",label="Ez")
    plt.title("E field relative average deviation plane by plane along x\n between control and test simulation")
    plt.xlabel('X [R_G]')
    plt.legend(loc="upper right")
    plt.ylim(0,1)
    # plt.show()
    plt.savefig("profil_ecart_relatif_moyen_sur_X_ExEyEz_temoin-"+date_T+"_"+date+".png")