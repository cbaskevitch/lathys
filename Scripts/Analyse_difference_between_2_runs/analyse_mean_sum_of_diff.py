from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

#Calcul la moyenne de la somme des differences des moments :
#1/ncell_tot * sum (abs(B-Bt)/Bt)

# dossier temoin
dir_T = sys.argv[1]
# dossier à comparer
dir = sys.argv[2]
date_T = sys.argv[3]
date = sys.argv[4]
# times = ["t00050","t00300"]
times = ["t00300"]

for t in times:
    ncfile_T = dir_T+"/"+t+"/Magw_"+date_T+"_"+t+".nc"
    ncfile = dir+"/"+t+"/Magw_"+date+"_"+t+".nc"
    ncid_T = Dataset(ncfile_T)
    var_nc_T = ncid_T.variables
    ncid = Dataset(ncfile)
    var_nc = ncid.variables

    centr_T      = var_nc_T['s_centr'][:]
    radius     = var_nc_T['r_planet'][:]
    gs_T         = var_nc_T['gstep'][:]
    Bx_T         = var_nc_T['Bx'][:]
    By_T         = var_nc_T['By'][:]
    Bz_T         = var_nc_T['Bz'][:]
    s_min_T = var_nc_T['s_min'][:]
    s_max_T      = var_nc_T["s_max"][:]

    centr      = var_nc['s_centr'][:]
    gs         = var_nc['gstep'][:]
    Bx         = var_nc['Bx'][:]
    By         = var_nc['By'][:]
    Bz         = var_nc['Bz'][:]
    s_min = var_nc['s_min'][:]
    s_max = var_nc['s_max'][:]

    # sum_ = np.fabs(Bx-Bx_T)/Bx_T
    # print(gs_T,"",gs)
    # print(len(Bx_T),len(Bx))
    # print(len(Bx_T[0]),len(Bx[0]))
    # print(len(Bx_T[0][0]),len(Bx[0][0]))
    # print(s_min_T,s_min)
    # print(s_max_T,s_max)

    sum_Bx = 0
    sum_Bx = np.sum(np.fabs(np.fabs(Bx[10:-10,10:-10,:]-Bx_T[10:len(Bx)-10,10:len(Bx[0])-10,:len(Bx[0][0])])/Bx_T[10:len(Bx)-10,10:len(Bx[0])-10,:len(Bx[0][0])]))
    cpt = (len(Bx)-20)*(len(Bx[0])-20)*len(Bx[0][0])
    sum_Bx = sum_Bx/cpt
    print(sum_Bx)

    sum_By = 0
    sum_By = np.sum(np.fabs(np.fabs(By[10:-10,10:-10,:]-By_T[10:len(Bx)-10,10:len(Bx[0])-10,:len(Bx[0][0])])/By_T[10:len(Bx)-10,10:len(Bx[0])-10,:len(Bx[0][0])]))
    sum_By = sum_By/cpt
    print(sum_By)

    sum_Bz = 0
    sum_Bz = np.sum(np.fabs(np.fabs(Bz[10:-10,10:-10,:]-Bz_T[10:len(Bx)-10,10:len(Bx[0])-10,:len(Bx[0][0])])/Bz_T[10:len(Bx)-10,10:len(Bx[0])-10,:len(Bx[0][0])]))
    sum_Bz = sum_Bz/cpt
    print(sum_Bz)
    print("\n")

    ncfile_T = dir_T+"/"+t+"/Thew_"+date_T+"_"+t+".nc"
    ncfile = dir+"/"+t+"/Thew_"+date+"_"+t+".nc"
    ncid_T = Dataset(ncfile_T)
    var_nc_T = ncid_T.variables
    ncid = Dataset(ncfile)
    var_nc = ncid.variables

    Ux_T         = var_nc_T['Ux'][:]
    Uy_T         = var_nc_T['Uy'][:]
    Uz_T         = var_nc_T['Uz'][:]
    Ux         = var_nc['Ux'][:]
    Uy         = var_nc['Uy'][:]
    Uz         = var_nc['Uz'][:]

    sum_Ux = 0
    sum_Ux = np.sum(np.fabs(np.fabs(Ux[10:-10,10:-10,:]-Ux_T[10:len(Ux)-10,10:len(Ux[0])-10,:len(Bx[0][0])])/Ux_T[10:len(Ux)-10,10:len(Ux[0])-10,:len(Bx[0][0])]))
    sum_Ux = sum_Ux/cpt
    print(sum_Ux)

    sum_Uy = 0
    sum_Uy = np.sum(np.fabs(np.fabs(Uy[10:-10,10:-10,:]-Uy_T[10:len(Ux)-10,10:len(Ux[0])-10,:len(Bx[0][0])])/Uy_T[10:len(Ux)-10,10:len(Ux[0])-10,:len(Bx[0][0])]))
    sum_Uy = sum_Uy/cpt
    print(sum_Uy)

    sum_Uz = 0
    sum_Uz = np.sum(np.fabs(np.fabs(Uz[10:-10,10:-10,:]-Uz_T[10:len(Ux)-10,10:len(Ux[0])-10,:len(Bx[0][0])])/Uz_T[10:len(Ux)-10,10:len(Ux[0])-10,:len(Bx[0][0])]))
    sum_Uz = sum_Uz/cpt
    print(sum_Uz)
    print("\n")

    ncfile_T = dir_T+"/"+t+"/Elew_"+date_T+"_"+t+".nc"
    ncfile = dir+"/"+t+"/Elew_"+date+"_"+t+".nc"
    ncid_T = Dataset(ncfile_T)
    var_nc_T = ncid_T.variables
    ncid = Dataset(ncfile)
    var_nc = ncid.variables

    Ex_T         = var_nc_T['Ex'][:]
    Ey_T         = var_nc_T['Ey'][:]
    Ez_T         = var_nc_T['Ez'][:]
    Ex         = var_nc['Ex'][:]
    Ey         = var_nc['Ey'][:]
    Ez         = var_nc['Ez'][:]

    sum_Ex = 0
    sum_Ex = np.sum(np.fabs(np.fabs(Ex[10:-10,10:-10,:]-Ex_T[10:len(Ex)-10,10:len(Ex[0])-10,:len(Bx[0][0])])/Ex_T[10:len(Ex)-10,10:len(Ex[0])-10,:len(Bx[0][0])]))
    sum_Ex = sum_Ex/cpt
    print(sum_Ex)

    sum_Ey = 0
    sum_Ey = np.sum(np.fabs(np.fabs(Ey[10:-10,10:-10,:]-Ey_T[10:len(Ex)-10,10:len(Ex[0])-10,:len(Bx[0][0])])/Ey_T[10:len(Ex)-10,10:len(Ex[0])-10,:len(Bx[0][0])]))
    sum_Ey = sum_Ey/cpt
    print(sum_Ey)

    sum_Ez = 0
    sum_Ez = np.sum(np.fabs(np.fabs(Ez[10:-10,10:-10,:]-Ez_T[10:len(Ex)-10,10:len(Ex[0])-10,:len(Bx[0][0])])/Ez_T[10:len(Ex)-10,10:len(Ex[0])-10,:len(Bx[0][0])]))
    sum_Ez = sum_Ez/cpt
    print(sum_Ez)