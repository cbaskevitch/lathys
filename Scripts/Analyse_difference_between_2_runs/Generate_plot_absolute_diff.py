from plot_B_field_absolute_diff import *
from plot_E_field_absolute_diff import *
from plot_Velocity_absolute_diff import *

# from plot_neutral_density_europa import *
import sys, os

if len(sys.argv) < 5:
	print("python Generate_plot_absolute_diff.py <ref_src_dir>  <ref_date> <ref_time> <test_src_dir>  <test_date> <test_time>")

src_dir1 = sys.argv[1]+"/"
rundate1 = sys.argv[2]
diagtime1 = sys.argv[3]

src_dir2 = sys.argv[4]+"/"
rundate2 = sys.argv[5]
diagtime2 = sys.argv[6]
# dest_dir = sys.argv[2]+"/"

ncfile1 = src_dir1 + "Magw_" + rundate1 + "_t" + diagtime1 + '.nc'
ncfile2 = src_dir2 + "Magw_" + rundate2 + "_t" + diagtime2 + '.nc'
plot_B_field(ncfile1,ncfile2,src_dir2,rundate1,rundate2)

ncfile1 = src_dir1 + "Elew_" + rundate1 + "_t" + diagtime1 + '.nc'
ncfile2 = src_dir2 + "Elew_" + rundate2 + "_t" + diagtime2 + '.nc'
plot_E_field(ncfile1,ncfile2,src_dir2,rundate1,rundate2)

ncfile1 = src_dir1 + "Thew_" + rundate1 + "_t" + diagtime1 + '.nc'
ncfile2 = src_dir2 + "Thew_" + rundate2 + "_t" + diagtime2 + '.nc'
plot_Velocity(ncfile1,ncfile2,src_dir2,rundate1,rundate2)
