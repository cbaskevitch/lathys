from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

### Ecart relatif des modules B, U et E entre la simulation de référence et le test
### @param dossier de la simulation de reference
### @param dossier de la simulation
### @param date de la simulation de reference
### @param date de la simulation

# dossier temoin
dir_T = sys.argv[1]
# dossier à comparer
dir = sys.argv[2]
date_T = sys.argv[3]
date = sys.argv[4]
# times = ["t00050","t00300"]
times = ["t00300"]

for t in times:
    ncfile_T = dir_T+"/"+t+"/Magw_"+date_T+"_"+t+".nc"
    ncfile = dir+"/Magw_"+date+"_t00300"+".nc"
    ncid_T = Dataset(ncfile_T)
    var_nc_T = ncid_T.variables
    ncid = Dataset(ncfile)
    var_nc = ncid.variables

    centr_T      = var_nc_T['s_centr'][:]
    radius     = var_nc_T['r_planet'][:]
    gs_T         = var_nc_T['gstep'][:]
    Bx_T         = var_nc_T['Bx'][:]
    By_T         = var_nc_T['By'][:]
    Bz_T         = var_nc_T['Bz'][:]
    s_min_T = var_nc_T['s_min'][:]
    s_max_T      = var_nc_T["s_max"][:]

    centr      = var_nc['s_centr'][:]
    gs         = var_nc['gstep'][:]
    Bx         = var_nc['Bx'][:]
    By         = var_nc['By'][:]
    Bz         = var_nc['Bz'][:]
    s_min = var_nc['s_min'][:]
    s_max = var_nc['s_max'][:]

    lenX = len(Bx[0][0])-1
    X = np.arange(0,lenX*gs[0],gs[0])
    X = X/radius - (centr[0]+0.5*gs[0])*np.ones(lenX)/ radius

    lenXT = len(Bx_T[0][0])
    XT = np.arange(0,lenXT*gs[0],gs_T[0])
    XT = XT/radius - (centr_T[0]+0.5*gs_T[0])*np.ones(lenXT)/ radius
    i_t = 0
    while XT[i_t] < X[0]:
        i_t += 1
    print("Xmin",X[0],XT[i_t])
    print("Xmax",X[-1],XT[i_t+lenX-1])

    lenY = len(Bx[0])
    Y = np.arange(0,lenY*gs[1],gs[1])
    Y = Y/radius - (centr[1]+0.5*gs[1])*np.ones(lenY)/ radius

    lenYT = len(Bx_T[0])
    YT = np.arange(0,lenYT*gs_T[1],gs[1])
    YT = YT/radius - (centr_T[1]+0.5*gs_T[1])*np.ones(lenYT)/ radius
    j_t = 0
    while YT[j_t] < Y[0]:
        j_t += 1
    print("Ymin",Y[10],YT[j_t+10])
    print("Ymax",Y[-10],YT[j_t+lenY-10])

    lenZ = len(Bx)
    Z = np.arange(0,lenZ*gs[2],gs[2])
    Z = Z/radius - (centr[2]+0.5*gs[2])*np.ones(lenZ)/ radius
    lenZT = len(Bx_T)
    ZT = np.arange(0,lenZT*gs_T[2],gs[2])
    ZT = ZT/radius - (centr_T[2]+0.5*gs_T[2])*np.ones(lenZT)/ radius
    k_t = 0
    while ZT[k_t] < Z[0]:
        k_t += 1
    print("Zmin",Z[10],ZT[k_t+10])
    print("Zmax",Z[-10],ZT[k_t+lenZ-10])

    sum_B = np.zeros(lenX)
    cpt = (len(Bx[0])-20)*(len(Bx)-20)
    for i in range(0,lenX):
        # if(X[i]<-1 or X[i]>1):
        kT_deb = k_t+10
        kT_fin = k_t+lenZ-10
        jT_deb = j_t+10
        jT_fin = j_t+lenY-10
        iT = i_t + i

        scalaireB = Bx[10:-10,10:-10,i]*Bx_T[kT_deb:kT_fin,jT_deb:jT_fin,iT] + By[10:-10,10:-10,i]*By_T[kT_deb:kT_fin,jT_deb:jT_fin,iT] + Bz[10:-10,10:-10,i]*Bz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]
        modB = modB = np.sqrt(Bx[10:-10,10:-10,i]*Bx[10:-10,10:-10,i] + By[10:-10,10:-10,i]*By[10:-10,10:-10,i] + Bz[10:-10,10:-10,i]*Bz[10:-10,10:-10,i])
        modB_T = np.sqrt(Bx_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]*Bx_T[kT_deb:kT_fin,jT_deb:jT_fin,iT] + By_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]*By_T[kT_deb:kT_fin,jT_deb:jT_fin,iT] + Bz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]*Bz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])

        sum_B[i] = np.sum(scalaireB/(modB*modB_T))

    sum_B = sum_B/cpt
    
    plt.plot(X[:-1],sum_B[:-1],c="blue",label="")
    plt.title("B field mean scalar product plane by plane along x\n between control and test simulation")
    plt.xlabel('X [R_G]')
    # plt.legend(loc="upper right")
    plt.ylim(0.95,1.01)
    # plt.show()
    plt.savefig("produit_scalaire_moyen_sur_X_B_temoin-"+date_T+"_"+date+".png")
    plt.close('all')

    ncfile_T = dir_T+"/"+t+"/Thew_"+date_T+"_"+t+".nc"
    ncfile = dir+"/Thew_"+date+"_t00300"+".nc"
    ncid_T = Dataset(ncfile_T)
    var_nc_T = ncid_T.variables
    ncid = Dataset(ncfile)
    var_nc = ncid.variables

    Ux_T         = var_nc_T['Ux'][:]
    Uy_T         = var_nc_T['Uy'][:]
    Uz_T         = var_nc_T['Uz'][:]
    Ux         = var_nc['Ux'][:]
    Uy         = var_nc['Uy'][:]
    Uz         = var_nc['Uz'][:]

    sum_U = np.zeros(lenX)
    cpt = (len(Ux[0])-20)*(len(Ux)-20)
    for i in range(0,lenX):
        # if(X[i]<-1 or X[i]>1):
        kT_deb = k_t+10
        kT_fin = k_t+lenZ-10
        jT_deb = j_t+10
        jT_fin = j_t+lenY-10
        iT = i_t + i
        scalaireU = Ux[10:-10,10:-10,i]*Ux_T[kT_deb:kT_fin,jT_deb:jT_fin,iT] + Uy[10:-10,10:-10,i]*Uy_T[kT_deb:kT_fin,jT_deb:jT_fin,iT] + Uz[10:-10,10:-10,i]*Uz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]
        modU = modU = np.sqrt(Ux[10:-10,10:-10,i]*Ux[10:-10,10:-10,i] + Uy[10:-10,10:-10,i]*Uy[10:-10,10:-10,i] + Uz[10:-10,10:-10,i]*Uz[10:-10,10:-10,i])
        modU_T = np.sqrt(Ux_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]*Ux_T[kT_deb:kT_fin,jT_deb:jT_fin,iT] + Uy_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]*Uy_T[kT_deb:kT_fin,jT_deb:jT_fin,iT] + Uz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]*Uz_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])

        sum_U[i] = np.sum(scalaireU/(modU*modU_T))
    sum_U = sum_U/cpt
    
    plt.plot(X[:-1],sum_U[:-1],c="blue",label="")
    plt.title("U mean scalar product plane by plane along x\n between control and test simulation")
    plt.xlabel('X [R_G]')
    # plt.legend(loc="upper right")
    plt.ylim(0.95,1.01)
    # plt.show()
    plt.savefig("produit_scalaire_moyen_sur_X_U_temoin-"+date_T+"_"+date+".png")

    plt.close('all')

    ncfile_T = dir_T+"/"+t+"/Elew_"+date_T+"_"+t+".nc"
    ncfile = dir+"/Elew_"+date+"_t00300"+".nc"
    ncid_T = Dataset(ncfile_T)
    var_nc_T = ncid_T.variables
    ncid = Dataset(ncfile)
    var_nc = ncid.variables

    Ex_T         = var_nc_T['Ex'][:]
    Ey_T         = var_nc_T['Ey'][:]
    Ez_T         = var_nc_T['Ez'][:]
    Ex         = var_nc['Ex'][:]
    Ey         = var_nc['Ey'][:]
    Ez         = var_nc['Ez'][:]

    sum_E = np.zeros(lenX)
    cpt = (len(Ex[0])-20)*(len(Ex)-20)
    for i in range(0,lenX):
        # if(X[i]<-1 or X[i]>1):
        kT_deb = k_t+10
        kT_fin = k_t+lenZ-10
        jT_deb = j_t+10
        jT_fin = j_t+lenY-10
        iT = i_t + i
        scalaireE = Ex[10:-10,10:-10,i]*Ex_T[kT_deb:kT_fin,jT_deb:jT_fin,iT] + Ey[10:-10,10:-10,i]*Ey_T[kT_deb:kT_fin,jT_deb:jT_fin,iT] + Ez[10:-10,10:-10,i]*Ez_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]
        modE = modE = np.sqrt(Ex[10:-10,10:-10,i]*Ex[10:-10,10:-10,i] + Ey[10:-10,10:-10,i]*Ey[10:-10,10:-10,i] + Ez[10:-10,10:-10,i]*Ez[10:-10,10:-10,i])
        modE_T = np.sqrt(Ex_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]*Ex_T[kT_deb:kT_fin,jT_deb:jT_fin,iT] + Ey_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]*Ey_T[kT_deb:kT_fin,jT_deb:jT_fin,iT] + Ez_T[kT_deb:kT_fin,jT_deb:jT_fin,iT]*Ez_T[kT_deb:kT_fin,jT_deb:jT_fin,iT])

        sum_E[i] = np.sum(scalaireE/(modE*modE_T))
    sum_E = sum_E/cpt
    
    plt.plot(X[:-1],sum_E[:-1],c="blue",label="")
    plt.title("E field mean scalar product plane by plane along x\n between control and test simulation")
    plt.xlabel('X [R_G]')
    # plt.legend(loc="upper right")
    plt.ylim(0.95,1.01)
    # plt.show()
    plt.savefig("produit_scalaire_moyen_sur_X_E_temoin-"+date_T+"_"+date+".png")
    # plt.show()
    plt.close('all')

    plt.title("B, U and E mean scalar product plane by plane along x\n between control and test simulation")
    plt.plot(X[:-1],sum_B[:-1],c="blue",label="B")
    plt.plot(X[:-1],sum_U[:-1],c="red",label="U")
    plt.plot(X[:-1],sum_E[:-1],c="green",label="E")
    plt.xlabel('X [R_G]')
    plt.legend(loc="upper right")
    plt.ylim(0.95,1.01)
    # plt.show()
    plt.savefig("produit_scalaire_moyen_sur_X_B-U-E_temoin-"+date_T+"_"+date+".png")
    # plt.show()
    plt.close('all')