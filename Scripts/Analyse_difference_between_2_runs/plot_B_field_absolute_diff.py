######################
# Plot_B_field_absolute_diff.py
# ---------------------
# This routine reads the magnetic
# field file of 2 simulations and plot 
# a 2D plan of the absolute difference
# between the 2 simulations
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# April 2021
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

def plot_B_field(ncfile1,ncfile2,dest_dir,rundate1,rundate2):
	ncid1 = Dataset(ncfile1)
	var_nc1 = ncid1.variables
	ncid2 = Dataset(ncfile2)
	var_nc2 = ncid2.variables
    
	centr1      = var_nc1['s_centr'][:]
	radius1     = var_nc1['r_planet'][:]
	gs1         = var_nc1['gstep'][:]
	Bx1         = var_nc1['Bx'][:]
	By1         = var_nc1['By'][:]
	Bz1         = var_nc1['Bz'][:]

	centr      = var_nc2['s_centr'][:]
	radius     = var_nc2['r_planet'][:]
	gs         = var_nc2['gstep'][:]
	Bx2         = var_nc2['Bx'][:]
	By2         = var_nc2['By'][:]
	Bz2         = var_nc2['Bz'][:]

	# radius=1
	nc = [len(Bx2[0][0]), len(Bx2[0]), len(Bx2)]

    # -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)

	# planet center in cell number (NB: cell number start at 0
	icentr = int(np.fix(centr[0]/gs[0]))-1
	jcentr = int(np.fix(centr[1]/gs[1]))-1
	kcentr = int(np.fix(centr[2]/gs[2]))-1
	iwake = int(icentr + np.fix(1.5*radius/gs[0]))

    #### On recupere les numeros de cellule pour matcher la taille de la petite boite dans la grande
	X = np.arange(0,nc[0]*gs[0],gs[0])
	X = X/radius - (centr[0]+0.5*gs[0])*np.ones(nc[0])/ radius
	XT = np.arange(0,len(Bx1[0][0])*gs1[0],gs1[0])
	XT = XT/radius1 - (centr1[0]+0.5*gs1[0])*np.ones(len(Bx1[0][0]))/ radius1
	i_t = 0
	while XT[i_t] < X[0]:
		i_t += 1
    
	lenY = len(Bx2[0])
	Y = np.arange(0,lenY*gs[1],gs[1])
	Y = Y/radius - (centr[1]+0.5*gs[1])*np.ones(lenY)/ radius

	lenYT = len(Bx1[0])
	YT = np.arange(0,lenYT*gs1[1],gs1[1])
	YT = YT/radius - (centr1[1]+0.5*gs1[1])*np.ones(lenYT)/ radius
	j_t = 0
	while YT[j_t] < Y[0]:
		j_t += 1

	Bx_diff = np.fabs(Bx1[j_t:j_t+nc[2],j_t:j_t+nc[1],i_t:i_t+nc[0]] - Bx2[:,:,:])
	By_diff = np.fabs(By1[j_t:j_t+nc[2],j_t:j_t+nc[1],i_t:i_t+nc[0]] - By2[:,:,:])
	Bz_diff = np.fabs(Bz1[j_t:j_t+nc[2],j_t:j_t+nc[1],i_t:i_t+nc[0]] - Bz2[:,:,:])

	Bx_XY        = np.zeros((nc[0],nc[1]))
	Bx_XY[:,:]   = np.matrix.transpose(Bx_diff[kcentr,:,:])
	By_XY        = np.zeros((nc[0],nc[1]))
	By_XY[:,:]   = np.matrix.transpose(By_diff[kcentr,:,:])
	Bz_XY        = np.zeros((nc[0],nc[1]))
	Bz_XY[:,:]   = np.matrix.transpose(Bz_diff[kcentr,:,:])
	
	Bx_XZ        = np.zeros((nc[0],nc[2]))
	Bx_XZ[:,:]   = np.matrix.transpose(Bx_diff[:,jcentr,:])
	By_XZ        = np.zeros((nc[0],nc[2]))
	By_XZ[:,:]   = np.matrix.transpose(By_diff[:,jcentr,:])
	Bz_XZ        = np.zeros((nc[0],nc[2]))
	Bz_XZ[:,:]   = np.matrix.transpose(Bz_diff[:,jcentr,:])
	
	Bx_YZ_term        = np.zeros((nc[1],nc[2]))
	Bx_YZ_term[:,:]   = np.matrix.transpose(Bx_diff[:,:,icentr])
	By_YZ_term        = np.zeros((nc[1],nc[2]))
	By_YZ_term[:,:]   = np.matrix.transpose(By_diff[:,:,icentr])
	Bz_YZ_term        = np.zeros((nc[1],nc[2]))
	Bz_YZ_term[:,:]   = np.matrix.transpose(Bz_diff[:,:,icentr])
	
	Bx_YZ_wake        = np.zeros((nc[1],nc[2]))
	Bx_YZ_wake[:,:]   = np.matrix.transpose(Bx_diff[:,:,iwake])
	By_YZ_wake        = np.zeros((nc[1],nc[2]))
	By_YZ_wake[:,:]   = np.matrix.transpose(By_diff[:,:,iwake])
	Bz_YZ_wake        = np.zeros((nc[1],nc[2]))
	Bz_YZ_wake[:,:]   = np.matrix.transpose(Bz_diff[:,:,iwake])

	fig_size = [[6,9.5],[6,7.5],[8,6],[10,6]] #differentes tailles de fenetres
	figsize_Xnum = 1  #numero de la taille de la fenetre pour les plans XZ et XY
	figsize_Ynum = 2  #numero de la taille de la fenetre pour les plans YZ

	colmap = ["jet","Greens_r","GnBu_r"]
	ncol = 0  # numero de la colormap

	Xmin = X[0]
	Xmax = X[-1]
	Ymin = Y[0]
	Ymax = Y[-1]
	Zmin = Y[0]
	Zmax = Y[-1]

	min_val    = [0.0,0.0,0.0]
	max_val    = [100.0,100.0,100.0]

	theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
	xp = np.cos(theta)
	yp = np.sin(theta)

	# -- Figure 1 & 2 -- Bx
	# **************************************************************************

	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, Bx_XY, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Bx [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	# plt.show()
	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Bx_XY.png")

	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, Bx_XZ, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Bx [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Bx_XZ.png")
	# plt.show()

	# -- Figure 3 & 4 -- By
	# **************************************************************************

	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, By_XY, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "By [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_By_XY.png")

	# --

	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, By_XZ, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "By [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_By_XZ.png")

	# -- Figure 3 & 4 -- Bz
	# **************************************************************************

	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, Bz_XY, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Bz [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Bz_XY.png")

	# --

	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, Bz_XZ, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Bz [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Bz_XZ.png")


	# =========== figure in YZ plane ==============================
	# Bx in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Bx_YZ_term, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Bx [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Bx_YZ_terminator.png")

	# --

	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Bx_YZ_wake, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Bx [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Bx_YZ_wake.png")

	# By in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,By_YZ_term, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "By [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_By_YZ_terminator.png")

	# --

	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,By_YZ_wake, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "By [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_By_YZ_wake.png")

	# Bz in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Bz_YZ_term, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Bz [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Bz_YZ_terminator.png")

	# --

	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Bz_YZ_wake, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
	ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Bz [nT] absolute difference"
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"absolute_diff_between_"+rundate1+"_"+rundate2+"_Bz_YZ_wake.png")

	
	plt.close('all')

	

    

