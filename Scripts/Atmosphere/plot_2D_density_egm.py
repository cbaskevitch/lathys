from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os
from matplotlib.patches import Polygon, Wedge, Arrow
import matplotlib as mpl
import matplotlib.colors as mpcol
from matplotlib.collections import PatchCollection
import warnings
warnings.filterwarnings("ignore")

# dir_simu="../23_01_23_creation_iono/"

# dir_simu_profile       = dir_simu+"atm_iono_profile_moy_log10/"
# dir_simu_profile_100km = dir_simu_profile+"dx_100km/"
# dir_simu_profile_50km  = dir_simu_profile+"dx_50km/"

# dir_simu_iono_control       = dir_simu+"atm_iono_control_moy_log10/"
# dir_simu_iono_control_100km = dir_simu_iono_control+"dx_100km/"
# dir_simu_iono_control_50km = dir_simu_iono_control+"dx_50km/"

# dir_simu_egm       = dir_simu+"atm_iono_egm_moy_log10/"
# dir_simu_egm_100km = dir_simu_egm+"dx_100km/"
# dir_simu_egm_50km  = dir_simu_egm+"dx_50km/"

# dir_simu_egm_subgrid_sph       = dir_simu+"atm_iono_egm_moy_log10_test_subgrid_sph/"
# dir_simu_egm_subgrid_sph_100km = dir_simu_egm_subgrid_sph+"dx_100km/"
# dir_simu_egm_subgrid_sph_50km = dir_simu_egm_subgrid_sph+"dx_50km/"

def plot_density_egm_XY(file,dest,moment,spe):

    ncidNe = Dataset(file)
    var_ncNe = ncidNe.variables

    density = var_ncNe[moment][:] #cm-3
    r_low = var_ncNe["altitude_low"][:]*1e-5 #km
    r_upp = var_ncNe["altitude_up"][:]*1e-5 #km
    t_low = var_ncNe["theta_low"][:] #rad
    t_upp = var_ncNe["theta_up"][:] #rad
    p_low = var_ncNe["phi_low"][:] #rad
    p_upp = var_ncNe["phi_up"][:] #rad
    # radius = var_ncNe["planet_radius"][:]*1e-5 #km
    radius = var_ncNe["Planetary_Radius"][:]*1e-5 #km

    nr = var_ncNe["npt_alt"][:][0]
    ntheta = var_ncNe["npt_lat"][:][0]
    nphi = var_ncNe["npt_lon"][:][0]
    print("nr=",nr," ntheta=",ntheta," nphi=",nphi)
    theta_ori = 90 #155.0 #math.pi/2.0										#equatorial angle (pi/2)
    theta = (180.0-theta_ori)*math.pi/180.0
    dtheta = 5.0*math.pi/180.0									#interval of the equatorial angle for statistical mean, in rad
    dphi = 0*math.pi/180.0
    # val_theta_cells = math.pi / len(theta_upp[0])			#theta value for one cell
    val_phi_cells = 2.0*math.pi / nphi
    # theta1 = int((theta - dtheta)/val_theta_cells)
    # theta2 = int((theta + dtheta)/val_theta_cells + 1.0)

    tt=0
    while not(t_low[0,tt,0] <= theta and theta < t_upp[0,tt,0]):
        tt += 1
    theta1 = (t_upp[0,tt,0]+t_low[0,tt,0])/2 - dtheta
    theta2 = (t_upp[0,tt,0]+t_low[0,tt,0])/2 + dtheta
    if theta1 <= 0:
        theta1=0
    else:
        tt=0
        while not(t_low[0,tt,0] <= theta1 and theta1 < t_upp[0,tt,0]):
            tt += 1
        theta1=tt
    if theta2 > math.pi:
        theta2 = ntheta
    else:
        tt=0
        while  not(t_low[0,tt,0] <= theta2 and theta2 < t_upp[0,tt,0]):
            tt += 1
        theta2=tt

    # subsol_long = subsol_long * 180.0 / math.pi
    # subsol_low = subsol_long - 90.0
    # subsol_upp = subsol_long + 90.0

    dens_eq_plane=[]
    for phi in range(0, nphi):
        sum_cells = np.zeros(shape=(nr), dtype=float)
        dens = np.zeros(shape=(nr), dtype=float)
        for t in range(theta1, theta2):
            if(t_upp[phi,t,0]>=(theta-dtheta) and t_low[phi,t,0]<=(theta+dtheta)):
                for r in range(0, nr):
                    dr = r_upp[phi,t,r] - r_low[phi,t,r]
                    dt = t_upp[phi,t,r] - t_low[phi,t,r]
                    dp = p_upp[phi,t,r] - p_low[phi,t,r]
                    Vcell = 0
                    if t_low[phi,t,r] == 0:
                        Vcell =  r_low[phi,t,r]**2 * np.sin(t_upp[phi,t,r]) * dr * dt * dp
                    else:
                        Vcell = r_low[phi,t,r]**2 * np.sin(t_low[phi,t,r]) * dr * dt * dp
                    # Vcell = 1.0/3.0 * (r_upp[r] ** 3 - r_low[r] ** 3) * (p_upp[phi] - p_low[phi])
                    # Vcell = Vcell * math.fabs(math.cos(t_low[t] - math.cos(t_upp[t])))
                    sum_cells[r] = sum_cells[r] + Vcell
                    dens[r] = dens[r] + density[phi][t][r]*Vcell #weight[specie][phi][t][r]/(Vcell*accumul_time_steps*dt)
                    Phi = (p_upp[phi,t,r]+p_low[phi,t,r])/2.0								#interval of the equatorial angle for statistical mean, in rad
                    phi1 = int((Phi - dphi)/val_phi_cells)
                    phi2 = int((Phi + dphi)/val_phi_cells)
                    for i in range(phi1, phi2):
                        if (i < nphi-1) and i > 0 and i != phi:
                            Vcell = 1.0/3.0 * (r_upp[phi,t,r] ** 3 - r_low[phi,t,r] ** 3) * (p_upp[i,t,r] - p_low[i,t,r])
                            Vcell = Vcell * math.fabs(math.cos(t_low[phi,t,r] - math.cos(t_upp[phi,t,r])))
                            dens[r] = dens[r] + density[phi][t][r]*Vcell #weight[specie][i][t][r]/(Vcell*accumul_time_steps*dt)
                            sum_cells[r] = sum_cells[r] + Vcell
        dens = np.divide(dens,sum_cells)
        dens_eq_plane.append(dens)
    dens_eq_plane = np.asarray(dens_eq_plane, dtype=float)
    dens_eq_plane = np.log10(dens_eq_plane)
    print("End computing density")

    print("Start ploting figure")
    fig, ax = plt.subplots()#figsize=[8,6])
    
    cmap = mpl.cm.get_cmap("inferno")
    print(np.amax(dens_eq_plane))
    print(np.amin(dens_eq_plane))
    
    if "O2" in spe:
        vmin_max = [0.0 , 9.0]
    elif "H2O" in spe:
        vmin_max = [0.0 , 6.0]
    else:
        vmin_max = [0.0 , 7.0]
    norm = mpcol.Normalize(vmin=vmin_max[0], vmax=vmin_max[1])
    
    # convertion en coordonnees cartesiennes
    # x = r * sin theta * cos phi
    # y = r * sin theta * sin phi
    # z = r * cos theta
    for r in range(0, nr):
        for phi in range(0, nphi):
            #coordonees bas gauche
            x_bg = r_upp[phi,0,r] * np.cos(p_upp[phi,0,r])
            y_bg = r_upp[phi,0,r] * np.sin(p_upp[phi,0,r])
            #coordonees haut gauche
            x_hg = r_upp[phi,0,r] * np.cos(p_low[phi,0,r])
            y_hg = r_upp[phi,0,r] * np.sin(p_low[phi,0,r])
            #coordonees bas droit
            x_bd = r_low[phi,0,r] * np.cos(p_upp[phi,0,r])
            y_bd = r_low[phi,0,r] * np.sin(p_upp[phi,0,r])
            #coordonees haut droit
            x_hd = r_low[phi,0,r] * np.cos(p_low[phi,0,r])
            y_hd = r_low[phi,0,r] * np.sin(p_low[phi,0,r])
            pts = np.array([[x_bg,y_bg], [x_hg,y_hg], [x_hd,y_hd], [x_bd,y_bd]])
            color = cmap(norm(dens_eq_plane[phi][r]))
            p = Polygon(pts, color=color, )
            # p = Polygon(pts, facecolor="white", edgecolor="black")
            ax = plt.gca()
            
            ax.add_patch(p)
    
    # w = Wedge(0.0, radius,  subsol_upp, subsol_low,color ="black")
    # ax = plt.gca()
    # ax.add_patch(w)
    lim = r_upp[-1]
    
    ax.set_xlim(-3000,3000)
    ax.set_ylim(-3000,3000)
    ax.set_xlabel("X [km]")
    ax.set_ylabel("Y [km]")

    i = ax.imshow(dens_eq_plane, cmap=cmap, vmin=vmin_max[0], vmax=vmin_max[1])
    cb = fig.colorbar(i)
    cb.set_label("log10 [$cm^{-3}$]")

    plt.setp(plt.getp(cb.ax.axes, 'yticklabels'))

    # tmp = file_ori.split("/")
    # file = tmp[len(tmp)-1][:-3]
    # file.replace('density_', '')
    # fig_title = "XY_"+spe+"_"+file+".png"
    # dir_name=""#"plot/"
    plt.savefig(dest+"/plan_XY_density_egm_"+spe+".png",facecolor=fig.get_facecolor(), edgecolor='none')
    print("End ploting figure")
    plt.close("all")

    
def plot_density_egm_XZ(file,dest,moment,spe):

    ncidNe = Dataset(file)
    var_ncNe = ncidNe.variables

    density = var_ncNe[moment][:] #cm-3
    r_low = var_ncNe["altitude_low"][:]*1e-5 #km
    r_upp = var_ncNe["altitude_up"][:]*1e-5 #km
    t_low = var_ncNe["theta_low"][:] #rad
    t_upp = var_ncNe["theta_up"][:] #rad
    p_low = var_ncNe["phi_low"][:] #rad
    p_upp = var_ncNe["phi_up"][:] #rad
    # radius = var_ncNe["planet_radius"][:]*1e-5 #km
    radius = var_ncNe["Planetary_Radius"][:]*1e-5 #km
    nr = var_ncNe["npt_alt"][:][0]
    ntheta = var_ncNe["npt_lat"][:][0]
    nphi = var_ncNe["npt_lon"][:][0]
    print("nr=",nr," ntheta=",ntheta," nphi=",nphi)

    phi_ori = [0, 180] # XZ
    # phi_ori = [230-180, 230] #math.pi/2.0										#equatorial angle (pi/2)
    pos = {phi_ori[0] : -1, phi_ori[1] : 1}
    print(pos)
    # phi_ori2 = phi_ori-180

    dphi = 5.0*math.pi/180.0									#interval of the equatorial angle for statistical mean, in rad
    dtheta = 0*math.pi/180.0

    # print "Start ploting figure"
    fig, ax = plt.subplots()#figsize=[8,6])
    
    cmap = mpl.cm.get_cmap("inferno")
    if "O2" in spe:
        vmin_max = [0.0 , 9.0]
    elif "H2O" in spe:
        vmin_max = [0.0 , 6.0]
    else:
        vmin_max = [0.0 , 7.0]
    
    norm = mpcol.Normalize(vmin=vmin_max[0], vmax=vmin_max[1])#np.amax(dens_eq_plane))

    for phi_deg in phi_ori:
        phi = phi_deg*math.pi/180.0

        pp=0
        while not(p_low[pp,0,0] <= phi and phi < p_upp[pp,0,0]):
            pp += 1
        phi1 = (p_upp[pp,0,0]+p_low[pp,0,0])/2 - dphi
        phi2 = (p_upp[pp,0,0]+p_low[pp,0,0])/2 + dphi
        if phi1 <= 0:
            phi1=0
        else:
            pp=0
            while not(p_low[pp,0,0] <= phi1 and phi1 < p_upp[pp,0,0]):
                pp += 1
            phi1=pp
        if phi2 > 2*math.pi:
            phi2 = nphi
        else:
            pp=0
            while not(p_low[pp,0,0] <= phi2 and phi2 < p_upp[pp,0,0]):
                pp += 1
            phi2=pp



        dens_eq_plane=[]
        for t in range(0, ntheta):
            sum_cells = np.zeros(shape=(nr), dtype=float)
            dens = np.zeros(shape=(nr), dtype=float)
            for p in range(phi1, phi2):
                if(p_upp[p,t,0]>=(phi-dphi) and p_low[p,t,0]<=(phi+dphi)):
                    for r in range(0, nr):
                        dr = r_upp[p,t,r] - r_low[p,t,r]
                        dt = t_upp[p,t,r] - t_low[p,t,r]
                        dp = p_upp[p,t,r] - p_low[p,t,r]
                        Vcell = 0
                        if t_low[p,t,r] == 0:
                            Vcell = r_low[p,t,r]**2 * np.sin(t_upp[p,t,r]) * dr * dt * dp
                        else:
                            Vcell = r_low[p,t,r]**2 * np.sin(t_low[p,t,r]) * dr * dt * dp
                        # Vcell = 1.0/3.0 * (r_upp[r] ** 3 - r_low[r] ** 3) * (p_upp[p] - p_low[p])
                        # Vcell = Vcell * math.fabs(math.cos(t_low[t] - math.cos(t_upp[t])))
                        sum_cells[r] = sum_cells[r] + Vcell
                        dens[r] = dens[r] + density[p][t][r]*Vcell 
                        # Phi = (p_upp[p]+p_low[p])/2.0								#interval of the equatorial angle for statistical mean, in rad
                        
            dens = np.divide(dens,sum_cells)
            dens_eq_plane.append(dens)
        dens_eq_plane = np.asarray(dens_eq_plane, dtype=float)
        dens_eq_plane = np.log10(dens_eq_plane)
        # print "End computing density"

        
        print(np.amax(dens_eq_plane))

        lim = r_upp[0,0,-1]
        # convertion en coordonnees cartesiennes
        # x = r * sin theta * cos phi
        # y = r * sin theta * sin phi
        # z = r * cos theta
        for r in range(0, nr):
            if(r_upp[0,0,r]<lim):
                for t in range(0, ntheta):
                    #coordonees bas gauche
                    x_bg = r_upp[0,t,r] * np.cos(t_upp[0,t,r]+math.pi/2) * pos[phi_deg]#-1.0
                    y_bg = r_upp[0,t,r] * np.sin(t_upp[0,t,r]+math.pi/2) 
                    #coordonees haut gauche
                    x_hg = r_upp[0,t,r] * np.cos(t_low[0,t,r]+math.pi/2) * pos[phi_deg]#-1.0
                    y_hg = r_upp[0,t,r] * np.sin(t_low[0,t,r]+math.pi/2) 
                    #coordonees bas droit
                    x_bd = r_low[0,t,r] * np.cos(t_upp[0,t,r]+math.pi/2) * pos[phi_deg]#-1.0
                    y_bd = r_low[0,t,r] * np.sin(t_upp[0,t,r]+math.pi/2) 
                    #coordonees haut droit
                    x_hd = r_low[0,t,r] * np.cos(t_low[0,t,r]+math.pi/2) * pos[phi_deg]#-1.0
                    y_hd = r_low[0,t,r] * np.sin(t_low[0,t,r]+math.pi/2) 

                    pts = np.array([[x_bg,y_bg], [x_hg,y_hg], [x_hd,y_hd], [x_bd,y_bd]])
                    color = cmap(norm(dens_eq_plane[t][r]))
                    p = Polygon(pts, color=color, )
                    ax = plt.gca()
                    
                    ax.add_patch(p)



    # w = Wedge(0.0, radius, 0, 360, edgecolor ="white",facecolor="black")
    # ax = plt.gca()
    # ax.add_patch(w)


    ax.set_xlim(-3000,3000)
    ax.set_ylim(-3000,3000)
    ax.set_xlabel("X [km]")
    ax.set_ylabel("Z [km]")

    i = ax.imshow(dens_eq_plane, cmap=cmap, vmin=vmin_max[0], vmax=vmin_max[1])
    cb = fig.colorbar(i)
    cb.set_label("log10 [$cm^{-3}$]")

    plt.setp(plt.getp(cb.ax.axes, 'yticklabels'))

    # tmp = file_ori.split("/")
    # file = tmp[len(tmp)-1][:-3]
    # file.replace('density_', '')
    # fig_title = "long"+str(phi_ori[0])+"_"+str(phi_ori[1])+"_"+spe+"_"+file+".png"
    # dir_name=""#"plot/density_plot_lat_p230/"
    plt.savefig(dest+"/plan_XZ_density_egm"+spe+".png",facecolor=fig.get_facecolor(), edgecolor='none')
    print("End ploting figure")
    plt.close("all")

def plot_density_egm_YZ(file,dest,moment,spe):

    ncidNe = Dataset(file)
    var_ncNe = ncidNe.variables

    density = var_ncNe[moment][:] #cm-3
    r_low = var_ncNe["altitude_low"][:]*1e-5 #km
    r_upp = var_ncNe["altitude_up"][:]*1e-5 #km
    t_low = var_ncNe["theta_low"][:] #rad
    t_upp = var_ncNe["theta_up"][:] #rad
    p_low = var_ncNe["phi_low"][:] #rad
    p_upp = var_ncNe["phi_up"][:] #rad
    # radius = var_ncNe["planet_radius"][:]*1e-5 #km
    radius = var_ncNe["Planetary_Radius"][:]*1e-5 #km
    nr = var_ncNe["npt_alt"][:][0]
    ntheta = var_ncNe["npt_lat"][:][0]
    nphi = var_ncNe["npt_lon"][:][0]
    print("nr=",nr," ntheta=",ntheta," nphi=",nphi)

    phi_ori = [90,270] # YZ
    # phi_ori = [230-180, 230] #math.pi/2.0										#equatorial angle (pi/2)
    pos = {phi_ori[0] : -1, phi_ori[1] : 1}
    print(pos)
    # phi_ori2 = phi_ori-180

    dphi = 5.0*math.pi/180.0									#interval of the equatorial angle for statistical mean, in rad
    dtheta = 0*math.pi/180.0

    # print "Start ploting figure"
    fig, ax = plt.subplots()#figsize=[8,6])
    
    cmap = mpl.cm.get_cmap("inferno")
    if "O2" in spe:
        vmin_max = [0.0 , 9.0]
    elif "H2O" in spe:
        vmin_max = [0.0 , 6.0]
    else:
        vmin_max = [0.0 , 7.0]
    
    norm = mpcol.Normalize(vmin=vmin_max[0], vmax=vmin_max[1])#np.amax(dens_eq_plane))

    for phi_deg in phi_ori:
        phi = phi_deg*math.pi/180.0

        pp=0
        while not(p_low[pp,0,0] <= phi and phi < p_upp[pp,0,0]):
            pp += 1
        phi1 = (p_upp[pp,0,0]+p_low[pp,0,0])/2 - dphi
        phi2 = (p_upp[pp,0,0]+p_low[pp,0,0])/2 + dphi
        if phi1 <= 0:
            phi1=0
        else:
            pp=0
            while not(p_low[pp,0,0] <= phi1 and phi1 < p_upp[pp,0,0]):
                pp += 1
            phi1=pp
        if phi2 > 2*math.pi:
            phi2 = nphi
        else:
            pp=0
            while not(p_low[pp,0,0] <= phi2 and phi2 < p_upp[pp,0,0]):
                pp += 1
            phi2=pp



        dens_eq_plane=[]
        for t in range(0, ntheta):
            sum_cells = np.zeros(shape=(nr), dtype=float)
            dens = np.zeros(shape=(nr), dtype=float)
            for p in range(phi1, phi2):
                if(p_upp[p,t,0]>=(phi-dphi) and p_low[p,t,0]<=(phi+dphi)):
                    for r in range(0, nr):
                        dr = r_upp[p,t,r] - r_low[p,t,r]
                        dt = t_upp[p,t,r] - t_low[p,t,r]
                        dp = p_upp[p,t,r] - p_low[p,t,r]
                        Vcell = 0
                        if t_low[p,t,r] == 0:
                            Vcell = r_low[p,t,r]**2 * np.sin(t_upp[p,t,r]) * dr * dt * dp
                        else:
                            Vcell = r_low[p,t,r]**2 * np.sin(t_low[p,t,r]) * dr * dt * dp
                        # Vcell = 1.0/3.0 * (r_upp[r] ** 3 - r_low[r] ** 3) * (p_upp[p] - p_low[p])
                        # Vcell = Vcell * math.fabs(math.cos(t_low[t] - math.cos(t_upp[t])))
                        sum_cells[r] = sum_cells[r] + Vcell
                        dens[r] = dens[r] + density[p][t][r]*Vcell 
                        # Phi = (p_upp[p]+p_low[p])/2.0								#interval of the equatorial angle for statistical mean, in rad
                        
            dens = np.divide(dens,sum_cells)
            dens_eq_plane.append(dens)
        dens_eq_plane = np.asarray(dens_eq_plane, dtype=float)
        dens_eq_plane = np.log10(dens_eq_plane)
        # print "End computing density"

        
        print(np.amax(dens_eq_plane))

        lim = r_upp[0,0,-1]
        # convertion en coordonnees cartesiennes
        # x = r * sin theta * cos phi
        # y = r * sin theta * sin phi
        # z = r * cos theta
        for r in range(0, nr):
            if(r_upp[0,0,r]<lim):
                for t in range(0, ntheta):
                    #coordonees bas gauche
                    x_bg = r_upp[0,t,r] * np.cos(t_upp[0,t,r]+math.pi/2) * pos[phi_deg]#-1.0
                    y_bg = r_upp[0,t,r] * np.sin(t_upp[0,t,r]+math.pi/2) 
                    #coordonees haut gauche
                    x_hg = r_upp[0,t,r] * np.cos(t_low[0,t,r]+math.pi/2) * pos[phi_deg]#-1.0
                    y_hg = r_upp[0,t,r] * np.sin(t_low[0,t,r]+math.pi/2) 
                    #coordonees bas droit
                    x_bd = r_low[0,t,r] * np.cos(t_upp[0,t,r]+math.pi/2) * pos[phi_deg]#-1.0
                    y_bd = r_low[0,t,r] * np.sin(t_upp[0,t,r]+math.pi/2) 
                    #coordonees haut droit
                    x_hd = r_low[0,t,r] * np.cos(t_low[0,t,r]+math.pi/2) * pos[phi_deg]#-1.0
                    y_hd = r_low[0,t,r] * np.sin(t_low[0,t,r]+math.pi/2) 

                    pts = np.array([[x_bg,y_bg], [x_hg,y_hg], [x_hd,y_hd], [x_bd,y_bd]])
                    color = cmap(norm(dens_eq_plane[t][r]))
                    p = Polygon(pts, color=color, )
                    ax = plt.gca()
                    
                    ax.add_patch(p)



    # w = Wedge(0.0, radius, 0, 360, edgecolor ="white",facecolor="black")
    # ax = plt.gca()
    # ax.add_patch(w)


    ax.set_xlim(-3000,3000)
    ax.set_ylim(-3000,3000)
    ax.set_xlabel("Y [km]")
    ax.set_ylabel("Z [km]")

    i = ax.imshow(dens_eq_plane, cmap=cmap, vmin=vmin_max[0], vmax=vmin_max[1])
    cb = fig.colorbar(i)
    cb.set_label("log10 [$cm^{-3}$]")

    plt.setp(plt.getp(cb.ax.axes, 'yticklabels'))

    # tmp = file_ori.split("/")
    # file = tmp[len(tmp)-1][:-3]
    # file.replace('density_', '')
    # fig_title = "long"+str(phi_ori[0])+"_"+str(phi_ori[1])+"_"+spe+"_"+file+".png"
    # dir_name=""#"plot/density_plot_lat_p230/"
    plt.savefig(dest+"/plan_YZ_density_egm_"+spe+".png",facecolor=fig.get_facecolor(), edgecolor='none')
    print("End ploting figure")
    plt.close("all")

plot_density_egm_XY(sys.argv[1],sys.argv[2],"density_O2","O2")
# plot_density_egm_XY(sys.argv[1],sys.argv[2],"density_H2O","H2O")
# plot_density_egm_XY(sys.argv[1],sys.argv[2],"density_H2","H2")

plot_density_egm_XZ(sys.argv[1],sys.argv[2],"density_O2","O2")
# plot_density_egm_XZ(sys.argv[1],sys.argv[2],"density_H2O","H2O")
# plot_density_egm_XZ(sys.argv[1],sys.argv[2],"density_H2","H2")

plot_density_egm_YZ(sys.argv[1],sys.argv[2],"density_O2","O2")
# plot_density_egm_YZ(sys.argv[1],sys.argv[2],"density_H2O","H2O")
# plot_density_egm_YZ(sys.argv[1],sys.argv[2],"density_H2","H2")
