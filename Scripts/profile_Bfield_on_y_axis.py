from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

### Plot profiles of Bx,By,Bz and Ux,Uy,Uz for 2 simulations on plane x=0, z=0
### @param Magw of simulation 1
### @param Magw of simulation 2
### @param Thew of simulation 1
### @param Thew of simulation 2

def plot_profile(Cx,Cy,Cz,var_nc1,var_nc2,ylab,y_lim):
	centr1      = var_nc1['s_centr'][:]
	radius     = var_nc1['r_planet'][:]
	gs1         = var_nc1['gstep'][:]
	Cx1         = var_nc1[Cx][:]
	Cy1         = var_nc1[Cy][:]
	Cz1         = var_nc1[Cz][:]

	centr2      = var_nc2['s_centr'][:]
	gs2         = var_nc2['gstep'][:]
	Cx2         = var_nc2[Cx][:]
	Cy2         = var_nc2[Cy][:]
	Cz2         = var_nc2[Cz][:]

	nc1 = [len(Cx1[0][0]), len(Cx1[0]), len(Cx1)]
	nc2 = [len(Cx2[0][0]), len(Cx2[0]), len(Cx2)]

	x = 0.0 #float(sys.argv[2])#4.0
	z = 0.0

	xi1 = 0
	zi1 = 0
	test = (xi1*gs1[0] - (centr1[0]+0.5*gs1[0]))/radius
	while not (x-0.05 < test and test < x+0.05):
		xi1 += 1
		test = (xi1*gs1[0] - (centr1[0]+0.5*gs1[0]))/radius
	test = (zi1*gs1[2] - (centr1[2]+0.5*gs1[2]))/radius
	while not (z-0.05 < test and test < z+0.05):
		zi1 += 1
		test = (zi1*gs1[2] - (centr1[2]+0.5*gs1[2]))/radius
	zi1 += 1
	test = (zi1*gs1[2] - (centr1[2]+0.5*gs1[2]))/radius

	Y1 = np.arange(0,(nc1[1])*gs1[1],gs1[1])
	Y1 = np.divide(Y1, radius) - np.divide((centr1[1]+0.5*gs1[1])*np.ones(nc1[1]), radius)

	xi2 = 0
	zi2 = 0
	test = (xi2*gs2[0] - (centr2[0]+0.5*gs2[0]))/radius
	while not (x-0.05 < test and test < x+0.05):
		xi2 += 1
		test = (xi2*gs2[0] - (centr2[0]+0.5*gs2[0]))/radius
	test = (zi2*gs2[2] - (centr2[2]+0.5*gs2[2]))/radius
	while not (z-0.05 < test and test < z+0.05):
		zi2 += 1
		test = (zi2*gs2[2] - (centr2[2]+0.5*gs2[2]))/radius
	zi2 += 1
	test = (zi2*gs2[2] - (centr2[2]+0.5*gs2[2]))/radius

	Y2 = np.arange(0,(nc2[1])*gs2[1],gs2[1])
	Y2 = np.divide(Y2, radius) - np.divide((centr2[1]+0.5*gs2[1])*np.ones(nc2[1]), radius)


	CXy1 = np.zeros(nc1[1])
	CXy1[:] = Cx1[zi1,:,xi1]
	CYy1 = np.zeros(nc1[1])
	CYy1[:] = Cy1[zi1,:,xi1]
	CZy1 = np.zeros(nc1[1])
	CZy1[:] = Cz1[zi1,:,xi1]

	CXy2 = np.zeros(nc2[1])
	CXy2[:] = Cx2[zi2,:,xi2]
	CYy2 = np.zeros(nc2[1])
	CYy2[:] = Cy2[zi2,:,xi2]
	CZy2 = np.zeros(nc2[1])
	CZy2[:] = Cz2[zi2,:,xi2]

	# fig, ax = plt.subplots(figsize=(8,6))
	plt.plot(Y1[:-1],CXy1[:-1],c="blue",label=Cx+" témoin")
	plt.plot(Y2[:-1],CXy2[:-1],c="red",label=Cx)
	titre = Cx+" profile at x="+str(x)+" and z=0.0"#+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	plt.xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	plt.ylabel(ylab)#,'fontsize',12,'fontweight','b');
	plt.legend(loc="upper right")
	plt.xlim(-6,6)
	plt.ylim(y_lim[0][0],y_lim[0][1])
	# plt.show()
	plt.savefig(Cx+"_profile_at_x_"+str(x)+"_z_"+str(z)+".png")
	plt.close('all')



	plt.plot(Y1[:-1],CYy1[:-1],c="blue",label=Cy+" témoin")
	plt.plot(Y2[:-1],CYy2[:-1],c="red",label=Cy)
	titre = Cy+" profile at x="+str(x)+" and z=0.0"#+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	plt.xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	plt.ylabel(ylab)#,'fontsize',12,'fontweight','b');
	plt.legend(loc="upper right")
	plt.xlim(-6,6)
	plt.ylim(y_lim[1][0],y_lim[1][1])
	# plt.show()
	plt.savefig(Cy+"_profile_at_x_"+str(x)+"_z_"+str(z)+".png")
	plt.close('all')



	plt.plot(Y1[:-1],CZy1[:-1],c="blue",label=Cz+" témoin")
	plt.plot(Y2[:-1],CZy2[:-1],c="red",label=Cz)
	titre = Cz+" profile at x="+str(x)+" and z=0.0"#+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	plt.xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	plt.ylabel(ylab)#,'fontsize',12,'fontweight','b');
	plt.legend(loc="upper right")
	plt.xlim(-6,6)
	plt.ylim(y_lim[2][0],y_lim[2][1])
	# plt.show()
	plt.savefig(Cz+"_profile_at_x_"+str(x)+"_z_"+str(z)+".png")
	plt.close('all')

ncfile1 = sys.argv[1]#dirname + "Magw_" + runname + "_" + diagtime + '.nc'
ncfile2 = sys.argv[2]

ncid1 = Dataset(ncfile1)
var_nc1 = ncid1.variables
ncid2 = Dataset(ncfile2)
var_nc2 = ncid2.variables

y_lim = [[0,150],[-200,-50],[-450,-300]]
plot_profile("Bx","By","Bz",var_nc1,var_nc2,"B [nT]",y_lim)

ncfile1 = sys.argv[3]#dirname + "Thew_" + runname + "_" + diagtime + '.nc'
ncfile2 = sys.argv[4]

ncid1 = Dataset(ncfile1)
var_nc1 = ncid1.variables
ncid2 = Dataset(ncfile2)
var_nc2 = ncid2.variables

y_lim = [[0,200],[-100,100],[-100,100]]
plot_profile("Ux","Uy","Uz",var_nc1,var_nc2,"U [km/s]",y_lim)