#######
# Take a lathys file and extract a sub grid of -/+4RE et X,Y,Z axis
#######


from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

filename = sys.argv[1]
ncid = Dataset(filename)
input_dims = ncid.dimensions
var_nc = ncid.variables


### Globla variables
# global_var = ["gstep", "iter", ]

# gstep = var_nc["gstep"][0]
# iter = var_nc["iter"][0]
# dt_t = var_nc["dt_t"][0]
# nptot = var_nc["nptot"][0]
# ns = var_nc["ns"][0]
# nfl = var_nc["nfl"][0]
# planetname = var_nc["planetname"][0]
# speciesname = var_nc["speciesname"][0]
# phys_density = var_nc["phys_density"][0]
# phys_mag = var_nc["phys_mag"][0]
# phys_speed = var_nc["phys_speed"][0]
phys_length = var_nc["phys_length"][:]
# phys_time = var_nc["phys_time"][0]
# phys_temp_e = var_nc["phys_temp_e"][0]
# absorpt_len = var_nc["absorpt_len"][0]
# s_centr = var_nc["s_centr"][0]
r_planet = var_nc["r_planet"][0]*phys_length[0]
# r_lim = var_nc["r_lim"][0]
# r_exo = var_nc["r_exo"][0]
# r_iono = var_nc["r_iono"][0]
# v_planet = var_nc["v_planet"][0]
# npp = var_nc["np"][0]
# ng = var_nc["np"][0]
# n1 = var_nc["n1"][0]
# n2 = var_nc["n2"][0]
# betae = var_nc["betae"][0]
# betas = var_nc["betas"][0]
# rvth = var_nc["rvth"][0]
# vth1 = var_nc["vth1"][0]
# vth2 = var_nc["vth2"][0]
# rmds = var_nc["rmds"][0]
# qms = var_nc["qms"][0]
# sq = var_nc["sq"][0]
# sm = var_nc["sm"][0]
# vxs = var_nc["vxs"][0]
# vys = var_nc["vys"][0]
# vzs = var_nc["vzs"][0]
# flux_tot = var_nc["flux_tot"][0]
# percent = var_nc["percent"][0]
# ratios_charges = var_nc["ratios_charges"][0]
# ratios_masses = var_nc["ratios_masses"][0]
# THesTH = var_nc["THesTH"][0]
# vtHesvtH = var_nc["vtHesvtH"][0]
# prob_extract = var_nc["prob_extract"][0]
# vplus = var_nc["vplus"][0]
# s_min = var_nc["s_min"][0]
# s_max = var_nc["s_max"][0]
# unit = var_nc["unit"][0]
X_axis = var_nc["X_axis"][:]
Y_axis = var_nc["Y_axis"][:]
Z_axis = var_nc["Z_axis"][:]
# Unit_axis = var_nc["Unit_axis"][0]
# Coordinate_system = var_nc["Coordinate_system"][0]

Re=1560 #km
X_lim = [-4*Re, 4*Re]
Y_lim = [-4*Re, 4*Re]
Z_lim = [-4*Re, 4*Re]

X_lim_id = [0,0]
Y_lim_id = [0,0]
Z_lim_id = [0,0]
centr_out = [X_lim[1]/phys_length+0.5,Y_lim[1]/phys_length+0.5,Z_lim[1]/phys_length+0.5]

while X_axis[X_lim_id[0]] < X_lim[0]:
    X_lim_id[0] += 1
X_lim_id[0] -= 1    
while X_axis[X_lim_id[1]] < X_lim[1]:
    X_lim_id[1] += 1
X_lim_id[1] += 1

while Y_axis[Y_lim_id[0]] < Y_lim[0]:
    Y_lim_id[0] += 1
Y_lim_id[0] -= 1
while Y_axis[Y_lim_id[1]] < Y_lim[1]:
    Y_lim_id[1] += 1
Y_lim_id[1] += 1

while Z_axis[Z_lim_id[0]] < Z_lim[0]:
    Z_lim_id[0] += 1
Z_lim_id[0] -= 1
while Z_axis[Z_lim_id[1]] < Z_lim[1]:
    Z_lim_id[1] += 1
Z_lim_id[1] += 1

if "Elew" in filename:
    X_lim_id[1] += 1
    Y_lim_id[1] += 1
    Z_lim_id[1] += 1

X_axis_out = X_axis[X_lim_id[0]:X_lim_id[1]]/r_planet
Y_axis_out = Y_axis[Y_lim_id[0]:Y_lim_id[1]]/r_planet
Z_axis_out = Z_axis[Z_lim_id[0]:Z_lim_id[1]]/r_planet

# centr_out = [np.argwhere(X_axis_out==0.0), np.argwhere(Y_axis_out==0.0), np.argwhere(Z_axis_out==0.0)]

# while X_axis_out[centr_out[2]] < 0:
#         centr_out[2] += 1
# while Y_axis_out[centr_out[1]] < 0:
#         centr_out[1] += 1
# while Z_axis_out[centr_out[0]] < 0:
#         centr_out[0] += 1
# print(X_axis_out[centr_out[2]])

#### Writing new file
# outfile = Dataset(sys.argv[1][:-3]+"_extract_4Re_grid.nc", "w", format="NETCDF4",diskless=True,persist=True)
outfile = Dataset(sys.argv[1][:-3]+"_extract_4Re_grid.nc", "w", format="NETCDF3_CLASSIC",diskless=True,persist=True)

output_dims = []
for dims in input_dims.keys():
    if "size_x" in dims:
        outfile.createDimension(dims,len(X_axis_out))
    elif "size_y" in dims:
        outfile.createDimension(dims,len(Y_axis_out))
    elif "size_z" in dims:
        outfile.createDimension(dims,len(Z_axis_out))
    else:
        outfile.createDimension(dims,input_dims[dims].size)

for var in var_nc.keys():
    if var not in ["Bx","By","Bz","Ex","Ey","Ez","X_axis","Y_axis","Z_axis","s_centr","Density","Ux","Uy","Uz","Temperature","Resistivity"]:
        var_out = outfile.createVariable(var,var_nc[var].dtype,var_nc[var].dimensions)
        var_out[:] = var_nc[var][:]
    else:
        if var in ["Bx","By","Bz","Ex","Ey","Ez","Density","Ux","Uy","Uz","Temperature","Resistivity"]:
            var_out = outfile.createVariable(var,var_nc[var].dtype,var_nc[var].dimensions)
            var_out[:] = var_nc[var][Z_lim_id[0]:Z_lim_id[1], Y_lim_id[0]:Y_lim_id[1], X_lim_id[0]:X_lim_id[1]]
        elif var == "s_centr":
            var_out = outfile.createVariable(var,var_nc[var].dtype,var_nc[var].dimensions)
            var_out[:] = centr_out[:]
        elif var == "X_axis":
            var_out = outfile.createVariable(var,var_nc[var].dtype,var_nc[var].dimensions)
            var_out[:] = var_nc[var][X_lim_id[0]:X_lim_id[1]]/r_planet
        elif var == "Y_axis":
            var_out = outfile.createVariable(var,var_nc[var].dtype,var_nc[var].dimensions)
            var_out[:] = var_nc[var][Y_lim_id[0]:Y_lim_id[1]]/r_planet
        else:
            var_out = outfile.createVariable(var,var_nc[var].dtype,var_nc[var].dimensions)
            var_out[:] = var_nc[var][Z_lim_id[0]:Z_lim_id[1]]/r_planet
        



# print(X_axis_out)
# print(Y_axis_out)
# print(Z_axis_out)

### putting global variables in new file
# gstep_out = outfile.createVariable("gstep",gstep.dtype,("space_dimension"))
# gstep_out.title = ""
# gstep_out.units = ""
# gstep_out[:] = gstep