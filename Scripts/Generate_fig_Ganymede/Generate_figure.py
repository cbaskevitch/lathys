##############################
# Generate_Figure.py
# ----------------------
# This routine generate automatically
# pdf files of different quantities
# in XY and XZ plane
# Quantities are :
#    - Magnetic field (module and components)
#    - Electric field (module and components)
#    - Electron number density
#    - Bulk speed ( module and components)
#    - Ion density (for each species)
#    - Neutral density (for each species)
#   (- Production for each species)
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
##############################

from plot_density_ne_ganymede import *
from plot_ion_density_ganymede import *
from plot_bulk_speed_ganymede import *
from plot_magnetic_field_ganymede import *
from plot_electric_field_ganymede import *
# from plot_neutral_density_ganymede import *
import sys, os

src_dir = sys.argv[1]+"/"
dest_dir = sys.argv[2]+"/"
rundate = sys.argv[3]
diagtime = sys.argv[4]

zoom = True
if zoom == False:
	field_lines_dens = 2
else:
	field_lines_dens = 1

# print("Electron density")
# plot_density_ne(src_dir,dest_dir,'Thew_',rundate,diagtime,zoom)
# print("Ion density")
# plot_ion_density(src_dir,dest_dir,"Read_moment_species_",rundate,diagtime,zoom)
print("Magnetic field")
plot_magnetic_field(src_dir,dest_dir,'Magw_PG_',rundate,diagtime,zoom,field_lines_dens)
# print("Bulk speed")
# plot_bulk_speed(src_dir,dest_dir,'Thew_',rundate,diagtime,zoom)
# print("Electric field")
# plot_electric_field(src_dir,dest_dir,'Elew_',rundate,diagtime,zoom,field_lines_dens)
# plot_neutral_density(dirname,'Atmw_',rundate,diagtime)
