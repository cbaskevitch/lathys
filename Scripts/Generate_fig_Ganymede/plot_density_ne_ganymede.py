#######################
# plot_density_ne_ganymede.py
#---------------------
# This routine reads the density
#  file and plot the map in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
#######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os


def plot_density_ne(src_dir,dest_dir,typefile,rundate,diagtime,zoom):
	ncfile = src_dir + typefile + rundate + "_t" + diagtime + '.nc'

	ncid = Dataset(ncfile)
	var_nc = ncid.variables

	planetname = var_nc['planetname'][:]
	centr      = var_nc['s_centr'][:]
	radius     = var_nc['r_planet'][:]
	gs         = var_nc['gstep'][:]
	nptot      = var_nc['nptot'][:]
	Dn         = var_nc['Density'][:]
	nrm        = var_nc['phys_density'][:]
	nrm_len    = var_nc['phys_length'][:]
	radius=1.0
	nc = [len(Dn[0][0]), len(Dn[0]), len(Dn)]

	Dn = np.where(Dn <= 0, float('NaN'), Dn)

	# maximum and minimum 
	if radius > 1.0:
		min_val = -1.0 # log(cm-3) for run with planete
		max_val = 2.5  # log(sm-3) for run with planete
	else:
		min_val = 0.35 # log(cm-3) for run without planete
		max_val = 0.65 # log(cm-3) for run without planete

	# -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)

	# planet center in cell number (NB: cell number start at 1
	icentr = int(np.fix(centr[0]/gs[0]))
	jcentr = int(np.fix(centr[1]/gs[1]))
	kcentr = int(np.fix(centr[2]/gs[2]))
	iwake = int(icentr + np.fix(1.5*radius/gs[0]))

	Dn_XY = np.zeros((nc[0],nc[1]))
	Dn_XY[:,:] = np.matrix.transpose(Dn[kcentr,:,:])

	Dn_XZ = np.zeros((nc[0],nc[2]))
	Dn_XZ[:,:] = np.matrix.transpose(Dn[:,jcentr,:])

	Dn_YZ_term = np.zeros((nc[1],nc[2]))
	Dn_YZ_term[:,:] = np.matrix.transpose(Dn[:,:,icentr])

	Dn_YZ_wake = np.zeros((nc[1],nc[2]))
	Dn_YZ_wake[:,:] = np.matrix.transpose(Dn[:,:,iwake])

	Dne_1D = np.zeros(nc[0])
	Dne_1D[:] = Dn[kcentr,jcentr,:]
	x = np.divide(np.arange(0, nc[0], 1.0) * gs[0]-centr[0], radius)
	# x = ([0:nc(1)-1]*gs(1)-centr(1))./radius;

	
	# planet drawing
	theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
	xp = np.cos(theta)
	yp = np.sin(theta)

	if zoom == True:
		Xmin = -4.0
		Xmax = 4.0
		Ymin = -4.0
		Ymax = 4.0
		Zmin = -4.0
		Zmax = 4.0

	# -- Figure 1 & 2 -- Dn
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XY, Y_XY, np.log10(Dn_XY), vmin=min_val, vmax=max_val, cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# lim = 20.0

	titre = "Density ne log[cm-3] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Dn_ne_XY_Ganymede_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, np.log10(Dn_XZ), vmin=min_val, vmax=max_val, cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# lim = 20.0

	titre = "Density ne log[cm-3] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Dn_ne_XZ_Ganymede_"+rundate+"_t"+diagtime+".png")

	# =========== figure in YZ plane ==============================
	# Dn in X=0 and X=1.5Rm

	# figure 3 & 4
	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(8,6))#figsize=(7,6.5))
	c = ax.pcolor(Y_YZ, Z_YZ, np.log10(Dn_YZ_term), vmin=min_val, vmax=max_val, cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# lim = 20.0

	titre = "Density ne log[cm-3] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Dn_ne_YZ_terminator_Ganymede_"+rundate+"_t"+diagtime+".png")

	# --
	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(8,6))#figsize=(7,6.5))
	c = ax.pcolor(Y_YZ, Z_YZ, np.log10(Dn_YZ_wake), vmin=min_val, vmax=max_val, cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	# lim = 20.0

	titre = "Density ne log[cm-3] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	# plt.show()
	plt.savefig(dest_dir+"Dn_ne_YZ_wake_Ganymede_"+rundate+"_t"+diagtime+".png")

	plt.close('all')
