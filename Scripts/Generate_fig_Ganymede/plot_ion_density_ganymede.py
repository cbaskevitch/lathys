######################
# plot_ion_density_ganymede.py
# ---------------------
# This routine reads the density
#  file and plot the map in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

def plot_ion_density(src_dir,dest_dir,typefile,rundate,diagtime,zoom):
	filename = src_dir + typefile + rundate + "_t" + diagtime + ".dat"
	fid = open(filename,"r")
	lines = fid.readlines()
	fid.close()

	nb_species = 0
	species_name = []

	l0 = lines[1].split(" ")
	for l in l0:
		if l != "" and l != "\n":
			nb_species += 1
			species_name.append(l)

	namearray = []
	for i in range(2,len(lines)):
		l0 = lines[i].split(" ")
		for l in l0:
			if l != "" and l != "\n":
				namearray.append(l)

	if zoom == True:
		Xmin = -4.0
		Xmax = 4.0
		Ymin = -4.0
		Ymax = 4.0
		Zmin = -4.0
		Zmax = 4.0

	for i in range(0, len(namearray)):
		ncid = Dataset(namearray[i])
		var_nc = ncid.variables

		planetname = var_nc['planetname'][:]
		centr      = var_nc['s_centr'][:]
		radius     = var_nc['r_planet'][:]
		gs         = var_nc['gstep'][:]
		nptot      = var_nc['nptot'][:]
		Dn         = var_nc['Density'][:]
		Ux_ion     = var_nc['Ux'][:]
		Uy_ion     = var_nc['Uy'][:]
		Uz_ion     = var_nc['Uz'][:]
		nrm        = var_nc['phys_density'][:]
		radius=1.0
		nc = [len(Dn[0][0]), len(Dn[0]), len(Dn)]
		Utot_ion = np.sqrt(Ux_ion*Ux_ion + Uy_ion*Uy_ion + Uz_ion*Uz_ion)
		Dn = np.where(Dn <= 0, float('NaN'), Dn)
		# Utot_ion = math.sqrt(Ux_ion.^2++Uy_ion.^2+Uz_ion.^2)

		# maximum and minimum
		if "O" in species_name[i] :
			if radius > 1.0:
				min_val = -1.0#-2.0 # log(cm-3) for run with planete
				max_val = 0.8#2.0 # log(sm-3) for run with planete
				min_valtot = 0.0 # km/s for run with planete
				max_valtot = 600.0 # km/s for run with planete
			else:
				min_val = 0.4 # log(cm-3) for run without planete
				max_val = 0.7 # log(sm-3) for run without planete
				min_valtot = 120.0 # km/s for run without planete
				max_valtot = 150.0 # km/s for run without planete
		else :
			if radius > 1.0:
				min_val = -1.0#-2.0 # log(cm-3) for run with planete
				max_val = 0.8#2.0 # log(sm-3) for run with planete
				min_valtot = 0.0 # km/s for run with planete
				max_valtot = 600.0 # km/s for run with planete
			else:
				min_val = 0.4 # log(cm-3) for run without planete
				max_val = 0.7 # log(sm-3) for run without planete
				min_valtot = 120.0 # km/s for run without planete
				max_valtot = 150.0 # km/s for run without planete


		# -- Creation of axis values centered on the planet ( normalized to planet radius)
		X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
		X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
		Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

		X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
		X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
		Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

		Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
		Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
		Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)

		# planet center in cell number (NB: cell number start at 1
		icentr = int(np.fix(centr[0]/gs[0]))
		jcentr = int(np.fix(centr[1]/gs[1]))
		kcentr = int(np.fix(centr[2]/gs[2]))
		iwake = icentr #int(icentr + np.fix(2.0*radius/gs[0]))

		Dn_XY = np.zeros((nc[0],nc[1]))
		Dn_XY[:,:] = np.matrix.transpose(Dn[kcentr,:,:])

		Utot_XY_ion = np.zeros((nc[0],nc[1]))
		Utot_XY_ion[:,:] = np.matrix.transpose(Utot_ion[kcentr,:,:])

		Dn_XZ = np.zeros((nc[0],nc[2]))
		Dn_XZ[:,:] = np.matrix.transpose(Dn[:,jcentr,:])

		Utot_XZ_ion = np.zeros((nc[0],nc[2]))
		Utot_XZ_ion[:,:] = np.matrix.transpose(Utot_ion[:,jcentr,:])

		Dn_YZ_term = np.zeros((nc[1],nc[2]))
		Dn_YZ_term[:,:] = np.matrix.transpose(Dn[:,:,icentr])

		Dn_YZ_wake = np.zeros((nc[1],nc[2]))
		Dn_YZ_wake[:,:] = np.matrix.transpose(Dn[:,:,iwake])

		Dn_1D = np.zeros(nc[0])
		Dn_1D[:] = Dn[kcentr,jcentr,:]
		x = np.divide(np.arange(0, nc[0], 1.0) * gs[0]-centr[0], radius)

		# planet drawing
		theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
		xp = np.cos(theta)
		yp = np.sin(theta)

		# -- Figure 1 & 2 -- Dn
		# **************************************************************************

		if zoom == True:
			fig, ax = plt.subplots(figsize=(7.5,6))
			ax.set_xlim(Xmin,Xmax)
			ax.set_ylim(Ymin,Ymax)
		else:
			fig, ax = plt.subplots(figsize=(6,9.5))
		c = ax.pcolor(X_XY, Y_XY, np.log10(Dn_XY), vmin=min_val, vmax=max_val, cmap="jet",shading='auto')
		fig.colorbar(c, ax=ax)
		ax.plot(xp,yp,c="black")

		titre = "Density "+species_name[i]+" log[cm-3] time: "+diagtime
		plt.title(titre)#,'fontsize',12,'fontweight','b');
		ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
		ax.set_ylabel('Y [R_G]')#,'fontsize',12,'fontweight','b');

		plt.savefig(dest_dir+"Dn_"+species_name[i]+"_XY_Ganymede_"+rundate+"_t"+diagtime+".png")

		# --

		if zoom == True:
			fig, ax = plt.subplots(figsize=(7.5,6))
			ax.set_xlim(Xmin,Xmax)
			ax.set_ylim(Ymin,Ymax)
		else:
			fig, ax = plt.subplots(figsize=(6,9.5))
		c = ax.pcolor(X_XZ, Z_XZ, np.log10(Dn_XZ), vmin=min_val, vmax=max_val, cmap="jet",shading='auto')
		fig.colorbar(c, ax=ax)
		ax.plot(xp,yp,c="black")

		titre = "Density "+species_name[i]+" log[cm-3] time: "+diagtime
		plt.title(titre)#,'fontsize',12,'fontweight','b');
		ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
		ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

		plt.savefig(dest_dir+"Dn_"+species_name[i]+"_XZ_Ganymede_"+rundate+"_t"+diagtime+".png")

		# =========== figure in YZ plane ==============================
		# Dn in X=0 and X=1.5Rm

		# figure 3 & 4
		if zoom == True:
			fig, ax = plt.subplots(figsize=(7.5,6))
			ax.set_xlim(Xmin,Xmax)
			ax.set_ylim(Ymin,Ymax)
		else:
			fig, ax = plt.subplots(figsize=(8,6))
		c = ax.pcolor(Y_YZ, Z_YZ, np.log10(Dn_YZ_term), vmin=min_val, vmax=max_val, cmap="jet",shading='auto')
		fig.colorbar(c, ax=ax)
		ax.plot(xp,yp,c="black")

		titre = "Density "+species_name[i]+" log[cm-3] time: "+diagtime
		plt.title(titre)#,'fontsize',12,'fontweight','b');
		ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
		ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

		plt.savefig(dest_dir+"Dn_"+species_name[i]+"_YZ_terminator_Ganymede_"+rundate+"_t"+diagtime+".png")

		# --
		if zoom == True:
			fig, ax = plt.subplots(figsize=(7.5,6))
			ax.set_xlim(Xmin,Xmax)
			ax.set_ylim(Ymin,Ymax)
		else:
			fig, ax = plt.subplots(figsize=(8,6))
		c = ax.pcolor(Y_YZ, Z_YZ, np.log10(Dn_YZ_wake), vmin=min_val, vmax=max_val, cmap="jet",shading='auto')
		fig.colorbar(c, ax=ax)
		ax.plot(xp,yp,c="black")

		titre = "Density "+species_name[i]+" log[cm-3] time: "+diagtime
		plt.title(titre)#,'fontsize',12,'fontweight','b');
		ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
		ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

		plt.savefig(dest_dir+"Dn_"+species_name[i]+"_YZ_wake_Ganymede_"+rundate+"_t"+diagtime+".png")

	# -- Figure 1 & 2 -- Utot
		# **************************************************************************

		if zoom == True:
			fig, ax = plt.subplots(figsize=(7.5,6))
			ax.set_xlim(Xmin,Xmax)
			ax.set_ylim(Ymin,Ymax)
		else:
			fig, ax = plt.subplots(figsize=(6,9.5))
		c = ax.pcolor(X_XY, Y_XY, Utot_XY_ion, vmin=min_valtot, vmax=max_valtot, cmap="jet",shading='auto')
		fig.colorbar(c, ax=ax)
		ax.plot(xp,yp,c="black")
		# ax.set_xlim(-3,3)
		# ax.set_ylim(-3,3)

		titre = "Utot "+species_name[i]+" [km/s] time: "+diagtime
		plt.title(titre)#,'fontsize',12,'fontweight','b');
		ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
		ax.set_ylabel('Y [R_G]')#,'fontsize',12,'fontweight','b');

		plt.savefig(dest_dir+"Utot_"+species_name[i]+"_XY_Ganymede_"+rundate+"_t"+diagtime+".png")

		# --

		if zoom == True:
			fig, ax = plt.subplots(figsize=(7.5,6))
			ax.set_xlim(Xmin,Xmax)
			ax.set_ylim(Ymin,Ymax)
		else:
			fig, ax = plt.subplots(figsize=(6,9.5))
		c = ax.pcolor(X_XZ, Z_XZ, Utot_XZ_ion, vmin=min_valtot, vmax=max_valtot, cmap="jet",shading='auto')
		fig.colorbar(c, ax=ax)
		ax.plot(xp,yp,c="black")
		# ax.set_xlim(-3,3)
		# ax.set_ylim(-3,3)

		titre = "Utot "+species_name[i]+" [km/s] time: "+diagtime
		plt.title(titre)#,'fontsize',12,'fontweight','b');
		ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
		ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

		plt.savefig(dest_dir+"Utot_"+species_name[i]+"_XZ_Ganymede_"+rundate+"_t"+diagtime+".png")

		# plt.show()
		plt.close('all')
