#######################
# plot_Ne_Te_north-corotation_north-Jup_eq_plane_ganymede.py
#---------------------
# This routine reads the density 
# and temperature from Thew file 
# and plot the map in 
# the equatorial North/Corotation
# and North/Jupiter planes
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
#######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

#constantes
Kb = 1.38e-23 ## Constante de Boltzmann en m^2 kg s^-2 K-1

src_dir = sys.argv[1]
dest_dir = sys.argv[2]
rundate = sys.argv[3]
diagtime = sys.argv[4]
ncfile = src_dir + "/Thew_PG_" + rundate + "_t" + diagtime + '.nc'
ncid = Dataset(ncfile)
var_nc = ncid.variables

centr      = var_nc['s_centr'][:]
radius     = var_nc['r_planet'][:]
gs         = var_nc['gstep'][:]
Te = var_nc["Temperature"][:]
Ne = var_nc["Density"][:]

nc = [len(Te[0][0]), len(Te[0]), len(Te)]
print("nc=",nc)

# Coordinates creation for North/corotation plane
X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

# Coordinates creation for North/Jupiter plane
Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)

# planet center in cell number (NB: cell number start at 1
icentr = int(np.fix(centr[0]/gs[0]))
jcentr = int(np.fix(centr[1]/gs[1]))
kcentr = int(np.fix(centr[2]/gs[2]))

# planet drawing
theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
xp = np.cos(theta)
yp = np.sin(theta)

fig_size = [[9,7],[6,7.5],[8.5,7],[10,6]] #differentes tailles de fenetres
figsize_Xnum = 0  #numero de la taille de la fenetre pour les plans XZ et XY
figsize_Ynum = 2  #numero de la taille de la fenetre pour les plans YZ
Xmin=X_XZ[0][0]
Xmax=X_XZ[len(X_XZ)-1][len(X_XZ[0])-1]
Ymin=Y_YZ[0][0]
Ymax=Y_YZ[len(Y_YZ)-1][len(Y_YZ[0])-1]
Zmin=Z_XZ[0][0]
Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1]

# For temperature in eV
min_val_Te = 1.8
max_val_Te = 2.3

# For density in cm-3
min_val_Ne = 0.0
max_val_Ne = 3.5

Te_XZ = np.zeros((nc[0],nc[2]))
Te_XZ[:,:] = np.matrix.transpose(Te[:,jcentr,:])

Te_YZ = np.zeros((nc[1],nc[2]))
Te_YZ[:,:] = np.matrix.transpose(Te[:,:,icentr])

Ne_XZ = np.zeros((nc[0],nc[2]))
Ne_XZ[:,:] = np.matrix.transpose(Ne[:,jcentr,:])

Ne_YZ = np.zeros((nc[1],nc[2]))
Ne_YZ[:,:] = np.matrix.transpose(Ne[:,:,icentr])


#Temperature plot
fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XZ, Z_XZ, np.log10(Te_XZ), vmin=min_val_Te, vmax=max_val_Te, cmap="hot",shading='auto')
if radius != 1:
    ax.plot(xp,yp,c="white")

fig.colorbar(c, ax=ax)
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Zmin,Zmax)

titre = "Temperature [eV] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"/temperature_XZ_Ganymede_"+rundate+"_t"+diagtime+".png")

#Temperature plot
fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(Y_YZ, Z_YZ, np.log10(Te_YZ), vmin=min_val_Te, vmax=max_val_Te, cmap="hot",shading='auto')
if radius != 1:
    ax.plot(xp,yp,c="black")

fig.colorbar(c, ax=ax)
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "Temperature [eV] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"/temperature_YZ_Ganymede_"+rundate+"_t"+diagtime+".png")





#Density plot
fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XZ, Z_XZ, np.log10(Ne_XZ), vmin=min_val_Ne, vmax=max_val_Ne, cmap="hot",shading='auto')
if radius != 1:
    ax.plot(xp,yp,c="black")

fig.colorbar(c, ax=ax)
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Zmin,Zmax)

titre = "Density [cm-3] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"/Density_XZ_Ganymede_"+rundate+"_t"+diagtime+".png")

#Density plot
fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
c = ax.pcolor(Y_YZ, Z_YZ, np.log10(Ne_YZ), vmin=min_val_Ne, vmax=max_val_Ne, cmap="hot",shading='auto')
if radius != 1:
    ax.plot(xp,yp,c="black")

fig.colorbar(c, ax=ax)
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "Density [cm-3] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"/Density_YZ_Ganymede_"+rundate+"_t"+diagtime+".png")