######################
# Plot_electric_field_ganymede.py
# ---------------------
# This routine reads the electric
# field file and plot the module
# and the E components in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os


def plot_electric_field(src_dir,dest_dir,typefile,rundate,diagtime,zoom,field_lines_dens):
	ncfile = src_dir + typefile + rundate + "_t" + diagtime + '.nc'

	ncid = Dataset(ncfile)
	var_nc = ncid.variables

	planetname = var_nc['planetname'][:]
	centr      = var_nc['s_centr'][:]
	radius     = var_nc['r_planet'][:]
	gs         = var_nc['gstep'][:]
	nptot      = var_nc['nptot'][:]
	Ex         = var_nc['Ex'][:]
	Ey         = var_nc['Ey'][:]
	Ez         = var_nc['Ez'][:]
	nrm        = var_nc['phys_mag'][:]
	nrm_len    = var_nc['phys_length'][:]
	radius=1.0
	nc = [len(Ex[0][0]), len(Ex[0]), len(Ex)]

	Etot = np.sqrt(Ex*Ex + Ey*Ey + Ez*Ez)

	# maximum and minimum 
	if radius > 1.0:
		min_val    = [0.0,0.0,0.0] # mV/m [Ex,Ey,Ez] for run with planete
		max_val    = [1.5,1.5,1.5]  # mV/m [Ex,Ey,Ez] for run with planete
		min_valtot = 0.0   # mV/m for run with planete
		max_valtot = 1.5 # mV/m for run with planete
	else:
		min_val    = [-1.0,-11.5,9.0] # mV/m [Ex,Ey,Ez] for run without planete
		max_val    = [1.0,-10.5,12.0]  # mV/m [Ex,Ey,Ez] for run without planete
		min_valtot = 0.0   # mV/m for run without planete
		max_valtot = 2.0 # mV/m for run without planete


	# -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)

	# planet center in cell number (NE: cell number start at 0
	icentr = int(np.fix(centr[0]/gs[0]))-1
	jcentr = int(np.fix(centr[1]/gs[1]))-1
	kcentr = int(np.fix(centr[2]/gs[2]))-1
	iwake = int(icentr + np.fix(1.5*radius/gs[0]))
	rp = radius/gs[0]

	Ex_XY        = np.zeros((nc[0],nc[1]))
	Ex_XY[:,:]   = np.matrix.transpose(Ex[kcentr,:,:])
	Ey_XY        = np.zeros((nc[0],nc[1]))
	Ey_XY[:,:]   = np.matrix.transpose(Ey[kcentr,:,:])
	Ez_XY        = np.zeros((nc[0],nc[1]))
	Ez_XY[:,:]   = np.matrix.transpose(Ez[kcentr,:,:])
	Etot_XY      = np.zeros((nc[0],nc[1]))
	Etot_XY[:,:] = np.matrix.transpose(Etot[kcentr,:,:])

	Ex_XZ        = np.zeros((nc[0],nc[2]))
	Ex_XZ[:,:]   = np.matrix.transpose(Ex[:,jcentr,:])
	Ey_XZ        = np.zeros((nc[0],nc[2]))
	Ey_XZ[:,:]   = np.matrix.transpose(Ey[:,jcentr,:])
	Ez_XZ        = np.zeros((nc[0],nc[2]))
	Ez_XZ[:,:]   = np.matrix.transpose(Ez[:,jcentr,:])
	Etot_XZ      = np.zeros((nc[0],nc[2]))
	Etot_XZ[:,:] = np.matrix.transpose(Etot[:,jcentr,:])

	Ex_YZ_term        = np.zeros((nc[1],nc[2]))
	Ex_YZ_term[:,:]   = np.matrix.transpose(Ex[:,:,icentr])
	Ey_YZ_term        = np.zeros((nc[1],nc[2]))
	Ey_YZ_term[:,:]   = np.matrix.transpose(Ey[:,:,icentr])
	Ez_YZ_term        = np.zeros((nc[1],nc[2]))
	Ez_YZ_term[:,:]   = np.matrix.transpose(Ez[:,:,icentr])
	Etot_YZ_term      = np.zeros((nc[1],nc[2]))
	Etot_YZ_term[:,:] = np.matrix.transpose(Etot[:,:,icentr])

	Ex_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ex_YZ_wake[:,:]   = np.matrix.transpose(Ex[:,:,iwake])
	Ey_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ey_YZ_wake[:,:]   = np.matrix.transpose(Ey[:,:,iwake])
	Ez_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ez_YZ_wake[:,:]   = np.matrix.transpose(Ez[:,:,iwake])
	Etot_YZ_wake      = np.zeros((nc[1],nc[2]))
	Etot_YZ_wake[:,:] = np.matrix.transpose(Etot[:,:,iwake])

	Ex_1D    = np.zeros(nc[0])
	Ex_1D[:] = Ex[kcentr,jcentr,:]
	Ey_1D    = np.zeros(nc[0])
	Ey_1D[:] = Ey[kcentr,jcentr,:]
	Ez_1D    = np.zeros(nc[0])
	Ez_1D[:] = Ez[kcentr,jcentr,:]
	x = np.divide(np.arange(0, nc[0], 1.0) * gs[0]-centr[0], radius)

	# planet drawing
	theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
	xp = np.cos(theta)
	yp = np.sin(theta)

	## Field lines
	Xmin=X_XY[0][0] #float(-1*icentr/rp)
	Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1] #float((nc[0]-icentr)/rp)
	X_norm=np.arange(Xmin, Xmax, (Xmax-Xmin)/(nc[0]))
	Ymin=Y_XY[0][0] #float(-jcentr/rp)
	Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1] #float((nc[1]-jcentr)/rp)
	Y_norm=np.arange(Ymin, Ymax, (Ymax-Ymin)/(nc[1]))
	Zmin=Z_XZ[0][0] #float(-kcentr/rp)
	Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1] #float((nc[1]-kcentr)/rp)
	Z_norm=np.arange(Zmin, Zmax, (Zmax-Zmin)/(nc[2]))

	if zoom == True:
		Xmin = -6.0
		Xmax = 6.0
		Ymin = -6.0
		Ymax = 6.0
		Zmin = -6.0
		Zmax = 6.0

	# -- Figure 1 & 2 -- Ex
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
	
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XY, Y_XY, Ex_XY, vmin=min_val[0], vmax=max_val[0], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.streamplot(X_norm,Y_norm,np.transpose(Ex_XY),np.transpose(Ey_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Ex [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_G]')#,'fontsize',12,'fontweight','b');

	# plt.show()
	plt.savefig(dest_dir+"Ex_XY_Ganymede_"+rundate+"_t"+diagtime+".png")
	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
	
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, Ex_XZ, vmin=min_val[0], vmax=max_val[0], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.streamplot(X_norm,Z_norm,np.transpose(Ex_XZ),np.transpose(Ez_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ex [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ex_XZ_Ganymede_"+rundate+"_t"+diagtime+".png")
	# plt.show()

	# -- Figure 3 & 4 -- Ey
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
	
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XY, Y_XY, Ey_XY, vmin=min_val[1], vmax=max_val[1], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.streamplot(X_norm,Y_norm,np.transpose(Ex_XY),np.transpose(Ey_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Ey [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ey_XY_Ganymede_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
	
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, Ey_XZ, vmin=min_val[1], vmax=max_val[1], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.streamplot(X_norm,Z_norm,np.transpose(Ex_XZ),np.transpose(Ez_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ey [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ey_XZ_Ganymede_"+rundate+"_t"+diagtime+".png")

	# -- Figure 3 & 4 -- Ez
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
	
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XY, Y_XY, Ez_XY, vmin=min_val[2], vmax=max_val[2], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.streamplot(X_norm,Y_norm,np.transpose(Ex_XY),np.transpose(Ey_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Ez [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ez_XY_Ganymede_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
	
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, Ez_XZ, vmin=min_val[2], vmax=max_val[2], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.streamplot(X_norm,Z_norm,np.transpose(Ex_XZ),np.transpose(Ez_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ez [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ez_XZ_Ganymede_"+rundate+"_t"+diagtime+".png")

	# -- Figure 3 & 4 -- Etot
	# **************************************************************************
	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
	
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XY, Y_XY, np.log10(Etot_XY), vmin=min_valtot, vmax=max_valtot, cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Etot [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Etot_XY_Ganymede_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
	
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, np.log10(Etot_XZ), vmin=min_valtot, vmax=max_valtot, cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Etot [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Etot_XZ_Ganymede_"+rundate+"_t"+diagtime+".png")

	# =========== figure in YZ plane ==============================
	# Ex in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
	
	else:
		fig, ax = plt.subplots(figsize=(8,6))
	c = ax.pcolor(Y_YZ,Z_YZ,Ex_YZ_term, vmin=min_val[0], vmax=max_val[0], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.streamplot(Y_norm,Z_norm,np.transpose(Ey_YZ_term),np.transpose(Ez_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ex [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ex_YZ_terminator_Ganymede_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
	
	else:
		fig, ax = plt.subplots(figsize=(8,6))
	c = ax.pcolor(Y_YZ,Z_YZ,Ex_YZ_wake, vmin=min_val[0], vmax=max_val[0], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.streamplot(Y_norm,Z_norm,np.transpose(Ey_YZ_wake),np.transpose(Ez_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ex [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ex_YZ_wake_Ganymede_"+rundate+"_t"+diagtime+".png")

	# Ey in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
	
	else:
		fig, ax = plt.subplots(figsize=(8,6))
	c = ax.pcolor(Y_YZ,Z_YZ,Ey_YZ_term, vmin=min_val[1], vmax=max_val[1], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.streamplot(Y_norm,Z_norm,np.transpose(Ey_YZ_term),np.transpose(Ez_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ey [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ey_YZ_terminator_Ganymede_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
	
	else:
		fig, ax = plt.subplots(figsize=(8,6))
	c = ax.pcolor(Y_YZ,Z_YZ,Ey_YZ_wake, vmin=min_val[1], vmax=max_val[1], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.streamplot(Y_norm,Z_norm,np.transpose(Ey_YZ_wake),np.transpose(Ez_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ey [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ey_YZ_wake_Ganymede_"+rundate+"_t"+diagtime+".png")

	# Ez in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
	
	else:
		fig, ax = plt.subplots(figsize=(8,6))
	c = ax.pcolor(Y_YZ,Z_YZ,Ez_YZ_term, vmin=min_val[2], vmax=max_val[2], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.streamplot(Y_norm,Z_norm,np.transpose(Ey_YZ_term),np.transpose(Ez_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ez [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ez_YZ_terminator_Ganymede_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
	
	else:
		fig, ax = plt.subplots(figsize=(8,6))
	c = ax.pcolor(Y_YZ,Z_YZ,Ez_YZ_wake, vmin=min_val[2], vmax=max_val[2], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.streamplot(Y_norm,Z_norm,np.transpose(Ey_YZ_wake),np.transpose(Ez_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ez [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ez_YZ_wake_Ganymede_"+rundate+"_t"+diagtime+".png")

	# Etot in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
	
	else:
		fig, ax = plt.subplots(figsize=(8,6))
	c = ax.pcolor(Y_YZ,Z_YZ,np.log10(Etot_YZ_term), vmin=min_valtot, vmax=max_valtot, cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Etot [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Etot_YZ_terminator_Ganymede_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
	
	else:
		fig, ax = plt.subplots(figsize=(8,6))
	c = ax.pcolor(Y_YZ,Z_YZ,np.log10(Etot_YZ_wake), vmin=min_valtot, vmax=max_valtot, cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Etot [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Etot_YZ_wake_Ganymede_"+rundate+"_t"+diagtime+".png")

	plt.close('all')
