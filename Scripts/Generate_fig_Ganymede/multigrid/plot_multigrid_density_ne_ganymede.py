#######################
# plot_density_ne_ganymede.py
#---------------------
# This routine reads the density
#  file and plot the map in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
#######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os
from multigrid_shared_plot_functions import *


def plot_multigrid_density_ne(src_dir,dest_dir,dest_dir_r,typefile,rundate,diagtime,zoom):
	ncfile = src_dir + typefile + rundate + "_t" + diagtime + '.nc'

	ncid = Dataset(ncfile)
	var_nc = ncid.variables

	planetname = var_nc['planetname'][:]
	centr      = var_nc['s_centr'][:]
	radius     = var_nc['r_planet'][:]
	gs         = var_nc['gstep'][:]
	gs_r       = var_nc['gstep_r'][:]
	s_min_r    = var_nc['s_min_r'][:] #indice de la premiere cellule de la grille rafinee dans la grande grille
	Dn         = var_nc['Density'][:]
	Dn_r       = var_nc['Density_r'][:]
	nrm        = var_nc['phys_density'][:]
	nrm_len    = var_nc['phys_length'][:]
	radius=1.0
	nc = [len(Dn[0][0]), len(Dn[0]), len(Dn)]
	nc_r = [len(Dn_r[0][0]), len(Dn_r[0]), len(Dn_r)]

	Dn = np.where(Dn <= 0, float('NaN'), Dn)
	Dn_r = np.where(Dn_r <= 0, float('NaN'), Dn_r)

	# maximum and minimum 
	if radius > 1.0:
		min_val = -1.0 # log(cm-3) for run with planete
		max_val = 2.5  # log(sm-3) for run with planete
	else:
		min_val = 0.4 # log(cm-3) for run without planete
		max_val = 0.7 # log(cm-3) for run without planete

	# -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)

	# -- Creation of axis values centered on the planet ( normalized to planet radius) for the small grid
	X_XY_r, Y_XY_r = np.meshgrid(np.arange(0,(nc_r[0])*gs_r[0],gs_r[0]),np.arange(0,(nc_r[1])*gs_r[1],gs_r[1]))
	X_XY_r = np.divide(np.matrix.transpose(X_XY_r), radius) - np.divide((centr[0]-s_min_r[0]+0.5*gs_r[0])*np.ones((nc_r[0],nc_r[1])), radius)
	Y_XY_r = np.divide(np.matrix.transpose(Y_XY_r), radius) - np.divide((centr[1]-s_min_r[1]+0.5*gs_r[1])*np.ones((nc_r[0],nc_r[1])), radius)

	X_XZ_r, Z_XZ_r = np.meshgrid(np.arange(0,(nc_r[0])*gs_r[0],gs_r[0]),np.arange(0,(nc_r[2])*gs_r[2],gs_r[2]))
	X_XZ_r = np.divide(np.matrix.transpose(X_XZ_r), radius) - np.divide((centr[0]-s_min_r[0]+0.5*gs_r[0])*np.ones((nc_r[0],nc_r[2])), radius)
	Z_XZ_r = np.divide(np.matrix.transpose(Z_XZ_r), radius) - np.divide((centr[2]-s_min_r[2]+0.5*gs_r[2])*np.ones((nc_r[0],nc_r[2])), radius)

	Y_YZ_r, Z_YZ_r = np.meshgrid(np.arange(0,(nc_r[1])*gs_r[1],gs_r[1]),np.arange(0,(nc_r[2])*gs_r[2],gs_r[2]))
	Y_YZ_r = np.divide(np.matrix.transpose(Y_YZ_r), radius) - np.divide((centr[1]-s_min_r[1]+0.5*gs_r[1])*np.ones((nc_r[1],nc_r[2])), radius)
	Z_YZ_r = np.divide(np.matrix.transpose(Z_YZ_r), radius) - np.divide((centr[2]-s_min_r[2]+0.5*gs_r[2])*np.ones((nc_r[1],nc_r[2])), radius)

	# planet center in cell number (NB: cell number start at 1
	icentr = int(np.fix(centr[0]/gs[0]))
	jcentr = int(np.fix(centr[1]/gs[1]))
	kcentr = int(np.fix(centr[2]/gs[2]))
	iwake = int(icentr + np.fix(1.5*radius/gs[0]))

	icentr_r = int(np.fix((centr[0]-s_min_r[0])/gs_r[0]))
	jcentr_r = int(np.fix((centr[1]-s_min_r[1])/gs_r[1]))
	kcentr_r = int(np.fix((centr[2]-s_min_r[2])/gs_r[2]))
	# iwake_r = int(icentr + np.fix(1.5*radius/gs_r[0]))

	print(icentr_r,jcentr_r,kcentr_r)

	Dn_XY = np.zeros((nc[0],nc[1]))
	Dn_XY[:,:] = np.matrix.transpose(Dn[kcentr,:,:])

	Dn_XZ = np.zeros((nc[0],nc[2]))
	Dn_XZ[:,:] = np.matrix.transpose(Dn[:,jcentr,:])

	Dn_YZ_term = np.zeros((nc[1],nc[2]))
	Dn_YZ_term[:,:] = np.matrix.transpose(Dn[:,:,icentr])

	Dn_YZ_wake = np.zeros((nc[1],nc[2]))
	Dn_YZ_wake[:,:] = np.matrix.transpose(Dn[:,:,iwake])

	Dn_XY_r = np.zeros((nc_r[0],nc_r[1]))
	Dn_XY_r[:,:] = np.matrix.transpose(Dn_r[kcentr_r,:,:])

	Dn_XZ_r = np.zeros((nc_r[0],nc_r[2]))
	Dn_XZ_r[:,:] = np.matrix.transpose(Dn_r[:,jcentr_r,:])

	Dn_YZ_term_r = np.zeros((nc_r[1],nc_r[2]))
	Dn_YZ_term_r[:,:] = np.matrix.transpose(Dn_r[:,:,icentr_r])

	# Dn_YZ_wake_r = np.zeros((nc_r[1],nc_r[2]))
	# Dn_YZ_wake_r[:,:] = np.matrix.transpose(Dn_r[:,:,iwake_r])


	# Dne_1D = np.zeros(nc[0])
	# Dne_1D[:] = Dn[kcentr,jcentr,:]
	# x = np.divide(np.arange(0, nc[0], 1.0) * gs[0]-centr[0], radius)
	# x = ([0:nc(1)-1]*gs(1)-centr(1))./radius;

	
	if zoom == True:
		Xmin = -6.0
		Xmax = 6.0
		Ymin = -6.0
		Ymax = 6.0
		Zmin = -6.0
		Zmax = 6.0
	else:
		Xmin=X_XY[0][0]
		Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1]
		Ymin=Y_XY[0][0]
		Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1]
		Zmin=Z_XZ[0][0]
		Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1]

	fig_size=(7.5,6)
	# -- Dn --
	# **************************************************************************
	plot_mltg(dest_dir,rundate,diagtime,"Density ne log[cm-3]","Dn_ne_mtlg_XY_Ganymede",X_XY,Y_XY,np.log10(Dn_XY),X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],np.log10(Dn_XY_r[:-1,:-1]),min_val,max_val,'X [R_G]','Y [R_G]',fig_size,"inferno",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Density ne log[cm-3]","Dn_ne_mtlg_r_XY_Ganymede",X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],np.log10(Dn_XY_r[:-1,:-1]),min_val,max_val,'X [R_G]','Y [R_G]',(25,6),"inferno")
	plot_mltg(dest_dir,rundate,diagtime,"Density ne log[cm-3]","Dn_ne_mtlg_XZ_Ganymede",X_XZ,Z_XZ,np.log10(Dn_XZ),X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],np.log10(Dn_XZ_r[:-1,:-1]),min_val,max_val,'X [R_G]','Z [R_G]',fig_size,"inferno",zoom,Xmin,Xmax,Zmin,Zmax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Density ne log[cm-3]","Dn_ne_mtlg_r_XZ_Ganymede",X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],np.log10(Dn_XZ_r[:-1,:-1]),min_val,max_val,'X [R_G]','Z [R_G]',(25,6),"inferno")
	plot_mltg(dest_dir,rundate,diagtime,"Density ne log[cm-3]","Dn_ne_mtlg_YZ_terminator_Ganymede",Y_YZ,Z_YZ,np.log10(Dn_YZ_term),Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],np.log10(Dn_YZ_term_r[:-1,:-1]),min_val,max_val,'Y [R_G]','Z [R_G]',fig_size,"inferno",zoom,Ymin,Ymax,Zmin,Zmax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Density ne log[cm-3]","Dn_ne_mtlg_r_YZ_terminator_Ganymede",Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],np.log10(Dn_YZ_term_r[:-1,:-1]),min_val,max_val,'Y [R_G]','Z [R_G]',(7.5,6),"inferno")

	plt.close('all')