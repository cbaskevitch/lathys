######################
# plot_bulk_speed_ganymede.py
# ---------------------
# This routine reads the bulk speed
#  file and plot the module
# and the U components in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

def plot_bulk_speed(src_dir,dest_dir,typefile,rundate,diagtime,zoom):
	ncfile = src_dir + typefile + rundate + "_t" + diagtime + '.nc'

	ncid = Dataset(ncfile)
	var_nc = ncid.variables

	planetname = var_nc['planetname'][:]
	centr      = var_nc['s_centr'][:]
	radius     = var_nc['r_planet'][:]
	gs         = var_nc['gstep'][:]
	nptot      = var_nc['nptot'][:]
	Ux         = var_nc['Ux'][:]
	Uy         = var_nc['Uy'][:]
	Uz         = var_nc['Uz'][:]
	nrm        = var_nc['phys_mag'][:]
	# radius=1.0
	nc = [len(Ux[0][0]), len(Ux[0]), len(Ux)]

	Utot = np.sqrt(Ux*Ux + Uy*Uy + Uz*Uz)

	# maximum and minimum
	if radius > 1.0:
		min_val = [-100.0,-100.0,-100.0] # km/s [Ux,Uy,Uz] for run with planete
		max_val = [100.0,100.0,100.0] # km/s  [Ux,Uy,Uz] for run with planete
		min_valtot = 50.0 # km/s for run with planete
		max_valtot = 100.0 # km/s for run with planete

	else:
		min_val = [135.0,-5.0,-5.0] # km/s [Ux,Uy,Uz]
		max_val = [145.0,5.0,5.0] # km/s  [Ux,Uy,Uz]
		min_valtot = 135.0 # km/s
		max_valtot = 145.0 # km/s

	# -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)

	# planet center in cell number (NB: cell number start at 1
	icentr = int(np.fix(centr[0]/gs[0]))
	jcentr = int(np.fix(centr[1]/gs[1]))
	kcentr = int(np.fix(centr[2]/gs[2]))
	iwake = int(icentr + np.fix(1.5*radius/gs[0]))

	Ux_XY        = np.zeros((nc[0],nc[1]))
	Ux_XY[:,:]   = np.matrix.transpose(Ux[kcentr,:,:])
	Uy_XY        = np.zeros((nc[0],nc[1]))
	Uy_XY[:,:]   = np.matrix.transpose(Uy[kcentr,:,:])
	Uz_XY        = np.zeros((nc[0],nc[1]))
	Uz_XY[:,:]   = np.matrix.transpose(Uz[kcentr,:,:])
	Utot_XY      = np.zeros((nc[0],nc[1]))
	Utot_XY[:,:] = np.matrix.transpose(Utot[kcentr,:,:])

	Ux_XZ        = np.zeros((nc[0],nc[2]))
	Ux_XZ[:,:]   = np.matrix.transpose(Ux[:,jcentr,:])
	Uy_XZ        = np.zeros((nc[0],nc[2]))
	Uy_XZ[:,:]   = np.matrix.transpose(Uy[:,jcentr,:])
	Uz_XZ        = np.zeros((nc[0],nc[2]))
	Uz_XZ[:,:]   = np.matrix.transpose(Uz[:,jcentr,:])
	Utot_XZ      = np.zeros((nc[0],nc[2]))
	Utot_XZ[:,:] = np.matrix.transpose(Utot[:,jcentr,:])

	Ux_YZ_term        = np.zeros((nc[1],nc[2]))
	Ux_YZ_term[:,:]   = np.matrix.transpose(Ux[:,:,icentr])
	Uy_YZ_term        = np.zeros((nc[1],nc[2]))
	Uy_YZ_term[:,:]   = np.matrix.transpose(Uy[:,:,icentr])
	Uz_YZ_term        = np.zeros((nc[1],nc[2]))
	Uz_YZ_term[:,:]   = np.matrix.transpose(Uz[:,:,icentr])
	Utot_YZ_term      = np.zeros((nc[1],nc[2]))
	Utot_YZ_term[:,:] = np.matrix.transpose(Utot[:,:,icentr])

	Ux_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ux_YZ_wake[:,:]   = np.matrix.transpose(Ux[:,:,iwake])
	Uy_YZ_wake        = np.zeros((nc[1],nc[2]))
	Uy_YZ_wake[:,:]   = np.matrix.transpose(Uy[:,:,iwake])
	Uz_YZ_wake        = np.zeros((nc[1],nc[2]))
	Uz_YZ_wake[:,:]   = np.matrix.transpose(Uz[:,:,iwake])
	Utot_YZ_wake      = np.zeros((nc[1],nc[2]))
	Utot_YZ_wake[:,:] = np.matrix.transpose(Utot[:,:,iwake])

	# planet drawing
	theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
	xp = np.cos(theta)
	yp = np.sin(theta)

	if zoom == True:
		Xmin = -4.0
		Xmax = 4.0
		Ymin = -4.0
		Ymax = 4.0
		Zmin = -4.0
		Zmax = 4.0

	# -- Figure 1 & 2 -- Ux
	# **************************************************************************
	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XY, Y_XY, Ux_XY, vmin=min_val[0], vmax=max_val[0], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Ux [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ux_XY_Ganymede_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, Ux_XZ, vmin=min_val[0], vmax=max_val[0], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Ux [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ux_XZ_Ganymede_"+rundate+"_t"+diagtime+".png")

	# -- Figure 3 & 4 -- Uy
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XY, Y_XY, Uy_XY, vmin=min_val[1], vmax=max_val[1], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Uy [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Uy_XY_Ganymede_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, Uy_XZ, vmin=min_val[1], vmax=max_val[1], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Uy [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Uy_XZ_Ganymede_"+rundate+"_t"+diagtime+".png")

	# -- Figure 3 & 4 -- Uz
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XY, Y_XY, Uz_XY, vmin=min_val[2], vmax=max_val[2], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Uz [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Uz_XY_Ganymede_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, Uz_XZ, vmin=min_val[2], vmax=max_val[2], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Uz [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Uz_XZ_Ganymede_"+rundate+"_t"+diagtime+".png")

	# -- Figure 3 & 4 -- Utot
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XY, Y_XY, Utot_XY, vmin=min_valtot, vmax=max_valtot, cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Utot [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Utot_XY_Ganymede_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, Utot_XZ, vmin=min_valtot, vmax=max_valtot, cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Utot [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Utot_XZ_Ganymede_"+rundate+"_t"+diagtime+".png")

	# =========== figure in YZ plane ==============================
	# Ux in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(8,6))
	c = ax.pcolor(Y_YZ,Z_YZ,Ux_YZ_term, vmin=min_val[0], vmax=max_val[0], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Ux [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ux_YZ_terminator_Ganymede_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(8,6))
	c = ax.pcolor(Y_YZ,Z_YZ,Ux_YZ_wake, vmin=min_val[0], vmax=max_val[0], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Ux [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ux_YZ_wake_Ganymede_"+rundate+"_t"+diagtime+".png")

	# Uy in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(8,6))
	c = ax.pcolor(Y_YZ,Z_YZ,Uy_YZ_term, vmin=min_val[1], vmax=max_val[1], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Uy [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Uy_YZ_terminator_Ganymede_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(8,6))
	c = ax.pcolor(Y_YZ,Z_YZ,Uy_YZ_wake, vmin=min_val[1], vmax=max_val[1], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Uy [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Uy_YZ_wake_Ganymede_"+rundate+"_t"+diagtime+".png")

	# Uz in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(8,6))
	c = ax.pcolor(Y_YZ,Z_YZ,Uz_YZ_term, vmin=min_val[2], vmax=max_val[2], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Uz [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Uz_YZ_terminator_Ganymede_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(8,6))
	c = ax.pcolor(Y_YZ,Z_YZ,Uz_YZ_wake, vmin=min_val[2], vmax=max_val[2], cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Uz [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Uz_YZ_wake_Ganymede_"+rundate+"_t"+diagtime+".png")

	# Utot in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(8,6))
	c = ax.pcolor(Y_YZ,Z_YZ,Utot_YZ_term, vmin=min_valtot, vmax=max_valtot, cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Utot [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Utot_YZ_terminator_Ganymede_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=(8,6))
	c = ax.pcolor(Y_YZ,Z_YZ,Utot_YZ_wake, vmin=min_valtot, vmax=max_valtot, cmap="jet",shading='auto')
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Utot [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_G]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_G]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Utot_YZ_wake_Ganymede_"+rundate+"_t"+diagtime+".png")

	plt.close('all')
