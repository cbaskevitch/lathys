######################
# Plot_neutral_density_mars.m
# ---------------------
# This routine reads the atmosphere
#  file and plot the map in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os
from shared_functions import pos_boundary

def plot_neutral_density(dirname,typefile,runname,diagtime):
	ncfile = dirname + typefile + runname + "_" + diagtime + '.nc'

	ncid = Dataset(ncfile)
	var_nc = ncid.variables

	planetname = var_nc['planetname'][:]
	centr      = var_nc['s_centr'][:]
	radius     = var_nc['r_planet'][:]
	gs         = var_nc['gstep'][:]
	nptot      = var_nc['nptot'][:]
	Dn_H         = var_nc['Den_H'][:]
	# Dn_He         = var_nc['Den_He'][:]
	Dn_Na         = var_nc['Den_Na'][:]
	nrm       = var_nc['phys_density'][:]

	nc = [len(Dn_H), len(Dn_H[0]), len(Dn_H[0][0])]

	Dn_H = Dn_H*nrm*1.e-6   # values in cm^-3
	# Dn_He = Dn_He*nrm*1.e-6 # values in cm^-3
	Dn_Na = Dn_Na*nrm*1.e-6 # values in cm^-3

	# maximum and minimum 
	min_val = 2.0 # log(cm-3)
	max_val = 4.0 # log(sm-3)

	# -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)

	# planet center in cell number (NB: cell number start at 1
	icentr = int(np.fix(centr[0]/gs[0]))
	jcentr = int(np.fix(centr[1]/gs[1]))
	kcentr = int(np.fix(centr[2]/gs[2]))

	Dn_H_XY        = np.zeros((nc[0],nc[1]))
	Dn_H_XY[:,:]   = Dn_H[:,:,kcentr]
	# Dn_He_XY        = np.zeros((nc[0],nc[1]))
	# Dn_He_XY[:,:]   = Dn_He[:,:,kcentr]
	Dn_Na_XY        = np.zeros((nc[0],nc[1]))
	Dn_Na_XY[:,:]   = Dn_Na[:,:,kcentr]

	Dn_H_XZ        = np.zeros((nc[0],nc[2]))
	Dn_H_XZ[:,:]   = Dn_H[:,jcentr,:]
	# Dn_He_XZ        = np.zeros((nc[0],nc[2]))
	# Dn_He_XZ[:,:]   = Dn_He[:,jcentr,:]
	Dn_Na_XZ        = np.zeros((nc[0],nc[2]))
	Dn_Na_XZ[:,:]   = Dn_Na[:,jcentr,:]

	Dn_H_YZ        = np.zeros((nc[1],nc[2]))
	Dn_H_YZ[:,:]   = Dn_H[icentr,:,:]
	# Dn_He_YZ        = np.zeros((nc[1],nc[2]))
	# Dn_He_YZ[:,:]   = Dn_He[icentr,:,:]
	Dn_Na_YZ        = np.zeros((nc[1],nc[2]))
	Dn_Na_YZ[:,:]   = Dn_Na[icentr,:,:]

	# planet drawing
	theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
	xp = np.cos(theta)
	yp = np.sin(theta)

	# -- Figure 1 & 2 -- Dn  H
	# **************************************************************************

	fig, ax = plt.subplots(figsize=(6,9))
	c = ax.pcolor(X_XY, Y_XY, np.log10(Dn_H_XY), vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Dn H log[cm-3] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Dn_Hneutral_XY_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots()
	c = ax.pcolor(X_XZ, Z_XZ, np.log10(Dn_H_XZ), vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Dn H log[cm-3] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Dn_Hneutral_XZ_Mercury_"+runname+"_"+diagtime+".png")
