##############################
# Generate_Figure.m
# ----------------------
# This routine generate automatically
# pdf files of different quantities
# in XY and XZ plane
# Quantities are :
#    - Magnetic field (module and components)
#    - Electric field (module and components)
#    - Electron number density
#    - Bulk speed ( module and components)
#    - Ion density (for each species)
#    - Neutral density (for each species)
#   (- Production for each species)
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
##############################

from plot_density_ne_mercury import *
from plot_ion_density_mercury import *
from plot_Bulk_speed_Mercury import *
from plot_magnetic_field_Mercury import *
from plot_electric_field_mercury import *
from plot_neutral_density_mercury import *
import sys, os

dirname = "./"
runname = sys.argv[1]
diagtime = sys.argv[2]

plot_density_ne(dirname,'Thew_',runname,diagtime)
plot_ion_density(dirname,"Read_moment_species_",runname,diagtime)
plot_magnetic_field(dirname,'Magw_',runname,diagtime)
plot_bulk_speed(dirname,'Thew_',runname,diagtime)

# plot_electric_field(dirname,'Elew_',runname,diagtime)
# plot_neutral_density(dirname,'Atmw_',runname,diagtime)