######################
# Plot_electric_field_Mercury.m
# ---------------------
# This routine reads the electric
# field file and plot the module
# and the E components in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os
from shared_functions import pos_boundary

def plot_electric_field(dirname,typefile,runname,diagtime):
	ncfile = dirname + typefile + runname + "_" + diagtime + '.nc'

	ncid = Dataset(ncfile)
	var_nc = ncid.variables

	planetname = var_nc['planetname'][:]
	centr      = var_nc['s_centr'][:]
	radius     = var_nc['r_planet'][:]
	gs         = var_nc['gstep'][:]
	nptot      = var_nc['nptot'][:]
	Ex         = var_nc['Ex'][:]
	Ey         = var_nc['Ey'][:]
	Ez         = var_nc['Ez'][:]
	nrm1       = var_nc['phys_mag'][:]
	nrm2       = var_nc['phys_speed'][:]

	nc = [len(Ex), len(Ex[0]), len(Ex[0][0])]

	Etot = np.sqrt(Ex*Ex + Ey*Ey + Ez*Ez)

	nrm=1.0
	Ex = Ex*nrm  # values in mV/m
	Ey = Ey*nrm  # values in mV/m
	Ez = Ez*nrm  # values in mV/m
	Etot = Etot*nrm; # values in mV/m

	# maximum and minimum 
	min_val    = -2.0 # mV/m
	max_val    = 2.0  # mV/m
	min_valtot = 0.0  # mV/m
	max_valtot = 10.0 # mV/m

	# -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)

	# planet center in cell number (NB: cell number start at 1
	icentr = int(np.fix(centr[0]/gs[0]))
	jcentr = int(np.fix(centr[1]/gs[1]))
	kcentr = int(np.fix(centr[2]/gs[2]))
	iwake = int(icentr + np.fix(1.5*radius/gs[0]))

	Ex_XY        = np.zeros((nc[0],nc[1]))
	Ex_XY[:,:]   = np.matrix.transpose(Ex[:,:,kcentr])
	Ey_XY        = np.zeros((nc[0],nc[1]))
	Ey_XY[:,:]   = np.matrix.transpose(Ey[:,:,kcentr])
	Ez_XY        = np.zeros((nc[0],nc[1]))
	Ez_XY[:,:]   = np.matrix.transpose(Ez[:,:,kcentr])
	Etot_XY      = np.zeros((nc[0],nc[1]))
	Etot_XY[:,:] = np.matrix.transpose(Etot[:,:,kcentr])

	Ex_XZ        = np.zeros((nc[0],nc[2]))
	Ex_XZ[:,:]   = np.matrix.transpose(Ex[:,jcentr,:])
	Ey_XZ        = np.zeros((nc[0],nc[2]))
	Ey_XZ[:,:]   = np.matrix.transpose(Ey[:,jcentr,:])
	Ez_XZ        = np.zeros((nc[0],nc[2]))
	Ez_XZ[:,:]   = np.matrix.transpose(Ez[:,jcentr,:])
	Etot_XZ      = np.zeros((nc[0],nc[2]))
	Etot_XZ[:,:] = np.matrix.transpose(Etot[:,jcentr,:])

	Ex_YZ_term        = np.zeros((nc[1],nc[2]))
	Ex_YZ_term[:,:]   = np.matrix.transpose(Ex[icentr,:,:])
	Ey_YZ_term        = np.zeros((nc[1],nc[2]))
	Ey_YZ_term[:,:]   = np.matrix.transpose(Ey[icentr,:,:])
	Ez_YZ_term        = np.zeros((nc[1],nc[2]))
	Ez_YZ_term[:,:]   = np.matrix.transpose(Ez[icentr,:,:])
	Etot_YZ_term      = np.zeros((nc[1],nc[2]))
	Etot_YZ_term[:,:] = np.matrix.transpose(Etot[icentr,:,:])

	Ex_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ex_YZ_wake[:,:]   = np.matrix.transpose(Ex[iwake,:,:])
	Ey_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ey_YZ_wake[:,:]   = np.matrix.transpose(Ey[iwake,:,:])
	Ez_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ez_YZ_wake[:,:]   = np.matrix.transpose(Ez[iwake,:,:])
	Etot_YZ_wake      = np.zeros((nc[1],nc[2]))
	Etot_YZ_wake[:,:] = np.matrix.transpose(Etot[iwake,:,:])

	Ex_1D    = np.zeros(nc[0])
	Ex_1D[:] = Ex[:,jcentr,kcentr]
	Ey_1D    = np.zeros(nc[0])
	Ey_1D[:] = Ey[:,jcentr,kcentr]
	Ez_1D    = np.zeros(nc[0]);
	Ez_1D[:] = Ez[:,jcentr,kcentr]

	# planet drawing
	theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
	xp = np.cos(theta)
	yp = np.sin(theta)

	# -- Figure 1 & 2 -- Ex
	# **************************************************************************

	fig, ax = plt.subplots(figsize=(6,9))
	c = ax.pcolor(X_XY, Y_XY, Ex_XY, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Ex [mV/m] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Ex_XY_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots()
	c = ax.pcolor(X_XZ, Z_XZ, Ex_XZ, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Ex [mV/m] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Ex_XZ_Mercury_"+runname+"_"+diagtime+".png")

	# -- Figure 3 & 4 -- Ey
	# **************************************************************************

	fig, ax = plt.subplots(figsize=(6,9))
	c = ax.pcolor(X_XY, Y_XY, Ey_XY, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Ey [mV/m] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Ey_XY_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots()
	c = ax.pcolor(X_XZ, Z_XZ, Ey_XZ, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Ey [mV/m] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Ey_XZ_Mercury_"+runname+"_"+diagtime+".png")

	# -- Figure 3 & 4 -- Ez
	# **************************************************************************

	fig, ax = plt.subplots(figsize=(6,9))
	c = ax.pcolor(X_XY, Y_XY, Ez_XY, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Ez [mV/m] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Ez_XY_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots()
	c = ax.pcolor(X_XZ, Z_XZ, Ez_XZ, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Ez [mV/m] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Ez_XZ_Mercury_"+runname+"_"+diagtime+".png")

	# -- Figure 3 & 4 -- Etot
	# **************************************************************************

	fig, ax = plt.subplots(figsize=(6,9))
	c = ax.pcolor(X_XY, Y_XY, Etot_XY, vmin=min_valtot, vmax=max_valtot, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Etot [mV/m] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Etot_XY_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots()
	c = ax.pcolor(X_XZ, Z_XZ, Etot_XZ, vmin=min_valtot, vmax=max_valtot, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Etot [mV/m] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Etot_XZ_Mercury_"+runname+"_"+diagtime+".png")

	# =========== figure in YZ plane ==============================
	# Ex in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=(13,5))
	c = ax.pcolor(Y_YZ,Z_YZ,Ex_YZ_term, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Ex [mV/m] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Ex_YZ_terminator_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(13,5))
	c = ax.pcolor(Y_YZ,Z_YZ,Ex_YZ_wake, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Ex [mV/m] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Ex_YZ_wake_Mercury_"+runname+"_"+diagtime+".png")

	# Ey in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=(13,5))
	c = ax.pcolor(Y_YZ,Z_YZ,Ey_YZ_term, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Ey [mV/m] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Ey_YZ_terminator_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(13,5))
	c = ax.pcolor(Y_YZ,Z_YZ,Ey_YZ_wake, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Ey [mV/m] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Ey_YZ_wake_Mercury_"+runname+"_"+diagtime+".png")

	# Ez in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=(13,5))
	c = ax.pcolor(Y_YZ,Z_YZ,Ez_YZ_term, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Ez [mV/m] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Ez_YZ_terminator_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(13,5))
	c = ax.pcolor(Y_YZ,Z_YZ,Ez_YZ_wake, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Ez [mV/m] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Ez_YZ_wake_Mercury_"+runname+"_"+diagtime+".png")

	# Etot in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=(13,5))
	c = ax.pcolor(Y_YZ,Z_YZ,Etot_YZ_term, vmin=min_valtot, vmax=max_valtot, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Etot [mV/m] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Etot_YZ_terminator_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(13,5))
	c = ax.pcolor(Y_YZ,Z_YZ,Etot_YZ_wake, vmin=min_valtot, vmax=max_valtot, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Etot [mV/m] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Etot_YZ_wake_Mercury_"+runname+"_"+diagtime+".png")

	plt.close('all')