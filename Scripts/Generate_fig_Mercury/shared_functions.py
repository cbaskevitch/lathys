
import math

def pos_boundary(x,x_F,eps,L):
	# y = sqrt((eps^2-1)*(x-x_F)^2-2.*eps*L*(x-x_F)+L^2);
	y = (eps*eps-1)*(x-x_F)*(x-x_F)-2*eps*L*(x-x_F)+L*L
	if y <= 0.0:
		y = float('NaN')
	else:
		y = math.sqrt((eps*eps-1.0)*(x-x_F)*(x-x_F)-2.0*eps*L*(x-x_F)+L*L)
	return y