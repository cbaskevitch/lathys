#######################
# plot_density_ne_Mercury.py
#---------------------
# This routine reads the density
#  file and plot the map in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
#######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os
from shared_functions import pos_boundary

# dirname = "./"
# runname = sys.argv[1]
# diagtime = sys.argv[2]

def plot_density_ne(dirname,typefile,runname,diagtime):
	ncfile = dirname + typefile + runname + "_" + diagtime + '.nc'

	ncid = Dataset(ncfile)
	var_nc = ncid.variables

	planetname = var_nc['planetname'][:]
	centr      = var_nc['s_centr'][:]
	radius     = var_nc['r_planet'][:]
	gs         = var_nc['gstep'][:]
	nptot      = var_nc['nptot'][:]
	Dn         = var_nc['Density'][:]
	nrm        = var_nc['phys_density'][:]
	nrm_len    = var_nc['phys_length'][:]

	nc = [len(Dn[0][0]), len(Dn[0]), len(Dn)]

	Dn = np.where(Dn <= 0, float('NaN'), Dn)

	# maximum and minimum 
	min_val = -1.0 # log(cm-3)
	max_val = 2.5  # log(sm-3)

	# -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)

	# planet center in cell number (NB: cell number start at 1
	icentr = int(np.fix(centr[0]/gs[0]))
	jcentr = int(np.fix(centr[1]/gs[1]))
	kcentr = int(np.fix(centr[2]/gs[2]))
	iwake = int(icentr + np.fix(1.5*radius/gs[0]))

	Dn_XY = np.zeros((nc[0],nc[1]))
	Dn_XY[:,:] = np.matrix.transpose(Dn[kcentr,:,:])

	Dn_XZ = np.zeros((nc[0],nc[2]))
	Dn_XZ[:,:] = np.matrix.transpose(Dn[:,jcentr,:])

	Dn_YZ_term = np.zeros((nc[1],nc[2]))
	Dn_YZ_term[:,:] = np.matrix.transpose(Dn[:,:,icentr])

	Dn_YZ_wake = np.zeros((nc[1],nc[2]))
	Dn_YZ_wake[:,:] = np.matrix.transpose(Dn[:,:,iwake])

	Dne_1D = np.zeros(nc[0])
	Dne_1D[:] = Dn[kcentr,jcentr,:]
	x = np.divide(np.arange(0, nc[0], 1.0) * gs[0]-centr[0], radius)
	# x = ([0:nc(1)-1]*gs(1)-centr(1))./radius;

	# -- Bow shock parameter
	# Extracted from Trotignon et al, PSS, 2006
	xshock = np.divide(np.arange(1, nc[0], 0.1) * gs[0] - centr[0], radius)
	eps = 1.026
	L = 2.081
	x_F = 0.6
	yshock = np.full(len(xshock), float('NaN'))
	for i in range(0,len(xshock)):
		yshock[i] = pos_boundary(xshock[i],x_F,eps,L)

	yshock = np.where(np.isnan(yshock), 0.0, yshock)
	yshock = np.concatenate((yshock, -1.0*yshock))
	xshock = np.concatenate((xshock, xshock))

	# -- IMB parameter
	# Extracted from Vignes et al, GRL, 2000
	ximb = np.divide(np.arange(1, nc[0], 0.1) * gs[0] - centr[0], radius)
	eps = 0.9
	L = 0.96
	x_F = 0.78
	yimb = np.full(len(ximb), float('NaN'))
	for i in range(0,len(ximb)):
		yimb[i] = pos_boundary(ximb[i], x_F, eps, L)

	yimb = np.where(np.isnan(yimb), 0.0, yimb)
	yimb = np.concatenate((yimb, -1.0*yimb))
	ximb = np.concatenate((ximb, ximb))

	# planet drawing
	theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
	xp = np.cos(theta)
	yp = np.sin(theta)

	# -- Figure 1 & 2 -- Dn
	# **************************************************************************

	fig, ax = plt.subplots(figsize=(6,8))
	c = ax.pcolor(X_XY, Y_XY, np.log10(Dn_XY), vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")

	titre = "Density ne log[cm-3] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Dn_ne_XY_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, np.log10(Dn_XZ), vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")

	titre = "Density ne log[cm-3] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Dn_ne_XZ_Mercury_"+runname+"_"+diagtime+".png")

	# =========== figure in YZ plane ==============================
	# Dn in X=0 and X=1.5Rm

	# figure 3 & 4
	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ, Z_YZ, np.log10(Dn_YZ_term), vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")

	titre = "Density ne log[cm-3] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Dn_ne_YZ_terminator_Mercury_"+runname+"_"+diagtime+".png")

	# --
	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ, Z_YZ, np.log10(Dn_YZ_wake), vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")

	titre = "Density ne log[cm-3] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	# plt.show()
	plt.savefig(dirname+"Dn_ne_YZ_wake_Mercury_"+runname+"_"+diagtime+".png")

	plt.close('all')