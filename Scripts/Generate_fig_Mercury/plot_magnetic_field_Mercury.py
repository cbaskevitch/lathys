######################
# Plot_magnetic_field_Mercury.py
# ---------------------
# This routine reads the magnetic
# field file and plot the module
# and the B components in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os
from shared_functions import pos_boundary

# dirname = "./"
# runname = sys.argv[1]
# diagtime = sys.argv[2]

def plot_magnetic_field(dirname,typefile,runname,diagtime):
	ncfile = dirname + typefile + runname + "_" + diagtime + '.nc'

	ncid = Dataset(ncfile)
	var_nc = ncid.variables

	planetname = var_nc['planetname'][:]
	centr      = var_nc['s_centr'][:]
	radius     = var_nc['r_planet'][:]
	gs         = var_nc['gstep'][:]
	nptot      = var_nc['nptot'][:]
	Bx         = var_nc['Bx'][:]
	By         = var_nc['By'][:]
	Bz         = var_nc['Bz'][:]
	nrm        = var_nc['phys_mag'][:]
	nrm_len    = var_nc['phys_length'][:]

	nc = [len(Bx[0][0]), len(Bx[0]), len(Bx)]

	Btot = np.sqrt(Bx*Bx + By*By + Bz*Bz)

	# maximum and minimum 
	min_val    = -50.0 # nT
	max_val    = 50.0  # nT
	min_valtot = 0.0   # nT
	max_valtot = 150.0 # nT

	# -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)

	# planet center in cell number (NB: cell number start at 1
	icentr = int(np.fix(centr[0]/gs[0]))
	jcentr = int(np.fix(centr[1]/gs[1]))
	kcentr = int(np.fix(centr[2]/gs[2]))
	iwake = int(icentr + np.fix(1.5*radius/gs[0]))

	Bx_XY        = np.zeros((nc[0],nc[1]))
	Bx_XY[:,:]   = np.matrix.transpose(Bx[kcentr,:,:])
	By_XY        = np.zeros((nc[0],nc[1]))
	By_XY[:,:]   = np.matrix.transpose(By[kcentr,:,:])
	Bz_XY        = np.zeros((nc[0],nc[1]))
	Bz_XY[:,:]   = np.matrix.transpose(Bz[kcentr,:,:])
	Btot_XY      = np.zeros((nc[0],nc[1]))
	Btot_XY[:,:] = np.matrix.transpose(Btot[kcentr,:,:])

	Bx_XZ        = np.zeros((nc[0],nc[2]))
	Bx_XZ[:,:]   = np.matrix.transpose(Bx[:,jcentr,:])
	By_XZ        = np.zeros((nc[0],nc[2]))
	By_XZ[:,:]   = np.matrix.transpose(By[:,jcentr,:])
	Bz_XZ        = np.zeros((nc[0],nc[2]))
	Bz_XZ[:,:]   = np.matrix.transpose(Bz[:,jcentr,:])
	Btot_XZ      = np.zeros((nc[0],nc[2]))
	Btot_XZ[:,:] = np.matrix.transpose(Btot[:,jcentr,:])

	Bx_YZ_term        = np.zeros((nc[1],nc[2]))
	Bx_YZ_term[:,:]   = np.matrix.transpose(Bx[:,:,icentr])
	By_YZ_term        = np.zeros((nc[1],nc[2]))
	By_YZ_term[:,:]   = np.matrix.transpose(By[:,:,icentr])
	Bz_YZ_term        = np.zeros((nc[1],nc[2]))
	Bz_YZ_term[:,:]   = np.matrix.transpose(Bz[:,:,icentr])
	Btot_YZ_term      = np.zeros((nc[1],nc[2]))
	Btot_YZ_term[:,:] = np.matrix.transpose(Btot[:,:,icentr])

	Bx_YZ_wake        = np.zeros((nc[1],nc[2]))
	Bx_YZ_wake[:,:]   = np.matrix.transpose(Bx[:,:,iwake])
	By_YZ_wake        = np.zeros((nc[1],nc[2]))
	By_YZ_wake[:,:]   = np.matrix.transpose(By[:,:,iwake])
	Bz_YZ_wake        = np.zeros((nc[1],nc[2]))
	Bz_YZ_wake[:,:]   = np.matrix.transpose(Bz[:,:,iwake])
	Btot_YZ_wake      = np.zeros((nc[1],nc[2]))
	Btot_YZ_wake[:,:] = np.matrix.transpose(Btot[:,:,iwake])

	Bx_1D    = np.zeros(nc[0])
	Bx_1D[:] = Bx[kcentr,jcentr,:]
	By_1D    = np.zeros(nc[0])
	By_1D[:] = By[kcentr,jcentr,:]
	Bz_1D    = np.zeros(nc[0]);
	Bz_1D[:] = Bz[kcentr,jcentr,:]
	x = np.divide(np.arange(0, nc[0], 1.0) * gs[0]-centr[0], radius)

	# -- Bow shock parameter
	# Extracted from Trotignon et al, PSS, 2006
	xshock = np.divide(np.arange(1, nc[0], 0.1) * gs[0] - centr[0], radius)
	eps = 1.026
	L = 2.081
	x_F = 0.6
	yshock = np.full(len(xshock), float('NaN'))
	for i in range(0,len(xshock)):
		yshock[i] = pos_boundary(xshock[i],x_F,eps,L)

	yshock = np.where(np.isnan(yshock), 0.0, yshock)
	yshock = np.concatenate((yshock, -1.0*yshock))
	xshock = np.concatenate((xshock, xshock))

	# -- IMB parameter
	# Extracted from Vignes et al, GRL, 2000
	ximb = np.divide(np.arange(1, nc[0], 0.1) * gs[0] - centr[0], radius)
	eps = 0.9
	L = 0.96
	x_F = 0.78
	yimb = np.full(len(ximb), float('NaN'))
	for i in range(0,len(ximb)):
		yimb[i] = pos_boundary(ximb[i], x_F, eps, L)

	yimb = np.where(np.isnan(yimb), 0.0, yimb)
	yimb = np.concatenate((yimb, -1.0*yimb))
	ximb = np.concatenate((ximb, ximb))

	# planet drawing
	theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
	xp = np.cos(theta)
	yp = np.sin(theta)

	Xmin=X_XY[0][0] #float(-1*icentr/rp)
	Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1] #float((nc[0]-icentr)/rp)
	X_norm=np.arange(Xmin, Xmax, (Xmax-Xmin)/(nc[0]))
	Ymin=Y_XY[0][0] #float(-jcentr/rp)
	Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1] #float((nc[1]-jcentr)/rp)
	Y_norm=np.arange(Ymin, Ymax, (Ymax-Ymin)/(nc[1]))
	Zmin=Z_XZ[0][0] #float(-kcentr/rp)
	Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1] #float((nc[1]-kcentr)/rp)
	Z_norm=np.arange(Zmin, Zmax, (Zmax-Zmin)/(nc[2]))

	# -- Figure 1 & 2 -- Bx
	# **************************************************************************

	fig, ax = plt.subplots(figsize=(6,8))
	c = ax.pcolor(X_XY, Y_XY, Bx_XY, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.streamplot(X_norm,Y_norm,np.transpose(Bx_XY),np.transpose(By_XY),color="white")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Bx [nT] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Bx_XY_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, Bx_XZ, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	ax.streamplot(X_norm,Z_norm,np.transpose(Bx_XZ),np.transpose(Bz_XZ),color="white")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Bx [nT] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Bx_XZ_Mercury_"+runname+"_"+diagtime+".png")

	# -- Figure 3 & 4 -- By
	# **************************************************************************

	fig, ax = plt.subplots(figsize=(6,8))
	c = ax.pcolor(X_XY, Y_XY, By_XY, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "By [nT] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"By_XY_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, By_XZ, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "By [nT] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"By_XZ_Mercury_"+runname+"_"+diagtime+".png")

	# -- Figure 3 & 4 -- Bz
	# **************************************************************************

	fig, ax = plt.subplots(figsize=(6,8))
	c = ax.pcolor(X_XY, Y_XY, Bz_XY, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Bz [nT] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Bz_XY_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, Bz_XZ, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Bz [nT] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Bz_XZ_Mercury_"+runname+"_"+diagtime+".png")

	# -- Figure 3 & 4 -- Btot
	# **************************************************************************

	fig, ax = plt.subplots(figsize=(6,8))
	c = ax.pcolor(X_XY, Y_XY, Btot_XY, vmin=min_valtot, vmax=max_valtot, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Btot [nT] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Btot_XY_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, Btot_XZ, vmin=min_valtot, vmax=max_valtot, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Btot [nT] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Btot_XZ_Mercury_"+runname+"_"+diagtime+".png")

	# =========== figure in YZ plane ==============================
	# Bx in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ,Z_YZ,Bx_YZ_term, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Bx [nT] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Bx_YZ_terminator_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ,Z_YZ,Bx_YZ_wake, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Bx [nT] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Bx_YZ_wake_Mercury_"+runname+"_"+diagtime+".png")

	# By in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ,Z_YZ,By_YZ_term, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "By [nT] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"By_YZ_terminator_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ,Z_YZ,By_YZ_wake, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "By [nT] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"By_YZ_wake_Mercury_"+runname+"_"+diagtime+".png")

	# Bz in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ,Z_YZ,Bz_YZ_term, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Bz [nT] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Bz_YZ_terminator_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ,Z_YZ,Bz_YZ_wake, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Bz [nT] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Bz_YZ_wake_Mercury_"+runname+"_"+diagtime+".png")

	# Btot in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ,Z_YZ,Btot_YZ_term, vmin=min_valtot, vmax=max_valtot, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Btot [nT] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Btot_YZ_terminator_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ,Z_YZ,Btot_YZ_wake, vmin=min_valtot, vmax=max_valtot, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Btot [nT] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Btot_YZ_wake_Mercury_"+runname+"_"+diagtime+".png")

	plt.close('all')