######################
# Plot_Bulk_speed_Mercury.m
# ---------------------
# This routine reads the bulk speed
#  file and plot the module
# and the V components in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

# dirname = "./"
# runname = sys.argv[1]
# diagtime = sys.argv[2]

def plot_bulk_speed(dirname,typefile,runname,diagtime):
	ncfile = dirname + typefile + runname + "_" + diagtime + '.nc'

	ncid = Dataset(ncfile)
	var_nc = ncid.variables

	planetname = var_nc['planetname'][:]
	centr      = var_nc['s_centr'][:]
	radius     = var_nc['r_planet'][:]
	gs         = var_nc['gstep'][:]
	nptot      = var_nc['nptot'][:]
	Vx         = var_nc['Ux'][:]
	Vy         = var_nc['Uy'][:]
	Vz         = var_nc['Uz'][:]
	nrm        = var_nc['phys_mag'][:]

	nc = [len(Vx[0][0]), len(Vx[0]), len(Vx)]

	Vtot = np.sqrt(Vx*Vx + Vy*Vy + Vz*Vz)

	# maximum and minimum 
	min_val = -100.0 # km/s
	max_val = 100.0 # km/s
	min_valtot = 0.0 # km/s
	max_valtot = 600.0 # km/s

	# -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)

	# planet center in cell number (NB: cell number start at 1
	icentr = int(np.fix(centr[0]/gs[0]))
	jcentr = int(np.fix(centr[1]/gs[1]))
	kcentr = int(np.fix(centr[2]/gs[2]))
	iwake = int(icentr + np.fix(1.5*radius/gs[0]))

	Vx_XY        = np.zeros((nc[0],nc[1]))
	Vx_XY[:,:]   = np.matrix.transpose(Vx[kcentr,:,:])
	Vy_XY        = np.zeros((nc[0],nc[1]))
	Vy_XY[:,:]   = np.matrix.transpose(Vy[kcentr,:,:])
	Vz_XY        = np.zeros((nc[0],nc[1]))
	Vz_XY[:,:]   = np.matrix.transpose(Vz[kcentr,:,:])
	Vtot_XY      = np.zeros((nc[0],nc[1]))
	Vtot_XY[:,:] = np.matrix.transpose(Vtot[kcentr,:,:])

	Vx_XZ        = np.zeros((nc[0],nc[2]))
	Vx_XZ[:,:]   = np.matrix.transpose(Vx[:,jcentr,:])
	Vy_XZ        = np.zeros((nc[0],nc[2]))
	Vy_XZ[:,:]   = np.matrix.transpose(Vy[:,jcentr,:])
	Vz_XZ        = np.zeros((nc[0],nc[2]))
	Vz_XZ[:,:]   = np.matrix.transpose(Vz[:,jcentr,:])
	Vtot_XZ      = np.zeros((nc[0],nc[2]))
	Vtot_XZ[:,:] = np.matrix.transpose(Vtot[:,jcentr,:])

	Vx_YZ_term        = np.zeros((nc[1],nc[2]))
	Vx_YZ_term[:,:]   = np.matrix.transpose(Vx[:,:,icentr])
	Vy_YZ_term        = np.zeros((nc[1],nc[2]))
	Vy_YZ_term[:,:]   = np.matrix.transpose(Vy[:,:,icentr])
	Vz_YZ_term        = np.zeros((nc[1],nc[2]))
	Vz_YZ_term[:,:]   = np.matrix.transpose(Vz[:,:,icentr])
	Vtot_YZ_term      = np.zeros((nc[1],nc[2]))
	Vtot_YZ_term[:,:] = np.matrix.transpose(Vtot[:,:,icentr])

	Vx_YZ_wake        = np.zeros((nc[1],nc[2]))
	Vx_YZ_wake[:,:]   = np.matrix.transpose(Vx[:,:,iwake])
	Vy_YZ_wake        = np.zeros((nc[1],nc[2]))
	Vy_YZ_wake[:,:]   = np.matrix.transpose(Vy[:,:,iwake])
	Vz_YZ_wake        = np.zeros((nc[1],nc[2]))
	Vz_YZ_wake[:,:]   = np.matrix.transpose(Vz[:,:,iwake])
	Vtot_YZ_wake      = np.zeros((nc[1],nc[2]))
	Vtot_YZ_wake[:,:] = np.matrix.transpose(Vtot[:,:,iwake])

	# planet drawing
	theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
	xp = np.cos(theta)
	yp = np.sin(theta)

	# -- Figure 1 & 2 -- Vx
	# **************************************************************************

	fig, ax = plt.subplots(figsize=(6,8))
	c = ax.pcolor(X_XY, Y_XY, Vx_XY, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Vx [km/s] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Vx_XY_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, Vx_XZ, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Vx [km/s] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Vx_XZ_Mercury_"+runname+"_"+diagtime+".png")

	# -- Figure 3 & 4 -- Vy
	# **************************************************************************

	fig, ax = plt.subplots(figsize=(6,8))
	c = ax.pcolor(X_XY, Y_XY, Vy_XY, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Vy [km/s] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Vy_XY_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, Vy_XZ, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Vy [km/s] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Vy_XZ_Mercury_"+runname+"_"+diagtime+".png")

	# -- Figure 3 & 4 -- Vz
	# **************************************************************************

	fig, ax = plt.subplots(figsize=(6,8))
	c = ax.pcolor(X_XY, Y_XY, Vz_XY, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Vz [km/s] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Vz_XY_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, Vz_XZ, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Vz [km/s] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Vz_XZ_Mercury_"+runname+"_"+diagtime+".png")

	# -- Figure 3 & 4 -- Vtot
	# **************************************************************************

	fig, ax = plt.subplots(figsize=(6,8))
	c = ax.pcolor(X_XY, Y_XY, Vtot_XY, vmin=min_valtot, vmax=max_valtot, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Vtot [km/s] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Vtot_XY_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(6,9.5))
	c = ax.pcolor(X_XZ, Z_XZ, Vtot_XZ, vmin=min_valtot, vmax=max_valtot, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Vtot [km/s] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Vtot_XZ_Mercury_"+runname+"_"+diagtime+".png")

	# =========== figure in YZ plane ==============================
	# Vx in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ,Z_YZ,Vx_YZ_term, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Vx [km/s] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Vx_YZ_terminator_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ,Z_YZ,Vx_YZ_wake, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Vx [km/s] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Vx_YZ_wake_Mercury_"+runname+"_"+diagtime+".png")

	# Vy in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ,Z_YZ,Vy_YZ_term, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Vy [km/s] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Vy_YZ_terminator_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ,Z_YZ,Vy_YZ_wake, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Vy [km/s] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Vy_YZ_wake_Mercury_"+runname+"_"+diagtime+".png")

	# Vz in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ,Z_YZ,Vz_YZ_term, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Vz [km/s] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Vz_YZ_terminator_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ,Z_YZ,Vz_YZ_wake, vmin=min_val, vmax=max_val, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Vz [km/s] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Vz_YZ_wake_Mercury_"+runname+"_"+diagtime+".png")

	# Vtot in X=0 and X=1.5Rm

	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ,Z_YZ,Vtot_YZ_term, vmin=min_valtot, vmax=max_valtot, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Vtot [km/s] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Vtot_YZ_terminator_Mercury_"+runname+"_"+diagtime+".png")

	# --

	fig, ax = plt.subplots(figsize=(7,6.5))
	c = ax.pcolor(Y_YZ,Z_YZ,Vtot_YZ_wake, vmin=min_valtot, vmax=max_valtot, cmap="jet")
	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = "Vtot [km/s] time: "+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_M]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_M]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dirname+"Vtot_YZ_wake_Mercury_"+runname+"_"+diagtime+".png")

	plt.close('all')