from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

### Plot profiles of Bx,By,Bz and Ux,Uy,Uz for 2 simulations on plane x=0, y=0
### @param Magw of simulation 1
### @param Magw of simulation 2
### @param Thew of simulation 1
### @param Thew of simulation 2

ncfile1 = sys.argv[1]#dirname + "Magw_" + runname + "_" + diagtime + '.nc'
ncfile2 = sys.argv[2]

ncid1 = Dataset(ncfile1)
var_nc1 = ncid1.variables
ncid2 = Dataset(ncfile2)
var_nc2 = ncid2.variables

def plot_profile(Cx,Cy,Cz,var_nc1,var_nc2,ylab,y_lim):

	centr1      = var_nc1['s_centr'][:]
	radius     = var_nc1['r_planet'][:]
	gs1         = var_nc1['gstep'][:]
	Cx1         = var_nc1[Cx][:]
	Cy1         = var_nc1[Cy][:]
	Cz1         = var_nc1[Cz][:]

	centr2      = var_nc2['s_centr'][:]
	gs2         = var_nc2['gstep'][:]
	Cx2         = var_nc2[Cx][:]
	Cy2         = var_nc2[Cy][:]
	Cz2         = var_nc2[Cz][:]

	nc1 = [len(Cx1[0][0]), len(Cx1[0]), len(Cx1)]
	nc2 = [len(Cx2[0][0]), len(Cx2[0]), len(Cx2)]

	x = 0.0 #float(sys.argv[2])#4.0
	y = 0.0

	xi1 = 0
	yi1 = 0
	test = (xi1*gs1[0] - (centr1[0]+0.5*gs1[0]))/radius
	while not (x-0.05 < test and test < x+0.05):
		xi1 += 1
		test = (xi1*gs1[0] - (centr1[0]+0.5*gs1[0]))/radius
	test = (yi1*gs1[1] - (centr1[1]+0.5*gs1[1]))/radius
	while not (y-0.05 < test and test < y+0.05):
		yi1 += 1
		test = (yi1*gs1[1] - (centr1[1]+0.5*gs1[1]))/radius
	yi1 += 1
	test = (yi1*gs1[1] - (centr1[1]+0.5*gs1[1]))/radius

	Y1 = np.arange(0,(nc1[2])*gs1[2],gs1[2])
	Y1 = np.divide(Y1, radius) - np.divide((centr1[2]+0.5*gs1[2])*np.ones(nc1[2]), radius)

	xi2 = 0
	yi2 = 0
	test = (xi2*gs2[0] - (centr2[0]+0.5*gs2[0]))/radius
	while not (x-0.05 < test and test < x+0.05):
		xi2 += 1
		test = (xi2*gs2[0] - (centr2[0]+0.5*gs2[0]))/radius
	test = (yi2*gs2[1] - (centr2[1]+0.5*gs2[1]))/radius
	while not (y-0.05 < test and test < y+0.05):
		yi2 += 1
		test = (yi2*gs2[1] - (centr2[1]+0.5*gs2[1]))/radius
	yi2 += 1
	test = (yi2*gs2[1] - (centr2[1]+0.5*gs2[1]))/radius

	Y2 = np.arange(0,(nc2[2])*gs2[2],gs2[2])
	Y2 = np.divide(Y2, radius) - np.divide((centr2[2]+0.5*gs2[2])*np.ones(nc2[2]), radius)


	CXz1 = np.zeros(nc1[2])
	CXz1[:] = Cx1[:,yi1,xi1]
	CYz1 = np.zeros(nc1[2])
	CYz1[:] = Cy1[:,yi1,xi1]
	CZz1 = np.zeros(nc1[2])
	CZz1[:] = Cz1[:,yi1,xi1]

	CXz2 = np.zeros(nc2[2])
	CXz2[:] = Cx2[:,yi2,xi2]
	CYz2 = np.zeros(nc2[2])
	CYz2[:] = Cy2[:,yi2,xi2]
	CZz2 = np.zeros(nc2[2])
	CZz2[:] = Cz2[:,yi2,xi2]

	# fig, ax = plt.subplots(figsize=(8,6))
	plt.plot(Y1[:-1],CXz1[:-1],c="blue",label=Cx+" témoin")
	plt.plot(Y2[:-1],CXz2[:-1],c="red",label=Cx)
	titre = Cx+" profile at x="+str(x)+" and y=0.0"#+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	plt.xlabel('Z [R_G]')#,'fontsize',12,'fontweight','b');
	plt.ylabel(ylab)#,'fontsize',12,'fontweight','b');
	plt.legend(loc="upper right")
	plt.xlim(-6,6)
	plt.ylim(y_lim[0][0],y_lim[0][1])
	# plt.show()
	plt.savefig(Cx+"_profile_at_x_"+str(x)+"_y_"+str(y)+".png")
	plt.close('all')



	plt.plot(Y1[:-1],CYz1[:-1],c="blue",label=Cy+" témoin")
	plt.plot(Y2[:-1],CYz2[:-1],c="red",label=Cy)
	titre = Cy+" profile at x="+str(x)+" and y=0.0"#+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	plt.xlabel('Z [R_G]')#,'fontsize',12,'fontweight','b');
	plt.ylabel(ylab)#,'fontsize',12,'fontweight','b');
	plt.legend(loc="upper right")
	plt.xlim(-6,6)
	plt.ylim(y_lim[1][0],y_lim[1][1])
	# plt.show()
	plt.savefig(Cy+"_profile_at_x_"+str(x)+"_y_"+str(y)+".png")
	plt.close('all')



	plt.plot(Y1[:-1],CZz1[:-1],c="blue",label=Cz+" témoin")
	plt.plot(Y2[:-1],CZz2[:-1],c="red",label=Cz)
	titre = Cz+" profile at x="+str(x)+" and y=0.0"#+diagtime.replace("t","")
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	plt.xlabel('Z [R_G]')#,'fontsize',12,'fontweight','b');
	plt.ylabel(ylab)#,'fontsize',12,'fontweight','b');
	plt.legend(loc="upper right")
	plt.xlim(-6,6)
	plt.ylim(y_lim[2][0],y_lim[2][1])
	# plt.show()
	plt.savefig(Cz+"_profile_at_x_"+str(x)+"_y_"+str(y)+".png")
	plt.close('all')

ncfile1 = sys.argv[1]#dirname + "Magw_" + runname + "_" + diagtime + '.nc'
ncfile2 = sys.argv[2]

ncid1 = Dataset(ncfile1)
var_nc1 = ncid1.variables
ncid2 = Dataset(ncfile2)
var_nc2 = ncid2.variables

y_lim = [[-100,250],[-350,0],[-550,-200]]
plot_profile("Bx","By","Bz",var_nc1,var_nc2,"B [nT]",y_lim)

ncfile1 = sys.argv[3]#dirname + "Thew_" + runname + "_" + diagtime + '.nc'
ncfile2 = sys.argv[4]

ncid1 = Dataset(ncfile1)
var_nc1 = ncid1.variables
ncid2 = Dataset(ncfile2)
var_nc2 = ncid2.variables

y_lim = [[0,150],[-50,100],[-50,100]]
plot_profile("Ux","Uy","Uz",var_nc1,var_nc2,"U [km/s]",y_lim)