from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os


src_dir = "./"
ncfile = sys.argv[1]
ncfile2 = sys.argv[2]


ncid = Dataset(ncfile)
var_nc = ncid.variables
ncid2 = Dataset(ncfile2)
var_nc2 = ncid2.variables

centr      = var_nc['s_centr'][:]
gs         = var_nc['gstep'][:]
Ux         = var_nc['Ux'][:]
Uy         = var_nc['Uy'][:]
Uz         = var_nc['Uz'][:]

Ux2         = var_nc2['Ux'][:]
Uy2         = var_nc2['Uy'][:]
Uz2         = var_nc2['Uz'][:]

radius=1
X = np.divide(np.arange(0,len(Ux[0][0])*gs[0],gs[0]),radius)- np.divide((centr[0]+0.5*gs[0])*np.ones(len(Ux[0][0])), radius)


Ux_mean = []
Uy_mean = []
Uz_mean = []
for x in range(0,len(X)):
	Uxm = np.sum(Ux[:,:,x])
	Uym = np.sum(Uy[:,:,x])
	Uzm = np.sum(Uz[:,:,x])
	Uxm = Uxm / (len(Ux)*len(Ux[0]))
	Uym = Uym / (len(Uy)*len(Uy[0]))
	Uzm = Uzm / (len(Uz)*len(Uz[0]))
	Ux_mean.append(Uxm)
	Uy_mean.append(Uym)
	Uz_mean.append(Uzm)

Ux_mean2 = []
Uy_mean2 = []
Uz_mean2 = []
for x in range(0,len(X)):
	Uxm = np.sum(Ux2[:,:,x])
	Uym = np.sum(Uy2[:,:,x])
	Uzm = np.sum(Uz2[:,:,x])
	Uxm = Uxm / (len(Ux2)*len(Ux2[0]))
	Uym = Uym / (len(Uy2)*len(Uy2[0]))
	Uzm = Uzm / (len(Uz2)*len(Uz2[0]))
	Ux_mean2.append(Uxm)
	Uy_mean2.append(Uym)
	Uz_mean2.append(Uzm)

fig, ax = plt.subplots(figsize=(8,6))
ax.plot(X,Ux_mean,c="blue",label="Ux 23_02_21 t=150")
ax.plot(X,Ux_mean2,c="red",label="Ux 23_02_21 t=300")
ax.set_xlabel('X [R_G]')
ax.set_ylabel('U [nT]')
ax.set_title("Profil de la moyenne de Ux en y et z")
ax.legend(loc="upper right")
plt.savefig("profil_moyenne_Ux_en_YZ.png")

fig, ax = plt.subplots(figsize=(8,6))
ax.plot(X,Uy_mean,c="blue",label="Uy 23_02_21 t=150")
ax.plot(X,Uy_mean2,c="red",label="Uy 23_02_21 t=300")
ax.set_xlabel('X [R_G]')
ax.set_ylabel('U [nT]')
ax.set_title("Profil de la moyenne de Uy en y et z")
ax.legend(loc="upper right")
plt.savefig("profil_moyenne_Uy_en_YZ.png")

fig, ax = plt.subplots(figsize=(8,6))
ax.plot(X,Uz_mean,c="blue",label="Uz 23_02_21 t=150")
ax.plot(X,Uz_mean2,c="red",label="Uz 23_02_21 t=300")
ax.set_xlabel('X [R_G]')
ax.set_ylabel('U [nT]')
ax.set_title("Profil de la moyenne de Uz en y et z")
ax.legend(loc="upper right")
plt.savefig("profil_moyenne_Uz_en_YZ.png")

# plt.show()
plt.close('all')