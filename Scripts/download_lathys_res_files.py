import sys, os

### Download results files of simulation (Read_moment_species, Thew, Magw, Elew and species files)
### @param src_dir : source directory where files are stored
### @param dest_dir : destination directory
### @param date : date of the simualtion
### @param time : time of the simulation

serveur = "cbaskevitch@spirit1.ipsl.fr:"
serveur = "mesopsl:"
# serveur = "cbaskevitch@camelot.ipsl.polytechnique.fr:"

if len(sys.argv) < 5:
	print("python download_lathys_res_files.py <path_to_source_directory> <path_to_dest_directory> dd_mm_yy ttttt")

dir_src = sys.argv[1]
dir_dest = sys.argv[2]
date = sys.argv[3]
time = sys.argv[4]

#Read_moment_species
cmd = "scp "+serveur+dir_src+"/Read_moment_species_"+date+"_t"+time+".dat "+dir_dest
print(cmd)
os.system(cmd)

#Thew
cmd = "scp "+serveur+dir_src+"/Thew_"+date+"_t"+time+".nc "+dir_dest
print(cmd)
os.system(cmd)

#Magw
cmd = "scp "+serveur+dir_src+"/Magw_"+date+"_t"+time+".nc "+dir_dest
print(cmd)
os.system(cmd)

#Elew
cmd = "scp "+serveur+dir_src+"/Elew_"+date+"_t"+time+".nc "+dir_dest
print(cmd)
os.system(cmd)

#Atmw
cmd = "scp "+serveur+dir_src+"/Atmw_"+date+"_t"+time+".nc "+dir_dest
print(cmd)
os.system(cmd)

fid = open(dir_dest+"/Read_moment_species_"+date+"_t"+time+".dat","r")
lines = fid.readlines()
fid.close()

for i in range(2,len(lines)):
	l = lines[i].replace(" ","").replace("\n","")
	cmd = "scp "+serveur+dir_src+"/"+l+" "+dir_dest
	print(cmd)
	os.system(cmd)
