from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st
import math
import sys, os

### Compute and plot histogram of Vx, Vy and Vz distribution for different times. 
### @param src_dir : directory of p3 files
### @param dest_dir : destination directory of the results
### @param date : the date of the simulation
### @param time : the time of the simulation
### @return it returns Vx, Vy, Vz distribution of simulation <date> and <file> in csv files stored in src_dir and it plots histograms of the results

if len(sys.argv) < 5:
    print("py velocity_distribution_histo.py <src_dir> <dest_dir> <date> <time>")

#Largeur à mi-hauteur = 2*vth*sqrt(ln(2))
kB = 1.38*1e-23
mu0 = 4*math.pi*1e-7

planet = "Europa"
print("ENVIRONMENT : ",planet)

if planet == "Europa":
    ### Europa E4 flyby (Rubin, 2015 and Harris 2020)
    m = 16*1.66*1e-27
    va = 5.5e5 #vitesse alfvènique m.s-1
    betas = 0.00513
    n = 20.e6 # m-3
    B = (65.0**2 + 173.0**2 + 412.0**2)**(1/2) * 1e-9 # T
elif planet == "Ganymede":
    ### Ganymede G1-G2 flybys (Jia, 2008)
    m = 16*1.66*1e-27
    va = 3.27e5 #vitesse alfvènique m.s-1
    betas = 0.046
    n = 3.48e6 # m-3
    B = 79e-9*np.sqrt(2.)#(6.0**2 + 77.0**2 + 77.0**2)**(1/2) * 1e-9 # T
elif planet == "SW":
    ### Ganymede G1-G2 flybys (Jia, 2008)
    m = 1*1.66*1e-27
    va = 0.433e5 #vitesse alfvènique m.s-1
    betas = 0.5 
    n = 2.3e6 # m-3
    B = 3.0e-9 # T

T = betas * B**2 / (2*mu0*kB*n) #* 1/1.16e4
vth = (2*kB*T/m)**(1/2)#/va
print("vth = ",vth, " T = ",T)
deltav = vth*0.832554*0.001  #vth * sqrt(ln(2))



src_dir = sys.argv[1]
dest_dir = sys.argv[2]
date = sys.argv[3]
time = sys.argv[4]

ncid = Dataset(src_dir+"/p3_000_"+date+"_t"+time+".nc")
var_nc = ncid.variables

nproc = var_nc["nproc"][:] # number of procs
nptot = var_nc["nptot"][:] # total number of particules of proc 0

qsm = var_nc["particule_char"][:]/var_nc["particule_mass"][:]
ids = np.argwhere(qsm == 1)
vx = np.take(var_nc["particule_vx"][:],ids) # contains all values of vx of all procs
vy = np.take(var_nc["particule_vy"][:],ids) # contains all values of vx of all procs
vz = np.take(var_nc["particule_vz"][:],ids) # contains all values of vx of all procs

# values of V0
vs = []
vs.append(var_nc["vxs"][:][0]*va*0.001)
vs.append(var_nc["vys"][:][0]*va*0.001)
vs.append(var_nc["vzs"][:][0]*va*0.001)
print("v0 = ",vs)


for i in range(1,nproc[0]):
    if i < 10:
        ncid = Dataset(src_dir+"/p3_00"+str(i)+"_"+date+"_t"+time+".nc")
    elif i < 100:
        ncid = Dataset(src_dir+"/p3_0"+str(i)+"_"+date+"_t"+time+".nc")
    else:
        ncid = Dataset(src_dir+"/p3_"+str(i)+"_"+date+"_t"+time+".nc")
    var_nc = ncid.variables
    nptot += var_nc["nptot"][:] # total number of particules of proc i
    qsm = var_nc["particule_char"][:]/var_nc["particule_mass"][:]
    ids = np.argwhere(qsm == 1)
    vx = np.concatenate((vx,np.take(var_nc["particule_vx"][:],ids)),axis=None)
    vy = np.concatenate((vy,np.take(var_nc["particule_vy"][:],ids)),axis=None)
    vz = np.concatenate((vz,np.take(var_nc["particule_vz"][:],ids)),axis=None)

#print(nptot,len(vx))

vx = vx*va*0.001
vy = vy*va*0.001
vz = vz*va*0.001

np.savetxt(src_dir+"/Vx_"+date+"_t"+time+".csv",vx,delimiter=',')
np.savetxt(src_dir+"/Vy_"+date+"_t"+time+".csv",vy,delimiter=',')
np.savetxt(src_dir+"/Vz_"+date+"_t"+time+".csv",vz,delimiter=',')

# Vx
q25, q75 = np.percentile(vx,[.25,.75])
bin_width = 2*(q75 - q25)*len(vx)**(-1/3)
bins = round((vx.max() - vx.min())/bin_width)
print("Vx, Freedman-Diaconis number of bins:", bins)

plt.hist(vx, bins = bins,density=True)#,weights=np.full(len(vx),np.sum(vx)))

mn, mx = plt.xlim()
plt.xlim(mn, mx)
kde_xs = np.linspace(mn, mx, 300)
kde = st.gaussian_kde(vx)
plt.plot(kde_xs, kde.pdf(kde_xs), label="PDF")

xmaxwell = np.linspace(-1.0, 1.0, 300)
xmaxwell = xmaxwell*vs[0]*2#*0.001
ymaxwell = (m/(2*np.pi*kB*T))**(1/2) * np.exp(-1*m*((xmaxwell-vs[0])*1000)**2 / (2*kB*T))*1000
plt.plot(xmaxwell, ymaxwell, color="black",ls="--",label="Maxwell\ndistribution")

ymin, ymax = plt.ylim()
plt.vlines(vs[0], ymin=ymin, ymax=ymax, colors="red", label="V0")


plt.hlines(ymax/2,xmin=vs[0]-deltav,xmax=vs[0]+deltav,color="yellow",label="mid-peak width")

plt.legend(loc="upper left")
plt.ylabel('Probability')
plt.xlabel('Vx [km/s]')
plt.title("Vx distribution "+date+" t="+time)

plt.savefig(dest_dir+"/Vx_distribution_t"+time+".png")
plt.close('all')



# Vy
q25, q75 = np.percentile(vy,[.25,.75])
bin_width = 2*(q75 - q25)*len(vy)**(-1/3)
bins = round((vy.max() - vy.min())/bin_width)
print("Vy, Freedman-Diaconis number of bins:", bins)

plt.hist(vy, bins = bins,density=True)

mn, mx = plt.xlim()
plt.xlim(mn, mx)
kde_xs = np.linspace(mn, mx, 300)
kde = st.gaussian_kde(vy)
plt.plot(kde_xs, kde.pdf(kde_xs), label="PDF")

ymaxwell = (m/(2*math.pi*kB*T))**(1/2) * np.exp(-1*m*((xmaxwell-vs[1])*1000)**2 / (2*kB*T))*1000
plt.plot(xmaxwell, ymaxwell, color="black",ls="--",label="Maxwell\ndistribution")

ymin, ymax = plt.ylim()
plt.vlines(vs[1], ymin=ymin, ymax=ymax, colors="red", label="V0")
plt.hlines(ymax/2,xmin=vs[1]-deltav,xmax=vs[1]+deltav,color="yellow",label="mid-peak width")

plt.legend(loc="upper left")
plt.ylabel('Probability')
plt.xlabel('Vy [km/s]')
plt.title("Vy distribution "+date+" t="+time)

plt.savefig(dest_dir+"/Vy_distribution_t"+time+".png")
plt.close('all')



# Vz
q25, q75 = np.percentile(vz,[.25,.75])
bin_width = 2*(q75 - q25)*len(vz)**(-1/3)
bins = round((vz.max() - vz.min())/bin_width)
print("Vz, Freedman-Diaconis number of bins:", bins)

plt.hist(vz, bins = bins,density=True)

mn, mx = plt.xlim()
plt.xlim(mn, mx)
kde_xs = np.linspace(mn, mx, 300)
kde = st.gaussian_kde(vz)
plt.plot(kde_xs, kde.pdf(kde_xs), label="PDF")

ymaxwell = (m/(2*math.pi*kB*T))**(1/2) * np.exp(-1*m*((xmaxwell-vs[2])*1000)**2 / (2*kB*T))*1000
plt.plot(xmaxwell, ymaxwell, color="black",ls="--",label="Maxwell\ndistribution")

ymin, ymax = plt.ylim()
plt.vlines(vs[2], ymin=ymin, ymax=ymax, colors="red", label="V0")
plt.hlines(ymax/2,xmin=vs[2]-deltav,xmax=vs[2]+deltav,color="yellow",label="mid-peak width")

plt.legend(loc="upper left")
plt.ylabel('Probability')
plt.xlabel('Vz [km/s]')
plt.title("Vz distribution "+date+" t="+time)

plt.savefig(dest_dir+"/Vz_distribution_t"+time+".png")
plt.close('all')