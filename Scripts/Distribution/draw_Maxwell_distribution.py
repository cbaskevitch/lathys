from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st
import math
import sys, os

V = [0.3744675, 0.0, 0.0]
# V = , 0.0, 0.0]
m = 48*1.66*10**(-27)
kB = 1.38*10**(-23)
va = 2.4e5

beta = 0.00548
n = 35.e6 
mu0 = 4*math.pi*10**(-7)
B = (77.6**2 + 140.7**2 + 441.3**2)**(1/2) * 10**(-9)
print("B ",B)
T = beta * B**2 / (2*mu0*kB*n) #* 1/1.16e4

print("V0 ",V[0])
print("T ",T)

vth = (2*kB*T/m)**(1/2)/va
print("vth ",vth*va*0.001)
deltav = vth*0.832554*va*0.001  #vth * sqrt(ln(2))
print("2Delta",2*deltav)

x = np.linspace(-1.0, 1.0, 300)
x = x*va*0.001
xlim = [[0.15,0.6],[-0.25,0.25]]
title=["Maxwell distribution of Vx="+str(V[0]), "Maxwell distribution of Vy and Vz="+str(V[1])]
file=["Maxwell_distribution_Vx.png","Maxwell_distribution_Vy-Vz.png"]


for i in range(0,len(V)-1):
    V[i] = V[i]*va*0.001
    xlim[i][0] = xlim[i][0]*va*0.001
    xlim[i][1] = xlim[i][1]*va*0.001
    y = (m/(2*math.pi*kB*T))**(1/2) * np.exp(-1*m*((x-V[i])*1000)**2 / (2*kB*T))*1000

    plt.plot(x, y, color="red")
    plt.xlim(xlim[i][0],xlim[i][1])
    ymin, ymax = plt.ylim()
    plt.vlines(V[i], ymin=ymin, ymax=ymax, colors="green", label="V0")
    plt.hlines(ymax/2,xmin=V[i]-deltav,xmax=V[i]+deltav,color="blue")
    plt.xlabel("V [km/s]")
    plt.ylabel("P")
    plt.title(title[i])
    # plt.show()
    plt.savefig(file[i])
    plt.close('all')
  