from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st
import math
import sys, os

### Take the files generated by velocity_distribution_histo.py and plot it in one graph also adding the Maxwell distribution function
### @param src_dir : directory of p3 files and csv files produced by velocity_distribution_histo.py
### @param dest_dir : destination directory of the results
### @param date : the date of the simulation
### @return it returns Vx, Vy, Vz distribution of simulation <date> and <file> in csv files stored in src_dir and it plots the results

#Largeur à mi-hauteur = 2*vth*sqrt(ln(2))
kB = 1.38*1e-23
mu0 = 4*math.pi*1e-7

planet = "Europa"
print("ENVIRONMENT : ",planet)

if planet == "Europa":
    ### Europa E4 flyby (Rubin, 2015 and Harris 2020)
    m = 16*1.66*1e-27
    va = 5.5e5 #vitesse alfvènique m.s-1
    betas = 0.00513
    n = 20.e6 
    B = (65.0**2 + 173.0**2 + 412.0**2)**(1/2) * 1e-9
elif planet == "Ganymede":
    ### Ganymede G1-G2 flybys (Jia, 2008)
    m = 16*1.66*1e-27
    va = 3.27e5 #vitesse alfvènique m.s-1
    betas = 0.046
    n = 3.48e6 
    B = (6.0**2 + 77.0**2 + 77.0**2)**(1/2) * 1e-9


T = betas * B**2 / (2*mu0*kB*n) #* 1/1.16e4
vth = (2*kB*T/m)**(1/2)#/va
print("vth = ",vth)
deltav = vth*0.832554*0.001  #vth * sqrt(ln(2))

print("T=",T," K =",T*1.16e-4," eV\nB=",B*1e9," nT\nvth=",vth*va*0.001," km/s")

xmaxwell = np.linspace(-1.0, 1.0, 300)
xmaxwell = xmaxwell*va*0.001

src_dir = sys.argv[1]
dest_dir = sys.argv[2]
date = sys.argv[3]

files = ["Vx","Vy","Vz"]
times = ["00000","00050","00100"]#["00000","00010","00050","00150","00300"]
col = ["orange","turquoise", "magenta"]#["yellow","orange", "forestgreen", "turquoise", "blueviolet", "magenta"]

ncid = Dataset(src_dir+"/p3_000_"+date+"_t00000.nc")
var_nc = ncid.variables

vs = []
vs.append(var_nc["vxs"][:][0]*va*0.001)
vs.append(var_nc["vys"][:][0]*va*0.001)
vs.append(var_nc["vzs"][:][0]*va*0.001)


for j in range(0,len(files)):
    for i in range(0,len(times)):
        v = np.loadtxt(src_dir+"/"+files[j]+"_"+date+"_t"+times[i]+".csv", delimiter=',')

        mn = np.min(v) 
        mx = np.max(v)
        plt.xlim(mn, mx)
        kde_xs = np.linspace(mn, mx, 300)
        kde = st.gaussian_kde(v)
        plt.plot(kde_xs, kde.pdf(kde_xs), label="t="+times[i], color=col[i])

    ymaxwell = (m/(2*math.pi*kB*T))**(1/2) * np.exp(-1*m*((xmaxwell-vs[j])*1000)**2 / (2*kB*T))*1000
    plt.plot(xmaxwell, ymaxwell, color="black",ls="--",label="Maxwell\ndistribution")

    ymin, ymax = plt.ylim()
    plt.vlines(vs[j], ymin=ymin, ymax=ymax, colors="red", label="V0")
    plt.hlines(ymax/2,xmin=vs[j]-deltav,xmax=vs[j]+deltav,color="green",label="mid-peak width")
    plt.legend(loc="upper right")
    plt.ylabel('Probability')
    plt.xlabel(files[j]+" [km/s]")
    plt.title(files[j]+" distribution "+date)
    # plt.show()
    plt.savefig(dest_dir+"/all_"+files[j]+"_distribution_curves_"+date+".png")
    plt.close('all')
    
