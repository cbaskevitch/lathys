from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
from numpy.lib.function_base import append
import scipy.stats as st
import math
import sys, os

### Compute and plot histogram of Vx, Vy and Vz distribution for different times. 
### @param src_dir : directory of p3 files
### @param dest_dir : destination directory of the results
### @param date : the date of the simulation
### @param time : the time of the simulation
### @return it returns Vx, Vy, Vz distribution of simulation <date> and <file> in csv files stored in src_dir and it plots histograms of the results

if len(sys.argv) < 5:
    print("py velocity_distribution_histo.py <src_dir> <dest_dir> <date> <time>")

#Largeur à mi-hauteur = 2*vth*sqrt(ln(2))
m = 48*1.66*10**(-27)
kB = 1.38*10**(-23)
va = 2.4e5 #vitesse alfvènique
beta = 0.00548
n = 35.e6 
mu0 = 4*math.pi*10**(-7)
B = (77.6**2 + 140.7**2 + 441.3**2)**(1/2) * 10**(-9)
T = beta * B**2 / (2*mu0*kB*n) #* 1/1.16e4
vth = (2*kB*T/m)**(1/2)#/va
print(vth)
deltav = vth*0.832554*0.001  #vth * sqrt(ln(2))

xmaxwell = np.linspace(-1.0, 1.0, 300)
xmaxwell = xmaxwell*va*0.001

src_dir = sys.argv[1]
dest_dir = sys.argv[2]
date = sys.argv[3]
time = sys.argv[4]

ncid = Dataset(src_dir+"/p3_000_"+date+"_t"+time+".nc")
var_nc = ncid.variables

nproc = var_nc["nproc"][:] # number of procs
nptot = var_nc["nptot"][:] # total number of particules of proc 0

pvx = var_nc["particule_vx"][:] # contains all values of vx of all procs
pvy = var_nc["particule_vy"][:] # contains all values of vy of all procs
pvz = var_nc["particule_vz"][:] # contains all values of vz of all procs
ppx = var_nc["particule_x"][:] # contains all values of x of all procs
ppy = var_nc["particule_y"][:] # contains all values of y of all procs
ppz = var_nc["particule_z"][:] # contains all values of z of all procs
gstep = var_nc["gstep"][:]
radius = var_nc["r_planet"][:]
s_min = [0,0,0] 
s_max = var_nc["nc_tot"][:]
zoneYmax = [s_max[1]*gstep[0]/radius - 10*gstep[0]/radius, s_max[1]*gstep[0]/radius]
zoneYmin = [s_min[1]*gstep[0]/radius, s_min[1]*gstep[0]/radius + 10*gstep[0]/radius]
zoneZmax = [s_max[2]*gstep[0]/radius - 10*gstep[0]/radius, s_max[2]*gstep[0]/radius]
zoneZmin = [s_min[2]*gstep[0]/radius, s_min[2]*gstep[0]/radius + 10*gstep[0]/radius]
nptot = var_nc["nptot"][:]
print(s_max)
print (zoneYmin[0],zoneYmin[1])
print (zoneYmax[0],zoneYmax[1])
print (zoneZmin[0],zoneZmin[1])
print (zoneZmax[0],zoneZmax[1])
vx=[]
vy=[]
vz=[]


py=[]
pz=[]

# for i in range(0,len(pvx)):
#     y = ppy[i]*gstep[1]/radius
#     z = ppz[i]*gstep[0]/radius
    
#     if y < zoneYmin[1] or y > zoneYmax[0]:
#         vx.append(pvx[i])
#         vy.append(pvy[i])
#         vz.append(pvz[i])
#         py.append(y)
#         pz.append(z)
#     elif z < zoneZmin[1] or z > zoneZmax[0]:
#         vx.append(pvx[i])
#         vy.append(pvy[i])
#         vz.append(pvz[i])
#         py.append(y)
#         pz.append(z)

# values of V0
vs = []
vs.append(var_nc["vxs"][:]*va*0.001)
vs.append(var_nc["vys"][:]*va*0.001)
vs.append(var_nc["vzs"][:]*va*0.001)

print(nproc,nproc[0])



for i in range(0,nproc[0]):
    if i < 10:
        ncid = Dataset(src_dir+"/p3_00"+str(i)+"_"+date+"_t"+time+".nc")
    elif i < 100:
        ncid = Dataset(src_dir+"/p3_0"+str(i)+"_"+date+"_t"+time+".nc")
    else:
        ncid = Dataset(src_dir+"/p3_"+str(i)+"_"+date+"_t"+time+".nc")
    var_nc = ncid.variables
    nptot += var_nc["nptot"][:] # total number of particules of proc i
    pvx = var_nc["particule_vx"][:]
    pvy = var_nc["particule_vy"][:]
    ppx = var_nc["particule_x"][:]
    ppy = var_nc["particule_y"][:]
    pvz = var_nc["particule_vz"][:]
    ppz = var_nc["particule_z"][:]
    for j in range(0,len(pvx)):
        y = ppy[j]/radius
        z = ppz[j]/radius
        # print(i,j,y,z)
        if y < zoneYmin[1] or y > zoneYmax[0]:
            vx.append(pvx[j])
            vy.append(pvy[j])
            vz.append(pvz[j])
            py.append(y)
            pz.append(z)
        elif z < zoneZmin[1] or z > zoneZmax[0]:
            vx.append(pvx[j])
            vy.append(pvy[j])
            vz.append(pvz[j])
            py.append(y)
            pz.append(z)
    # np.concatenate((vx,var_nc["particule_vx"][:]),axis=None)
    # np.concatenate((vy,var_nc["particule_vy"][:]),axis=None)
    # np.concatenate((vz,var_nc["particule_vz"][:]),axis=None)
# sys.exit()
vx = np.array(vx)
vy = np.array(vy)
vz = np.array(vz)
vx = vx*va*0.001
vy = vy*va*0.001
vz = vz*va*0.001

np.savetxt(src_dir+"/Vx_"+date+"_t"+time+".csv",vx,delimiter=',')
np.savetxt(src_dir+"/Vy_"+date+"_t"+time+".csv",vy,delimiter=',')
np.savetxt(src_dir+"/Vz_"+date+"_t"+time+".csv",vz,delimiter=',')

# Vx
q25, q75 = np.percentile(vx,[.25,.75])
bin_width = 2*(q75 - q25)*len(vx)**(-1/3)
bins = round((vx.max() - vx.min())/bin_width)
print("Freedman-Diaconis number of bins:", bins)

plt.hist(vx, bins = bins,density=True)#,weights=np.full(len(vx),np.sum(vx)))

mn, mx = plt.xlim()
# plt.xlim(mn, mx)
plt.xlim(0, 200)
kde_xs = np.linspace(mn, mx, 300)
kde = st.gaussian_kde(vx)
plt.plot(kde_xs, kde.pdf(kde_xs), label="PDF")

ymaxwell = (m/(2*math.pi*kB*T))**(1/2) * np.exp(-1*m*((xmaxwell-vs[0])*1000)**2 / (2*kB*T))*1000
plt.plot(xmaxwell, ymaxwell, color="black",ls="--",label="Maxwell\ndistribution")

# ymin, ymax = plt.ylim()
ymin = 0
ymax = np.amax(ymaxwell)
plt.vlines(vs[0], ymin=ymin, ymax=ymax, colors="red", label="V0")


plt.hlines(np.amax(ymaxwell)/2,xmin=vs[0]-deltav,xmax=vs[0]+deltav,color="yellow",label="mid-peak width")

plt.legend(loc="upper left")
plt.ylabel('Number of particles')
plt.xlabel('Vx [km/s]')
plt.title("Vx distribution "+date+" t="+time)
# plt.show()
plt.savefig(dest_dir+"/Vx_distribution_zone_tampon_t"+time+".png")
plt.close('all')



# Vy
q25, q75 = np.percentile(vy,[.25,.75])
bin_width = 2*(q75 - q25)*len(vy)**(-1/3)
bins = round((vy.max() - vy.min())/bin_width)
print("Freedman-Diaconis number of bins:", bins)

plt.hist(vy, bins = bins,density=True)

mn, mx = plt.xlim()
# plt.xlim(mn, mx)
plt.xlim(-100, 100)
# plt.ylim(0, 0.05)
kde_xs = np.linspace(mn, mx, 300)
kde = st.gaussian_kde(vy)
plt.plot(kde_xs, kde.pdf(kde_xs), label="PDF")

ymaxwell = (m/(2*math.pi*kB*T))**(1/2) * np.exp(-1*m*((xmaxwell-vs[1])*1000)**2 / (2*kB*T))*1000
plt.plot(xmaxwell, ymaxwell, color="black",ls="--",label="Maxwell\ndistribution")

# ymin, ymax = plt.ylim()
ymin = 0
ymax = np.amax(ymaxwell)
plt.vlines(vs[1], ymin=ymin, ymax=ymax, colors="red", label="V0")
plt.hlines(ymax/2,xmin=vs[1]-deltav,xmax=vs[1]+deltav,color="yellow",label="mid-peak width")

plt.legend(loc="upper left")
plt.ylabel('Number of particles')
plt.xlabel('Vy [km/s]')
plt.title("Vy distribution "+date+" t="+time)
# plt.show()
plt.savefig(dest_dir+"/Vy_distribution_zone_tampon_t"+time+".png")
plt.close('all')



# Vz
q25, q75 = np.percentile(vz,[.25,.75])
bin_width = 2*(q75 - q25)*len(vz)**(-1/3)
bins = round((vz.max() - vz.min())/bin_width)
print("Freedman-Diaconis number of bins:", bins)

plt.hist(vz, bins = bins,density=True)

mn, mx = plt.xlim()
# plt.xlim(mn, mx)
plt.xlim(-100, 100)
# plt.ylim(0, 0.05)
kde_xs = np.linspace(mn, mx, 300)
kde = st.gaussian_kde(vz)
plt.plot(kde_xs, kde.pdf(kde_xs), label="PDF")

ymaxwell = (m/(2*math.pi*kB*T))**(1/2) * np.exp(-1*m*((xmaxwell-vs[2])*1000)**2 / (2*kB*T))*1000
plt.plot(xmaxwell, ymaxwell, color="black",ls="--",label="Maxwell\ndistribution")

# ymin, ymax = plt.ylim()
ymin = 0
ymax = np.amax(ymaxwell)
plt.vlines(vs[2], ymin=ymin, ymax=ymax, colors="red", label="V0")
plt.hlines(ymax/2,xmin=vs[2]-deltav,xmax=vs[2]+deltav,color="yellow",label="mid-peak width")

plt.legend(loc="upper left")
plt.ylabel('Number of particles')
plt.xlabel('Vz [km/s]')
plt.title("Vz distribution "+date+" t="+time)
# plt.show()
plt.savefig(dest_dir+"/Vz_distribution_zone_tampon_t"+time+".png")
plt.close('all')

plt.plot(py,pz,".")
plt.xlim(zoneYmin[0],zoneYmax[1])
plt.ylim(zoneZmin[0],zoneZmax[1])
plt.hlines(zoneZmax[0],zoneYmin[0],zoneYmax[1],color="green")
plt.hlines(zoneZmin[1],zoneYmin[0],zoneYmax[1],color="green")
plt.vlines(zoneYmin[1],zoneZmin[0],zoneZmax[1],color="green")
plt.vlines(zoneYmax[0],zoneZmin[0],zoneZmax[1],color="green")
plt.show()