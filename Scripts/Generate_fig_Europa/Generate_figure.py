##############################
# Generate_Figure.py
# ----------------------
# This routine generate automatically
# pdf files of different quantities
# in XY and XZ plane
# Quantities are :
#    - Magnetic field (module and components)
#    - Electric field (module and components)
#    - Electron number density
#    - Bulk speed ( module and components)
#    - Ion density (for each species)
#    - Neutral density (for each species)
#   (- Production for each species)
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
##############################

from plot_density_ne_europa import *
from plot_temperature_europa import *
from plot_ion_density_europa import *
from plot_bulk_speed_europa import *
from plot_magnetic_field_europa import *
from plot_electric_field_europa import *
from plot_resistivity_europa import *
from plot_neutral_density import *
import sys, os

if len(sys.argv) < 5:
	print("python Generate_Figure.py <path_to_source_directory> <path_to_destination_directory> <date_of_simulation> <time_of_simulation>")

src_dir = sys.argv[1]+"/"
dest_dir = sys.argv[2]+"/"
rundate = sys.argv[3]
diagtime = sys.argv[4]

buffer_zone=False
zoom = False
if zoom == False:
	field_lines_dens = 2
else:
	field_lines_dens = 2
	buffer_zone=False

print("Electron density")
plot_density_ne(src_dir,dest_dir,'Thew_',rundate,diagtime,zoom,buffer_zone)
# plot_density_ne(src_dir,dest_dir,'Ojv_',rundate,diagtime,zoom,buffer_zone)
# print("Electronic temperature")
# plot_temperature(src_dir,dest_dir,'O2pl_',rundate,diagtime,True,buffer_zone)
# print("Resistivity")
# plot_resistivity(src_dir,dest_dir,'Thew_',rundate,diagtime,True,buffer_zone)
print("Ion density")
plot_ion_density(src_dir,dest_dir,"Read_moment_species_",rundate,diagtime,zoom,buffer_zone)
print("Magnetic field")
plot_magnetic_field(src_dir,dest_dir,'Magw_',rundate,diagtime,zoom,field_lines_dens,buffer_zone)
plot_magnetic_field(src_dir,dest_dir,'Magw_',rundate,diagtime,zoom,field_lines_dens,buffer_zone)
print("Bulk speed")
plot_bulk_speed(src_dir,dest_dir,'Thew_',rundate,diagtime,zoom,field_lines_dens,buffer_zone)
print("Electric field")
plot_electric_field(src_dir,dest_dir,'Elew_',rundate,diagtime,zoom,field_lines_dens,buffer_zone)
plot_neutral_density(src_dir,dest_dir,'Atmw_',rundate,diagtime,True,False)
