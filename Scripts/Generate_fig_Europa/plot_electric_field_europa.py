######################
# Plot_electric_field_europa.py
# ---------------------
# This routine reads the electric
# field file and plot the module
# and the E components in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os


def plot_electric_field(src_dir,dest_dir,typefile,rundate,diagtime,zoom,field_lines_dens,buffer_zone):
	ncfile = src_dir + typefile + rundate + "_t" + diagtime + ".nc" #'_extract_4Re_grid.nc'

	ncid = Dataset(ncfile)
	var_nc = ncid.variables

	planetname = var_nc['planetname'][:]
	centr      = var_nc['s_centr'][:]
	radius     = var_nc['r_planet'][:]
	gs         = var_nc['gstep'][:]
	nptot      = var_nc['nptot'][:]
	Ex         = var_nc['Ex'][:]
	Ey         = var_nc['Ey'][:]
	Ez         = var_nc['Ez'][:]
	nrm        = var_nc['phys_mag'][:]
	nrm_len    = var_nc['phys_length'][:]
	# radius=1
	nc = [len(Ex[0][0]), len(Ex[0]), len(Ex)]
	print("nc = ",nc[0],nc[1],nc[2])

	Etot = np.sqrt(Ex*Ex + Ey*Ey + Ez*Ez)
	# maximum and minimum 
	if radius > 1.0:
		min_val    = [-15.0,-60.0,0.0] # mV/m [Ex,Ey,Ez]
		max_val    = [15.0,0.0,20.0]  # mV/m [Ex,Ey,Ez]
		min_valtot = 1.55   # mV/m
		max_valtot = 1.65 # mV/m
	else:
		min_val    = [-0.07,-38.1,12.05] # mV/m [Ex,Ey,Ez] for run without planete
		max_val    = [0,-37.9,12.2]  # mV/m [Ex,Ey,Ez] for run without planete
		min_valtot = 1.55   # mV/m for run without planete
		max_valtot = 1.65 # mV/m for run without planete

	# -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide(centr[0]*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide(centr[1]*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide(centr[0]*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide(centr[2]*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide(centr[1]*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide(centr[2]*np.ones((nc[1],nc[2])), radius)

	# planet center in cell number (NE: cell number start at 0
	icentr = int(np.fix(centr[0]/gs[0]))-1
	jcentr = int(np.fix(centr[1]/gs[1]))-1
	kcentr = int(np.fix(centr[2]/gs[2]))-1
	iwake = int(icentr + np.fix(1.5*radius/gs[0]))
	# rp = radius/gs[0]

	Ex_XY        = np.zeros((nc[0],nc[1]))
	Ex_XY[:,:]   = np.matrix.transpose(Ex[kcentr,:,:])
	Ey_XY        = np.zeros((nc[0],nc[1]))
	Ey_XY[:,:]   = np.matrix.transpose(Ey[kcentr,:,:])
	Ez_XY        = np.zeros((nc[0],nc[1]))
	Ez_XY[:,:]   = np.matrix.transpose(Ez[kcentr,:,:])
	Etot_XY      = np.zeros((nc[0],nc[1]))
	Etot_XY[:,:] = np.matrix.transpose(Etot[kcentr,:,:])

	Ex_XZ        = np.zeros((nc[0],nc[2]))
	Ex_XZ[:,:]   = np.matrix.transpose(Ex[:,jcentr,:])
	Ey_XZ        = np.zeros((nc[0],nc[2]))
	Ey_XZ[:,:]   = np.matrix.transpose(Ey[:,jcentr,:])
	Ez_XZ        = np.zeros((nc[0],nc[2]))
	Ez_XZ[:,:]   = np.matrix.transpose(Ez[:,jcentr,:])
	Etot_XZ      = np.zeros((nc[0],nc[2]))
	Etot_XZ[:,:] = np.matrix.transpose(Etot[:,jcentr,:])

	Ex_YZ_term        = np.zeros((nc[1],nc[2]))
	Ex_YZ_term[:,:]   = np.matrix.transpose(Ex[:,:,icentr])
	Ey_YZ_term        = np.zeros((nc[1],nc[2]))
	Ey_YZ_term[:,:]   = np.matrix.transpose(Ey[:,:,icentr])
	Ez_YZ_term        = np.zeros((nc[1],nc[2]))
	Ez_YZ_term[:,:]   = np.matrix.transpose(Ez[:,:,icentr])
	Etot_YZ_term      = np.zeros((nc[1],nc[2]))
	Etot_YZ_term[:,:] = np.matrix.transpose(Etot[:,:,icentr])

	Ex_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ex_YZ_wake[:,:]   = np.matrix.transpose(Ex[:,:,iwake])
	Ey_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ey_YZ_wake[:,:]   = np.matrix.transpose(Ey[:,:,iwake])
	Ez_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ez_YZ_wake[:,:]   = np.matrix.transpose(Ez[:,:,iwake])
	Etot_YZ_wake      = np.zeros((nc[1],nc[2]))
	Etot_YZ_wake[:,:] = np.matrix.transpose(Etot[:,:,iwake])

	# Ex_1D    = np.zeros(nc[0])
	# Ex_1D[:] = Ex[kcentr,jcentr,:]
	# Ey_1D    = np.zeros(nc[0])
	# Ey_1D[:] = Ey[kcentr,jcentr,:]
	# Ez_1D    = np.zeros(nc[0])
	# Ez_1D[:] = Ez[kcentr,jcentr,:]
	# x = np.divide(np.arange(0, nc[0], 1.0) * gs[0]-centr[0], radius)

	# planet drawing
	theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
	xp = np.cos(theta)
	yp = np.sin(theta)

	## Field lines
	Xmin=X_XY[0][0] #float(-1*icentr/rp)
	Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1] #float((nc[0]-icentr)/rp)
	# X_norm=np.arange(Xmin, Xmax, (Xmax-Xmin)/(nc[0]))
	X_norm = np.linspace(0 , (nc[0]-1) * gs[0] , num=nc[0]) / radius - centr[0]/radius
	Ymin=Y_XY[0][0] #float(-jcentr/rp)
	Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1] #float((nc[1]-jcentr)/rp)
	# Y_norm=np.arange(Ymin, Ymax, (Ymax-Ymin)/(nc[1]))
	Y_norm = np.linspace(0 , (nc[1]-1) * gs[1] , num=nc[1])/ radius - centr[1]/radius
	Zmin=Z_XZ[0][0] #float(-kcentr/rp)
	Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1] #float((nc[1]-kcentr)/rp)
	# Z_norm=np.arange(Zmin, Zmax, (Zmax-Zmin)/(nc[2]))
	Z_norm = np.linspace(0 , (nc[2]-1) * gs[2] , num=nc[2])/ radius - centr[2]/radius

	fig_size = [[6,9.5],[6,7.5],[8,6],[10,6]] #differentes tailles de fenetres
	figsize_Xnum = 1  #numero de la taille de la fenetre pour les plans XZ et XY
	figsize_Ynum = 2  #numero de la taille de la fenetre pour les plans YZ

	colmap = ["viridis","inferno","GnBu_r"]
	ncol = 0  # numero de la colormap


	if zoom == True:
		Xmin = -5.6
		Xmax = 2.9
		Ymin = -6.5
		Ymax = 6.5
		Zmin = -6.5
		Zmax = 6.5

	print("Xmin",Xmin,"Xmax",Xmax)
	print("Ymin",Ymin,"Ymax",Ymax)
	print("Zmin",Zmin,"Zmax",Zmax)

	print(len(X_norm),len(Y_norm))
	print(np.size(Ex_XY),len(Ex_XY[0]),len(Ey_XY),len(Ey_XY[0]))
	# -- Figure 1 & 2 -- Ex
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, Ex_XY, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.streamplot(X_norm,Y_norm,np.transpose(Ex_XY),np.transpose(Ey_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Ex [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	# plt.show()
	plt.savefig(dest_dir+"Ex_XY_Europa_"+rundate+"_t"+diagtime+".png")
	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, Ex_XZ, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.streamplot(X_norm,Z_norm,np.transpose(Ex_XZ),np.transpose(Ez_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ex [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ex_XZ_Europa_"+rundate+"_t"+diagtime+".png")
	# plt.show()

	# -- Figure 3 & 4 -- Ey
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, Ey_XY, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.streamplot(X_norm,Y_norm,np.transpose(Ex_XY),np.transpose(Ey_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Ey [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ey_XY_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, Ey_XZ, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.streamplot(X_norm,Z_norm,np.transpose(Ex_XZ),np.transpose(Ez_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ey [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ey_XZ_Europa_"+rundate+"_t"+diagtime+".png")

	# -- Figure 3 & 4 -- Ez
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, Ez_XY, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.streamplot(X_norm,Y_norm,np.transpose(Ex_XY),np.transpose(Ey_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Ez [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ez_XY_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, Ez_XZ, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.streamplot(X_norm,Z_norm,np.transpose(Ex_XZ),np.transpose(Ez_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ez [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ez_XZ_Europa_"+rundate+"_t"+diagtime+".png")

	# -- Figure 3 & 4 -- Etot
	# **************************************************************************
	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, np.log10(Etot_XY), vmin=min_valtot, vmax=max_valtot, cmap=colmap[0],shading='auto')
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Etot [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Etot_XY_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, np.log10(Etot_XZ), vmin=min_valtot, vmax=max_valtot, cmap=colmap[0],shading='auto')
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Etot [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Etot_XZ_Europa_"+rundate+"_t"+diagtime+".png")

	# =========== figure in YZ plane ==============================
	# Ex in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Ex_YZ_term, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.streamplot(Y_norm,Z_norm,np.transpose(Ey_YZ_term),np.transpose(Ez_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ex [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ex_YZ_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Ex_YZ_wake, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.streamplot(Y_norm,Z_norm,np.transpose(Ey_YZ_wake),np.transpose(Ez_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ex [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ex_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

	# Ey in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Ey_YZ_term, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.streamplot(Y_norm,Z_norm,np.transpose(Ey_YZ_term),np.transpose(Ez_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ey [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ey_YZ_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Ey_YZ_wake, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.streamplot(Y_norm,Z_norm,np.transpose(Ey_YZ_wake),np.transpose(Ez_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ey [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ey_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

	# Ez in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Ez_YZ_term, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.streamplot(Y_norm,Z_norm,np.transpose(Ey_YZ_term),np.transpose(Ez_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ez [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ez_YZ_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Ez_YZ_wake, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	ax.streamplot(Y_norm,Z_norm,np.transpose(Ey_YZ_wake),np.transpose(Ez_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ez [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ez_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

	# Etot in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,np.log10(Etot_YZ_term), vmin=min_valtot, vmax=max_valtot, cmap=colmap[0],shading='auto')
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Etot [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Etot_YZ_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,np.log10(Etot_YZ_wake), vmin=min_valtot, vmax=max_valtot, cmap=colmap[0],shading='auto')
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	# ax.plot(xp,yp,c="black")
	# ax.fill(xp,yp,c="white")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Etot [mV/m] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Etot_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

	plt.close('all')