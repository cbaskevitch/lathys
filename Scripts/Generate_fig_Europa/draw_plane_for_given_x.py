from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os


### Draw plane for a given x value
zoom = True
src_dir = sys.argv[1]+"/"
dest_dir = sys.argv[2]+"/"
rundate = sys.argv[3]
diagtime = sys.argv[4]
typefile = "Thew_"
x = sys.argv[5]

ncfile = src_dir + typefile + rundate + "_t" + diagtime + '.nc'

ncid = Dataset(ncfile)
var_nc = ncid.variables

# planetname = var_nc['planetname'][:]
centr      = var_nc['s_centr'][:]
radius     = var_nc['r_planet'][:]
gs         = var_nc['gstep'][:]
# nptot      = var_nc['nptot'][:]
Ux         = var_nc['Ux'][:]
Uy         = var_nc['Uy'][:]
Uz         = var_nc['Uz'][:]
# nrm        = var_nc['phys_mag'][:]

# radius=1
nc = [len(Ux[0][0]), len(Ux[0]), len(Ux)]

Utot = np.sqrt(Ux*Ux + Uy*Uy + Uz*Uz)

if typefile == "Magw_":
	# maximum and minimum 
	if radius > 1.0:
		min_val    = [20.0,-180.0,-450.0]
		max_val    = [100.0,-100.0,-380.0]
		# min_val    = [-150.0,-50.0,-450.0]
		# max_val    = [150.0,100.0,-350.0]
		min_valtot = 350.0   # nT
		max_valtot = 550.0 # nT
	else:
		min_val    = [74.1,-135.2,-422.8] #[74.1,-135.2,-423.0] # nT [Ux,Uy,Uz] for run without planete
		max_val    = [74.5,-134.8,-422.65]#[74.5,-134.8,-422.5]  # nT [Ux,Uy,Uz] for run without planete
		min_valtot = 440.0   # nT
		max_valtot = 460.0 # nT
elif typefile == "Thew_":
    # maximum and minimum 
    if radius > 1.0:
        min_val = [0.0,-30.0,-10.0] # km/s [Ux,Uy,Uz]
        max_val = [140.0,30.0,10.0] # km/s  [Ux,Uy,Uz]
        min_valtot = 0.0 # km/s
        max_valtot = 150.0 # km/s
    else:
        min_val = [89.5,-0.5,-0.5] # km/s [Ux,Uy,Uz] for run without planete
        max_val = [90.5,0.5,0.5] # km/s  [Ux,Uy,Uz] for run without planete
        min_valtot = 80.0 # km/s for run without planete
        max_valtot = 95.0 # km/s for run without planete

# -- Creation of axis values centered on the planet ( normalized to planet radius)
X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)

# planet center in cell number (NU: cell number start at 1
icentr = int(x)#int(np.fix(centr[0]/gs[0]))
# jcentr = int(np.fix(centr[1]/gs[1]))
# kcentr = int(np.fix(centr[2]/gs[2]))
# iwake = int(icentr + np.fix(1.5*radius/gs[0]))

Ux_YZ        = np.zeros((nc[1],nc[2]))
Ux_YZ[:,:]   = np.matrix.transpose(Ux[:,:,icentr])
Uy_YZ        = np.zeros((nc[1],nc[2]))
Uy_YZ[:,:]   = np.matrix.transpose(Uy[:,:,icentr])
Uz_YZ        = np.zeros((nc[1],nc[2]))
Uz_YZ[:,:]   = np.matrix.transpose(Uz[:,:,icentr])
Utot_YZ      = np.zeros((nc[1],nc[2]))
Utot_YZ[:,:] = np.matrix.transpose(Utot[:,:,icentr])

# Ux_YZ_wake        = np.zeros((nc[1],nc[2]))
# Ux_YZ_wake[:,:]   = np.matrix.transpose(Ux[:,:,iwake])
# Uy_YZ_wake        = np.zeros((nc[1],nc[2]))
# Uy_YZ_wake[:,:]   = np.matrix.transpose(Uy[:,:,iwake])
# Uz_YZ_wake        = np.zeros((nc[1],nc[2]))
# Uz_YZ_wake[:,:]   = np.matrix.transpose(Uz[:,:,iwake])
# Utot_YZ_wake      = np.zeros((nc[1],nc[2]))
# Utot_YZ_wake[:,:] = np.matrix.transpose(Utot[:,:,iwake])

# planet drawing
theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
xp = np.cos(theta)
yp = np.sin(theta)

fig_size = [[6,9.5],[6,7.5],[8,6],[10,6]] #differentes tailles de fenetres
figsize_Xnum = 1  #numero de la taille de la fenetre pour les plans XZ et XY
figsize_Ynum = 2  #numero de la taille de la fenetre pour les plans YZ

if zoom == True:
    Xmin = -5.6
    Xmax = 2.9
    Ymin = -6.5
    Ymax = 6.5
    Zmin = -6.5
    Zmax = 6.5
else:
    Ymin=Y_XY[0][0]
    Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1]
    Zmin=Z_XZ[0][0]
    Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1]

# =========== figure in YZ plane ==============================
# Ux in X=given_by_user and X=1.5Rm

fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
c = ax.pcolor(Y_YZ,Z_YZ,Ux_YZ, vmin=min_val[0], vmax=max_val[0], cmap="jet",shading='auto')
ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
fig.colorbar(c, ax=ax)
ax.plot(xp,yp,c="black")
# ax.fill(xp,yp,c="white")
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "Ux [km/s] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Ux_x"+str(X_XY[icentr][icentr])+"_YZ_Europa_"+rundate+"_t"+diagtime+".png")

# # --

# if zoom == True:
#     fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
# else:
#     fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
# c = ax.pcolor(Y_YZ,Z_YZ,Ux_YZ_wake, vmin=min_val[0], vmax=max_val[0], cmap="jet",shading='auto')
# ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# fig.colorbar(c, ax=ax)
# ax.plot(xp,yp,c="black")
# ax.fill(xp,yp,c="white")
# ax.set_xlim(Ymin,Ymax)
# ax.set_ylim(Zmin,Zmax)

# titre = "Ux [km/s] time: "+diagtime
# plt.title(titre)#,'fontsize',12,'fontweight','b');
# ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
# ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

# plt.savefig(dest_dir+"Ux_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

# Uy in X=0 and X=1.5Rm

fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
c = ax.pcolor(Y_YZ,Z_YZ,Uy_YZ, vmin=min_val[1], vmax=max_val[1], cmap="jet",shading='auto')
ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
fig.colorbar(c, ax=ax)
ax.plot(xp,yp,c="black")
# ax.fill(xp,yp,c="white")
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "Uy [km/s] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Uy_x"+str(X_XY[icentr][icentr])+"_YZ_Europa_"+rundate+"_t"+diagtime+".png")

# # --

# if zoom == True:
#     fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
# else:
#     fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
# c = ax.pcolor(Y_YZ,Z_YZ,Uy_YZ_wake, vmin=min_val[1], vmax=max_val[1], cmap="jet",shading='auto')
# ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# fig.colorbar(c, ax=ax)
# ax.plot(xp,yp,c="black")
# ax.fill(xp,yp,c="white")
# ax.set_xlim(Ymin,Ymax)
# ax.set_ylim(Zmin,Zmax)

# titre = "Uy [km/s] time: "+diagtime
# plt.title(titre)#,'fontsize',12,'fontweight','b');
# ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
# ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

# plt.savefig(dest_dir+"Uy_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

# Uz in X=0 and X=1.5Rm

fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
c = ax.pcolor(Y_YZ,Z_YZ,Uz_YZ, vmin=min_val[2], vmax=max_val[2], cmap="jet",shading='auto')
ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
fig.colorbar(c, ax=ax)
ax.plot(xp,yp,c="black")
# ax.fill(xp,yp,c="white")
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "Uz [km/s] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Uz_x"+str(X_XY[icentr][icentr])+"_YZ_Europa_"+rundate+"_t"+diagtime+".png")

# # --

# if zoom == True:
#     fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
# else:
#     fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
# c = ax.pcolor(Y_YZ,Z_YZ,Uz_YZ_wake, vmin=min_val[2], vmax=max_val[2], cmap="jet",shading='auto')
# ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# fig.colorbar(c, ax=ax)
# ax.plot(xp,yp,c="black")
# ax.fill(xp,yp,c="white")
# ax.set_xlim(Ymin,Ymax)
# ax.set_ylim(Zmin,Zmax)

# titre = "Uz [km/s] time: "+diagtime
# plt.title(titre)#,'fontsize',12,'fontweight','b');
# ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
# ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

# plt.savefig(dest_dir+"Uz_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

# Utot in X=0 and X=1.5Rm

fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
c = ax.pcolor(Y_YZ,Z_YZ,Utot_YZ, vmin=min_valtot, vmax=max_valtot, cmap="jet",shading='auto')
ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
fig.colorbar(c, ax=ax)
ax.plot(xp,yp,c="black")
# ax.fill(xp,yp,c="white")
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "Utot [km/s] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Utot_x"+str(X_XY[icentr][icentr])+"_YZ_Europa_"+rundate+"_t"+diagtime+".png")

# # --

# if zoom == True:
#     fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
# else:
#     fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
# c = ax.pcolor(Y_YZ,Z_YZ,Utot_YZ_wake, vmin=min_valtot, vmax=max_valtot, cmap="jet",shading='auto')
# ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# fig.colorbar(c, ax=ax)
# ax.plot(xp,yp,c="black")
# ax.fill(xp,yp,c="white")
# ax.set_xlim(Ymin,Ymax)
# ax.set_ylim(Zmin,Zmax)

# titre = "Utot [km/s] time: "+diagtime
# plt.title(titre)#,'fontsize',12,'fontweight','b');
# ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
# ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

# plt.savefig(dest_dir+"Utot_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

plt.close('all')