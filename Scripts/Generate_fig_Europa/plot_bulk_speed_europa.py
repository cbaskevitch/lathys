######################
# plot_bulk_speed_europa.py
# ---------------------
# This routine reads the bulk speed
#  file and plot the module
# and the U components in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

def plot_bulk_speed(src_dir,dest_dir,typefile,rundate,diagtime,zoom,field_lines_dens,buffer_zone):
	ncfile = src_dir + typefile + rundate + "_t" + diagtime + '.nc'

	ncid = Dataset(ncfile)
	var_nc = ncid.variables

	# planetname = var_nc['planetname'][:]
	centr      = var_nc['s_centr'][:]
	radius     = var_nc['r_planet'][:]
	gs         = var_nc['gstep'][:]
	# nptot      = var_nc['nptot'][:]
	Ux         = var_nc['Ux'][:]
	Uy         = var_nc['Uy'][:]
	Uz         = var_nc['Uz'][:]
	# nrm        = var_nc['phys_mag'][:]

	# radius=1
	nc = [len(Ux[0][0]), len(Ux[0]), len(Ux)]

	Utot = np.sqrt(Ux*Ux + Uy*Uy + Uz*Uz)

	# maximum and minimum 
	if radius > 1.0:
		min_val = [0.0,-30.0,-10.0] # km/s [Ux,Uy,Uz]
		max_val = [140.0,30.0,10.0] # km/s  [Ux,Uy,Uz]
		min_valtot = 0.0 # km/s
		max_valtot = 150.0 # km/s
	else:
		min_val = [89.5,-0.5,-0.5] # km/s [Ux,Uy,Uz] for run without planete
		max_val = [90.5,0.5,0.5] # km/s  [Ux,Uy,Uz] for run without planete
		min_valtot = 80.0 # km/s for run without planete
		max_valtot = 95.0 # km/s for run without planete

	# -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide(centr[0]*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide(centr[1]*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide(centr[0]*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide(centr[2]*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide(centr[1]*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide(centr[2]*np.ones((nc[1],nc[2])), radius)

	# planet center in cell number (NB: cell number start at 1
	icentr = int(np.fix(centr[0]/gs[0]))
	jcentr = int(np.fix(centr[1]/gs[1]))
	kcentr = int(np.fix(centr[2]/gs[2]))
	iwake = int(icentr + np.fix(1.5*radius/gs[0]))

	Ux_XY        = np.zeros((nc[0],nc[1]))
	Ux_XY[:,:]   = np.matrix.transpose(Ux[kcentr,:,:])
	Uy_XY        = np.zeros((nc[0],nc[1]))
	Uy_XY[:,:]   = np.matrix.transpose(Uy[kcentr,:,:])
	Uz_XY        = np.zeros((nc[0],nc[1]))
	Uz_XY[:,:]   = np.matrix.transpose(Uz[kcentr,:,:])
	Utot_XY      = np.zeros((nc[0],nc[1]))
	Utot_XY[:,:] = np.matrix.transpose(Utot[kcentr,:,:])

	Ux_XZ        = np.zeros((nc[0],nc[2]))
	Ux_XZ[:,:]   = np.matrix.transpose(Ux[:,jcentr,:])
	Uy_XZ        = np.zeros((nc[0],nc[2]))
	Uy_XZ[:,:]   = np.matrix.transpose(Uy[:,jcentr,:])
	Uz_XZ        = np.zeros((nc[0],nc[2]))
	Uz_XZ[:,:]   = np.matrix.transpose(Uz[:,jcentr,:])
	Utot_XZ      = np.zeros((nc[0],nc[2]))
	Utot_XZ[:,:] = np.matrix.transpose(Utot[:,jcentr,:])

	Ux_YZ_term        = np.zeros((nc[1],nc[2]))
	Ux_YZ_term[:,:]   = np.matrix.transpose(Ux[:,:,icentr])
	Uy_YZ_term        = np.zeros((nc[1],nc[2]))
	Uy_YZ_term[:,:]   = np.matrix.transpose(Uy[:,:,icentr])
	Uz_YZ_term        = np.zeros((nc[1],nc[2]))
	Uz_YZ_term[:,:]   = np.matrix.transpose(Uz[:,:,icentr])
	Utot_YZ_term      = np.zeros((nc[1],nc[2]))
	Utot_YZ_term[:,:] = np.matrix.transpose(Utot[:,:,icentr])

	Ux_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ux_YZ_wake[:,:]   = np.matrix.transpose(Ux[:,:,iwake])
	Uy_YZ_wake        = np.zeros((nc[1],nc[2]))
	Uy_YZ_wake[:,:]   = np.matrix.transpose(Uy[:,:,iwake])
	Uz_YZ_wake        = np.zeros((nc[1],nc[2]))
	Uz_YZ_wake[:,:]   = np.matrix.transpose(Uz[:,:,iwake])
	Utot_YZ_wake      = np.zeros((nc[1],nc[2]))
	Utot_YZ_wake[:,:] = np.matrix.transpose(Utot[:,:,iwake])

	# planet drawing
	theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
	xp = np.cos(theta)
	yp = np.sin(theta)

	## Field lines
	Xmin=X_XY[0][0] #float(-1*icentr/rp)
	Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1] #float((nc[0]-icentr)/rp)
	# print("xmin = ",Xmin, " xmax = ",Xmax, "pas = ", (Xmax-Xmin)/(nc[0]-1))
	# X_norm=np.arange(Xmin, Xmax, (Xmax-Xmin)/(nc[0]))
	X_norm = np.linspace(0 , (nc[0]-1) * gs[0] , num=nc[0]) / radius - centr[0]/radius
	Ymin=Y_XY[0][0] #float(-jcentr/rp)
	Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1] #float((nc[1]-jcentr)/rp)
	# Y_norm=np.arange(Ymin, Ymax, (Ymax-Ymin)/(nc[1]))
	Y_norm = np.linspace(0 , (nc[1]-1) * gs[1] , num=nc[1])/ radius - centr[1]/radius
	Zmin=Z_XZ[0][0] #float(-kcentr/rp)
	Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1] #float((nc[1]-kcentr)/rp)
	# Z_norm=np.arange(Zmin, Zmax, (Zmax-Zmin)/(nc[2]))
	Z_norm = np.linspace(0 , (nc[2]-1) * gs[2] , num=nc[2])/ radius - centr[2]/radius

	fig_size = [[6,9.5],[6,7.5],[8,6],[10,6]] #differentes tailles de fenetres
	figsize_Xnum = 1  #numero de la taille de la fenetre pour les plans XZ et XY
	figsize_Ynum = 2  #numero de la taille de la fenetre pour les plans YZ

	if zoom == True:
		Xmin = -5.6
		Xmax = 2.9
		Ymin = -6.5
		Ymax = 6.5
		Zmin = -6.5
		Zmax = 6.5
	else:
		Xmin=X_XY[0][0]
		Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1]
		Ymin=Y_XY[0][0]
		Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1]
		Zmin=Z_XZ[0][0]
		Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1]

	# -- Figure 1 & 2 -- Ux
	# **************************************************************************
	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, Ux_XY, vmin=min_val[0], vmax=max_val[0], cmap="viridis",shading='auto')
	ax.streamplot(X_norm,Y_norm,np.transpose(Ux_XY),np.transpose(Uy_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Ux [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ux_XY_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, Ux_XZ, vmin=min_val[0], vmax=max_val[0], cmap="viridis",shading='auto')
	ax.streamplot(X_norm,Z_norm,np.transpose(Ux_XZ),np.transpose(Uz_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ux [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ux_XZ_Europa_"+rundate+"_t"+diagtime+".png")

	# -- Figure 3 & 4 -- Uy
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, Uy_XY, vmin=min_val[1], vmax=max_val[1], cmap="viridis",shading='auto')
	ax.streamplot(X_norm,Y_norm,np.transpose(Ux_XY),np.transpose(Uy_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Uy [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Uy_XY_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, Uy_XZ, vmin=min_val[1], vmax=max_val[1], cmap="viridis",shading='auto')
	ax.streamplot(X_norm,Z_norm,np.transpose(Ux_XZ),np.transpose(Uz_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Uy [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Uy_XZ_Europa_"+rundate+"_t"+diagtime+".png")

	# -- Figure 3 & 4 -- Uz
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, Uz_XY, vmin=min_val[2], vmax=max_val[2], cmap="viridis",shading='auto')
	ax.streamplot(X_norm,Y_norm,np.transpose(Ux_XY),np.transpose(Uy_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Uz [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Uz_XY_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, Uz_XZ, vmin=min_val[2], vmax=max_val[2], cmap="viridis",shading='auto')
	ax.streamplot(X_norm,Z_norm,np.transpose(Ux_XZ),np.transpose(Uz_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Uz [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Uz_XZ_Europa_"+rundate+"_t"+diagtime+".png")

	# -- Figure 3 & 4 -- Utot
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, Utot_XY, vmin=min_valtot, vmax=max_valtot, cmap="viridis",shading='auto')
	ax.streamplot(X_norm,Y_norm,np.transpose(Ux_XY),np.transpose(Uy_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Utot [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Utot_XY_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, Utot_XZ, vmin=min_valtot, vmax=max_valtot, cmap="viridis",shading='auto')
	ax.streamplot(X_norm,Z_norm,np.transpose(Ux_XZ),np.transpose(Uz_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Utot [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Utot_XZ_Europa_"+rundate+"_t"+diagtime+".png")

	plt.close('all')
	# =========== figure in YZ plane ==============================
	# Ux in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Ux_YZ_term, vmin=min_val[0], vmax=max_val[0], cmap="viridis",shading='auto')
	ax.streamplot(Y_norm,Z_norm,np.transpose(Uy_YZ_term),np.transpose(Uz_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ux [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ux_YZ_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Ux_YZ_wake, vmin=min_val[0], vmax=max_val[0], cmap="viridis",shading='auto')
	ax.streamplot(Y_norm,Z_norm,np.transpose(Uy_YZ_wake),np.transpose(Uz_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Ux [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Ux_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

	# Uy in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Uy_YZ_term, vmin=min_val[1], vmax=max_val[1], cmap="viridis",shading='auto')
	ax.streamplot(Y_norm,Z_norm,np.transpose(Uy_YZ_term),np.transpose(Uz_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Uy [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Uy_YZ_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Uy_YZ_wake, vmin=min_val[1], vmax=max_val[1], cmap="viridis",shading='auto')
	ax.streamplot(Y_norm,Z_norm,np.transpose(Uy_YZ_wake),np.transpose(Uz_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Uy [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Uy_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

	# Uz in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Uz_YZ_term, vmin=min_val[2], vmax=max_val[2], cmap="viridis",shading='auto')
	ax.streamplot(Y_norm,Z_norm,np.transpose(Uy_YZ_term),np.transpose(Uz_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Uz [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Uz_YZ_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Uz_YZ_wake, vmin=min_val[2], vmax=max_val[2], cmap="viridis",shading='auto')
	ax.streamplot(Y_norm,Z_norm,np.transpose(Uy_YZ_wake),np.transpose(Uz_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Uz [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Uz_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

	# Utot in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Utot_YZ_term, vmin=min_valtot, vmax=max_valtot, cmap="viridis",shading='auto')
	ax.streamplot(Y_norm,Z_norm,np.transpose(Uy_YZ_term),np.transpose(Uz_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Utot [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Utot_YZ_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Utot_YZ_wake, vmin=min_valtot, vmax=max_valtot, cmap="viridis",shading='auto')
	ax.streamplot(Y_norm,Z_norm,np.transpose(Uy_YZ_wake),np.transpose(Uz_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Utot [km/s] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Utot_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

	plt.close('all')