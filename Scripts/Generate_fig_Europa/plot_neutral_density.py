######################
# plot_ion_density_europa.py
# ---------------------
# This routine reads the density
#  file and plot the map in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

def plot_neutral_density(src_dir,dest_dir,typefile,rundate,diagtime,zoom,buffer_zone):
    file = src_dir + typefile + rundate + "_t" + diagtime + ".nc"
    ncid = Dataset(file)
    var_nc = ncid.variables
    species=["O2","H2O","H2"]

    # planetname = var_nc['planetname'][:]
    centr      = var_nc['s_centr'][:]
    radius     = var_nc['r_planet'][:]
    gs         = var_nc['gstep'][:]
    # nptot      = var_nc['nptot'][:]

    for spe in species:
        Dn         = var_nc['Den_'+spe][:]
        # nrm        = var_nc['phys_density'][:]
        # radius=1
        nc = [len(Dn[0][0]), len(Dn[0]), len(Dn)]
        Dn = np.where(Dn <= 0, float('NaN'), Dn)

        if spe=="H2O":
            min_val = -4.0 # log(cm-3)
            max_val = 6.0 # log(sm-3)
        elif spe=="O2":
            min_val = 0.0 # log(cm-3)
            max_val = 8.5 # log(sm-3)
        # -- Creation of axis values centered on the planet ( normalized to planet radius)
        X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
        X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide(centr[0]*np.ones((nc[0],nc[1])), radius)
        Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide(centr[1]*np.ones((nc[0],nc[1])), radius)

        X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
        X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide(centr[0]*np.ones((nc[0],nc[2])), radius)
        Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide(centr[2]*np.ones((nc[0],nc[2])), radius)

        Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
        Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide(centr[1]*np.ones((nc[1],nc[2])), radius)
        Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide(centr[2]*np.ones((nc[1],nc[2])), radius)

        # planet center in cell number (NB: cell number start at 1
        icentr = int(np.fix(centr[0]/gs[0]))
        jcentr = int(np.fix(centr[1]/gs[1]))
        kcentr = int(np.fix(centr[2]/gs[2]))
        iwake = icentr#int(icentr + np.fix(2.0*radius/gs[0]))
        print(icentr,jcentr,kcentr,iwake)

        Dn_XY = np.zeros((nc[0],nc[1]))
        Dn_XY[:,:] = np.matrix.transpose(Dn[kcentr,:,:])

        Dn_XZ = np.zeros((nc[0],nc[2]))
        Dn_XZ[:,:] = np.matrix.transpose(Dn[:,jcentr,:])

        Dn_YZ_term = np.zeros((nc[1],nc[2]))
        Dn_YZ_term[:,:] = np.matrix.transpose(Dn[:,:,icentr])

        Dn_YZ_wake = np.zeros((nc[1],nc[2]))
        Dn_YZ_wake[:,:] = np.matrix.transpose(Dn[:,:,iwake])

        # planet drawing
        theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
        xp = np.cos(theta)
        yp = np.sin(theta)

        if zoom == True:
            Xmin = -6
            Xmax = 6
            Ymin = -6
            Ymax = 6
            Zmin = -6
            Zmax = 6
        else:
            Xmin=X_XY[0][0]
            Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1]
            Ymin=Y_XY[0][0]
            Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1]
            Zmin=Z_XZ[0][0]
            Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1]

        fig_size = [[6,9.5],[6,7.5],[8,6],[10,6]] #differentes tailles de fenetres
        figsize_Xnum = 2  #numero de la taille de la fenetre pour les plans XZ et XY
        figsize_Ynum = 2  #numero de la taille de la fenetre pour les plans YZ

        # -- Figure 1 & 2 -- Dn
        # **************************************************************************

        if zoom == True:
            fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
        else:
            fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
        c = ax.pcolor(X_XY, Y_XY, np.log10(Dn_XY), vmin=min_val, vmax=max_val, cmap="jet",shading='auto')
        if radius != 1:
            if buffer_zone == True:
                ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
                ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
            ax.plot(xp,yp,c="black")
        fig.colorbar(c, ax=ax)
        ax.set_xlim(Xmin,Xmax)
        ax.set_ylim(Ymin,Ymax)

        titre = "Density "+spe+" "+" log[cm-3] time: "+diagtime
        plt.title(titre)#,'fontsize',12,'fontweight','b');
        ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
        ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

        plt.savefig(dest_dir+"Dn_"+spe+"_XY_Europa_"+rundate+"_t"+diagtime+".png")

        # --

        if zoom == True:
            fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
        
        else:
            fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
        c = ax.pcolor(X_XZ, Z_XZ, np.log10(Dn_XZ), vmin=min_val, vmax=max_val, cmap="jet",shading='auto')
        if radius != 1:
            if buffer_zone == True:
                ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
                ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
            ax.plot(xp,yp,c="black")
        fig.colorbar(c, ax=ax)
        ax.set_xlim(Xmin,Xmax)
        ax.set_ylim(Zmin,Zmax)

        titre = "Density "+spe+" log[cm-3] time: "+diagtime
        plt.title(titre)#,'fontsize',12,'fontweight','b');
        ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
        ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

        plt.savefig(dest_dir+"Dn_"+spe+"_XZ_Europa_"+rundate+"_t"+diagtime+".png")

        # =========== figure in YZ plane ==============================
        # Dn in X=0 and X=1.5Rm

        # figure 3 & 4
        if zoom == True:
            fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
        else:
            fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
        c = ax.pcolor(Y_YZ, Z_YZ, np.log10(Dn_YZ_term), vmin=min_val, vmax=max_val, cmap="jet",shading='auto')
        if radius != 1:
            ax.plot(xp,yp,c="black")
            if buffer_zone == True:
                ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
                ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
                ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
                ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
        fig.colorbar(c, ax=ax)
        ax.set_xlim(Ymin,Ymax)
        ax.set_ylim(Zmin,Zmax)

        titre = "Density "+spe+" log[cm-3] time: "+diagtime
        plt.title(titre)#,'fontsize',12,'fontweight','b');
        ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
        ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

        plt.savefig(dest_dir+"Dn_"+spe+"_YZ_Europa_"+rundate+"_t"+diagtime+".png")

        # --
        # if zoom == True:
        #     fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
        # else:
        #     fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
        # c = ax.pcolor(Y_YZ, Z_YZ, np.log10(Dn_YZ_wake), vmin=min_val, vmax=max_val, cmap="jet",shading='auto')
        # if radius != 1:
        #     ax.plot(xp,yp,c="black")
        #     if buffer_zone == True:
        #         ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
        #         ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
        #         ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
        #         ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
        # fig.colorbar(c, ax=ax)
        # ax.set_xlim(Ymin,Ymax)
        # ax.set_ylim(Zmin,Zmax)

        # titre = "Density "+spe+" log[cm-3] time: "+diagtime
        # plt.title(titre)#,'fontsize',12,'fontweight','b');
        # ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
        # ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

        # plt.savefig(dest_dir+"Dn_"+spe+"_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")


        # plt.show()
        plt.close('all')