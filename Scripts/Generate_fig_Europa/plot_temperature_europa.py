#######################
# plot_Temperature_ne_europa.py
#---------------------
# This routine reads the Temperature
#  file and plot the map in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
#######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

me = 1 #9.1094e-31 # kg
Kb = 1.38e-23 ## Constante de Boltzmann en m^2 kg s^-2 K-1
K_to_eV = Kb/1.602e-19

def plot_temperature(src_dir,dest_dir,typefile,rundate,diagtime,zoom,buffer_zone):
	ncfile = src_dir + typefile + rundate + "_t" + diagtime + '.nc'

	ncid = Dataset(ncfile)
	var_nc = ncid.variables

	# planetname = var_nc['planetname'][:]
	centr      = var_nc['s_centr'][:]
	radius     = var_nc['r_planet'][:]
	gs         = var_nc['gstep'][:]
	nptot      = var_nc['nptot'][:]
	temp       = var_nc['Temperature'][:]
	
	# nrm        = var_nc['phys_density'][:]
	# nrm_len    = var_nc['phys_length'][:]
	
	# radius=1
	nc = [len(temp[0][0]), len(temp[0]), len(temp)]
	print("nc=",nc)

	temp = np.where(temp <= 0, float('NaN'), temp)
	# maximum and minimum 
	if radius > 1.0:
		min_val = 0 #200.
		max_val = 5 #30 #600
	else:
		min_val = 1.5 # log(cm-3) for run without planete
		max_val = 1.6  # log(sm-3) for run without planete

	# -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide(centr[0]*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide(centr[1]*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide(centr[0]*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide(centr[2]*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide(centr[1]*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide(centr[2]*np.ones((nc[1],nc[2])), radius)

	# planet center in cell number (NB: cell number start at 1
	icentr = int(np.fix(centr[0]/gs[0]))
	jcentr = int(np.fix(centr[1]/gs[1]))
	kcentr = int(np.fix(centr[2]/gs[2]))
	iwake = int(icentr + np.fix(1.5*radius/gs[0]))

	temp_XY = np.zeros((nc[0],nc[1]))
	temp_XY[:,:] = np.matrix.transpose(temp[kcentr,:,:])
	# temp_XY[:,:] = np.matrix.transpose(U[kcentr,:,:]*me/Kb*K_to_eV)

	print(np.min(temp_XY),np.max(temp_XY))

	temp_XZ = np.zeros((nc[0],nc[2]))
	temp_XZ[:,:] = np.matrix.transpose(temp[:,jcentr,:])
	# temp_XZ[:,:] = np.matrix.transpose(U[:,jcentr,:]*me/Kb*K_to_eV)

	temp_YZ_term = np.zeros((nc[1],nc[2]))
	temp_YZ_term[:,:] = np.matrix.transpose(temp[:,:,icentr])
	# temp_YZ_term[:,:] = np.matrix.transpose(U[:,:,icentr]*me/Kb*K_to_eV)

	temp_YZ_wake = np.zeros((nc[1],nc[2]))
	temp_YZ_wake[:,:] = np.matrix.transpose(temp[:,:,iwake])
	# temp_YZ_wake[:,:] = np.matrix.transpose(U[:,:,iwake]*me/Kb*K_to_eV)

	tempe_1D = np.zeros(nc[0])
	tempe_1D[:] = temp[kcentr,jcentr,:]
	# x = np.divide(np.arange(0, nc[0], 1.0) * gs[0]-centr[0], radius)
	# x = ([0:nc(1)-1]*gs(1)-centr(1))./radius;

	
	# planet drawing
	theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
	xp = np.cos(theta)
	yp = np.sin(theta)

	fig_size = [[6,9.5],[6,7.5],[8,6],[10,6],[7,6]] #differentes tailles de fenetres
	figsize_Xnum = 4  #numero de la taille de la fenetre pour les plans XZ et XY
	figsize_Ynum = 4  #numero de la taille de la fenetre pour les plans YZ

	if zoom == True:
		Xmin = -2.0
		Xmax = 2.0
		Ymin = -2.0
		Ymax = 2.0
		Zmin = -2.0
		Zmax = 2.0
	else:
		Xmin=X_XY[0][0]
		Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1]
		Ymin=Y_XY[0][0]
		Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1]
		Zmin=Z_XZ[0][0]
		Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1]

	# -- Figure 1 & 2 -- temp
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, temp_XY, vmin=min_val, vmax=max_val, cmap="inferno",shading='auto')
	if radius != 1:
		ax.plot(xp,yp,c="pink")
		if buffer_zone == True:
			ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="pink",ls="--")
			ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="pink",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "$O_2^+$ temperature [eV] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [$R_E$]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [$R_E$]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"temp_O2pl_XY_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, temp_XZ, vmin=min_val, vmax=max_val, cmap="inferno",shading='auto')
	if radius != 1:
		ax.plot(xp,yp,c="pink")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="pink",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="pink",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "$O_2^+$ temperature [eV] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [$R_E$]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [$R_E$]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"temp_O2pl_XZ_Europa_"+rundate+"_t"+diagtime+".png")

	# =========== figure in YZ plane ==============================
	# temp in X=0 and X=1.5Rm

	# figure 3 & 4
	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ, Z_YZ, temp_YZ_term, vmin=min_val, vmax=max_val, cmap="inferno",shading='auto')
	if radius != 1:
		ax.plot(xp,yp,c="pink")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="pink",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="pink",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="pink",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="pink",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "$O_2^+$ temperature [eV] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [$R_E$]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [$R_E$]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"temp_O2pl_YZ_Europa_"+rundate+"_t"+diagtime+".png")

	# --
	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ, Z_YZ, temp_YZ_wake, vmin=min_val, vmax=max_val, cmap="inferno",shading='auto')
	if radius != 1:
		ax.plot(xp,yp,c="pink")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="pink",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="pink",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="pink",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="pink",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "$O_2^+$ temperature [eV] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [$R_E$]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [$R_E$]')#,'fontsize',12,'fontweight','b');

	# plt.show()
	plt.savefig(dest_dir+"temp_O2pl_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

	plt.close('all')