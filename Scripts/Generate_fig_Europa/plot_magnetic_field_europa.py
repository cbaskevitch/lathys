######################
# Plot_magnetic_field_europa.py
# ---------------------
# This routine reads the magnetic
# field file and plot the module
# and the B components in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

def plot_magnetic_field(src_dir,dest_dir,typefile,rundate,diagtime,zoom,field_lines_dens,buffer_zone):
	ncfile = src_dir + typefile + rundate + "_t" + diagtime + '.nc' #'_extract_4Re_grid.nc'

	ncid = Dataset(ncfile)
	var_nc = ncid.variables

	# planetname = var_nc['planetname'][:]
	centr      = var_nc['s_centr'][:]
	radius     = var_nc['r_planet'][:]
	gs         = var_nc['gstep'][:]
	# nptot      = var_nc['nptot'][:]
	Bx         = var_nc['Bx'][:]
	By         = var_nc['By'][:]
	Bz         = var_nc['Bz'][:]
	# nrm        = var_nc['phys_mag'][:]
	nrm_len    = var_nc['phys_length'][:]

	# radius=1
	nc = [len(Bx[0][0]), len(Bx[0]), len(Bx)]
	print("nc = ",nc[0],nc[1],nc[2])

	Btot = np.sqrt(Bx*Bx + By*By + Bz*Bz)

	# maximum and minimum 
	if radius > 1.0:
		# B E4 flyby
		min_val    = [-50.0,-180.0,-450.0]
		max_val    = [100.0,-120.0,-350.0]
		min_valtot = 400.0   # nT
		max_valtot = 500.0 # nT

		# B JUICE E2 flyby
		# min_val    = [-50.0,180.0,-500.0]
		# max_val    = [100.0,100.0,-420.0]
		# min_valtot = 400.0   # nT
		# max_valtot = 550.0 # nT
		
		# B along Z
		# min_val    = [-75.0,-50.0,-500.0]
		# max_val    = [75.0,50.0,-400.0]
		
	else:
		min_val    = [74.1,-135.2,-422.8] #[74.1,-135.2,-423.0] # nT [Bx,By,Bz] for run without planete
		max_val    = [74.5,-134.8,-422.65]#[74.5,-134.8,-422.5]  # nT [Bx,By,Bz] for run without planete
		min_valtot = 440.0   # nT
		max_valtot = 460.0 # nT

	# -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	# X_XY = np.divide(X_XY, radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[1],nc[0])), radius)
	# Y_XY = np.divide(Y_XY, radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[0])), radius)
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide(centr[0]*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide(centr[1]*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	# X_XZ = np.divide(X_XZ, radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[2],nc[0])), radius)
	# Z_XZ = np.divide(Z_XZ, radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[2],nc[0])), radius)
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide(centr[0]*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide(centr[2]*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	# Y_YZ = np.divide(Y_YZ, radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[2],nc[1])), radius)
	# Z_YZ = np.divide(Z_YZ, radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[2],nc[1])), radius)
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide(centr[1]*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide(centr[2]*np.ones((nc[1],nc[2])), radius)

	# planet center in cell number (NB: cell number start at 0
	icentr = int(np.fix(centr[0]/gs[0]))
	jcentr = int(np.fix(centr[1]/gs[1]))
	kcentr = int(np.fix(centr[2]/gs[2]))
	iwake = int(icentr + np.fix(1.5*radius/gs[0]))
	print(centr,gs)
	print(icentr,jcentr,kcentr,iwake)
	# rp = radius/gs[0]

	# Bx_XY        = np.zeros((nc[1],nc[0]))
	# Bx_XY[:,:]   = Bx[kcentr,:,:]
	# By_XY        = np.zeros((nc[1],nc[0]))
	# By_XY[:,:]   = By[kcentr,:,:]
	# Bz_XY        = np.zeros((nc[1],nc[0]))
	# Bz_XY[:,:]   = Bz[kcentr,:,:]
	Bx_XY        = np.zeros((nc[0],nc[1]))
	Bx_XY[:,:]   = np.matrix.transpose(Bx[kcentr,:,:])
	By_XY        = np.zeros((nc[0],nc[1]))
	By_XY[:,:]   = np.matrix.transpose(By[kcentr,:,:])
	Bz_XY        = np.zeros((nc[0],nc[1]))
	Bz_XY[:,:]   = np.matrix.transpose(Bz[kcentr,:,:])
	Btot_XY      = np.zeros((nc[0],nc[1]))
	Btot_XY[:,:] = np.matrix.transpose(Btot[kcentr,:,:])

	# Bx_XZ        = np.zeros((nc[2],nc[0]))
	# Bx_XZ[:,:]   = Bx[:,jcentr,:]
	# By_XZ        = np.zeros((nc[2],nc[0]))
	# By_XZ[:,:]   = By[:,jcentr,:]
	# Bz_XZ        = np.zeros((nc[2],nc[0]))
	# Bz_XZ[:,:]   = Bz[:,jcentr,:]
	# Btot_XZ      = np.zeros((nc[2],nc[0]))
	Bx_XZ        = np.zeros((nc[0],nc[2]))
	Bx_XZ[:,:]   = np.matrix.transpose(Bx[:,jcentr,:])
	By_XZ        = np.zeros((nc[0],nc[2]))
	By_XZ[:,:]   = np.matrix.transpose(By[:,jcentr,:])
	Bz_XZ        = np.zeros((nc[0],nc[2]))
	Bz_XZ[:,:]   = np.matrix.transpose(Bz[:,jcentr,:])
	Btot_XZ      = np.zeros((nc[0],nc[2]))
	Btot_XZ[:,:] = np.matrix.transpose(Btot[:,jcentr,:])

	# Bx_YZ_term        = np.zeros((nc[2],nc[1]))
	# Bx_YZ_term[:,:]   = Bx[:,:,icentr]
	# By_YZ_term        = np.zeros((nc[2],nc[1]))
	# By_YZ_term[:,:]   = By[:,:,icentr]
	# Bz_YZ_term        = np.zeros((nc[2],nc[1]))
	# Bz_YZ_term[:,:]   = Bz[:,:,icentr]
	Bx_YZ_term        = np.zeros((nc[1],nc[2]))
	Bx_YZ_term[:,:]   = np.matrix.transpose(Bx[:,:,icentr])
	By_YZ_term        = np.zeros((nc[1],nc[2]))
	By_YZ_term[:,:]   = np.matrix.transpose(By[:,:,icentr])
	Bz_YZ_term        = np.zeros((nc[1],nc[2]))
	Bz_YZ_term[:,:]   = np.matrix.transpose(Bz[:,:,icentr])
	Btot_YZ_term      = np.zeros((nc[1],nc[2]))
	Btot_YZ_term[:,:] = np.matrix.transpose(Btot[:,:,icentr])

	# Bx_YZ_wake        = np.zeros((nc[2],nc[1]))
	# Bx_YZ_wake[:,:]   = Bx[:,:,iwake]
	# By_YZ_wake        = np.zeros((nc[2],nc[1]))
	# By_YZ_wake[:,:]   = By[:,:,iwake]
	# Bz_YZ_wake        = np.zeros((nc[2],nc[1]))
	# Bz_YZ_wake[:,:]   = Bz[:,:,iwake]
	Bx_YZ_wake        = np.zeros((nc[1],nc[2]))
	Bx_YZ_wake[:,:]   = np.matrix.transpose(Bx[:,:,iwake])
	By_YZ_wake        = np.zeros((nc[1],nc[2]))
	By_YZ_wake[:,:]   = np.matrix.transpose(By[:,:,iwake])
	Bz_YZ_wake        = np.zeros((nc[1],nc[2]))
	Bz_YZ_wake[:,:]   = np.matrix.transpose(Bz[:,:,iwake])
	Btot_YZ_wake      = np.zeros((nc[1],nc[2]))
	Btot_YZ_wake[:,:] = np.matrix.transpose(Btot[:,:,iwake])

	# Bx_1D    = np.zeros(nc[0])
	# Bx_1D[:] = Bx[kcentr,jcentr,:]
	# By_1D    = np.zeros(nc[0])
	# By_1D[:] = By[kcentr,jcentr,:]
	# Bz_1D    = np.zeros(nc[0])
	# Bz_1D[:] = Bz[kcentr,jcentr,:]
	# # x = np.divide(np.arange(0, nc[0], 1.0) * gs[0]-centr[0], radius)

	# planet drawing
	theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
	xp = np.cos(theta)
	yp = np.sin(theta)

	## Field lines
	Xmin=X_XY[0][0] #float(-1*icentr/rp)
	Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1] #float((nc[0]-icentr)/rp)
	# print("xmin = ",Xmin, " xmax = ",Xmax, "pas = ", (Xmax-Xmin)/(nc[0]-1))
	# X_norm=np.arange(Xmin, Xmax, (Xmax-Xmin)/(nc[0]))
	X_norm = np.linspace(0 , (nc[0]-1) * gs[0] , num=nc[0]) / radius - centr[0]/radius
	Ymin=Y_XY[0][0] #float(-jcentr/rp)
	Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1] #float((nc[1]-jcentr)/rp)
	# Y_norm=np.arange(Ymin, Ymax, (Ymax-Ymin)/(nc[1]))
	Y_norm = np.linspace(0 , (nc[1]-1) * gs[1] , num=nc[1])/ radius - centr[1]/radius
	Zmin=Z_XZ[0][0] #float(-kcentr/rp)
	Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1] #float((nc[1]-kcentr)/rp)
	# Z_norm=np.arange(Zmin, Zmax, (Zmax-Zmin)/(nc[2]))
	Z_norm = np.linspace(0 , (nc[2]-1) * gs[2] , num=nc[2])/ radius - centr[2]/radius

	fig_size = [[6,9.5],[6,7.5],[8,6],[10,6]] #differentes tailles de fenetres
	figsize_Xnum = 1  #numero de la taille de la fenetre pour les plans XZ et XY
	figsize_Ynum = 2  #numero de la taille de la fenetre pour les plans YZ

	colmap = ["viridis","inferno","GnBu_r"]
	ncol = 0  # numero de la colormap

	if zoom == True:
		Xmin = -5.6
		Xmax = 2.9
		Ymin = -6.5
		Ymax = 6.5
		Zmin = -6.5
		Zmax = 6.5

	print("Xmin",Xmin,"Xmax",Xmax)
	print("Ymin",Ymin,"Ymax",Ymax)
	print("Zmin",Zmin,"Zmax",Zmax)

	print(len(X_norm),len(Y_norm))
	print(len(Bx_XY),len(Bx_XY[0]),len(By_XY),len(By_XY[0]))
	# print(np.transpose(len(Bx_XY)),np.transpose(len(Bx_XY[0])),np.transpose(len(By_XY)),np.transpose(len(By_XY[0])))

	# for i in range(0,nc[1]-1):
	# 	plt.plot(i,Y_norm[i+1]-Y_norm[i],'.')
	# 	print("i ",i, Y_norm[i+1]-Y_norm[i])
	# plt.show()

	# -- Figure 1 & 2 -- Bx
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, Bx_XY, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	# ax.streamplot(np.transpose(X_norm),Y_norm,Bx_XY,By_XY,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	ax.streamplot(X_norm,Y_norm,np.transpose(Bx_XY),np.transpose(By_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Bx [nT] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	# plt.show()
	plt.savefig(dest_dir+"Bx_XY_Europa_"+rundate+"_t"+diagtime+".png")
	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, Bx_XZ, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	# ax.streamplot(X_norm,Z_norm,Bx_XZ,Bz_XZ,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	ax.streamplot(X_norm,Z_norm,np.transpose(Bx_XZ),np.transpose(Bz_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Bx [nT] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Bx_XZ_Europa_"+rundate+"_t"+diagtime+".png")
	# plt.show()

	# -- Figure 3 & 4 -- By
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, By_XY, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	# ax.streamplot(X_norm,Y_norm,Bx_XY,By_XY,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	ax.streamplot(X_norm,Y_norm,np.transpose(Bx_XY),np.transpose(By_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "By [nT] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"By_XY_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, By_XZ, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	# ax.streamplot(X_norm,Z_norm,Bx_XZ,Bz_XZ,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	ax.streamplot(X_norm,Z_norm,np.transpose(Bx_XZ),np.transpose(Bz_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "By [nT] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"By_XZ_Europa_"+rundate+"_t"+diagtime+".png")

	# -- Figure 3 & 4 -- Bz
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, Bz_XY, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	# ax.streamplot(X_norm,Y_norm,Bx_XY,By_XY,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	ax.streamplot(X_norm,Y_norm,np.transpose(Bx_XY),np.transpose(By_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Bz [nT] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Bz_XY_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, Bz_XZ, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	# ax.streamplot(X_norm,Z_norm,Bx_XZ,Bz_XZ,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	ax.streamplot(X_norm,Z_norm,np.transpose(Bx_XZ),np.transpose(Bz_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Bz [nT] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Bz_XZ_Europa_"+rundate+"_t"+diagtime+".png")

	# -- Figure 3 & 4 -- Btot
	# **************************************************************************

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XY, Y_XY, Btot_XY, vmin=min_valtot, vmax=max_valtot, cmap=colmap[ncol],shading='auto')
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Ymin,Ymax)

	titre = "Btot [nT] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Btot_XY_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
	c = ax.pcolor(X_XZ, Z_XZ, Btot_XZ, vmin=min_valtot, vmax=max_valtot, cmap=colmap[ncol],shading='auto')
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Xmin,Xmax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Btot [nT] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Btot_XZ_Europa_"+rundate+"_t"+diagtime+".png")

	plt.close('all')
	# =========== figure in YZ plane ==============================
	# Bx in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Bx_YZ_term, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	# ax.streamplot(Y_norm,Z_norm,By_YZ_term,Bz_YZ_term,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	ax.streamplot(Y_norm,Z_norm,np.transpose(By_YZ_term),np.transpose(Bz_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Bx [nT] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Bx_YZ_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Bx_YZ_wake, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	# ax.streamplot(Y_norm,Z_norm,By_YZ_wake,Bz_YZ_wake,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	ax.streamplot(Y_norm,Z_norm,np.transpose(By_YZ_wake),np.transpose(Bz_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Bx [nT] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Bx_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

	# By in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,By_YZ_term, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	# ax.streamplot(Y_norm,Z_norm,By_YZ_term,Bz_YZ_term,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	ax.streamplot(Y_norm,Z_norm,np.transpose(By_YZ_term),np.transpose(Bz_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "By [nT] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"By_YZ_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,By_YZ_wake, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	# ax.streamplot(Y_norm,Z_norm,By_YZ_wake,Bz_YZ_wake,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	ax.streamplot(Y_norm,Z_norm,np.transpose(By_YZ_wake),np.transpose(Bz_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "By [nT] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"By_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

	# Bz in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Bz_YZ_term, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	# ax.streamplot(Y_norm,Z_norm,By_YZ_term,Bz_YZ_term,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	ax.streamplot(Y_norm,Z_norm,np.transpose(By_YZ_term),np.transpose(Bz_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Bz [nT] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Bz_YZ_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Bz_YZ_wake, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
	fig.colorbar(c, ax=ax)
	# ax.streamplot(Y_norm,Z_norm,By_YZ_wake,Bz_YZ_wake,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	ax.streamplot(Y_norm,Z_norm,np.transpose(By_YZ_wake),np.transpose(Bz_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Bz [nT] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Bz_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

	# Btot in X=0 and X=1.5Rm

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Btot_YZ_term, vmin=min_valtot, vmax=max_valtot, cmap=colmap[ncol],shading='auto')
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Btot [nT] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Btot_YZ_Europa_"+rundate+"_t"+diagtime+".png")

	# --

	if zoom == True:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	
	else:
		fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
	c = ax.pcolor(Y_YZ,Z_YZ,Btot_YZ_wake, vmin=min_valtot, vmax=max_valtot, cmap=colmap[ncol],shading='auto')
	if radius != 1:
		ax.plot(xp,yp,c="black")
		# ax.fill(xp,yp,c="white")
		if buffer_zone == True:
			ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
			ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
			ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
	fig.colorbar(c, ax=ax)
	ax.set_xlim(Ymin,Ymax)
	ax.set_ylim(Zmin,Zmax)

	titre = "Btot [nT] time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
	ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+"Btot_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

	plt.close('all')