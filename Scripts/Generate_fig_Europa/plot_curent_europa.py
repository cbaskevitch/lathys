######################
# Plot_curent_europa.py
# ---------------------
# This routine reads the magnetic
# field file and plot the module
# and the J components in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

# def plot_curent(src_dir,dest_dir,typefile,rundate,diagtime,zoom,field_lines_dens,buffer_zone):
zoom=True
buffer_zone=False
field_lines_dens = 4
ncfile = sys.argv[1] #MagJw file
dest_dir = sys.argv[2]
rundate = sys.argv[3]
diagtime = sys.argv[4]

ncid = Dataset(ncfile)
var_nc = ncid.variables

# planetname = var_nc['planetname'][:]
centr      = var_nc['s_centr'][:]
radius     = var_nc['r_planet'][:]
gs         = var_nc['gstep'][:]
Jx         = var_nc['Jx'][:]
Jy         = var_nc['Jy'][:]
Jz         = var_nc['Jz'][:]
Jxperp     = var_nc['Jxperp'][:]
Jyperp     = var_nc['Jyperp'][:]
Jzperp     = var_nc['Jzperp'][:]
# Jx=Jx*4.*np.pi*0.0000001#/4.0
# Jy=Jy*4.*np.pi*0.0000001#/4.0
# Jz=Jz*4.*np.pi*0.0000001#/4.0
# Jxperp=Jxperp*4.*np.pi*0.0000001#/4.0
# Jyperp=Jyperp*4.*np.pi*0.0000001#/4.0
# Jzperp=Jzperp*4.*np.pi*0.0000001#/4.0
# nrm        = var_nc['phys_mag'][:]
nrm_len    = var_nc['phys_length'][:]

# radius=1
nc = [len(Jx[0][0]), len(Jx[0]), len(Jx)]
print("nc = ",nc[0],nc[1],nc[2])

Jtot = np.sqrt(Jx*Jx + Jy*Jy + Jz*Jz)

# maximum and minimum 
if radius > 1.0:
	# min_val    = [20.0,-180.0,-450.0]
	# max_val    = [100.0,-100.0,-380.0]
	min_val    = [-5.0,-5.0,-5.0]
	max_val    = [5.0,5.0,5.0]
	# min_val    = [-150.0,-50.0,-450.0]
	# max_val    = [150.0,100.0,-350.0]
	min_valtot = 0.0   # nT
	max_valtot = 50.0 # nT
else:
	min_val    = [74.1,-135.2,-422.8] #[74.1,-135.2,-423.0] # nT [Jx,Jy,Jz] for run without planete
	max_val    = [74.5,-134.8,-422.65]#[74.5,-134.8,-422.5]  # nT [Jx,Jy,Jz] for run without planete
	min_valtot = 440.0   # nT
	max_valtot = 460.0 # nT

# -- Creation of axis values centered on the planet ( normalized to planet radius)
X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide(centr[0]*np.ones((nc[0],nc[1])), radius)
Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide(centr[1]*np.ones((nc[0],nc[1])), radius)

X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide(centr[0]*np.ones((nc[0],nc[2])), radius)
Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide(centr[2]*np.ones((nc[0],nc[2])), radius)

Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide(centr[1]*np.ones((nc[1],nc[2])), radius)
Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide(centr[2]*np.ones((nc[1],nc[2])), radius)

# planet center in cell number (NJ: cell number start at 0
icentr = int(np.fix(centr[0]/gs[0]))
jcentr = int(np.fix(centr[1]/gs[1]))
kcentr = int(np.fix(centr[2]/gs[2]))
iwake = int(icentr + np.fix(1.5*radius/gs[0]))
# rp = radius/gs[0]


Jx_XY        = np.zeros((nc[0],nc[1]))
Jx_XY[:,:]   = np.matrix.transpose(Jx[kcentr,:,:])
Jy_XY        = np.zeros((nc[0],nc[1]))
Jy_XY[:,:]   = np.matrix.transpose(Jy[kcentr,:,:])
Jz_XY        = np.zeros((nc[0],nc[1]))
Jz_XY[:,:]   = np.matrix.transpose(Jz[kcentr,:,:])
Jtot_XY      = np.zeros((nc[0],nc[1]))
Jtot_XY[:,:] = np.matrix.transpose(Jtot[kcentr,:,:])

Jxperp_XY        = np.zeros((nc[0],nc[1]))
Jxperp_XY[:,:]   = np.matrix.transpose(Jxperp[kcentr,:,:])
Jyperp_XY        = np.zeros((nc[0],nc[1]))
Jyperp_XY[:,:]   = np.matrix.transpose(Jyperp[kcentr,:,:])
Jzperp_XY        = np.zeros((nc[0],nc[1]))
Jzperp_XY[:,:]   = np.matrix.transpose(Jzperp[kcentr,:,:])


Jx_XZ        = np.zeros((nc[0],nc[2]))
Jx_XZ[:,:]   = np.matrix.transpose(Jx[:,jcentr,:])
Jy_XZ        = np.zeros((nc[0],nc[2]))
Jy_XZ[:,:]   = np.matrix.transpose(Jy[:,jcentr,:])
Jz_XZ        = np.zeros((nc[0],nc[2]))
Jz_XZ[:,:]   = np.matrix.transpose(Jz[:,jcentr,:])
Jtot_XZ      = np.zeros((nc[0],nc[2]))
Jtot_XZ[:,:] = np.matrix.transpose(Jtot[:,jcentr,:])

Jxperp_XZ        = np.zeros((nc[0],nc[2]))
Jxperp_XZ[:,:]   = np.matrix.transpose(Jxperp[:,jcentr,:])
Jyperp_XZ        = np.zeros((nc[0],nc[2]))
Jyperp_XZ[:,:]   = np.matrix.transpose(Jyperp[:,jcentr,:])
Jzperp_XZ        = np.zeros((nc[0],nc[2]))
Jzperp_XZ[:,:]   = np.matrix.transpose(Jzperp[:,jcentr,:])


Jx_YZ_term        = np.zeros((nc[1],nc[2]))
Jx_YZ_term[:,:]   = np.matrix.transpose(Jx[:,:,icentr])
Jy_YZ_term        = np.zeros((nc[1],nc[2]))
Jy_YZ_term[:,:]   = np.matrix.transpose(Jy[:,:,icentr])
Jz_YZ_term        = np.zeros((nc[1],nc[2]))
Jz_YZ_term[:,:]   = np.matrix.transpose(Jz[:,:,icentr])
Jtot_YZ_term      = np.zeros((nc[1],nc[2]))
Jtot_YZ_term[:,:] = np.matrix.transpose(Jtot[:,:,icentr])

Jxperp_YZ_term        = np.zeros((nc[1],nc[2]))
Jxperp_YZ_term[:,:]   = np.matrix.transpose(Jxperp[:,:,icentr])
Jyperp_YZ_term        = np.zeros((nc[1],nc[2]))
Jyperp_YZ_term[:,:]   = np.matrix.transpose(Jyperp[:,:,icentr])
Jzperp_YZ_term        = np.zeros((nc[1],nc[2]))
Jzperp_YZ_term[:,:]   = np.matrix.transpose(Jzperp[:,:,icentr])


# Jx_YZ_wake        = np.zeros((nc[1],nc[2]))
# Jx_YZ_wake[:,:]   = np.matrix.transpose(Jx[:,:,iwake])
# Jy_YZ_wake        = np.zeros((nc[1],nc[2]))
# Jy_YZ_wake[:,:]   = np.matrix.transpose(Jy[:,:,iwake])
# Jz_YZ_wake        = np.zeros((nc[1],nc[2]))
# Jz_YZ_wake[:,:]   = np.matrix.transpose(Jz[:,:,iwake])
# Jtot_YZ_wake      = np.zeros((nc[1],nc[2]))
# Jtot_YZ_wake[:,:] = np.matrix.transpose(Jtot[:,:,iwake])



# planet drawing
theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
xp = np.cos(theta)
yp = np.sin(theta)

## Field lines
Xmin=X_XY[0][0] #float(-1*icentr/rp)
Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1] #float((nc[0]-icentr)/rp)
# print("xmin = ",Xmin, " xmax = ",Xmax, "pas = ", (Xmax-Xmin)/(nc[0]-1))
# X_norm=np.arange(Xmin, Xmax, (Xmax-Xmin)/(nc[0]))
X_norm = np.linspace(0 , (nc[0]-1) * gs[0] , num=nc[0]) / radius - centr[0]/radius
Ymin=Y_XY[0][0] #float(-jcentr/rp)
Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1] #float((nc[1]-jcentr)/rp)
# Y_norm=np.arange(Ymin, Ymax, (Ymax-Ymin)/(nc[1]))
Y_norm = np.linspace(0 , (nc[1]-1) * gs[1] , num=nc[1])/ radius - centr[1]/radius
Zmin=Z_XZ[0][0] #float(-kcentr/rp)
Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1] #float((nc[1]-kcentr)/rp)
# Z_norm=np.arange(Zmin, Zmax, (Zmax-Zmin)/(nc[2]))
Z_norm = np.linspace(0 , (nc[2]-1) * gs[2] , num=nc[2])/ radius - centr[2]/radius

fig_size = [[6,9.5],[6,7.5],[8,6],[10,6],[7,6]] #differentes tailles de fenetres
figsize_Xnum = 2  #numero de la taille de la fenetre pour les plans XZ et XY
figsize_Ynum = 2  #numero de la taille de la fenetre pour les plans YZ

colmap = ["viridis","inferno","GnBu_r"]
ncol = 0  # numero de la colormap

if zoom == True:
	Xmin = -4.0
	Xmax = 4.0
	Ymin = -4.0
	Ymax = 4.0
	Zmin = -4.0
	Zmax = 4.0

print("Xmin",Xmin,"Xmax",Xmax)
print("Ymin",Ymin,"Ymax",Ymax)
print("Zmin",Zmin,"Zmax",Zmax)
print("maxJx",np.max(Jx),"minJx",np.min(Jx))
print("maxJy",np.max(Jy),"minJy",np.min(Jy))
print("maxJz",np.max(Jz),"minJz",np.min(Jz))
print("maxJtot",np.max(Jtot),"minJtot",np.min(Jtot))

print(len(X_norm),len(Y_norm))
print(np.size(Jx_XY),len(Jx_XY[0]),len(Jy_XY),len(Jy_XY[0]))
# print(np.transpose(len(Jx_XY)),np.transpose(len(Jx_XY[0])),np.transpose(len(Jy_XY)),np.transpose(len(Jy_XY[0])))

# for i in range(0,nc[1]-1):
# 	plt.plot(i,Y_norm[i+1]-Y_norm[i],'.')
# 	print("i ",i, Y_norm[i+1]-Y_norm[i])
# plt.show()

# -- Figure 1 & 2 -- Jx
# **************************************************************************

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XY, Y_XY, Jx_XY, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(np.transpose(X_norm),Y_norm,np.transpose(Jx_XY),np.transpose(Jy_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
		ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Ymin,Ymax)

titre = "Jx [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

# plt.show()
plt.savefig(dest_dir+"Jx_XY_Europa_"+rundate+"_t"+diagtime+".png")
# --

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XZ, Z_XZ, Jx_XZ, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(X_norm,Z_norm,np.transpose(Jx_XZ),np.transpose(Jz_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
		ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Zmin,Zmax)

titre = "Jx [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jx_XZ_Europa_"+rundate+"_t"+diagtime+".png")
# plt.show()

# -- Figure 3 & 4 -- Jy
# **************************************************************************

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XY, Y_XY, Jy_XY, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(X_norm,Y_norm,np.transpose(Jx_XY),np.transpose(Jy_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
		ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Ymin,Ymax)

titre = "Jy [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jy_XY_Europa_"+rundate+"_t"+diagtime+".png")

# --

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XZ, Z_XZ, Jy_XZ, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(X_norm,Z_norm,np.transpose(Jx_XZ),np.transpose(Jz_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
		ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Zmin,Zmax)

titre = "Jy [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jy_XZ_Europa_"+rundate+"_t"+diagtime+".png")

# -- Figure 3 & 4 -- Jz
# **************************************************************************

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XY, Y_XY, Jz_XY, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(X_norm,Y_norm,np.transpose(Jx_XY),np.transpose(Jy_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
		ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Ymin,Ymax)

titre = "Jz [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jz_XY_Europa_"+rundate+"_t"+diagtime+".png")

# --

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XZ, Z_XZ, Jz_XZ, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(X_norm,Z_norm,np.transpose(Jx_XZ),np.transpose(Jz_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
		ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Zmin,Zmax)

titre = "Jz [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jz_XZ_Europa_"+rundate+"_t"+diagtime+".png")

# -- Figure 3 & 4 -- Jtot
# **************************************************************************

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XY, Y_XY, Jtot_XY, vmin=min_valtot, vmax=max_valtot, cmap=colmap[ncol],shading='auto')
ax.streamplot(X_norm,Y_norm,np.transpose(Jx_XY),np.transpose(Jy_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)

if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
		ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
fig.colorbar(c, ax=ax)
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Ymin,Ymax)

titre = "Jtot [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jtot_XY_Europa_"+rundate+"_t"+diagtime+".png")

# --

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XZ, Z_XZ, Jtot_XZ, vmin=min_valtot, vmax=max_valtot, cmap=colmap[ncol],shading='auto')
ax.streamplot(X_norm,Z_norm,np.transpose(Jx_XZ),np.transpose(Jz_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
		ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
fig.colorbar(c, ax=ax)
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Zmin,Zmax)

titre = "Jtot [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jtot_XZ_Europa_"+rundate+"_t"+diagtime+".png")

# =========== figure in YZ plane ==============================
# Jx in X=0 and X=1.5Rm

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
c = ax.pcolor(Y_YZ,Z_YZ,Jx_YZ_term, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(Y_norm,Z_norm,np.transpose(Jy_YZ_term),np.transpose(Jz_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
		ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
		ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
		ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "Jx [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jx_YZ_Europa_"+rundate+"_t"+diagtime+".png")

# --

# if zoom == True:
# 	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])

# else:
# 	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
# c = ax.pcolor(Y_YZ,Z_YZ,Jx_YZ_wake, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
# fig.colorbar(c, ax=ax)
# # #ax.streamplot(Y_norm,Z_norm,Jy_YZ_wake,Jz_YZ_wake,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
# #ax.streamplot(Y_norm,Z_norm,np.transpose(Jy_YZ_wake),np.transpose(Jz_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
# if radius != 1:
# 	ax.plot(xp,yp,c="black")
# 	ax.fill(xp,yp,c="white")
# 	if buffer_zone == True:
# 		ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# 		ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# 		ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# 		ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# ax.set_xlim(Ymin,Ymax)
# ax.set_ylim(Zmin,Zmax)

# titre = "Jx [nA.m-2] time: "+diagtime
# plt.title(titre)#,'fontsize',12,'fontweight','b');
# ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
# ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

# plt.savefig(dest_dir+"Jx_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

# Jy in X=0 and X=1.5Rm

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
c = ax.pcolor(Y_YZ,Z_YZ,Jy_YZ_term, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(Y_norm,Z_norm,np.transpose(Jy_YZ_term),np.transpose(Jz_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
		ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
		ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
		ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "Jy [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jy_YZ_Europa_"+rundate+"_t"+diagtime+".png")

# --

# if zoom == True:
# 	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])

# else:
# 	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
# c = ax.pcolor(Y_YZ,Z_YZ,Jy_YZ_wake, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
# fig.colorbar(c, ax=ax)
# # #ax.streamplot(Y_norm,Z_norm,Jy_YZ_wake,Jz_YZ_wake,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
# #ax.streamplot(Y_norm,Z_norm,np.transpose(Jy_YZ_wake),np.transpose(Jz_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
# if radius != 1:
# 	ax.plot(xp,yp,c="black")
# 	ax.fill(xp,yp,c="white")
# 	if buffer_zone == True:
# 		ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# 		ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# 		ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# 		ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# ax.set_xlim(Ymin,Ymax)
# ax.set_ylim(Zmin,Zmax)

# titre = "Jy [nA.m-2] time: "+diagtime
# plt.title(titre)#,'fontsize',12,'fontweight','b');
# ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
# ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

# plt.savefig(dest_dir+"Jy_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

# Jz in X=0 and X=1.5Rm

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
c = ax.pcolor(Y_YZ,Z_YZ,Jz_YZ_term, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(Y_norm,Z_norm,np.transpose(Jy_YZ_term),np.transpose(Jz_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
		ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
		ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
		ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "Jz [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jz_YZ_Europa_"+rundate+"_t"+diagtime+".png")

# --

# if zoom == True:
# 	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])

# else:
# 	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
# c = ax.pcolor(Y_YZ,Z_YZ,Jz_YZ_wake, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
# fig.colorbar(c, ax=ax)
# # #ax.streamplot(Y_norm,Z_norm,Jy_YZ_wake,Jz_YZ_wake,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
# #ax.streamplot(Y_norm,Z_norm,np.transpose(Jy_YZ_wake),np.transpose(Jz_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
# if radius != 1:
# 	ax.plot(xp,yp,c="black")
# 	ax.fill(xp,yp,c="white")
# 	if buffer_zone == True:
# 		ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# 		ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# 		ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# 		ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# ax.set_xlim(Ymin,Ymax)
# ax.set_ylim(Zmin,Zmax)

# titre = "Jz [nA.m-2] time: "+diagtime
# plt.title(titre)#,'fontsize',12,'fontweight','b');
# ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
# ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

# plt.savefig(dest_dir+"Jz_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

# Jtot in X=0 and X=1.5Rm

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
c = ax.pcolor(Y_YZ,Z_YZ,Jtot_YZ_term, vmin=min_valtot, vmax=max_valtot, cmap=colmap[ncol],shading='auto')
ax.streamplot(Y_norm,Z_norm,np.transpose(Jy_YZ_term),np.transpose(Jz_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
		ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
		ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
		ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
fig.colorbar(c, ax=ax)
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "Jtot [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jtot_YZ_Europa_"+rundate+"_t"+diagtime+".png")

#--

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
c = ax.pcolor(Y_YZ,Z_YZ,Jtot_YZ_wake, vmin=min_valtot, vmax=max_valtot, cmap=colmap[ncol],shading='auto')
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
		ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
		ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
		ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
fig.colorbar(c, ax=ax)
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "Jtot [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jtot_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

plt.close('all')




if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XY, Y_XY, Jxperp_XY, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(np.transpose(X_norm),Y_norm,np.transpose(Jxperp_XY),np.transpose(Jyperp_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
		ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Ymin,Ymax)

titre = "Jxperp [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

# plt.show()
plt.savefig(dest_dir+"Jxperp_XY_Europa_"+rundate+"_t"+diagtime+".png")
# --

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XZ, Z_XZ, Jxperp_XZ, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(X_norm,Z_norm,np.transpose(Jxperp_XZ),np.transpose(Jzperp_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
		ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Zmin,Zmax)

titre = "Jxperp [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jxperp_XZ_Europa_"+rundate+"_t"+diagtime+".png")
# plt.show()

plt.close('all')

# -- Figure 3 & 4 -- Jy
# **************************************************************************

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XY, Y_XY, Jyperp_XY, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(X_norm,Y_norm,np.transpose(Jxperp_XY),np.transpose(Jyperp_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
		ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Ymin,Ymax)

titre = "Jyperp [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jyperp_XY_Europa_"+rundate+"_t"+diagtime+".png")

# --

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XZ, Z_XZ, Jyperp_XZ, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(X_norm,Z_norm,np.transpose(Jxperp_XZ),np.transpose(Jzperp_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
		ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Zmin,Zmax)

titre = "Jyperp [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jyperp_XZ_Europa_"+rundate+"_t"+diagtime+".png")

plt.close('all')

# -- Figure 3 & 4 -- Jz
# **************************************************************************

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XY, Y_XY, Jzperp_XY, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(X_norm,Y_norm,np.transpose(Jxperp_XY),np.transpose(Jyperp_XY),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Ymin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
		ax.hlines(Ymax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Ymin,Ymax)

titre = "Jzperp [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jzperp_XY_Europa_"+rundate+"_t"+diagtime+".png")

# --

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XZ, Z_XZ, Jzperp_XZ, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(X_norm,Z_norm,np.transpose(Jxperp_XZ),np.transpose(Jzperp_XZ),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Zmin+10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
		ax.hlines(Zmax-10*gs[0]/radius,xmin=Xmin,xmax=Xmax,color="black",ls="--")
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Zmin,Zmax)

titre = "Jzperp [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jzperp_XZ_Europa_"+rundate+"_t"+diagtime+".png")

plt.close('all')


# =========== figure in YZ plane ==============================
# Jx in X=0 and X=1.5Rm

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
c = ax.pcolor(Y_YZ,Z_YZ,Jxperp_YZ_term, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(Y_norm,Z_norm,np.transpose(Jyperp_YZ_term),np.transpose(Jzperp_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
		ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
		ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
		ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "Jxperp [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jxperp_YZ_Europa_"+rundate+"_t"+diagtime+".png")

# --

# if zoom == True:
# 	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])

# else:
# 	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
# c = ax.pcolor(Y_YZ,Z_YZ,Jx_YZ_wake, vmin=min_val[0], vmax=max_val[0], cmap=colmap[ncol],shading='auto')
# fig.colorbar(c, ax=ax)
# # #ax.streamplot(Y_norm,Z_norm,Jy_YZ_wake,Jz_YZ_wake,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
# #ax.streamplot(Y_norm,Z_norm,np.transpose(Jy_YZ_wake),np.transpose(Jz_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
# if radius != 1:
# 	ax.plot(xp,yp,c="black")
# 	ax.fill(xp,yp,c="white")
# 	if buffer_zone == True:
# 		ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# 		ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# 		ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# 		ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# ax.set_xlim(Ymin,Ymax)
# ax.set_ylim(Zmin,Zmax)

# titre = "Jx [nA.m-2] time: "+diagtime
# plt.title(titre)#,'fontsize',12,'fontweight','b');
# ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
# ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

# plt.savefig(dest_dir+"Jx_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

# Jy in X=0 and X=1.5Rm

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])

else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
c = ax.pcolor(Y_YZ,Z_YZ,Jyperp_YZ_term, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(Y_norm,Z_norm,np.transpose(Jyperp_YZ_term),np.transpose(Jzperp_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
		ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
		ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
		ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "Jyperp [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jyperp_YZ_Europa_"+rundate+"_t"+diagtime+".png")

plt.close('all')
# --

# if zoom == True:
# 	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])

# else:
# 	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
# c = ax.pcolor(Y_YZ,Z_YZ,Jy_YZ_wake, vmin=min_val[1], vmax=max_val[1], cmap=colmap[ncol],shading='auto')
# fig.colorbar(c, ax=ax)
# # #ax.streamplot(Y_norm,Z_norm,Jy_YZ_wake,Jz_YZ_wake,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
# #ax.streamplot(Y_norm,Z_norm,np.transpose(Jy_YZ_wake),np.transpose(Jz_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
# if radius != 1:
# 	ax.plot(xp,yp,c="black")
# 	ax.fill(xp,yp,c="white")
# 	if buffer_zone == True:
# 		ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# 		ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# 		ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# 		ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# ax.set_xlim(Ymin,Ymax)
# ax.set_ylim(Zmin,Zmax)

# titre = "Jy [nA.m-2] time: "+diagtime
# plt.title(titre)#,'fontsize',12,'fontweight','b');
# ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
# ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

# plt.savefig(dest_dir+"Jy_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

# Jz in X=0 and X=1.5Rm

if zoom == True:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
else:
	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
c = ax.pcolor(Y_YZ,Z_YZ,Jzperp_YZ_term, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
fig.colorbar(c, ax=ax)
ax.streamplot(Y_norm,Z_norm,np.transpose(Jyperp_YZ_term),np.transpose(Jzperp_YZ_term),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
if radius != 1:
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")
	if buffer_zone == True:
		ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
		ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
		ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
		ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "Jzperp [nA.m-2] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"Jzperp_YZ_Europa_"+rundate+"_t"+diagtime+".png")

# --

# if zoom == True:
# 	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])

# else:
# 	fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
# c = ax.pcolor(Y_YZ,Z_YZ,Jz_YZ_wake, vmin=min_val[2], vmax=max_val[2], cmap=colmap[ncol],shading='auto')
# fig.colorbar(c, ax=ax)
# # #ax.streamplot(Y_norm,Z_norm,Jy_YZ_wake,Jz_YZ_wake,color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
# #ax.streamplot(Y_norm,Z_norm,np.transpose(Jy_YZ_wake),np.transpose(Jz_YZ_wake),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)
# if radius != 1:
# 	ax.plot(xp,yp,c="black")
# 	ax.fill(xp,yp,c="white")
# 	if buffer_zone == True:
# 		ax.hlines(Zmin+10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# 		ax.hlines(Zmax-10*gs[0]/radius,xmin=Ymin,xmax=Ymax,color="black",ls="--")
# 		ax.vlines(Ymin+10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# 		ax.vlines(Ymax-10*gs[0]/radius,ymin=Zmin,ymax=Zmax,color="black",ls="--")
# ax.set_xlim(Ymin,Ymax)
# ax.set_ylim(Zmin,Zmax)

# titre = "Jz [nA.m-2] time: "+diagtime
# plt.title(titre)#,'fontsize',12,'fontweight','b');
# ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
# ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

# plt.savefig(dest_dir+"Jz_YZ_wake_Europa_"+rundate+"_t"+diagtime+".png")

plt.close('all')