##############################
# Generate_Figure.py
# ----------------------
# This routine generate automatically
# pdf files of different quantities
# in XY and XZ plane
# Quantities are :
#    - Magnetic field (module and components)
#    - Electric field (module and components)
#    - Electron number density
#    - Bulk speed ( module and components)
#    - Ion density (for each species)
#    - Neutral density (for each species)
#   (- Production for each species)
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
##############################

from plot_multigrid_density_ne_europa import *
from plot_multigrid_ion_density_europa import *
from plot_multigrid_bulk_speed_europa import *
from plot_multigrid_magnetic_field_europa import *
from plot_multigrid_electric_field_europa import *
from plot_multigrid_neutral_density_europa import *
import sys, os

src_dir = sys.argv[1]+"/"
dest_dir = sys.argv[2]+"/"
rundate = sys.argv[3]
diagtime = sys.argv[4]

zoom = False
if zoom == False:
	field_lines_dens = 2
else:
	field_lines_dens = 4

dest_dir_r = dest_dir+"grille_raffinee/"
cmd="mkdir "+dest_dir_r
print(cmd)
os.system(cmd)


print("Electron density")
plot_multigrid_density_ne(src_dir,dest_dir,dest_dir_r,'Thew_',rundate,diagtime,zoom)
print("Ion density")
plot_multigrid_ion_density(src_dir,dest_dir,dest_dir_r,"Read_moment_species_",rundate,diagtime,zoom)
print("Magnetic field")
plot_multigrid_magnetic_field(src_dir,dest_dir,dest_dir_r,'Magw_',rundate,diagtime,zoom,field_lines_dens)
print("Bulk speed")
plot_multigrid_bulk_speed(src_dir,dest_dir,dest_dir_r,'Thew_',rundate,diagtime,zoom)
print("Electric field")
plot_multigrid_electric_field(src_dir,dest_dir,dest_dir_r,'Elew_',rundate,diagtime,zoom,field_lines_dens)
# plot_multigrid_neutral_density(src_dir,dest_dir,dest_dir_r,"Atmw_",rundate,diagtime,zoom)
