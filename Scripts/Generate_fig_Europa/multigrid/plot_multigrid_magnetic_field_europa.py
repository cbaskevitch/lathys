######################
# Plot_magnetic_field_Europa.py
# ---------------------
# This routine reads the magnetic
# field file and plot the module
# and the B components in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os
from multigrid_shared_plot_functions import *


def plot_multigrid_magnetic_field(src_dir,dest_dir,dest_dir_r,typefile,rundate,diagtime,zoom,field_lines_dens):
	ncfile = src_dir + typefile + rundate + "_t" + diagtime + '.nc'

	ncid = Dataset(ncfile)
	var_nc = ncid.variables

	planetname = var_nc['planetname'][:]
	centr      = var_nc['s_centr'][:]
	radius     = var_nc['r_planet'][:]
	gs         = var_nc['gstep'][:]
	gs_r       = var_nc['gstep_r'][:]
	s_min_r    = var_nc['s_min_r'][:] #indice de la premiere cellule de la grille rafinee dans la grande grille
	Bx         = var_nc['Bx'][:]
	By         = var_nc['By'][:]
	Bz         = var_nc['Bz'][:]
	Bx_r       = var_nc['Bx_r'][:]
	By_r       = var_nc['By_r'][:]
	Bz_r       = var_nc['Bz_r'][:]
	nrm        = var_nc['phys_mag'][:]
	nrm_len    = var_nc['phys_length'][:]

	# radius=1.0
	nc = [len(Bx[0][0]), len(Bx[0]), len(Bx)]
	nc_r = [len(Bx_r[0][0]), len(Bx_r[0]), len(Bx_r)]

	Btot = np.sqrt(Bx*Bx + By*By + Bz*Bz)
	Btot_r = np.sqrt(Bx_r*Bx_r + By_r*By_r + Bz_r*Bz_r)

	# maximum and minimum 
	if radius > 1.0:
		# B E4 flyby
		min_val    = [-50.0,-180.0,-450.0]
		max_val    = [100.0,-120.0,-350.0]
		min_valtot = 400.0   # nT
		max_valtot = 500.0 # nT

		# B JUICE E2 flyby
		# min_val    = [-50.0,180.0,-500.0]
		# max_val    = [100.0,100.0,-420.0]
		# min_valtot = 400.0   # nT
		# max_valtot = 550.0 # nT
		
		# B along Z
		# min_val    = [-75.0,-50.0,-500.0]
		# max_val    = [75.0,50.0,-400.0]
		
	else:
		min_val    = [74.1,-135.2,-422.8] #[74.1,-135.2,-423.0] # nT [Bx,By,Bz] for run without planete
		max_val    = [74.5,-134.8,-422.65]#[74.5,-134.8,-422.5]  # nT [Bx,By,Bz] for run without planete
		min_valtot = 440.0   # nT
		max_valtot = 460.0 # nT


	# -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide(centr[0]*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide(centr[1]*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide(centr[0]*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide(centr[2]*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide(centr[1]*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide(centr[2]*np.ones((nc[1],nc[2])), radius)

	# -- Creation of axis values centered on the planet ( normalized to planet radius) for the small grid
	X_XY_r, Y_XY_r = np.meshgrid(np.arange(0,(nc_r[0])*gs_r[0],gs_r[0]),np.arange(0,(nc_r[1])*gs_r[1],gs_r[1]))
	X_XY_r = np.divide(np.matrix.transpose(X_XY_r), radius) - np.divide((centr[0]-s_min_r[0])*np.ones((nc_r[0],nc_r[1])), radius)
	Y_XY_r = np.divide(np.matrix.transpose(Y_XY_r), radius) - np.divide((centr[1]-s_min_r[1])*np.ones((nc_r[0],nc_r[1])), radius)

	X_XZ_r, Z_XZ_r = np.meshgrid(np.arange(0,(nc_r[0])*gs_r[0],gs_r[0]),np.arange(0,(nc_r[2])*gs_r[2],gs_r[2]))
	X_XZ_r = np.divide(np.matrix.transpose(X_XZ_r), radius) - np.divide((centr[0]-s_min_r[0])*np.ones((nc_r[0],nc_r[2])), radius)
	Z_XZ_r = np.divide(np.matrix.transpose(Z_XZ_r), radius) - np.divide((centr[2]-s_min_r[2])*np.ones((nc_r[0],nc_r[2])), radius)

	Y_YZ_r, Z_YZ_r = np.meshgrid(np.arange(0,(nc_r[1])*gs_r[1],gs_r[1]),np.arange(0,(nc_r[2])*gs_r[2],gs_r[2]))
	Y_YZ_r = np.divide(np.matrix.transpose(Y_YZ_r), radius) - np.divide((centr[1]-s_min_r[1])*np.ones((nc_r[1],nc_r[2])), radius)
	Z_YZ_r = np.divide(np.matrix.transpose(Z_YZ_r), radius) - np.divide((centr[2]-s_min_r[2])*np.ones((nc_r[1],nc_r[2])), radius)

	# planet center in cell number (NB: cell number start at 0
	icentr = int(np.fix(centr[0]/gs[0]))
	jcentr = int(np.fix(centr[1]/gs[1]))
	kcentr = int(np.fix(centr[2]/gs[2]))
	iwake = int(icentr + np.fix(1.5*radius/gs[0]))
	rp = radius/gs[0]
	# print(X_XY[icentr,jcentr],Y_XY[icentr,jcentr],Z_XZ[icentr,kcentr])
	
	icentr_r = int(np.fix((centr[0]-s_min_r[0])/gs_r[0]))
	jcentr_r = int(np.fix((centr[1]-s_min_r[1])/gs_r[1]))
	kcentr_r = int(np.fix((centr[2]-s_min_r[2])/gs_r[2]))
	# iwake_r = int(icentr + np.fix(1.5*radius/gs_r[0]))
	# print(X_XY_r[icentr_r,jcentr_r],Y_XY_r[icentr_r,jcentr_r],Z_XZ_r[icentr_r,kcentr_r])

	Bx_XY        = np.zeros((nc[0],nc[1]))
	Bx_XY[:,:]   = np.matrix.transpose(Bx[kcentr,:,:])
	By_XY        = np.zeros((nc[0],nc[1]))
	By_XY[:,:]   = np.matrix.transpose(By[kcentr,:,:])
	Bz_XY        = np.zeros((nc[0],nc[1]))
	Bz_XY[:,:]   = np.matrix.transpose(Bz[kcentr,:,:])
	Btot_XY      = np.zeros((nc[0],nc[1]))
	Btot_XY[:,:] = np.matrix.transpose(Btot[kcentr,:,:])

	Bx_XZ        = np.zeros((nc[0],nc[2]))
	Bx_XZ[:,:]   = np.matrix.transpose(Bx[:,jcentr,:])
	By_XZ        = np.zeros((nc[0],nc[2]))
	By_XZ[:,:]   = np.matrix.transpose(By[:,jcentr,:])
	Bz_XZ        = np.zeros((nc[0],nc[2]))
	Bz_XZ[:,:]   = np.matrix.transpose(Bz[:,jcentr,:])
	Btot_XZ      = np.zeros((nc[0],nc[2]))
	Btot_XZ[:,:] = np.matrix.transpose(Btot[:,jcentr,:])

	Bx_YZ_term        = np.zeros((nc[1],nc[2]))
	Bx_YZ_term[:,:]   = np.matrix.transpose(Bx[:,:,icentr])
	By_YZ_term        = np.zeros((nc[1],nc[2]))
	By_YZ_term[:,:]   = np.matrix.transpose(By[:,:,icentr])
	Bz_YZ_term        = np.zeros((nc[1],nc[2]))
	Bz_YZ_term[:,:]   = np.matrix.transpose(Bz[:,:,icentr])
	Btot_YZ_term      = np.zeros((nc[1],nc[2]))
	Btot_YZ_term[:,:] = np.matrix.transpose(Btot[:,:,icentr])

	Bx_YZ_wake        = np.zeros((nc[1],nc[2]))
	Bx_YZ_wake[:,:]   = np.matrix.transpose(Bx[:,:,iwake])
	By_YZ_wake        = np.zeros((nc[1],nc[2]))
	By_YZ_wake[:,:]   = np.matrix.transpose(By[:,:,iwake])
	Bz_YZ_wake        = np.zeros((nc[1],nc[2]))
	Bz_YZ_wake[:,:]   = np.matrix.transpose(Bz[:,:,iwake])
	Btot_YZ_wake      = np.zeros((nc[1],nc[2]))
	Btot_YZ_wake[:,:] = np.matrix.transpose(Btot[:,:,iwake])

	### refined grid

	Bx_XY_r        = np.zeros((nc_r[0],nc_r[1]))
	Bx_XY_r[:,:]   = np.matrix.transpose(Bx_r[kcentr_r,:,:])
	By_XY_r        = np.zeros((nc_r[0],nc_r[1]))
	By_XY_r[:,:]   = np.matrix.transpose(By_r[kcentr_r,:,:])
	Bz_XY_r        = np.zeros((nc_r[0],nc_r[1]))
	Bz_XY_r[:,:]   = np.matrix.transpose(Bz_r[kcentr_r,:,:])
	Btot_XY_r      = np.zeros((nc_r[0],nc_r[1]))
	Btot_XY_r[:,:] = np.matrix.transpose(Btot_r[kcentr_r,:,:])

	Bx_XZ_r        = np.zeros((nc_r[0],nc_r[2]))
	Bx_XZ_r[:,:]   = np.matrix.transpose(Bx_r[:,jcentr_r,:])
	By_XZ_r        = np.zeros((nc_r[0],nc_r[2]))
	By_XZ_r[:,:]   = np.matrix.transpose(By_r[:,jcentr_r,:])
	Bz_XZ_r        = np.zeros((nc_r[0],nc_r[2]))
	Bz_XZ_r[:,:]   = np.matrix.transpose(Bz_r[:,jcentr_r,:])
	Btot_XZ_r      = np.zeros((nc_r[0],nc_r[2]))
	Btot_XZ_r[:,:] = np.matrix.transpose(Btot_r[:,jcentr_r,:])

	Bx_YZ_term_r        = np.zeros((nc_r[1],nc_r[2]))
	Bx_YZ_term_r[:,:]   = np.matrix.transpose(Bx_r[:,:,icentr_r])
	By_YZ_term_r        = np.zeros((nc_r[1],nc_r[2]))
	By_YZ_term_r[:,:]   = np.matrix.transpose(By_r[:,:,icentr_r])
	Bz_YZ_term_r        = np.zeros((nc_r[1],nc_r[2]))
	Bz_YZ_term_r[:,:]   = np.matrix.transpose(Bz_r[:,:,icentr_r])
	Btot_YZ_term_r      = np.zeros((nc_r[1],nc_r[2]))
	Btot_YZ_term_r[:,:] = np.matrix.transpose(Btot_r[:,:,icentr_r])

	# Bx_1D    = np.zeros(nc[0])
	# Bx_1D[:] = Bx[kcentr,jcentr,:]
	# By_1D    = np.zeros(nc[0])
	# By_1D[:] = By[kcentr,jcentr,:]
	# Bz_1D    = np.zeros(nc[0])
	# Bz_1D[:] = Bz[kcentr,jcentr,:]
	# x = np.divide(np.arange(0, nc[0], 1.0) * gs[0]-centr[0], radius)

	Xmin=np.min(X_XY)#-4.67
	Xmax=np.max(X_XY)#2.36
	Ymin=np.min(Y_XY)#-7.08
	Ymax=np.max(Y_XY)#7.08
	Zmin=np.min(Z_XZ)#-7.08
	Zmax=np.max(Z_XZ)#7.08
	print(Xmax)
	print(Ymin)
	print(Ymax)
	print(Zmin)
	print(Zmax)

	if zoom == True:
		Xmin = -6.0
		Xmax = 6.0
		Ymin = -6.0
		Ymax = 6.0
		Zmin = -6.0
		Zmax = 6.0
	else:
		Xmin=X_XY[0][0]
		Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1]
		Ymin=Y_XY[0][0]
		Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1]
		Zmin=Z_XZ[0][0]
		Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1]

	fig_size=(7.5,6)
	
	# -- Bx --
	# **************************************************************************
	plot_mltg(dest_dir,rundate,diagtime,"Bx [nT]","Bx_mtlg_XY_Europa",X_XY,Y_XY,Bx_XY,X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Bx_XY_r[:-1,:-1],min_val[0],max_val[0],'X [R_G]','Y [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=Bx_XY,FLdata2=By_XY,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Bx [nT]","Bx_mtlg_r_XY_Europa",X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Bx_XY_r[:-1,:-1],min_val[0],max_val[0],'X [R_G]','Y [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=Bx_XY_r[:-1,:-1],FLdata2=By_XY_r[:-1,:-1])
	plot_mltg(dest_dir,rundate,diagtime,"Bx [nT]","Bx_mtlg_XZ_Europa",X_XZ,Z_XZ,Bx_XZ,X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Bx_XZ_r[:-1,:-1],min_val[0],max_val[0],'X [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=Bx_XZ,FLdata2=Bz_XZ,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Bx [nT]","Bx_mtlg_r_XZ_Europa",X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Bx_XZ_r[:-1,:-1],min_val[0],max_val[0],'X [R_G]','Z [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=Bx_XZ_r[:-1,:-1],FLdata2=Bz_XZ_r[:-1,:-1])
	plot_mltg(dest_dir,rundate,diagtime,"Bx [nT]","Bx_mtlg_YZ_terminator_Europa",Y_YZ,Z_YZ,Bx_YZ_term,Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Bx_YZ_term_r[:-1,:-1],min_val[0],max_val[0],'Y [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=By_YZ_term,FLdata2=Bz_YZ_term,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Bx [nT]","Bx_mtlg_r_YZ_terminator_Europa",Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Bx_YZ_term_r[:-1,:-1],min_val[0],max_val[0],'Y [R_G]','Z [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=By_YZ_term_r[:-1,:-1],FLdata2=Bz_YZ_term_r[:-1,:-1])

	plt.close('all')

	# -- By --
	# **************************************************************************
	plot_mltg(dest_dir,rundate,diagtime,"By [nT]","By_mtlg_XY_Europa",X_XY,Y_XY,By_XY,X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],By_XY_r[:-1,:-1],min_val[1],max_val[1],'X [R_G]','Y [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=Bx_XY,FLdata2=By_XY,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"By [nT]","By_mtlg_r_XY_Europa",X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],By_XY_r[:-1,:-1],min_val[1],max_val[1],'X [R_G]','Y [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=Bx_XY_r[:-1,:-1],FLdata2=By_XY_r[:-1,:-1])
	plot_mltg(dest_dir,rundate,diagtime,"By [nT]","By_mtlg_XZ_Europa",X_XZ,Z_XZ,By_XZ,X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],By_XZ_r[:-1,:-1],min_val[1],max_val[1],'X [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=Bx_XZ,FLdata2=Bz_XZ,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"By [nT]","By_mtlg_r_XZ_Europa",X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],By_XZ_r[:-1,:-1],min_val[1],max_val[1],'X [R_G]','Z [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=Bx_XZ_r[:-1,:-1],FLdata2=Bz_XZ_r[:-1,:-1])
	plot_mltg(dest_dir,rundate,diagtime,"By [nT]","By_mtlg_YZ_terminator_Europa",Y_YZ,Z_YZ,By_YZ_term,Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],By_YZ_term_r[:-1,:-1],min_val[1],max_val[1],'Y [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=By_YZ_term,FLdata2=Bz_YZ_term,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"By [nT]","By_mtlg_r_YZ_terminator_Europa",Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],By_YZ_term_r[:-1,:-1],min_val[1],max_val[1],'Y [R_G]','Z [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=By_YZ_term_r[:-1,:-1],FLdata2=Bz_YZ_term_r[:-1,:-1])

	plt.close('all')
	
	# -- Bz --
	# **************************************************************************
	plot_mltg(dest_dir,rundate,diagtime,"Bz [nT]","Bz_mtlg_XY_Europa",X_XY,Y_XY,Bz_XY,X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Bz_XY_r[:-1,:-1],min_val[2],max_val[2],'X [R_G]','Y [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=Bx_XY,FLdata2=By_XY,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Bz [nT]","Bz_mtlg_r_XY_Europa",X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Bz_XY_r[:-1,:-1],min_val[2],max_val[2],'X [R_G]','Y [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=Bx_XY_r[:-1,:-1],FLdata2=By_XY_r[:-1,:-1])
	plot_mltg(dest_dir,rundate,diagtime,"Bz [nT]","Bz_mtlg_XZ_Europa",X_XZ,Z_XZ,Bz_XZ,X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Bz_XZ_r[:-1,:-1],min_val[2],max_val[2],'X [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=Bx_XZ,FLdata2=Bz_XZ,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Bz [nT]","Bz_mtlg_r_XZ_Europa",X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Bz_XZ_r[:-1,:-1],min_val[2],max_val[2],'X [R_G]','Z [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=Bx_XZ_r[:-1,:-1],FLdata2=Bz_XZ_r[:-1,:-1])
	plot_mltg(dest_dir,rundate,diagtime,"Bz [nT]","Bz_mtlg_YZ_terminator_Europa",Y_YZ,Z_YZ,Bz_YZ_term,Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Bz_YZ_term_r[:-1,:-1],min_val[2],max_val[2],'Y [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=By_YZ_term,FLdata2=Bz_YZ_term,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Bz [nT]","Bz_mtlg_r_YZ_terminator_Europa",Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Bz_YZ_term_r[:-1,:-1],min_val[2],max_val[2],'Y [R_G]','Z [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=By_YZ_term_r[:-1,:-1],FLdata2=Bz_YZ_term_r[:-1,:-1])

	plt.close('all')
	
	# -- Btot --
	# **************************************************************************
	plot_mltg(dest_dir,rundate,diagtime,"||B|| [nT]","Btot_mtlg_XY_Europa",X_XY,Y_XY,Btot_XY,X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Btot_XY_r[:-1,:-1],min_valtot,max_valtot,'X [R_G]','Y [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"||B|| [nT]","Btot_mtlg_r_XY_Europa",X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Btot_XY_r[:-1,:-1],min_valtot,max_valtot,'X [R_G]','Y [R_G]',fig_size,"viridis")
	plot_mltg(dest_dir,rundate,diagtime,"||B|| [nT]","Btot_mtlg_XZ_Europa",X_XZ,Z_XZ,Btot_XZ,X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Btot_XZ_r[:-1,:-1],min_valtot,max_valtot,'X [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"||B|| [nT]","Btot_mtlg_r_XZ_Europa",X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Btot_XZ_r[:-1,:-1],min_valtot,max_valtot,'X [R_G]','Z [R_G]',fig_size,"viridis")
	plot_mltg(dest_dir,rundate,diagtime,"||B|| [nT]","Btot_mtlg_YZ_terminator_Europa",Y_YZ,Z_YZ,Btot_YZ_term,Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Btot_YZ_term_r[:-1,:-1],min_valtot,max_valtot,'Y [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"||B|| [nT]","Btot_mtlg_r_YZ_terminator_Europa",Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Btot_YZ_term_r[:-1,:-1],min_valtot,max_valtot,'Y [R_G]','Z [R_G]',fig_size,"viridis")
