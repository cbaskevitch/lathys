from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

def plot_mltg(dest_dir,rundate,diagtime,figtitle,filetitle,X_XY,Y_XY,data_XY,X_XY_r,Y_XY_r,data_XY_r,min_val,max_val,xlabel,ylabel,fig_size=(7.5,6),col="jet",zoom=False,Xmin=-6,Xmax=6,Ymin=-6,Ymax=6,isFieldLines=False,FLdata1=None,FLdata2=None,FieldCol="white"):
    # planet drawing
	theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
	xp = np.cos(theta)
	yp = np.sin(theta)

	if zoom == True:
		fig, ax = plt.subplots(figsize=(7.5,6))
		ax.set_xlim(Xmin,Xmax)
		ax.set_ylim(Ymin,Ymax)
	else:
		fig, ax = plt.subplots(figsize=fig_size)
	c = ax.pcolor(X_XY, Y_XY, data_XY, vmin=min_val, vmax=max_val, cmap=col,shading='auto')
	## Field lines
	if isFieldLines==True:
		if zoom == True:
			field_lines_dens = 2
		else:
			field_lines_dens = 1
		
		Xmin=X_XY[0][0] #float(-1*icentr/rp)
		Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1] #float((nc[0]-icentr)/rp)
		X_norm=np.linspace(Xmin, Xmax, len(FLdata1))
		Ymin=Y_XY[0][0] #float(-jcentr/rp)
		Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1] #float((nc[1]-jcentr)/rp)
		Y_norm=np.linspace(Ymin, Ymax, len(FLdata1[0]))
		ax.streamplot(X_norm,Y_norm,np.transpose(FLdata1),np.transpose(FLdata2),color=FieldCol,density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)

	c_r = ax.pcolor(X_XY_r, Y_XY_r, data_XY_r, vmin=min_val, vmax=max_val, cmap=col,shading='auto')

	fig.colorbar(c, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = figtitle+" time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel(xlabel)#,'fontsize',12,'fontweight','b');
	ax.set_ylabel(ylabel)#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+filetitle+"_"+rundate+"_t"+diagtime+".png")



def plot_mltg_r(dest_dir,rundate,diagtime,figtitle,filetitle,X_XY_r,Y_XY_r,data_XY_r,min_val,max_val,xlabel,ylabel,fig_size=(25,6),col="jet",isFieldLines=False,FLdata1=None,FLdata2=None):
    # planet drawing
	theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
	xp = np.cos(theta)
	yp = np.sin(theta)

	fig, ax = plt.subplots(figsize=fig_size)
	c_r = ax.pcolor(X_XY_r, Y_XY_r, data_XY_r, vmin=min_val, vmax=max_val, cmap=col,shading='auto')

	## Field lines
	if isFieldLines==True:
		field_lines_dens = 2
		
		Xmin=X_XY_r[0][0] #float(-1*icentr/rp)
		Xmax=X_XY_r[len(X_XY_r)-1][len(X_XY_r[0])-1] #float((nc[0]-icentr)/rp)
		X_norm=np.linspace(Xmin, Xmax, len(FLdata1))
		Ymin=Y_XY_r[0][0] #float(-jcentr/rp)
		Ymax=Y_XY_r[len(Y_XY_r)-1][len(Y_XY_r[0])-1] #float((nc[1]-jcentr)/rp)
		Y_norm=np.linspace(Ymin, Ymax, len(FLdata1[0]))
		ax.streamplot(X_norm,Y_norm,np.transpose(FLdata1),np.transpose(FLdata2),color="white",density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=0.5)
	
	fig.colorbar(c_r, ax=ax)
	ax.plot(xp,yp,c="black")
	ax.fill(xp,yp,c="white")

	titre = figtitle+" time: "+diagtime
	plt.title(titre)#,'fontsize',12,'fontweight','b');
	ax.set_xlabel(xlabel)#,'fontsize',12,'fontweight','b');
	ax.set_ylabel(ylabel)#,'fontsize',12,'fontweight','b');

	plt.savefig(dest_dir+filetitle+"_"+rundate+"_t"+diagtime+".png")