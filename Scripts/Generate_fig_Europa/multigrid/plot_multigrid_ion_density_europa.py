######################
# plot_ion_density_europa.py
# ---------------------
# This routine reads the density
#  file and plot the map in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os
from multigrid_shared_plot_functions import *

def plot_multigrid_ion_density(src_dir,dest_dir,dest_dir_r,typefile,rundate,diagtime,zoom):
	filename = src_dir + typefile + rundate + "_t" + diagtime + ".dat"
	fid = open(filename,"r")
	lines = fid.readlines()
	fid.close()

	nb_species = 0
	species_name = []

	for i in range(1,len(lines)):
		l0 = lines[i].split(" ")
		for l in l0:
			if l != "" and l != "\n" and ".nc" not in l:
				nb_species += 1
				species_name.append(l)

	namearray = []
	for i in range(2,len(lines)):
		l0 = lines[i].split(" ")
		for l in l0:
			if ".nc" in l:
				namearray.append(l)

	if zoom == True:
		Xmin = -6.0
		Xmax = 6.0
		Ymin = -6.0
		Ymax = 6.0
		Zmin = -6.0
		Zmax = 6.0

	for i in range(0, len(namearray)):
		ncid = Dataset(namearray[i])
		var_nc = ncid.variables

		planetname = var_nc['planetname'][:]
		centr      = var_nc['s_centr'][:]
		radius     = var_nc['r_planet'][:]
		gs         = var_nc['gstep'][:]
		gs_r       = var_nc['gstep_r'][:]
		s_min_r    = var_nc['s_min_r'][:] #indice de la premiere cellule de la grille rafinee dans la grande grille
		Dn         = var_nc['Density'][:]
		Dn_r       = var_nc['Density_r'][:]
		Ux_ion     = var_nc['Ux'][:]
		Uy_ion     = var_nc['Uy'][:]
		Uz_ion     = var_nc['Uz'][:]
		Ux_ion_r   = var_nc['Ux_r'][:]
		Uy_ion_r   = var_nc['Uy_r'][:]
		Uz_ion_r   = var_nc['Uz_r'][:]
		nrm        = var_nc['phys_density'][:]
		# radius=1.0

		nc = [len(Dn[0][0]), len(Dn[0]), len(Dn)]
		nc_r = [len(Dn_r[0][0]), len(Dn_r[0]), len(Dn_r)]
		
		Utot_ion = np.sqrt(Ux_ion*Ux_ion + Uy_ion*Uy_ion + Uz_ion*Uz_ion)
		Utot_ion_r = np.sqrt(Ux_ion_r*Ux_ion_r + Uy_ion_r*Uy_ion_r + Uz_ion_r*Uz_ion_r)

		Dn = np.where(Dn <= 0, float('NaN'), Dn)
		Dn_r = np.where(Dn_r <= 0, float('NaN'), Dn_r)
		# Utot_ion = math.sqrt(Ux_ion.^2++Uy_ion.^2+Uz_ion.^2)

		# maximum and minimum
		# maximum and minimum
		if "Ojv" in species_name[i] :
			if radius > 1.0:
				min_val = 0.0 # log(cm-3)
				max_val = 2.2 # log(cm-3)
				min_valtot = 50.0 # km/s
				max_valtot = 150.0 # km/s
			else:
				min_val = 1.4 # log(cm-3) for run without planete
				max_val = 1.7 # log(sm-3) for run without planete
				min_valtot = 80.0 # km/s for run without planete
				max_valtot = 95.0 # km/s for run without planete
		if "O2pl" in species_name[i] or "O2li" in species_name[i] or "O2fi" in species_name[i]:
			if radius > 1.0:
				min_val = 0.0 # log(cm-3)
				max_val = 3.5 # log(sm-3)
				min_valtot = 50.0 # km/s
				max_valtot = 150.0 # km/s
			else:
				min_val = 1.4 # log(cm-3) for run without planete
				max_val = 1.7 # log(sm-3) for run without planete
				min_valtot = 80.0 # km/s for run without planete
				max_valtot = 95.0 # km/s for run without planete
		else : 
			min_val = 0.0 # log(cm-3)
			max_val = 3.5 # log(sm-3)
			min_valtot = 50.0 # km/s
			max_valtot = 150.0 # km/s


		# -- Creation of axis values centered on the planet ( normalized to planet radius)
		X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
		X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide(centr[0]*np.ones((nc[0],nc[1])), radius)
		Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide(centr[1]*np.ones((nc[0],nc[1])), radius)

		X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
		X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide(centr[0]*np.ones((nc[0],nc[2])), radius)
		Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide(centr[2]*np.ones((nc[0],nc[2])), radius)

		Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
		Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide(centr[1]*np.ones((nc[1],nc[2])), radius)
		Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide(centr[2]*np.ones((nc[1],nc[2])), radius)

		# -- Creation of axis values centered on the planet ( normalized to planet radius) for the small grid
		X_XY_r, Y_XY_r = np.meshgrid(np.arange(0,(nc_r[0])*gs_r[0],gs_r[0]),np.arange(0,(nc_r[1])*gs_r[1],gs_r[1]))
		X_XY_r = np.divide(np.matrix.transpose(X_XY_r), radius) - np.divide((centr[0]-s_min_r[0])*np.ones((nc_r[0],nc_r[1])), radius)
		Y_XY_r = np.divide(np.matrix.transpose(Y_XY_r), radius) - np.divide((centr[1]-s_min_r[1])*np.ones((nc_r[0],nc_r[1])), radius)

		X_XZ_r, Z_XZ_r = np.meshgrid(np.arange(0,(nc_r[0])*gs_r[0],gs_r[0]),np.arange(0,(nc_r[2])*gs_r[2],gs_r[2]))
		X_XZ_r = np.divide(np.matrix.transpose(X_XZ_r), radius) - np.divide((centr[0]-s_min_r[0])*np.ones((nc_r[0],nc_r[2])), radius)
		Z_XZ_r = np.divide(np.matrix.transpose(Z_XZ_r), radius) - np.divide((centr[2]-s_min_r[2])*np.ones((nc_r[0],nc_r[2])), radius)

		Y_YZ_r, Z_YZ_r = np.meshgrid(np.arange(0,(nc_r[1])*gs_r[1],gs_r[1]),np.arange(0,(nc_r[2])*gs_r[2],gs_r[2]))
		Y_YZ_r = np.divide(np.matrix.transpose(Y_YZ_r), radius) - np.divide((centr[1]-s_min_r[1])*np.ones((nc_r[1],nc_r[2])), radius)
		Z_YZ_r = np.divide(np.matrix.transpose(Z_YZ_r), radius) - np.divide((centr[2]-s_min_r[2])*np.ones((nc_r[1],nc_r[2])), radius)

		# planet center in cell number (NB: cell number start at 1
		icentr = int(np.fix(centr[0]/gs[0]))
		jcentr = int(np.fix(centr[1]/gs[1]))
		kcentr = int(np.fix(centr[2]/gs[2]))
		iwake = icentr #int(icentr + np.fix(2.0*radius/gs[0]))

		icentr_r = int(np.fix((centr[0]-s_min_r[0])/gs_r[0]))
		jcentr_r = int(np.fix((centr[1]-s_min_r[1])/gs_r[1]))
		kcentr_r = int(np.fix((centr[2]-s_min_r[2])/gs_r[2]))
		# iwake_r = int(icentr + np.fix(1.5*radius/gs_r[0]))

		Dn_XY = np.zeros((nc[0],nc[1]))
		Dn_XY[:,:] = np.matrix.transpose(Dn[kcentr,:,:])

		Utot_XY_ion = np.zeros((nc[0],nc[1]))
		Utot_XY_ion[:,:] = np.matrix.transpose(Utot_ion[kcentr,:,:])

		Dn_XZ = np.zeros((nc[0],nc[2]))
		Dn_XZ[:,:] = np.matrix.transpose(Dn[:,jcentr,:])

		Utot_XZ_ion = np.zeros((nc[0],nc[2]))
		Utot_XZ_ion[:,:] = np.matrix.transpose(Utot_ion[:,jcentr,:])

		Dn_YZ_term = np.zeros((nc[1],nc[2]))
		Dn_YZ_term[:,:] = np.matrix.transpose(Dn[:,:,icentr])

		Dn_YZ_wake = np.zeros((nc[1],nc[2]))
		Dn_YZ_wake[:,:] = np.matrix.transpose(Dn[:,:,iwake])

		### refined grid

		Dn_XY_r = np.zeros((nc_r[0],nc_r[1]))
		Dn_XY_r[:,:] = np.matrix.transpose(Dn_r[kcentr_r,:,:])

		Utot_XY_ion_r = np.zeros((nc_r[0],nc_r[1]))
		Utot_XY_ion_r[:,:] = np.matrix.transpose(Utot_ion_r[kcentr_r,:,:])

		Dn_XZ_r = np.zeros((nc_r[0],nc_r[2]))
		Dn_XZ_r[:,:] = np.matrix.transpose(Dn_r[:,jcentr_r,:])

		Utot_XZ_ion_r = np.zeros((nc_r[0],nc_r[2]))
		Utot_XZ_ion_r[:,:] = np.matrix.transpose(Utot_ion_r[:,jcentr_r,:])

		Dn_YZ_term_r = np.zeros((nc_r[1],nc_r[2]))
		Dn_YZ_term_r[:,:] = np.matrix.transpose(Dn_r[:,:,icentr_r])

		if zoom == True:
			Xmin = -6.0
			Xmax = 6.0
			Ymin = -6.0
			Ymax = 6.0
			Zmin = -6.0
			Zmax = 6.0
		else:
			Xmin=X_XY[0][0]
			Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1]
			Ymin=Y_XY[0][0]
			Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1]
			Zmin=Z_XZ[0][0]
			Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1]

		fig_size=(7.5,6)
		# -- Dn --
		# **************************************************************************
		plot_mltg(dest_dir,rundate,diagtime,"Density "+species_name[i]+" log[cm-3]","Dn_"+species_name[i]+"_mtlg_XY_Europa",X_XY,Y_XY,np.log10(Dn_XY),X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],np.log10(Dn_XY_r[:-1,:-1]),min_val,max_val,'X [R_G]','Y [R_G]',fig_size,"inferno",zoom,Xmin,Xmax,Ymin,Ymax)
		plot_mltg_r(dest_dir_r,rundate,diagtime,"Density "+species_name[i]+" log[cm-3]","Dn_"+species_name[i]+"_mtlg_r_XY_Europa",X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],np.log10(Dn_XY_r[:-1,:-1]),min_val,max_val,'X [R_G]','Y [R_G]',fig_size,"inferno")
		plot_mltg(dest_dir,rundate,diagtime,"Density "+species_name[i]+" log[cm-3]","Dn_"+species_name[i]+"_mtlg_XZ_Europa",X_XZ,Z_XZ,np.log10(Dn_XZ),X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],np.log10(Dn_XZ_r[:-1,:-1]),min_val,max_val,'X [R_G]','Z [R_G]',fig_size,"inferno",zoom,Xmin,Xmax,Zmin,Zmax)
		plot_mltg_r(dest_dir_r,rundate,diagtime,"Density "+species_name[i]+" log[cm-3]","Dn_"+species_name[i]+"_mtlg_r_XZ_Europa",X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],np.log10(Dn_XZ_r[:-1,:-1]),min_val,max_val,'X [R_G]','Z [R_G]',fig_size,"inferno")
		plot_mltg(dest_dir,rundate,diagtime,"Density "+species_name[i]+" log[cm-3]","Dn_"+species_name[i]+"_mtlg_YZ_terminator_Europa",Y_YZ,Z_YZ,np.log10(Dn_YZ_term),Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],np.log10(Dn_YZ_term_r[:-1,:-1]),min_val,max_val,'Y [R_G]','Z [R_G]',fig_size,"inferno",zoom,Ymin,Ymax,Zmin,Zmax)
		plot_mltg_r(dest_dir_r,rundate,diagtime,"Density "+species_name[i]+" log[cm-3]","Dn_"+species_name[i]+"_mtlg_r_YZ_terminator_Europa",Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],np.log10(Dn_YZ_term_r[:-1,:-1]),min_val,max_val,'Y [R_G]','Z [R_G]',fig_size,"inferno")

		plt.close('all')