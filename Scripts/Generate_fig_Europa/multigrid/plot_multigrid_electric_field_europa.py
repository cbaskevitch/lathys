######################
# Plot_magnetic_field_Europa.py
# ---------------------
# This routine reads the magnetic
# field file and plot the module
# and the B components in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os
from multigrid_shared_plot_functions import *


def plot_multigrid_electric_field(src_dir,dest_dir,dest_dir_r,typefile,rundate,diagtime,zoom,field_lines_dens):
	ncfile = src_dir + typefile + rundate + "_t" + diagtime + '.nc'

	ncid = Dataset(ncfile)
	var_nc = ncid.variables

	planetname = var_nc['planetname'][:]
	centr      = var_nc['s_centr'][:]
	radius     = var_nc['r_planet'][:]
	gs         = var_nc['gstep'][:]
	gs_r       = var_nc['gstep_r'][:]
	s_min_r    = var_nc['s_min_r'][:] #indice de la premiere cellule de la grille rafinee dans la grande grille
	Ex         = var_nc['Ex'][:]
	Ey         = var_nc['Ey'][:]
	Ez         = var_nc['Ez'][:]
	Ex_r       = var_nc['Ex_r'][:]
	Ey_r       = var_nc['Ey_r'][:]
	Ez_r       = var_nc['Ez_r'][:]
	nrm        = var_nc['phys_mag'][:]
	nrm_len    = var_nc['phys_length'][:]
	# radius=1.0
	nc = [len(Ex[0][0]), len(Ex[0]), len(Ex)]
	nc_r = [len(Ex_r[0][0]), len(Ex_r[0]), len(Ex_r)]

	Etot = np.sqrt(Ex*Ex + Ey*Ey + Ez*Ez)
	Etot_r = np.sqrt(Ex_r*Ex_r + Ey_r*Ey_r + Ez_r*Ez_r)

	Etot = np.where(Etot <= 0, float('NaN'), Etot)
	Etot_r = np.where(Etot_r <= 0, float('NaN'), Etot_r)

	# maximum and minimum 
	if radius > 1.0:
		min_val    = [-15.0,-60.0,0.0] # mV/m [Ex,Ey,Ez]
		max_val    = [15.0,0.0,20.0]  # mV/m [Ex,Ey,Ez]
		min_valtot = 1.55   # mV/m
		max_valtot = 1.65 # mV/m
	else:
		min_val    = [-0.07,-38.1,12.05] # mV/m [Ex,Ey,Ez] for run without planete
		max_val    = [0,-37.9,12.2]  # mV/m [Ex,Ey,Ez] for run without planete
		min_valtot = 1.55   # mV/m for run without planete
		max_valtot = 1.65 # mV/m for run without planete




	# -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide(centr[0]*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide(centr[1]*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide(centr[0]*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide(centr[2]*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide(centr[1]*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide(centr[2]*np.ones((nc[1],nc[2])), radius)

	# -- Creation of axis values centered on the planet ( normalized to planet radius) for the small grid
	X_XY_r, Y_XY_r = np.meshgrid(np.arange(0,(nc_r[0])*gs_r[0],gs_r[0]),np.arange(0,(nc_r[1])*gs_r[1],gs_r[1]))
	X_XY_r = np.divide(np.matrix.transpose(X_XY_r), radius) - np.divide((centr[0]-s_min_r[0])*np.ones((nc_r[0],nc_r[1])), radius)
	Y_XY_r = np.divide(np.matrix.transpose(Y_XY_r), radius) - np.divide((centr[1]-s_min_r[1])*np.ones((nc_r[0],nc_r[1])), radius)

	X_XZ_r, Z_XZ_r = np.meshgrid(np.arange(0,(nc_r[0])*gs_r[0],gs_r[0]),np.arange(0,(nc_r[2])*gs_r[2],gs_r[2]))
	X_XZ_r = np.divide(np.matrix.transpose(X_XZ_r), radius) - np.divide((centr[0]-s_min_r[0])*np.ones((nc_r[0],nc_r[2])), radius)
	Z_XZ_r = np.divide(np.matrix.transpose(Z_XZ_r), radius) - np.divide((centr[2]-s_min_r[2])*np.ones((nc_r[0],nc_r[2])), radius)

	Y_YZ_r, Z_YZ_r = np.meshgrid(np.arange(0,(nc_r[1])*gs_r[1],gs_r[1]),np.arange(0,(nc_r[2])*gs_r[2],gs_r[2]))
	Y_YZ_r = np.divide(np.matrix.transpose(Y_YZ_r), radius) - np.divide((centr[1]-s_min_r[1])*np.ones((nc_r[1],nc_r[2])), radius)
	Z_YZ_r = np.divide(np.matrix.transpose(Z_YZ_r), radius) - np.divide((centr[2]-s_min_r[2])*np.ones((nc_r[1],nc_r[2])), radius)

	# planet center in cell number (NB: cell number start at 0
	icentr = int(np.fix(centr[0]/gs[0]))
	jcentr = int(np.fix(centr[1]/gs[1]))
	kcentr = int(np.fix(centr[2]/gs[2]))
	iwake = int(icentr + np.fix(1.5*radius/gs[0]))
	rp = radius/gs[0]

	icentr_r = int(np.fix((centr[0]-s_min_r[0])/gs_r[0]))
	jcentr_r = int(np.fix((centr[1]-s_min_r[1])/gs_r[1]))
	kcentr_r = int(np.fix((centr[2]-s_min_r[2])/gs_r[2]))
	# iwake_r = int(icentr + np.fix(1.5*radius/gs_r[0]))

	Ex_XY        = np.zeros((nc[0],nc[1]))
	Ex_XY[:,:]   = np.matrix.transpose(Ex[kcentr,:,:])
	Ey_XY        = np.zeros((nc[0],nc[1]))
	Ey_XY[:,:]   = np.matrix.transpose(Ey[kcentr,:,:])
	Ez_XY        = np.zeros((nc[0],nc[1]))
	Ez_XY[:,:]   = np.matrix.transpose(Ez[kcentr,:,:])
	Etot_XY      = np.zeros((nc[0],nc[1]))
	Etot_XY[:,:] = np.matrix.transpose(Etot[kcentr,:,:])

	Ex_XZ        = np.zeros((nc[0],nc[2]))
	Ex_XZ[:,:]   = np.matrix.transpose(Ex[:,jcentr,:])
	Ey_XZ        = np.zeros((nc[0],nc[2]))
	Ey_XZ[:,:]   = np.matrix.transpose(Ey[:,jcentr,:])
	Ez_XZ        = np.zeros((nc[0],nc[2]))
	Ez_XZ[:,:]   = np.matrix.transpose(Ez[:,jcentr,:])
	Etot_XZ      = np.zeros((nc[0],nc[2]))
	Etot_XZ[:,:] = np.matrix.transpose(Etot[:,jcentr,:])

	Ex_YZ_term        = np.zeros((nc[1],nc[2]))
	Ex_YZ_term[:,:]   = np.matrix.transpose(Ex[:,:,icentr])
	Ey_YZ_term        = np.zeros((nc[1],nc[2]))
	Ey_YZ_term[:,:]   = np.matrix.transpose(Ey[:,:,icentr])
	Ez_YZ_term        = np.zeros((nc[1],nc[2]))
	Ez_YZ_term[:,:]   = np.matrix.transpose(Ez[:,:,icentr])
	Etot_YZ_term      = np.zeros((nc[1],nc[2]))
	Etot_YZ_term[:,:] = np.matrix.transpose(Etot[:,:,icentr])

	Ex_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ex_YZ_wake[:,:]   = np.matrix.transpose(Ex[:,:,iwake])
	Ey_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ey_YZ_wake[:,:]   = np.matrix.transpose(Ey[:,:,iwake])
	Ez_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ez_YZ_wake[:,:]   = np.matrix.transpose(Ez[:,:,iwake])
	Etot_YZ_wake      = np.zeros((nc[1],nc[2]))
	Etot_YZ_wake[:,:] = np.matrix.transpose(Etot[:,:,iwake])

	### refined grid

	Ex_XY_r        = np.zeros((nc_r[0],nc_r[1]))
	Ex_XY_r[:,:]   = np.matrix.transpose(Ex_r[kcentr_r,:,:])
	Ey_XY_r        = np.zeros((nc_r[0],nc_r[1]))
	Ey_XY_r[:,:]   = np.matrix.transpose(Ey_r[kcentr_r,:,:])
	Ez_XY_r        = np.zeros((nc_r[0],nc_r[1]))
	Ez_XY_r[:,:]   = np.matrix.transpose(Ez_r[kcentr_r,:,:])
	Etot_XY_r      = np.zeros((nc_r[0],nc_r[1]))
	Etot_XY_r[:,:] = np.matrix.transpose(Etot_r[kcentr_r,:,:])

	Ex_XZ_r        = np.zeros((nc_r[0],nc_r[2]))
	Ex_XZ_r[:,:]   = np.matrix.transpose(Ex_r[:,jcentr_r,:])
	Ey_XZ_r        = np.zeros((nc_r[0],nc_r[2]))
	Ey_XZ_r[:,:]   = np.matrix.transpose(Ey_r[:,jcentr_r,:])
	Ez_XZ_r        = np.zeros((nc_r[0],nc_r[2]))
	Ez_XZ_r[:,:]   = np.matrix.transpose(Ez_r[:,jcentr_r,:])
	Etot_XZ_r      = np.zeros((nc_r[0],nc_r[2]))
	Etot_XZ_r[:,:] = np.matrix.transpose(Etot_r[:,jcentr_r,:])

	Ex_YZ_term_r        = np.zeros((nc_r[1],nc_r[2]))
	Ex_YZ_term_r[:,:]   = np.matrix.transpose(Ex_r[:,:,icentr_r])
	Ey_YZ_term_r        = np.zeros((nc_r[1],nc_r[2]))
	Ey_YZ_term_r[:,:]   = np.matrix.transpose(Ey_r[:,:,icentr_r])
	Ez_YZ_term_r        = np.zeros((nc_r[1],nc_r[2]))
	Ez_YZ_term_r[:,:]   = np.matrix.transpose(Ez_r[:,:,icentr_r])
	Etot_YZ_term_r      = np.zeros((nc_r[1],nc_r[2]))
	Etot_YZ_term_r[:,:] = np.matrix.transpose(Etot_r[:,:,icentr_r])

	# Ex_1D    = np.zeros(nc[0])
	# Ex_1D[:] = Ex[kcentr,jcentr,:]
	# Ey_1D    = np.zeros(nc[0])
	# Ey_1D[:] = Ey[kcentr,jcentr,:]
	# Ez_1D    = np.zeros(nc[0])
	# Ez_1D[:] = Ez[kcentr,jcentr,:]
	# x = np.divide(np.arange(0, nc[0], 1.0) * gs[0]-centr[0], radius)

	Xmin=np.min(X_XY)#-4.67
	Xmax=np.max(X_XY)#2.36
	Ymin=np.min(Y_XY)#-7.08
	Ymax=np.max(Y_XY)#7.08
	Zmin=np.min(Z_XZ)#-7.08
	Zmax=np.max(Z_XZ)#7.08
	print(Xmax)
	print(Ymin)
	print(Ymax)
	print(Zmin)
	print(Zmax)

	if zoom == True:
		Xmin = -6.0
		Xmax = 6.0
		Ymin = -6.0
		Ymax = 6.0
		Zmin = -6.0
		Zmax = 6.0
	else:
		Xmin=X_XY[0][0]
		Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1]
		Ymin=Y_XY[0][0]
		Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1]
		Zmin=Z_XZ[0][0]
		Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1]

	fig_size=(7.5,6)
	
	# -- Ex --
	# **************************************************************************
	plot_mltg(dest_dir,rundate,diagtime,"Ex [mV/m]","Ex_mtlg_XY_Europa",X_XY,Y_XY,Ex_XY,X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Ex_XY_r[:-1,:-1],min_val[0],max_val[0],'X [R_G]','Y [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=Ex_XY,FLdata2=Ey_XY,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Ex [mV/m]","Ex_mtlg_r_XY_Europa",X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Ex_XY_r[:-1,:-1],min_val[0],max_val[0],'X [R_G]','Y [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=Ex_XY_r[:-1,:-1],FLdata2=Ey_XY_r[:-1,:-1])
	plot_mltg(dest_dir,rundate,diagtime,"Ex [mV/m]","Ex_mtlg_XZ_Europa",X_XZ,Z_XZ,Ex_XZ,X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Ex_XZ_r[:-1,:-1],min_val[0],max_val[0],'X [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=Ex_XZ,FLdata2=Ez_XZ,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Ex [mV/m]","Ex_mtlg_r_XZ_Europa",X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Ex_XZ_r[:-1,:-1],min_val[0],max_val[0],'X [R_G]','Z [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=Ex_XZ_r[:-1,:-1],FLdata2=Ez_XZ_r[:-1,:-1])
	plot_mltg(dest_dir,rundate,diagtime,"Ex [mV/m]","Ex_mtlg_YZ_terminator_Europa",Y_YZ,Z_YZ,Ex_YZ_term,Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Ex_YZ_term_r[:-1,:-1],min_val[0],max_val[0],'Y [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=Ey_YZ_term,FLdata2=Ez_YZ_term,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Ex [mV/m]","Ex_mtlg_r_YZ_terminator_Europa",Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Ex_YZ_term_r[:-1,:-1],min_val[0],max_val[0],'Y [R_G]','Z [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=Ey_YZ_term_r[:-1,:-1],FLdata2=Ez_YZ_term_r[:-1,:-1])

	plt.close('all')

	# -- Ey --
	# **************************************************************************
	plot_mltg(dest_dir,rundate,diagtime,"Ey [mV/m]","Ey_mtlg_XY_Europa",X_XY,Y_XY,Ey_XY,X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Ey_XY_r[:-1,:-1],min_val[1],max_val[1],'X [R_G]','Y [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=Ex_XY,FLdata2=Ey_XY,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Ey [mV/m]","Ey_mtlg_r_XY_Europa",X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Ey_XY_r[:-1,:-1],min_val[1],max_val[1],'X [R_G]','Y [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=Ex_XY_r[:-1,:-1],FLdata2=Ey_XY_r[:-1,:-1])
	plot_mltg(dest_dir,rundate,diagtime,"Ey [mV/m]","Ey_mtlg_XZ_Europa",X_XZ,Z_XZ,Ey_XZ,X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Ey_XZ_r[:-1,:-1],min_val[1],max_val[1],'X [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=Ex_XZ,FLdata2=Ez_XZ,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Ey [mV/m]","Ey_mtlg_r_XZ_Europa",X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Ey_XZ_r[:-1,:-1],min_val[1],max_val[1],'X [R_G]','Z [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=Ex_XZ_r[:-1,:-1],FLdata2=Ez_XZ_r[:-1,:-1])
	plot_mltg(dest_dir,rundate,diagtime,"Ey [mV/m]","Ey_mtlg_YZ_terminator_Europa",Y_YZ,Z_YZ,Ey_YZ_term,Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Ey_YZ_term_r[:-1,:-1],min_val[1],max_val[1],'Y [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=Ey_YZ_term,FLdata2=Ez_YZ_term,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Ey [mV/m]","Ey_mtlg_r_YZ_terminator_Europa",Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Ey_YZ_term_r[:-1,:-1],min_val[1],max_val[1],'Y [R_G]','Z [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=Ey_YZ_term_r[:-1,:-1],FLdata2=Ez_YZ_term_r[:-1,:-1])

	plt.close('all')
	
	# -- Ez --
	# **************************************************************************
	plot_mltg(dest_dir,rundate,diagtime,"Ez [mV/m]","Ez_mtlg_XY_Europa",X_XY,Y_XY,Ez_XY,X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Ez_XY_r[:-1,:-1],min_val[2],max_val[2],'X [R_G]','Y [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=Ex_XY,FLdata2=Ey_XY,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Ez [mV/m]","Ez_mtlg_r_XY_Europa",X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Ez_XY_r[:-1,:-1],min_val[2],max_val[2],'X [R_G]','Y [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=Ex_XY_r[:-1,:-1],FLdata2=Ey_XY_r[:-1,:-1])
	plot_mltg(dest_dir,rundate,diagtime,"Ez [mV/m]","Ez_mtlg_XZ_Europa",X_XZ,Z_XZ,Ez_XZ,X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Ez_XZ_r[:-1,:-1],min_val[2],max_val[2],'X [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=Ex_XZ,FLdata2=Ez_XZ,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Ez [mV/m]","Ez_mtlg_r_XZ_Europa",X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Ez_XZ_r[:-1,:-1],min_val[2],max_val[2],'X [R_G]','Z [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=Ex_XZ_r[:-1,:-1],FLdata2=Ez_XZ_r[:-1,:-1])
	plot_mltg(dest_dir,rundate,diagtime,"Ez [mV/m]","Ez_mtlg_YZ_terminator_Europa",Y_YZ,Z_YZ,Ez_YZ_term,Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Ez_YZ_term_r[:-1,:-1],min_val[2],max_val[2],'Y [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax,isFieldLines=True,FLdata1=Ey_YZ_term,FLdata2=Ez_YZ_term,FieldCol="black")
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Ez [mV/m]","Ez_mtlg_r_YZ_terminator_Europa",Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Ez_YZ_term_r[:-1,:-1],min_val[2],max_val[2],'Y [R_G]','Z [R_G]',fig_size,"viridis",isFieldLines=True,FLdata1=Ey_YZ_term_r[:-1,:-1],FLdata2=Ez_YZ_term_r[:-1,:-1])

	plt.close('all')
	
	# -- Etot --
	# **************************************************************************
	plot_mltg(dest_dir,rundate,diagtime,"||E|| [mV/m]","Etot_mtlg_XY_Europa",X_XY,Y_XY,np.log10(Etot_XY),X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],np.log10(Etot_XY_r[:-1,:-1]),min_valtot,max_valtot,'X [R_G]','Y [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"||E|| [mV/m]","Etot_mtlg_r_XY_Europa",X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],np.log10(Etot_XY_r[:-1,:-1]),min_valtot,max_valtot,'X [R_G]','Y [R_G]',fig_size,"viridis")
	plot_mltg(dest_dir,rundate,diagtime,"||E|| [mV/m]","Etot_mtlg_XZ_Europa",X_XZ,Z_XZ,np.log10(Etot_XZ),X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],np.log10(Etot_XZ_r)[:-1,:-1],min_valtot,max_valtot,'X [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"||E|| [mV/m]","Etot_mtlg_r_XZ_Europa",X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],np.log10(Etot_XZ_r[:-1,:-1]),min_valtot,max_valtot,'X [R_G]','Z [R_G]',fig_size,"viridis")
	plot_mltg(dest_dir,rundate,diagtime,"||E|| [mV/m]","Etot_mtlg_YZ_terminator_Europa",Y_YZ,Z_YZ,np.log10(Etot_YZ_term),Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],np.log10(Etot_YZ_term_r[:-1,:-1]),min_valtot,max_valtot,'Y [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"||E|| [mV/m]","Etot_mtlg_r_YZ_terminator_Europa",Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],np.log10(Etot_YZ_term_r[:-1,:-1]),min_valtot,max_valtot,'Y [R_G]','Z [R_G]',fig_size,"viridis")

	plt.close('all')
