######################
# plot_bulk_speed_Europa.py
# ---------------------
# This routine reads the bulk speed
#  file and plot the module
# and the U components in 
# the XY and XZ plane
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os
from multigrid_shared_plot_functions import *



def plot_multigrid_bulk_speed(src_dir,dest_dir,dest_dir_r,typefile,rundate,diagtime,zoom):
	ncfile = src_dir + typefile + rundate + "_t" + diagtime + '.nc'

	ncid = Dataset(ncfile)
	var_nc = ncid.variables

	planetname = var_nc['planetname'][:]
	centr      = var_nc['s_centr'][:]
	radius     = var_nc['r_planet'][:]
	gs         = var_nc['gstep'][:]
	gs_r       = var_nc['gstep_r'][:]
	s_min_r    = var_nc['s_min_r'][:] #indice de la premiere cellule de la grille rafinee dans la grande grille
	Ux         = var_nc['Ux'][:]
	Uy         = var_nc['Uy'][:]
	Uz         = var_nc['Uz'][:]
	Ux_r       = var_nc['Ux_r'][:]
	Uy_r       = var_nc['Uy_r'][:]
	Uz_r       = var_nc['Uz_r'][:]
	nrm        = var_nc['phys_mag'][:]
	# radius=1.0
	nc = [len(Ux[0][0]), len(Ux[0]), len(Ux)]
	nc_r = [len(Ux_r[0][0]), len(Ux_r[0]), len(Ux_r)]

	Utot = np.sqrt(Ux*Ux + Uy*Uy + Uz*Uz)
	Utot_r = np.sqrt(Ux_r*Ux_r + Uy_r*Uy_r + Uz_r*Uz_r)

	# maximum and minimum
	if radius > 1.0:
		min_val = [0.0,-30.0,-10.0] # km/s [Ux,Uy,Uz] for run with planete
		max_val = [140.0,30.0,10.0] # km/s  [Ux,Uy,Uz] for run with planete
		min_valtot = 0.0 # km/s for run with planete
		max_valtot = 150.0 # km/s for run with planete

	else:
		min_val = [95.0,-5.0,-5.0] # km/s [Ux,Uy,Uz] for run without planete
		max_val = [105.0,5.0,5.0] # km/s  [Ux,Uy,Uz] for run without planete
		min_valtot = 80.0 # km/s for run without planete
		max_valtot = 95.0 # km/s for run without planete

	# -- Creation of axis values centered on the planet ( normalized to planet radius)
	X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
	X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide(centr[0]*np.ones((nc[0],nc[1])), radius)
	Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide(centr[1]*np.ones((nc[0],nc[1])), radius)

	X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
	X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide(centr[0]*np.ones((nc[0],nc[2])), radius)
	Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide(centr[2]*np.ones((nc[0],nc[2])), radius)

	Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
	Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide(centr[1]*np.ones((nc[1],nc[2])), radius)
	Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide(centr[2]*np.ones((nc[1],nc[2])), radius)

	# -- Creation of axis values centered on the planet ( normalized to planet radius) for the small grid
	X_XY_r, Y_XY_r = np.meshgrid(np.arange(0,(nc_r[0])*gs_r[0],gs_r[0]),np.arange(0,(nc_r[1])*gs_r[1],gs_r[1]))
	X_XY_r = np.divide(np.matrix.transpose(X_XY_r), radius) - np.divide((centr[0]-s_min_r[0])*np.ones((nc_r[0],nc_r[1])), radius)
	Y_XY_r = np.divide(np.matrix.transpose(Y_XY_r), radius) - np.divide((centr[1]-s_min_r[1])*np.ones((nc_r[0],nc_r[1])), radius)

	X_XZ_r, Z_XZ_r = np.meshgrid(np.arange(0,(nc_r[0])*gs_r[0],gs_r[0]),np.arange(0,(nc_r[2])*gs_r[2],gs_r[2]))
	X_XZ_r = np.divide(np.matrix.transpose(X_XZ_r), radius) - np.divide((centr[0]-s_min_r[0])*np.ones((nc_r[0],nc_r[2])), radius)
	Z_XZ_r = np.divide(np.matrix.transpose(Z_XZ_r), radius) - np.divide((centr[2]-s_min_r[2])*np.ones((nc_r[0],nc_r[2])), radius)

	Y_YZ_r, Z_YZ_r = np.meshgrid(np.arange(0,(nc_r[1])*gs_r[1],gs_r[1]),np.arange(0,(nc_r[2])*gs_r[2],gs_r[2]))
	Y_YZ_r = np.divide(np.matrix.transpose(Y_YZ_r), radius) - np.divide((centr[1]-s_min_r[1])*np.ones((nc_r[1],nc_r[2])), radius)
	Z_YZ_r = np.divide(np.matrix.transpose(Z_YZ_r), radius) - np.divide((centr[2]-s_min_r[2])*np.ones((nc_r[1],nc_r[2])), radius)

	# planet center in cell number (NB: cell number start at 1
	icentr = int(np.fix(centr[0]/gs[0]))
	jcentr = int(np.fix(centr[1]/gs[1]))
	kcentr = int(np.fix(centr[2]/gs[2]))
	iwake = int(icentr + np.fix(1.5*radius/gs[0]))

	icentr_r = int(np.fix((centr[0]-s_min_r[0])/gs_r[0]))
	jcentr_r = int(np.fix((centr[1]-s_min_r[1])/gs_r[1]))
	kcentr_r = int(np.fix((centr[2]-s_min_r[2])/gs_r[2]))
	# iwake_r = int(icentr + np.fix(1.5*radius/gs_r[0]))

	Ux_XY        = np.zeros((nc[0],nc[1]))
	Ux_XY[:,:]   = np.matrix.transpose(Ux[kcentr,:,:])
	Uy_XY        = np.zeros((nc[0],nc[1]))
	Uy_XY[:,:]   = np.matrix.transpose(Uy[kcentr,:,:])
	Uz_XY        = np.zeros((nc[0],nc[1]))
	Uz_XY[:,:]   = np.matrix.transpose(Uz[kcentr,:,:])
	Utot_XY      = np.zeros((nc[0],nc[1]))
	Utot_XY[:,:] = np.matrix.transpose(Utot[kcentr,:,:])

	Ux_XZ        = np.zeros((nc[0],nc[2]))
	Ux_XZ[:,:]   = np.matrix.transpose(Ux[:,jcentr,:])
	Uy_XZ        = np.zeros((nc[0],nc[2]))
	Uy_XZ[:,:]   = np.matrix.transpose(Uy[:,jcentr,:])
	Uz_XZ        = np.zeros((nc[0],nc[2]))
	Uz_XZ[:,:]   = np.matrix.transpose(Uz[:,jcentr,:])
	Utot_XZ      = np.zeros((nc[0],nc[2]))
	Utot_XZ[:,:] = np.matrix.transpose(Utot[:,jcentr,:])

	Ux_YZ_term        = np.zeros((nc[1],nc[2]))
	Ux_YZ_term[:,:]   = np.matrix.transpose(Ux[:,:,icentr])
	Uy_YZ_term        = np.zeros((nc[1],nc[2]))
	Uy_YZ_term[:,:]   = np.matrix.transpose(Uy[:,:,icentr])
	Uz_YZ_term        = np.zeros((nc[1],nc[2]))
	Uz_YZ_term[:,:]   = np.matrix.transpose(Uz[:,:,icentr])
	Utot_YZ_term      = np.zeros((nc[1],nc[2]))
	Utot_YZ_term[:,:] = np.matrix.transpose(Utot[:,:,icentr])

	Ux_YZ_wake        = np.zeros((nc[1],nc[2]))
	Ux_YZ_wake[:,:]   = np.matrix.transpose(Ux[:,:,iwake])
	Uy_YZ_wake        = np.zeros((nc[1],nc[2]))
	Uy_YZ_wake[:,:]   = np.matrix.transpose(Uy[:,:,iwake])
	Uz_YZ_wake        = np.zeros((nc[1],nc[2]))
	Uz_YZ_wake[:,:]   = np.matrix.transpose(Uz[:,:,iwake])
	Utot_YZ_wake      = np.zeros((nc[1],nc[2]))
	Utot_YZ_wake[:,:] = np.matrix.transpose(Utot[:,:,iwake])

	### refined grid

	Ux_XY_r        = np.zeros((nc_r[0],nc_r[1]))
	Ux_XY_r[:,:]   = np.matrix.transpose(Ux_r[kcentr_r,:,:])
	Uy_XY_r        = np.zeros((nc_r[0],nc_r[1]))
	Uy_XY_r[:,:]   = np.matrix.transpose(Uy_r[kcentr_r,:,:])
	Uz_XY_r        = np.zeros((nc_r[0],nc_r[1]))
	Uz_XY_r[:,:]   = np.matrix.transpose(Uz_r[kcentr_r,:,:])
	Utot_XY_r      = np.zeros((nc_r[0],nc_r[1]))
	Utot_XY_r[:,:] = np.matrix.transpose(Utot_r[kcentr_r,:,:])

	Ux_XZ_r        = np.zeros((nc_r[0],nc_r[2]))
	Ux_XZ_r[:,:]   = np.matrix.transpose(Ux_r[:,jcentr_r,:])
	Uy_XZ_r        = np.zeros((nc_r[0],nc_r[2]))
	Uy_XZ_r[:,:]   = np.matrix.transpose(Uy_r[:,jcentr_r,:])
	Uz_XZ_r        = np.zeros((nc_r[0],nc_r[2]))
	Uz_XZ_r[:,:]   = np.matrix.transpose(Uz_r[:,jcentr_r,:])
	Utot_XZ_r      = np.zeros((nc_r[0],nc_r[2]))
	Utot_XZ_r[:,:] = np.matrix.transpose(Utot_r[:,jcentr_r,:])

	Ux_YZ_term_r        = np.zeros((nc_r[1],nc_r[2]))
	Ux_YZ_term_r[:,:]   = np.matrix.transpose(Ux_r[:,:,icentr_r])
	Uy_YZ_term_r        = np.zeros((nc_r[1],nc_r[2]))
	Uy_YZ_term_r[:,:]   = np.matrix.transpose(Uy_r[:,:,icentr_r])
	Uz_YZ_term_r        = np.zeros((nc_r[1],nc_r[2]))
	Uz_YZ_term_r[:,:]   = np.matrix.transpose(Uz_r[:,:,icentr_r])
	Utot_YZ_term_r      = np.zeros((nc_r[1],nc_r[2]))
	Utot_YZ_term_r[:,:] = np.matrix.transpose(Utot_r[:,:,icentr_r])

	if zoom == True:
		Xmin = -6.0
		Xmax = 6.0
		Ymin = -6.0
		Ymax = 6.0
		Zmin = -6.0
		Zmax = 6.0
	else:
		Xmin=X_XY[0][0]
		Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1]
		Ymin=Y_XY[0][0]
		Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1]
		Zmin=Z_XZ[0][0]
		Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1]

	fig_size=(7.5,6)

	# -- Ux --
	# **************************************************************************
	plot_mltg(dest_dir,rundate,diagtime,"Ux [km/s]","Ux_mtlg_XY_Europa",X_XY,Y_XY,Ux_XY,X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Ux_XY_r[:-1,:-1],min_val[0],max_val[0],'X [R_G]','Y [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Ux [km/s]","Ux_mtlg_r_XY_Europa",X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Ux_XY_r[:-1,:-1],min_val[0],max_val[0],'X [R_G]','Y [R_G]',fig_size,"viridis")
	plot_mltg(dest_dir,rundate,diagtime,"Ux [km/s]","Ux_mtlg_XZ_Europa",X_XZ,Z_XZ,Ux_XZ,X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Ux_XZ_r[:-1,:-1],min_val[0],max_val[0],'X [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Ux [km/s]","Ux_mtlg_r_XZ_Europa",X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Ux_XZ_r[:-1,:-1],min_val[0],max_val[0],'X [R_G]','Z [R_G]',fig_size,"viridis")
	plot_mltg(dest_dir,rundate,diagtime,"Ux [km/s]","Ux_mtlg_YZ_terminator_Europa",Y_YZ,Z_YZ,Ux_YZ_term,Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Ux_YZ_term_r[:-1,:-1],min_val[0],max_val[0],'Y [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Ux [km/s]","Ux_mtlg_r_YZ_terminator_Europa",Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Ux_YZ_term_r[:-1,:-1],min_val[0],max_val[0],'Y [R_G]','Z [R_G]',fig_size,"viridis")

	plt.close('all')

	# -- Uy --
	# **************************************************************************
	plot_mltg(dest_dir,rundate,diagtime,"Uy [km/s]","Uy_mtlg_XY_Europa",X_XY,Y_XY,Uy_XY,X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Uy_XY_r[:-1,:-1],min_val[1],max_val[1],'X [R_G]','Y [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Uy [km/s]","Uy_mtlg_r_XY_Europa",X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Uy_XY_r[:-1,:-1],min_val[1],max_val[1],'X [R_G]','Y [R_G]',fig_size,"viridis")
	plot_mltg(dest_dir,rundate,diagtime,"Uy [km/s]","Uy_mtlg_XZ_Europa",X_XZ,Z_XZ,Uy_XZ,X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Uy_XZ_r[:-1,:-1],min_val[1],max_val[1],'X [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Uy [km/s]","Uy_mtlg_r_XZ_Europa",X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Uy_XZ_r[:-1,:-1],min_val[1],max_val[1],'X [R_G]','Z [R_G]',fig_size,"viridis")
	plot_mltg(dest_dir,rundate,diagtime,"Uy [km/s]","Uy_mtlg_YZ_terminator_Europa",Y_YZ,Z_YZ,Uy_YZ_term,Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Uy_YZ_term_r[:-1,:-1],min_val[1],max_val[1],'Y [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Uy [km/s]","Uy_mtlg_r_YZ_terminator_Europa",Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Uy_YZ_term_r[:-1,:-1],min_val[1],max_val[1],'Y [R_G]','Z [R_G]',fig_size,"viridis")

	plt.close('all')
	
	# -- Uz --
	# **************************************************************************
	plot_mltg(dest_dir,rundate,diagtime,"Uz [km/s]","Uz_mtlg_XY_Europa",X_XY,Y_XY,Uz_XY,X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Uz_XY_r[:-1,:-1],min_val[2],max_val[2],'X [R_G]','Y [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Uz [km/s]","Uz_mtlg_r_XY_Europa",X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Uz_XY_r[:-1,:-1],min_val[2],max_val[2],'X [R_G]','Y [R_G]',fig_size,"viridis")
	plot_mltg(dest_dir,rundate,diagtime,"Uz [km/s]","Uz_mtlg_XZ_Europa",X_XZ,Z_XZ,Uz_XZ,X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Uz_XZ_r[:-1,:-1],min_val[2],max_val[2],'X [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Uz [km/s]","Uz_mtlg_r_XZ_Europa",X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Uz_XZ_r[:-1,:-1],min_val[2],max_val[2],'X [R_G]','Z [R_G]',fig_size,"viridis")
	plot_mltg(dest_dir,rundate,diagtime,"Uz [km/s]","Uz_mtlg_YZ_terminator_Europa",Y_YZ,Z_YZ,Uz_YZ_term,Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Uz_YZ_term_r[:-1,:-1],min_val[2],max_val[2],'Y [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"Uz [km/s]","Uz_mtlg_r_YZ_terminator_Europa",Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Uz_YZ_term_r[:-1,:-1],min_val[2],max_val[2],'Y [R_G]','Z [R_G]',fig_size,"viridis")

	plt.close('all')
	
	# -- Utot --
	# **************************************************************************
	plot_mltg(dest_dir,rundate,diagtime,"||U|| [km/s]","Utot_mtlg_XY_Europa",X_XY,Y_XY,Utot_XY,X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Utot_XY_r[:-1,:-1],min_valtot,max_valtot,'X [R_G]','Y [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"||U|| [km/s]","Utot_mtlg_r_XY_Europa",X_XY_r[:-1,:-1],Y_XY_r[:-1,:-1],Utot_XY_r[:-1,:-1],min_valtot,max_valtot,'X [R_G]','Y [R_G]',fig_size,"viridis")
	plot_mltg(dest_dir,rundate,diagtime,"||U|| [km/s]","Utot_mtlg_XZ_Europa",X_XZ,Z_XZ,Utot_XZ,X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Utot_XZ_r[:-1,:-1],min_valtot,max_valtot,'X [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"||U|| [km/s]","Utot_mtlg_r_XZ_Europa",X_XZ_r[:-1,:-1],Z_XZ_r[:-1,:-1],Utot_XZ_r[:-1,:-1],min_valtot,max_valtot,'X [R_G]','Z [R_G]',fig_size,"viridis")
	plot_mltg(dest_dir,rundate,diagtime,"||U|| [km/s]","Utot_mtlg_YZ_terminator_Europa",Y_YZ,Z_YZ,Utot_YZ_term,Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Utot_YZ_term_r[:-1,:-1],min_valtot,max_valtot,'Y [R_G]','Z [R_G]',fig_size,"viridis",zoom,Xmin,Xmax,Ymin,Ymax)
	plot_mltg_r(dest_dir_r,rundate,diagtime,"||U|| [km/s]","Utot_mtlg_r_YZ_terminator_Europa",Y_YZ_r[:-1,:-1],Z_YZ_r[:-1,:-1],Utot_YZ_term_r[:-1,:-1],min_valtot,max_valtot,'Y [R_G]','Z [R_G]',fig_size,"viridis")

