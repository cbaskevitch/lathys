from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

datatype = sys.argv[1]

ncid = Dataset(sys.argv[2])
var_nc = ncid.variables

planetname = var_nc['planetname'][:]
centr      = var_nc['s_centr'][:]
radius     = var_nc['r_planet'][:]
gs         = var_nc['gstep'][:]
gs_r       = var_nc['gstep_r'][:]
s_min_r    = var_nc['s_min_r'][:] #indice de la premiere cellule de la grille rafinee dans la grande grille
data         = var_nc[datatype][:]
data_r       = var_nc[datatype+'_r'][:]
n0          = var_nc['phys_density'][:]
phys_length  = var_nc['phys_length'][:][0]
radius=1.0
nc = [len(data[0][0]), len(data[0]), len(data)]
nc_r = [len(data_r[0][0]), len(data_r[0]), len(data_r)]

if datatype == "Density":
    data = np.where(data <= 0, float('NaN'), np.log10(data))
    data_r = np.where(data_r <= 0, float('NaN'), np.log10(data_r))

# maximum and minimum 
# if radius > 1.0:
#     min_val = -1.0 # log(cm-3) for run with planete
#     max_val = 2.5  # log(sm-3) for run with planete
# else:

min_val = np.min([np.min(data), np.min(data_r)]) # log(cm-3) for run without planete
max_val = np.max([np.max(data), np.max(data_r)]) # log(cm-3) for run without planete
min_val = -412 # log(cm-3) for run without planete
max_val = -408 # log(cm-3) for run without planete
print("min/max = ",min_val,max_val)

# -- Creation of axis values centered on the planet ( normalized to planet radius)
X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)

# -- Creation of axis values centered on the planet ( normalized to planet radius) for the small grid
X_XY_r, Y_XY_r = np.meshgrid(np.arange(0,(nc_r[0])*gs_r[0],gs_r[0]),np.arange(0,(nc_r[1])*gs_r[1],gs_r[1]))
X_XY_r = np.divide(np.matrix.transpose(X_XY_r), radius) - np.divide((centr[0]-s_min_r[0]+0.5*gs_r[0])*np.ones((nc_r[0],nc_r[1])), radius)
Y_XY_r = np.divide(np.matrix.transpose(Y_XY_r), radius) - np.divide((centr[1]-s_min_r[1]+0.5*gs_r[1])*np.ones((nc_r[0],nc_r[1])), radius)

X_XZ_r, Z_XZ_r = np.meshgrid(np.arange(0,(nc_r[0])*gs_r[0],gs_r[0]),np.arange(0,(nc_r[2])*gs_r[2],gs_r[2]))
X_XZ_r = np.divide(np.matrix.transpose(X_XZ_r), radius) - np.divide((centr[0]-s_min_r[0]+0.5*gs_r[0])*np.ones((nc_r[0],nc_r[2])), radius)
Z_XZ_r = np.divide(np.matrix.transpose(Z_XZ_r), radius) - np.divide((centr[2]-s_min_r[2]+0.5*gs_r[2])*np.ones((nc_r[0],nc_r[2])), radius)

Y_YZ_r, Z_YZ_r = np.meshgrid(np.arange(0,(nc_r[1])*gs_r[1],gs_r[1]),np.arange(0,(nc_r[2])*gs_r[2],gs_r[2]))
Y_YZ_r = np.divide(np.matrix.transpose(Y_YZ_r), radius) - np.divide((centr[1]-s_min_r[1]+0.5*gs_r[1])*np.ones((nc_r[1],nc_r[2])), radius)
Z_YZ_r = np.divide(np.matrix.transpose(Z_YZ_r), radius) - np.divide((centr[2]-s_min_r[2]+0.5*gs_r[2])*np.ones((nc_r[1],nc_r[2])), radius)

# planet center in cell number (NB: cell number start at 1
icentr = int(np.fix(centr[0]/gs[0]))
jcentr = int(np.fix(centr[1]/gs[1]))
kcentr = int(np.fix(centr[2]/gs[2]))
iwake = int(icentr + np.fix(1.5*radius/gs[0]))

icentr_r = int(np.fix((centr[0]-s_min_r[0])/gs_r[0]))
jcentr_r = int(np.fix((centr[1]-s_min_r[1])/gs_r[1]))
kcentr_r = int(np.fix((centr[2]-s_min_r[2])/gs_r[2]))
# iwake_r = int(icentr + np.fix(1.5*radius/gs_r[0]))

print(icentr_r,jcentr_r,kcentr_r)

data_XY = np.zeros((nc[0],nc[1]))
data_XY[:,:] = np.matrix.transpose(data[kcentr,:,:])

data_XZ = np.zeros((nc[0],nc[2]))
data_XZ[:,:] = np.matrix.transpose(data[:,jcentr,:])

data_YZ_term = np.zeros((nc[1],nc[2]))
data_YZ_term[:,:] = np.matrix.transpose(data[:,:,icentr])

data_YZ_wake = np.zeros((nc[1],nc[2]))
data_YZ_wake[:,:] = np.matrix.transpose(data[:,:,iwake])

data_XY_r = np.zeros((nc_r[0],nc_r[1]))
data_XY_r[:,:] = np.matrix.transpose(data_r[kcentr_r,:,:])

data_XZ_r = np.zeros((nc_r[0],nc_r[2]))
data_XZ_r[:,:] = np.matrix.transpose(data_r[:,jcentr_r,:])

data_YZ_term_r = np.zeros((nc_r[1],nc_r[2]))
data_YZ_term_r[:,:] = np.matrix.transpose(data_r[:,:,icentr_r])

# data_YZ_wake_r = np.zeros((nc_r[1],nc_r[2]))
# data_YZ_wake_r[:,:] = np.matrix.transpose(data_r[:,:,iwake_r])


# datae_1D = np.zeros(nc[0])
# datae_1D[:] = data[kcentr,jcentr,:]
# x = np.divide(np.arange(0, nc[0], 1.0) * gs[0]-centr[0], radius)
# x = ([0:nc(1)-1]*gs(1)-centr(1))./radius;


# if zoom == True:
#     Xmin = -6.0
#     Xmax = 6.0
#     Ymin = -6.0
#     Ymax = 6.0
#     Zmin = -6.0
#     Zmax = 6.0
# else:
Xmin=X_XY[0][0]
Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1]
Ymin=Y_XY[0][0]
Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1]
Zmin=Z_XZ[0][0]
Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1]

fig_size=(7.5,6)


# planet drawing
theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
xp = np.cos(theta)
yp = np.sin(theta)

# if zoom == True:
#     fig, ax = plt.subplots(figsize=(7.5,6))
#     ax.set_xlim(Xmin,Xmax)
#     ax.set_ylim(Ymin,Ymax)
# else:
fig, ax = plt.subplots(figsize=fig_size)
c = ax.pcolor(X_XY, Y_XY, data_XY, vmin=min_val, vmax=max_val, cmap="viridis",shading='auto')
## Field lines
# if isFieldLines==True:
#     if zoom == True:
#         field_lines_dens = 4
#     else:
#         field_lines_dens = 1
    
Xmin=X_XY[0][0] #float(-1*icentr/rp)
Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1] #float((nc[0]-icentr)/rp)
X_norm=np.arange(Xmin, Xmax, (Xmax-Xmin)/len(data_XY))
Ymin=Y_XY[0][0] #float(-jcentr/rp)
Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1] #float((nc[1]-jcentr)/rp)
Y_norm=np.arange(Ymin, Ymax, (Ymax-Ymin)/len(data_XY[0]))
# ax.streamplot(X_norm,Y_norm,np.transpose(FLdata1),np.transpose(FLdata2),color=FieldCol,density=field_lines_dens, arrowstyle='->', arrowsize=1.,linewidth=1)

c_r = ax.pcolor(X_XY_r, Y_XY_r, data_XY_r, vmin=min_val, vmax=max_val, cmap="viridis",shading='auto')
rect = plt.Rectangle((X_XY_r[0][0],Y_XY_r[0][0]),X_XY_r[-1][-1]-X_XY_r[0][0], Y_XY_r[-1][-1]-Y_XY_r[0][0],fill=False)
ax.add_patch(rect)

for iproc in range(0,64):
    if iproc<10:
        ncid = Dataset("../Pro3/Pro3_00"+str(iproc)+"_19_09_22_t00300.nc")
    else:
        ncid = Dataset("../Pro3/Pro3_0"+str(iproc)+"_19_09_22_t00300.nc")
    var_nc = ncid.variables
    s_min_loc = var_nc["s_min_loc"][:]
    s_max_loc = var_nc["s_max_loc"][:]
    y_proc_min = s_min_loc[1]#*phys_length*gs[1]/radius
    y_proc_max = s_max_loc[1]#*phys_length*gs[1]/radius
    # print(s_min_loc[1],s_max_loc[1],phys_length,gs[1],radius,y_proc_min,y_proc_max)
    rect = plt.Rectangle((X_XY[0][0],Y_XY[0][0]+y_proc_min),X_XY[-1][-1]-X_XY[0][0], y_proc_max-y_proc_min,ec="red",fill=False)
    ax.add_patch(rect)

fig.colorbar(c, ax=ax)
# ax.plot(xp,yp,c="black")
# ax.fill(xp,yp,c="white")

titre = datatype
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel("X")#,'fontsize',12,'fontweight','b');
ax.set_ylabel("Y")#,'fontsize',12,'fontweight','b');

plt.savefig(datatype+"_XY_limits_subdomains_and_refined_grid.png")

plt.close('all')

fig, ax = plt.subplots(figsize=fig_size)
c = ax.pcolor(X_XZ, Z_XZ, data_XZ, vmin=min_val, vmax=max_val, cmap="viridis",shading='auto')

c_r = ax.pcolor(X_XZ_r, Z_XZ_r, data_XZ_r, vmin=min_val, vmax=max_val, cmap="viridis",shading='auto')
rect = plt.Rectangle((X_XZ_r[0][0],Z_XZ_r[0][0]),X_XZ_r[-1][-1]-X_XZ_r[0][0], Z_XZ_r[-1][-1]-Z_XZ_r[0][0],fill=False)
ax.add_patch(rect)

for iproc in range(0,64):
    if iproc<10:
        ncid = Dataset("../Pro3/Pro3_00"+str(iproc)+"_19_09_22_t00300.nc")
    else:
        ncid = Dataset("../Pro3/Pro3_0"+str(iproc)+"_19_09_22_t00300.nc")
    var_nc = ncid.variables
    s_min_loc = var_nc["s_min_loc"][:]
    s_max_loc = var_nc["s_max_loc"][:]
    y_proc_min = s_min_loc[2]#*phys_length*gs[1]/radius
    y_proc_max = s_max_loc[2]#*phys_length*gs[1]/radius
    # print(s_min_loc[1],s_max_loc[1],phys_length,gs[1],radius,y_proc_min,y_proc_max)
    rect = plt.Rectangle((X_XZ[0][0],Z_XZ[0][0]+y_proc_min),X_XZ[-1][-1]-X_XZ[0][0], y_proc_max-y_proc_min,ec="red",fill=False)
    ax.add_patch(rect)

fig.colorbar(c, ax=ax)
# ax.plot(xp,yp,c="black")
# ax.fill(xp,yp,c="white")

titre = datatype
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel("X")#,'fontsize',12,'fontweight','b');
ax.set_ylabel("Z")#,'fontsize',12,'fontweight','b');

plt.savefig(datatype+"_XZ_limits_subdomains_and_refined_grid.png")

plt.close('all')

fig, ax = plt.subplots(figsize=fig_size)
c = ax.pcolor(Y_YZ, Z_YZ, data_YZ_term, vmin=min_val, vmax=max_val, cmap="viridis",shading='auto')

c_r = ax.pcolor(Y_YZ_r, Z_YZ_r, data_YZ_term_r, vmin=min_val, vmax=max_val, cmap="viridis",shading='auto')
rect = plt.Rectangle((Y_YZ_r[0][0],Z_YZ_r[0][0]),Y_YZ_r[-1][-1]-Y_YZ_r[0][0], Z_YZ_r[-1][-1]-Z_YZ_r[0][0],fill=False)
ax.add_patch(rect)

for iproc in range(0,64):
    if iproc<10:
        ncid = Dataset("../Pro3/Pro3_00"+str(iproc)+"_19_09_22_t00300.nc")
    else:
        ncid = Dataset("../Pro3/Pro3_0"+str(iproc)+"_19_09_22_t00300.nc")
    var_nc = ncid.variables
    s_min_loc = var_nc["s_min_loc"][:]
    s_max_loc = var_nc["s_max_loc"][:]
    x_proc_min = s_min_loc[1]#*phys_length*gs[1]/radius
    x_proc_max = s_max_loc[1]#*phys_length*gs[1]/radius
    y_proc_min = s_min_loc[2]#*phys_length*gs[1]/radius
    y_proc_max = s_max_loc[2]#*phys_length*gs[1]/radius
    # print(s_min_loc[1],s_max_loc[1],phys_length,gs[1],radius,y_proc_min,y_proc_max)
    rect = plt.Rectangle((Y_YZ[0][0]+x_proc_min,Z_YZ[0][0]+y_proc_min),x_proc_max-x_proc_min, y_proc_max-y_proc_min,ec="red",fill=False)
    ax.add_patch(rect)

fig.colorbar(c, ax=ax)
# ax.plot(xp,yp,c="black")
# ax.fill(xp,yp,c="white")

titre = datatype
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel("Y")#,'fontsize',12,'fontweight','b');
ax.set_ylabel("Z")#,'fontsize',12,'fontweight','b');

plt.savefig(datatype+"_YZ_limits_subdomains_and_refined_grid.png")

