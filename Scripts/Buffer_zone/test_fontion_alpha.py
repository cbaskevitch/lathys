import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

BF=10
xmin = np.linspace(1,BF,10)
xmax = np.linspace(150-BF,150,10)

alphamin = -1/BF*xmax + 150/BF
alphamax = 1/(BF-1)*xmin - 1/(BF-1)

plt.plot(xmin,alphamin,"r")
plt.plot(xmin,alphamax,"b")
plt.show()