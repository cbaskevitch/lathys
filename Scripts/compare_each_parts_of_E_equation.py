## Calcul chaque composants de l'eq de E et les compare ##

from cmath import nan
from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

mu0 = 4e-7*np.pi
e = 1.6e-19

ncMagw = sys.argv[1] # Magw file
ncidMagw = Dataset(ncMagw)
var_ncMagw = ncidMagw.variables

centr      = var_ncMagw['s_centr'][:]
radius     = var_ncMagw['r_planet'][:]
r_iono     = var_ncMagw['r_iono'][:]
gs         = var_ncMagw['gstep'][:]
B0         = var_ncMagw['phys_mag'][:]
V0         = var_ncMagw['phys_speed'][:]
n0         = var_ncMagw['phys_density'][:]
Te0        = 2*B0**2/(n0*2*mu0*e)#var_ncMagw['phys_temp_e'][:]
x0         = var_ncMagw['phys_length'][:]

X_axis = var_ncMagw["X_axis"][:]
Y_axis = var_ncMagw["Y_axis"][:]
Z_axis = var_ncMagw["Z_axis"][:]

Bx         = var_ncMagw["Bx"][:]*1e-9/B0
By         = var_ncMagw["By"][:]*1e-9/B0
Bz         = var_ncMagw["Bz"][:]*1e-9/B0


ncThew = sys.argv[2] # Thew file
ncidThew = Dataset(ncThew)
var_ncThew = ncidThew.variables

Ux = var_ncThew["Ux"][:]/V0
Uy = var_ncThew["Uy"][:]/V0
Uz = var_ncThew["Uz"][:]/V0
Dn = var_ncThew["Density"][:]*1e6/n0
Te = var_ncThew["Temperature"][:]/Te0
resis = var_ncThew["Resistivity"][:]

ncOjv = sys.argv[3] # Ojv file
ncidOjv = Dataset(ncOjv)
var_ncOjv = ncidOjv.variables
DnOjv = var_ncOjv['Density'][:]*1e6/n0

ncO2pl = sys.argv[4] # O2pl file
ncidO2pl = Dataset(ncO2pl)
var_ncO2pl = ncidO2pl.variables
DnO2pl = var_ncO2pl['Density'][:]*1e6/n0

ncElew = sys.argv[5] # Elew file
ncidElew = Dataset(ncElew)
var_ncElew = ncidElew.variables
E0 = V0*B0
Ex = var_ncElew['Ex'][:]*1e-6/E0
Ey = var_ncElew['Ey'][:]*1e-6/E0
Ez = var_ncElew['Ez'][:]*1e-6/E0
E = np.sqrt(Ex**2 + Ey**2 + Ez**2)

# Jx         = var_ncMagw["Jxperp"][:]*1e-9*B0/(mu0*x0)
# Jy         = var_ncMagw["Jyperp"][:]*1e-9*B0/(mu0*x0)
# Jz         = var_ncMagw["Jzperp"][:]*1e-9*B0/(mu0*x0)

nc = [len(Bx[0][0]),len(Bx[0]),len(Bx)]
gstep_invq = 1/(4.0*gs[:])

selector = np.zeros((8,3))
selector[:,0] = gstep_invq[0]*np.array([-1., 1.,-1., 1.,-1., 1.,-1., 1.])
selector[:,1] = gstep_invq[1]*np.array([-1.,-1., 1., 1.,-1.,-1., 1., 1.])
selector[:,2] = gstep_invq[2]*np.array([-1.,-1.,-1.,-1., 1., 1., 1., 1.])

rmu0 = 1 # car rho0 = 1 dans la simulation
gamma = 5/3


icentr = int(np.fix(centr[0]/gs[0]))-1
jcentr = int(np.fix(centr[1]/gs[1]))-1
kcentr = int(np.fix(centr[2]/gs[2]))-1
iminmax = [icentr-int(np.fix(3*radius/gs[0])), icentr+int(np.fix(3*radius/gs[0]))]
jminmax = [jcentr-int(np.fix(3*radius/gs[1])), jcentr+int(np.fix(3*radius/gs[1]))]
kminmax = [kcentr-int(np.fix(3*radius/gs[2])), kcentr+int(np.fix(3*radius/gs[2]))]

print(iminmax)
print(jminmax)
print(kminmax)

r_axis = np.arange(radius*x0,2*radius*x0,x0*gs[0])
print("r_axis ",r_axis)

u_b_moy = np.zeros(len(r_axis)-1)
g_p_moy = np.zeros(len(r_axis)-1)
r_b_moy = np.zeros(len(r_axis)-1)
E_moy   = np.zeros(len(r_axis)-1)
E_test_moy = np.zeros(len(r_axis)-1)

count = 0


#### E = -(U X B) - (grad_Pe)/rho + (curl(B) X B)/(mu0*rho) + eta*curl(B)   //// avec curl(B)=J
for ii in range(iminmax[0],iminmax[1]):
    for jj in range(jminmax[0],jminmax[1]):
        for kk in range(kminmax[0],kminmax[1]):

            rb = np.sqrt(X_axis[ii]**2 + Y_axis[jj]**2 + Z_axis[kk]**2)
            if r_axis[0] <= rb and rb <= r_axis[-1]:
                for r in range(0,len(r_axis)-1):
                    if r_axis[r] <= rb and rb < r_axis[r+1]:
                        theta = np.arccos(Z_axis[kk]/rb)
                        phi=nan
                        if (X_axis[ii] != 0.0):
                            if X_axis[ii] > 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii])
                            elif Y_axis[jj] >= 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) + np.pi
                            elif Y_axis[jj] < 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) - np.pi
                            elif Y_axis[jj] > 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) + np.pi
                        elif Y_axis[jj] > 0:
                            phi = np.pi/2
                        elif Y_axis[jj] < 0:
                            phi = -np.pi/2
                        phi = (phi+4*np.pi)%(2*np.pi)
                            
                        if np.pi/2.0 <= phi and phi <= 3*np.pi/2.0:    
                            b_eightx = np.array([Bx[kk-1,jj-1,ii-1],Bx[kk-1,jj-1,ii], Bx[kk-1,jj,ii-1], Bx[kk-1,jj,ii], Bx[kk,jj-1,ii-1], Bx[kk,jj-1,ii], Bx[kk,jj,ii-1], Bx[kk,jj,ii]])#*1e-9
                            b_eighty = np.array([By[kk-1,jj-1,ii-1],By[kk-1,jj-1,ii], By[kk-1,jj,ii-1], By[kk-1,jj,ii], By[kk,jj-1,ii-1], By[kk,jj-1,ii], By[kk,jj,ii-1], By[kk,jj,ii]])#*1e-9
                            b_eightz = np.array([Bz[kk-1,jj-1,ii-1],Bz[kk-1,jj-1,ii], Bz[kk-1,jj,ii-1], Bz[kk-1,jj,ii], Bz[kk,jj-1,ii-1], Bz[kk,jj-1,ii], Bz[kk,jj,ii-1], Bz[kk,jj,ii]])#*1e-9

                            # curl B
                            curl_bx = np.sum(selector[:,1]*b_eightz[:]-selector[:,2]*b_eighty[:])
                            curl_by = np.sum(selector[:,2]*b_eightx[:]-selector[:,0]*b_eightz[:])
                            curl_bz = np.sum(selector[:,0]*b_eighty[:]-selector[:,1]*b_eightx[:])

                            # B local
                            bix = 1/8*np.sum(b_eightx)
                            biy = 1/8*np.sum(b_eighty)
                            biz = 1/8*np.sum(b_eightz)

                            # U local
                            uix = 1/8*(Ux[kk-1,jj-1,ii-1] + Ux[kk-1,jj-1,ii] + Ux[kk-1,jj,ii-1] + Ux[kk-1,jj,ii] + Ux[kk,jj-1,ii-1] + Ux[kk,jj-1,ii] + Ux[kk,jj,ii-1] + Ux[kk,jj,ii])
                            uiy = 1/8*(Uy[kk-1,jj-1,ii-1] + Uy[kk-1,jj-1,ii] + Uy[kk-1,jj,ii-1] + Uy[kk-1,jj,ii] + Uy[kk,jj-1,ii-1] + Uy[kk,jj-1,ii] + Uy[kk,jj,ii-1] + Uy[kk,jj,ii])
                            uiz = 1/8*(Uz[kk-1,jj-1,ii-1] + Uz[kk-1,jj-1,ii] + Uz[kk-1,jj,ii-1] + Uz[kk-1,jj,ii] + Uz[kk,jj-1,ii-1] + Uz[kk,jj-1,ii] + Uz[kk,jj,ii-1] + Uz[kk,jj,ii])

                            # ui cross bi
                            u_bx = uiy*biz - uiz*biy
                            u_by = uiz*bix - uix*biz
                            u_bz = uix*biy - uiy*bix

                            # curl B X bi
                            r_bx = curl_by*biz - curl_bz*biy
                            r_by = curl_bz*bix - curl_bx*biz
                            r_bz = curl_bx*biy - curl_by*bix

                            # Dn local
                            dn = 1/8*(Dn[kk-1,jj-1,ii-1] + Dn[kk-1,jj-1,ii] + Dn[kk-1,jj,ii-1] + Dn[kk-1,jj,ii] + Dn[kk,jj-1,ii-1] + Dn[kk,jj-1,ii] + Dn[kk,jj,ii-1] + Dn[kk,jj,ii])
                            dni = 1/dn

                            #Pe local
                            pe_local = np.zeros(8)
                            gamma_iono = gamma
                            
                            if DnO2pl[kk,jj,ii] > 1.1:
                                gamma_iono = 1/np.log10(DnO2pl[kk,jj,ii])
                                gamma_iono = gamma_iono/(np.sqrt(np.sqrt(1+gamma_iono**4/gamma**4)))
                            if (DnO2pl[kk,jj,ii]+DnOjv[kk,jj,ii]) > 0:
                                print("Dn_O2pl ",DnO2pl[kk-1,jj-1,ii-1],DnO2pl[kk-1,jj-1,ii],DnO2pl[kk-1,jj,ii-1],DnO2pl[kk-1,jj,ii],DnO2pl[kk,jj-1,ii-1],DnO2pl[kk,jj-1,ii],DnO2pl[kk,jj,ii-1],DnO2pl[kk,jj,ii])
                                print("Dn_Ojv  ",DnOjv[kk-1,jj-1,ii-1],DnOjv[kk-1,jj-1,ii],DnOjv[kk-1,jj,ii-1],DnOjv[kk-1,jj,ii],DnOjv[kk,jj-1,ii-1],DnOjv[kk,jj-1,ii],DnOjv[kk,jj,ii-1],DnOjv[kk,jj,ii])
                                print("Te_jv   ",Te[kk-1,jj-1,ii-1],Te[kk-1,jj-1,ii],Te[kk-1,jj,ii-1],Te[kk-1,jj,ii],Te[kk,jj-1,ii-1],Te[kk,jj-1,ii],Te[kk,jj,ii-1],Te[kk,jj,ii])

                                pe_tmp = Te[kk-1,jj-1,ii-1]*DnOjv[kk-1,jj-1,ii-1]**gamma + Te[kk-1,jj-1,ii-1]*0.001*DnO2pl[kk-1,jj-1,ii-1]**gamma_iono
                                pe_local[0] = pe_tmp
                                pe_tmp = Te[kk-1,jj-1,ii]*DnOjv[kk-1,jj-1,ii]**gamma + Te[kk-1,jj-1,ii]*0.001*DnO2pl[kk-1,jj-1,ii]**gamma_iono
                                pe_local[1] = pe_tmp
                                pe_tmp = Te[kk-1,jj,ii-1]*DnOjv[kk-1,jj,ii-1]**gamma + Te[kk-1,jj,ii-1]*0.001*DnO2pl[kk-1,jj,ii-1]**gamma_iono
                                pe_local[2] = pe_tmp
                                pe_tmp = Te[kk-1,jj,ii]*DnOjv[kk-1,jj,ii]**gamma + Te[kk-1,jj,ii]*0.001*DnO2pl[kk-1,jj,ii]**gamma_iono
                                pe_local[3] = pe_tmp
                                pe_tmp = Te[kk,jj-1,ii-1]*DnOjv[kk,jj-1,ii-1]**gamma + Te[kk,jj-1,ii-1]*0.001*DnO2pl[kk,jj-1,ii-1]**gamma_iono
                                pe_local[4] = pe_tmp
                                pe_tmp = Te[kk,jj-1,ii]*DnOjv[kk,jj-1,ii]**gamma + Te[kk,jj-1,ii]*0.001*DnO2pl[kk,jj-1,ii]**gamma_iono
                                pe_local[5] = pe_tmp
                                pe_tmp = Te[kk,jj,ii-1]*DnOjv[kk,jj,ii-1]**gamma + Te[kk,jj,ii-1]*0.001*DnO2pl[kk,jj,ii-1]**gamma_iono
                                pe_local[6] = pe_tmp
                                pe_tmp = Te[kk,jj,ii]*DnOjv[kk,jj,ii]**gamma + Te[kk,jj,ii]*0.001*DnO2pl[kk,jj,ii]**gamma_iono
                                pe_local[7] = pe_tmp
                            
                            # grad(Pe)
                            g_p = np.matmul(pe_local,selector)*dni

                            # E = -(U X B) - (grad_Pe)/rho + (curl(B) X B)/(mu0*rho) + eta*curl(B)   //// avec curl(B)=J
                            Ex_test = -u_bx*dni - g_p[0] + r_bx*dni + curl_bx*resis[kk,jj,ii]
                            Ey_test = -u_by*dni - g_p[1] + r_by*dni + curl_by*resis[kk,jj,ii]
                            Ez_test = -u_bz*dni - g_p[2] + r_bz*dni + curl_bz*resis[kk,jj,ii]
                            E_test = np.sqrt(Ex_test**2 + Ey_test**2 + Ez_test**2)

                            ## Fin des calculs des differentes parties de E
                            ## Calcul des moyennes

                            u_b_moy[r] += np.sqrt(u_bx**2 + u_by**2 + u_bz**2)*dni
                            r_b_moy[r] += np.sqrt(r_bx**2 + r_by**2 + r_bz**2)*dni
                            g_p_moy[r] += np.sqrt(g_p[0]**2 + g_p[1]**2 + g_p[2]**2)
                            E_moy[r]   += E[kk,jj,ii]
                            E_test_moy[r] += E_test

                            count += 1
                    

u_b_moy = u_b_moy/count
r_b_moy = r_b_moy/count
g_p_moy = g_p_moy/count
E_moy   = E_moy/count
E_test_moy = E_test_moy/count

print("u_b    = ",u_b_moy)
print("r_b    = ",r_b_moy)
print("g_p    = ",g_p_moy)
print("E      = ",E_moy)
print("E test = ",E_test_moy)

plt.plot(r_axis[:-1]-r_axis[0],u_b_moy,c="blue",ls="--",label="$U\\times B$")
plt.plot(r_axis[:-1]-r_axis[0],r_b_moy,c="red",label="$\\frac{(\\nabla \\times B)\\times B}{\\mu_0 \\rho}$")
plt.plot(r_axis[:-1]-r_axis[0],g_p_moy,c="green",label="$\\frac{\\nabla P_e}{\\rho}$")
plt.grid(True)
plt.yscale('log')
plt.legend(loc="lower right")
plt.xlabel('altitude from surface [$km$]')
plt.ylabel('normalized')
plt.savefig("compare_each_parts_of_E_equation.png")

plt.close('all')

plt.plot(r_axis[:-1]-r_axis[0],E_test_moy,c="blue",ls="--",label="$E^{test}$")
plt.plot(r_axis[:-1]-r_axis[0],E_moy,c="red",label="$E^{sim}$")
plt.grid(True)
plt.yscale('log')
plt.legend(loc="lower right")
plt.xlabel('altitude from surface [$km$]')
plt.ylabel('E [$E_0$]')
plt.savefig("verify_E_equation.png")

plt.close('all')
