#######
# Take a lathys file and extract a sub grid of -/+4RE et X,Y,Z axis
#######


from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

filename = sys.argv[1]
ncid = Dataset(filename)
input_dims = ncid.dimensions
var_nc = ncid.variables


#### Writing new file
outfile = Dataset("EGM_O2_Harris_profile.nc", "w", format="NETCDF4",diskless=True,persist=True)

output_dims = []
for dims in input_dims.keys():
    outfile.createDimension(dims,input_dims[dims].size)

for var in var_nc.keys():
    if ("density" not in var) and ("Temperature" not in var) and ("Ur" not in var) and ("Ut" not in var) and ("Up" not in var) :
        var_out = outfile.createVariable(var,var_nc[var].dtype,var_nc[var].dimensions)
        var_out[:] = var_nc[var][:]

nO2_Harris = np.zeros((input_dims["longitude"].size, input_dims["latitude"].size, input_dims["altitude"].size))
altitude = var_nc["altitude"][:]
radius = var_nc["Planetary_Radius"][:]*1e-5
n0 = 2.5e7 # cm-3
H0=100 #km

for i in range(0,input_dims["altitude"].size):
    r = altitude[0,0,i] - radius[0]
    print(r,n0 * np.exp(-r/H0))
    nO2_Harris[:,:,i] = n0 * np.exp(-r/H0)

var_out = outfile.createVariable("density_O2",var_nc["density_O2"].dtype,var_nc["density_O2"].dimensions)
var_out[:,:,:] = nO2_Harris[:,:,:]
        