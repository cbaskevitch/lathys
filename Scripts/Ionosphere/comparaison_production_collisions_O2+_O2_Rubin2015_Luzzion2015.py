import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

# Ce programme calcul la moyenne d une fonction qui calcule la proba de collision lors de reaction
# d echange de charge entre O2 et O2+. <f(x)> = 1/(b-a)*int[a,b](f(x)dx)
# J ai calcule la moyenne sur papier et donc ce programme verifie qu il soit bon.
# P = k_O2_O2+ * nO2+ * dt * phys_time
# k_O2_O2+ -> eq 26 (Rubin, 2015)

Kb   = 1.38e-23  ## Constante de Boltzmann en m^2 kg s^-2 K-1
e_Cb = 1.602e-19 ## Charge elementaire en C (A.s)

t0 = 0.36 # phys_time
dt = 1e-2 # pas de temps classique que j utilise dans les simuls

# altitude
h = np.linspace(0,600,600) # km

nO2  = np.zeros(len(h))
nO2p = np.zeros(len(h))

# O2+
n0_O2p = 2500 #cm-3
H0_O2p = 240   #km
H1_O2p = 440   #km
n1_O2p = n0_O2p*np.exp(-300/H0_O2p)*np.exp(300/H1_O2p) #cm-3

# nO2p = n0*np.exp(-h/H0)
for i in range(0,len(h)):
    if h[i] <= 300:
        nO2p[i]=n0_O2p*np.exp(-h[i]/H0_O2p)
    else:
        nO2p[i]=n1_O2p*np.exp(-h[i]/H1_O2p)

# O2
n0_O2 = 2.5e7 #cm-3
H0_O2 = 100   # km
nO2 = n0_O2*np.exp(-h/H0_O2)

Tr = 0.01*e_Cb/Kb#2.0*e_Cb/Kb # Kelvin

kO2_O2p_Rubin = 2.59e-11 * nO2 * np.sqrt(Tr) *(1 - 0.073 * np.log10(Tr))**2 #m3.s-1

P_Rubin = (kO2_O2p_Rubin * dt * t0)*100


### Elastic collision Liuzzo 2015, cf eq 17 and 18

kO2_O2p_Liuzzo = 1.060e-8*np.ones(len(h))  #cm3.s-1
P_Liuzzo = (kO2_O2p_Liuzzo * nO2 * dt * t0)*100

plt.plot(kO2_O2p_Rubin, h,label="$k_{O_{2}.O_{2}^{+}}$ Rubin 2015")
plt.plot(kO2_O2p_Liuzzo,h,label="$k_{O_{2}.O_{2}^{+}}$ Liuzzo 2015")
plt.ylim(0,600)
# plt.xlim(1e-1,1e3)
plt.xlabel("collision rates [$cm^3.s^{-1}$]")
plt.ylabel("altitude [km]")
# plt.xscale("log")
plt.grid(True,'both','both',ls=":",color="lightgrey")
plt.legend(loc="upper right")
plt.savefig("comparision_coll_rates_O2+_O2_Rubin2015_Liuzzo2015.png")
plt.close('all')


# print(P)

plt.plot(P_Rubin,h,label="$P(z)_{O_{2}.O_{2}^{+}}$ Rubin 2015")
plt.plot(P_Liuzzo,h,label="$P(z)_{O_{2}.O_{2}^{+}}$ Liuzzo 2015")
plt.ylim(0,600)
# plt.xlim(1e-1,1e3)
plt.xlabel("collision probability [%]")
plt.ylabel("altitude [km]")
# plt.xscale("log")
plt.grid(True,'both','both',ls=":",color="lightgrey")
plt.legend(loc="upper right")
plt.savefig("comparision_proba_coll_O2+_O2_Rubin2015_Liuzzo2015.png")
plt.close('all')
