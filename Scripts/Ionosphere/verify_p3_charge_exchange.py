from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import sys, os


src_dir = sys.argv[1]
# dest_dir = sys.argv[2]
date = sys.argv[2]
time = sys.argv[3]

ncid = Dataset(src_dir+"/p3_000_"+date+"_t"+time+".nc")
var_nc = ncid.variables

nproc = var_nc["nproc"][:] # number of procs
nptot = var_nc["nptot"][:] # total number of particules of proc 0

orig = var_nc["particule_exc"][:]
mask = np.where(orig > 0, True, False) # loading ionosphere
vx = np.extract(mask, var_nc["particule_vx"][:])
vy = np.extract(mask, var_nc["particule_vy"][:])
vz = np.extract(mask, var_nc["particule_vz"][:])

npexc = 0 # nb of charge exchange parts
npexc += len(vx)

vxexc = vx
vyexc = vy
vzexc = vz


for i in range(1,nproc[0]):
    if i < 10:
        ncid = Dataset(src_dir+"/p3_00"+str(i)+"_"+date+"_t"+time+".nc")
    elif i < 100:
        ncid = Dataset(src_dir+"/p3_0"+str(i)+"_"+date+"_t"+time+".nc")
    else:
        ncid = Dataset(src_dir+"/p3_"+str(i)+"_"+date+"_t"+time+".nc")
    var_nc = ncid.variables


    orig = var_nc["particule_exc"][:]
    mask = np.where(orig > 0, True, False) # loading ionosphere
    vx = np.extract(mask, var_nc["particule_vx"][:])
    vy = np.extract(mask, var_nc["particule_vy"][:])
    vz = np.extract(mask, var_nc["particule_vz"][:])

    npexc += len(vx)
    vxexc = np.concatenate((vxexc,vx),axis=None)
    vyexc = np.concatenate((vyexc,vy),axis=None)
    vzexc = np.concatenate((vzexc,vz),axis=None)


print("charge exchange : ",npexc," ; ", "% ; <vx> = ",np.sum(vxexc)/npexc, " ; <vy> = ",np.sum(vyexc)/npexc, " ; <vz> = ",np.sum(vzexc)/npexc)
# print("frozen iono  : ",npf," ; ","{:4.2f}".format(npf/(npli+npf)), "% ; <vx> = ",np.sum(vxf)/npf, " ; <vy> = ",np.sum(vyf)/npf, " ; <vz> = ",np.sum(vzf)/npf)
# print("total parts  : ", npli+npf)

