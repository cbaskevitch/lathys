import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

# Ce programme calcul la moyenne d une fonction qui calcule la proba de collision lors de reaction
# d echange de charge entre O2 et O2+. <f(x)> = 1/(b-a)*int[a,b](f(x)dx)
# J ai calcule la moyenne sur papier et donc ce programme verifie qu il soit bon.
# P = k_O2_O2+ * nO2+ * dt * phys_time
# k_O2_O2+ -> eq 26 (Rubin, 2015)

Kb   = 1.38e-23  ## Constante de Boltzmann en m^2 kg s^-2 K-1
e_Cb = 1.602e-19 ## Charge elementaire en C (A.s)

t0 = 0.36 # phys_time
dt = 1e-2 # pas de temps classique que j utilise dans les simuls

# altitude
h = np.linspace(0,600,60000)

nO2  = np.zeros(len(h))
nO2p = np.zeros(len(h))

# O2+
n0_O2p = 2500 #cm-3
H0_O2p = 240   #km
H1_O2p = 440   #km
n1_O2p = n0_O2p*np.exp(-300/H0_O2p)*np.exp(300/H1_O2p) #cm-3

# nO2p = n0*np.exp(-h/H0)
for i in range(0,len(h)):
    if h[i] <= 300:
        nO2p[i]=n0_O2p*np.exp(-h[i]/H0_O2p)
    else:
        nO2p[i]=n1_O2p*np.exp(-h[i]/H1_O2p)

# O2
n0_O2 = 2.5e7 #cm-3
H0_O2 = 100   # km
nO2 = n0_O2*np.exp(-h/H0_O2)

Tr = 0.05*e_Cb/Kb#2.0*e_Cb/Kb # Kelvin

kO2_O2p = 2.59e-17 * nO2*1e6 * np.sqrt(Tr) *(1 - 0.073 * np.log10(Tr))**2

P = (kO2_O2p * nO2p * dt * t0)*100
# print(P)

plt.plot(P,h,label="$P(z)_{O_{2} + O_{2}^{+}}$ ")
plt.ylim(0,600)
# plt.xlim(1e-1,1e3)
plt.xlabel("collision probability [%]")
plt.ylabel("altitude [km]")
# plt.xscale("log")
plt.grid(True,'both','both',ls=":",color="lightgrey")
plt.legend(loc="upper right")
plt.savefig("collision_probability_ce_O2+_O2_Rubin2015.png")
plt.close('all')

interval_length = 25
nb_intervals = int(h[-1]/interval_length) # chaque interval fait 10km
# print(interval_length,nb_intervals)
# print(h)
P_moy = np.zeros(nb_intervals)
h_moy = np.zeros(nb_intervals)
count = np.zeros(nb_intervals)

#Utiliser le milieu d un interval ou la borne inf ne change rien a la courbe de la probabilite finale
for i in range (0,len(P)-1):
    interval = int(((h[i]+h[i+1])/2)/interval_length)
    # print(h[i],interval)
    P_moy[interval] += (P[i]+P[i+1])/2
    h_moy[interval] += (h[i]+h[i+1])/2
    count[interval] += 1

P_moy /= count
h_moy /= count
# print(h_moy)
# print(P_moy)

P_moy_integral = np.zeros(nb_intervals)
A = 1/interval_length * 2.59e-11 * np.sqrt(Tr) *(1 - 0.073 * np.log10(Tr))**2 * dt * t0 * n0_O2
z = 0
for i in range(0,nb_intervals):
    if z <= 300:
        H_O2p = H0_O2p
        A2 = A*n0_O2p
    else:
        H_O2p = H1_O2p
        A2 = A*n1_O2p
    c = (H0_O2 + H_O2p)/(H0_O2*H_O2p)
    P_moy_integral[i] = A2 * (-1/c * (np.exp(-(z+interval_length)*c) - np.exp(-z*c)))
    z += interval_length

print(P_moy_integral)
P_moy_integral *= 100

plt.plot(P,h,label="$P(z)_{O_{2} + O_{2}^{+}}$")
plt.plot(P_moy,h_moy,ls="dashdot",label="$P(z)_{O_{2} + O_{2}^{+}}$ by intervals of "+str(interval_length)+" km")
plt.plot(P_moy_integral,h_moy,ls="--",label="$P(z)_{O_{2} + O_{2}^{+}}$ by intervals of "+str(interval_length)+" km with integral")
plt.ylim(0,600)
# plt.xlim(1e-1,1e3)
plt.xlabel("collision probability [%]")
plt.ylabel("altitude [km]")
# plt.xscale("log")
plt.grid(True,'both','both',ls=":",color="lightgrey")
plt.legend(loc="upper right")
plt.savefig("collision_probability_by_intervals_"+str(interval_length)+"km_ce_O2+_O2_Rubin2015.png")
