import numpy as np
from netCDF4 import Dataset
import matplotlib.pyplot as plt
import math
import sys, os

# Ce programme calcul la moyenne d une fonction qui calcule la proba de collision lors de reaction
# d echange de charge entre O2 et O2+. <f(x)> = 1/(b-a)*int[a,b](f(x)dx)
# J ai calcule la moyenne sur papier et donc ce programme verifie qu il soit bon.
# P = k_O2_O2+ * nO2+ * dt * phys_time
# k_O2_O2+ -> eq 26 (Rubin, 2015)

Kb   = 1.38e-23  ## Constante de Boltzmann en m^2 kg s^-2 K-1
e_Cb = 1.602e-19 ## Charge elementaire en C (A.s)

#### Calcul densite moyenne cote ram ####

ncid = Dataset(sys.argv[1]) # O2pl
var_nc = ncid.variables
Dn = var_nc["Density"][:]

ncid = Dataset(sys.argv[2]) # Thew
var_nc2 = ncid.variables
Te = var_nc2["Temperature"][:]

X_axis = var_nc["X_axis"][:]
Y_axis = var_nc["Y_axis"][:]
Z_axis = var_nc["Z_axis"][:]

centr       = var_nc['s_centr'][:]
radius      = var_nc['r_planet'][:]
r_iono      = var_nc['r_iono'][:]
gs          = var_nc['gstep'][:]
phys_length = var_nc['phys_length'][:]


nc = [len(Dn[0][0]), len(Dn[0]), len(Dn)]

icentr = int(np.fix(centr[0]/gs[0]))-1
jcentr = int(np.fix(centr[1]/gs[1]))-1
kcentr = int(np.fix(centr[2]/gs[2]))-1
iminmax = [icentr-int(np.fix(3*radius/gs[0])), icentr+int(np.fix(3*radius/gs[0]))]
jminmax = [jcentr-int(np.fix(3*radius/gs[1])), jcentr+int(np.fix(3*radius/gs[1]))]
kminmax = [kcentr-int(np.fix(3*radius/gs[2])), kcentr+int(np.fix(3*radius/gs[2]))]

print(iminmax)
print(jminmax)
print(kminmax)

r_axis = np.arange(1500,1.5*radius*phys_length,phys_length*gs[0])
print("r_axis ",r_axis)

count = np.zeros(len(r_axis)-1)
Dni = np.zeros( len(r_axis)-1)
Tr = np.zeros( len(r_axis)-1)

pts_x = []
pts_y = []
pts_z = []

# jj=jcentr
# kk=kcentr
# print("X_axis", X_axis[iminmax[0]:iminmax[1]])
# print("Y_axis", Y_axis[jminmax[0]:jminmax[1]])
# print("Z_axis", Z_axis[kminmax[0]:kminmax[1]])
for ii in range(iminmax[0],iminmax[1]):#len(X_axis)):
    for jj in range(jminmax[0],jminmax[1]):#len(Y_axis)):
        for kk in range(kminmax[0],kminmax[1]):#len(Z_axis)):
            rb = np.sqrt(X_axis[ii]**2 + Y_axis[jj]**2 + Z_axis[kk]**2)
            if r_axis[0] <= rb and rb <= r_axis[-1]:
                for r in range(0,len(r_axis)-1):
                    if r_axis[r] <= rb and rb < r_axis[r+1]:
                        theta = np.arccos(Z_axis[kk]/rb)
                        phi=np.nan
                        if (X_axis[ii] != 0.0):
                            if X_axis[ii] > 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii])
                            elif Y_axis[jj] >= 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) + np.pi
                            elif Y_axis[jj] < 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) - np.pi
                        elif Y_axis[jj] > 0:
                            phi = np.pi/2
                        elif Y_axis[jj] < 0:
                            phi = -np.pi/2
                        phi = (phi+4*np.pi)%(2*np.pi)
                            
                        if not(np.pi/2.0 <= phi and phi <= 3*np.pi/2.0):

                    
                            count[r] += 1
                            Dni[r] += Dn[kk,jj,ii]
                            Tr[r] += Te[kk,jj,ii]
                            
                            # if rb < r_axis[1]:
                            #     pts_x.append(X_axis[ii])
                            #     pts_y.append(Y_axis[jj])
                            #     pts_z.append(Z_axis[kk])
                            # break


Dni = Dni/count #np.where(Dni != np.nan, Dni/count, 0)
Tr = Tr/count
print(Dni)
print(Tr)
# print(count)

r_axis[:] = r_axis[:] - r_axis[0]
# print(r_axis)

#### Calcul proba ####

t0 = 0.36 # phys_time
dt = 1e-2 # pas de temps classique que j utilise dans les simuls

# altitude
# h = np.linspace(0,600,60000)

nO2  = np.zeros(len(r_axis)-1)
nO2p = np.zeros(len(r_axis)-1)

# O2+
n0_O2p = 2500 #cm-3
H0_O2p = 240   #km
H1_O2p = 440   #km
n1_O2p = n0_O2p*np.exp(-300/H0_O2p)*np.exp(300/H1_O2p) #cm-3

# nO2p = n0*np.exp(-h/H0)
for i in range(0,len(r_axis)-1):
    if r_axis[i] <= 300:
        nO2p[i]=n0_O2p*np.exp(-r_axis[i]/H0_O2p)
    else:
        nO2p[i]=n1_O2p*np.exp(-r_axis[i]/H1_O2p)
print(nO2p)
# O2
n0_O2 = 2.5e7 #cm-3
H0_O2 = 100   # km
nO2 = n0_O2*np.exp(-r_axis/H0_O2)

Tr = Tr*e_Cb/Kb#2.0*e_Cb/Kb # Kelvin

# kO2_O2p = 2.59e-17 * nO2*1e6 * np.sqrt(Tr) *(1 - 0.073 * np.log10(Tr))**2

# P_theorique = (kO2_O2p * nO2p * dt * t0)*100
# print(P)





# interval_length = len(r_axis)-1
# nb_intervals = int(h[-1]/interval_length) # chaque interval fait 10km
# print(interval_length,nb_intervals)
# print(h)
P_simu = np.zeros(len(r_axis)-1)

#Utiliser le milieu d un interval ou la borne inf ne change rien a la courbe de la probabilite finale
# kO2_O2p = 2.59e-17 *1e6 * np.sqrt(Tr) *(1 - 0.073 * np.log10(Tr))**2  * dt * t0
for i in range (0,len(P_simu)):
    hh = (r_axis[i])#+r_axis[i+1])/2
    nO2s=n0_O2*np.exp(-hh/H0_O2)
    # kO2_O2p1 = kO2_O2p * nO2
    # P_simu[i] = (kO2_O2p1 * Dni[i])*100
    kO2_O2p = 2.59e-17 * nO2s*1e6 * np.sqrt(Tr[i]) *(1 - 0.073 * np.log10(Tr[i]))**2

    P_simu[i] = (kO2_O2p * Dni[i] * dt * t0)*100
    
   

# plt.plot(P_theorique,r_axis,label="$P(z)_{O_{2} + O_{2}^{+}}$")
plt.plot(P_simu,r_axis[:-1],label="$P(z)_{O_{2} + O_{2}^{+}}$ density from simulation")
plt.ylim(0,600)
# plt.xlim(1e-1,1e3)
plt.xlabel("collision probability [%]")
plt.ylabel("altitude [km]")
# plt.xscale("log")
plt.grid(True,'both','both',ls=":",color="lightgrey")
plt.legend(loc="upper right")
plt.savefig("collision_probability_ram_Tejv_side_Dniono_ce_O2+_O2_Rubin2015.png")
