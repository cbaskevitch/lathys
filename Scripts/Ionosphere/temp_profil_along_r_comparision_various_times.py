from cmath import nan
from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import scipy.stats as st
import math
import sys, os

####
# plot un profil moyen de la temperature en altitude de 1Re à 2Re
# Moyenne sur la longitude et latitude (à voir si besoin de restraindre au cote ram uniquement)
####

e = 1.6e-19
kB = 1.38e-23

number_of_files = int(sys.argv[1])
temp_files = []
ncids = []
var_ncs = []
Ti = []
times = []
for i in range(0,number_of_files):
    temp_files.append(sys.argv[i+2])
    ncids.append(Dataset(temp_files[i]))
    var_ncs.append(ncids[i].variables)
    Ti.append(var_ncs[i]["Temperature"][:])
    times.append(temp_files[i][-8:-3])
# ncid = Dataset(thew_file)
# ncid2 = Dataset(Ojv_file)
# ncid3 = Dataset(Oiono_file)
# var_nc = ncid.variables
# var_nc2 = ncid2.variables
# var_nc = ncid3.variables
X_axis = var_ncs[0]["X_axis"][:]
Y_axis = var_ncs[0]["Y_axis"][:]
Z_axis = var_ncs[0]["Z_axis"][:]

centr      = var_ncs[0]['s_centr'][:]
radius     = var_ncs[0]['r_planet'][:]
r_iono     = var_ncs[0]['r_iono'][:]
gs         = var_ncs[0]['gstep'][:]
phys_length = var_ncs[0]['phys_length'][:]
# Dnjv = var_nc2["Density"][:]
# Dniono = var_nc3["Density"][:]
# Te = var_nc["Temperature"][:] # temperature moyenne e- + ions
# Tjv = var_nc2["Temperature"][:]
# Tiono = var_ncs[0]["Temperature"][:]


nc = [len(Ti[0][0][0]), len(Ti[0][0]), len(Ti[0])]

icentr = int(np.fix(centr[0]/gs[0]))-1
jcentr = int(np.fix(centr[1]/gs[1]))-1
kcentr = int(np.fix(centr[2]/gs[2]))-1
iminmax = [icentr-int(np.fix(3*radius/gs[0])), icentr+int(np.fix(3*radius/gs[0]))]
jminmax = [jcentr-int(np.fix(3*radius/gs[1])), jcentr+int(np.fix(3*radius/gs[1]))]
kminmax = [kcentr-int(np.fix(3*radius/gs[2])), kcentr+int(np.fix(3*radius/gs[2]))]

print(iminmax)
print(jminmax)
print(kminmax)

r_axis = np.arange(radius*phys_length,1.5*radius*phys_length,phys_length*gs[0])
print("r_axis ",r_axis)

count = np.zeros((number_of_files, len(r_axis)-1))
# Tmoy = np.zeros(len(r_axis)-1)
# Tejv = np.zeros(len(r_axis)-1)
Tis = np.zeros((number_of_files, len(r_axis)-1))
# nmoyjv =  np.zeros(len(r_axis)-1)
# nmoyiono =  np.zeros(len(r_axis)-1)
pts_x = []
pts_y = []
pts_z = []


for ii in range(iminmax[0],iminmax[1]):#len(X_axis)):
    for jj in range(jminmax[0],jminmax[1]):#len(Y_axis)):
        for kk in range(kminmax[0],kminmax[1]):#len(Z_axis)):
            rb = np.sqrt(X_axis[ii]**2 + Y_axis[jj]**2 + Z_axis[kk]**2)
            if r_axis[0] <= rb and rb <= r_axis[-1]:
                for r in range(0,len(r_axis)-1):
                    if r_axis[r] <= rb and rb < r_axis[r+1]:
                        theta = np.arccos(Z_axis[kk]/rb)
                        phi=nan
                        if (X_axis[ii] != 0.0):
                            if X_axis[ii] > 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii])
                            elif Y_axis[jj] >= 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) + np.pi
                            elif Y_axis[jj] < 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) - np.pi
                        elif Y_axis[jj] > 0:
                            phi = np.pi/2
                        elif Y_axis[jj] < 0:
                            phi = -np.pi/2
                        phi = (phi+4*np.pi)%(2*np.pi)
                            
                        if np.pi/2.0 <= phi and phi <= 3*np.pi/2.0:    
                            # print(X_axis[ii],Y_axis[jj],Z_axis[kk],ii,jj,kk,rb,theta,phi)

                            for f in range(0,number_of_files):
                                count[f][r] += 1
                                Tis[f][r] += Ti[f][kk,jj,ii]
                            
                            if rb < r_axis[1]:
                                pts_x.append(X_axis[ii])
                                pts_y.append(Y_axis[jj])
                                pts_z.append(Z_axis[kk])
                            break

# Tmoy = np.where(Tmoy != nan, Tmoy/count, 0)
# Tejv = np.where(Tejv != nan, Tejv/count, 0)
# nmoyjv = np.where(Tejv != nan, nmoyjv/count, 0)
# print(Tis,count)
Tis = np.where(Tis != nan, Tis/count, 0)
# Teiono = Teiono*1e-3
# nmoyiono = np.where(Teiono != nan, nmoyiono/count, 0)

# print(Tmoy)
# print(Tejv)
print(Tis)

r_axis[:] = r_axis[:] - r_axis[0]

for f in range(0,number_of_files):
    plt.plot(Tis[f],r_axis[:-1],label="$Te_{iono}$, time="+times[f])
plt.grid(True,'both','both',ls=":",color="lightgrey")
plt.legend(loc="upper right")
plt.ylabel('altitude [$R_E$]')
plt.xlabel('Te [eV]')
plt.savefig("mean_temp_profile_along_r_comparision_various_times.png")

plt.close('all')

sys.exit()
