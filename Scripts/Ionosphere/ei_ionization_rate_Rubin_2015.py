


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import math
import sys, os

from scipy.interpolate import lagrange,CubicSpline
from numpy.polynomial.polynomial import Polynomial
import scipy.optimize as optimization

def plotting(mol,fiono,testpoly=None,log=False):

    plt.plot(Te*Kb/1.602e-19,fiono,"r",label="$\\nu^{ei}$")
    if testpoly is not None:
        plt.plot(Te*Kb/1.602e-19,np.exp(testpoly),"b",label="Fit curve")
 
    plt.xlabel("Te [eV]")
    plt.ylabel("$\\nu^{ei} s^{-1}$")
    plt.legend(loc="upper right")
    plt.grid(True)
    if log:
        plt.yscale('log')
        plt.xscale('log')
        plt.gca().yaxis.set_major_locator(tck.LogLocator(base=10.0,numticks=12))
        plt.gca().yaxis.set_minor_locator(tck.LogLocator(base=10.0,subs=(0.2,0.4,0.6,0.8),numticks=12))
        plt.gca().yaxis.set_minor_formatter(tck.NullFormatter())
    # plt.ylim(0,1e-8)
    # plt.title("nO2 = n0*exp(-r/H0) avec n0 = 2.5e7cm-3 et H0=100km (Harris)\n")
    # plt.savefig("frequence_impact_e-_"+mol+"_en_fct_Te_Rubin.png")#_and_fit_curve.png")
    fig_name="frequence_impact_e-_"+mol+"_en_fct_Te_Rubin"
    if log:
        fig_name+="_log"
    if testpoly is not None:
        fig_name+="_and_fit_curve"
    fig_name+=".png"
    plt.savefig(fig_name)
    plt.close("all")

def func(x,a0,a1,a2,a3,a4):
    return np.exp(a0 + a1*x + a2*x**2 + a3*x**3 + a4*x**4)

Kb = 1.38e-23 ## Constante de Boltzmann en m^2 kg s^-2 K-1
Te = np.linspace(1,1500,10000)
Te = Te*1.602e-19/Kb#1.5e6  ## Electron temperature en K
R = 13.61 ## eV
a0 = 0.5292 ## Angstrom

def ei_ionisation_rate(U,B,N,mol,nb_orbitals):
    ###### Calcul des sections efficaces d'ionisation de Hwang et al 1996 ######
    T = np.linspace(20, 1500,num=10000) ## High incident energy in eV
    sigma = np.zeros(len(T))  ## section efficace en m^2
    for i in range(0,len(U)):
        t = T/B[i]
        u = U[i]/B[i]
        S = 4e-16*math.pi * a0*a0 * N[i] * R*R / (B[i]*B[i])
        sigma += S / (t+u+1) * (np.log(t)/2 * (1-1/(t*t)) + 1 - 1/t - np.log(t)/(t+1))

    ###### Maxwellian energy distribution  ######
    dE = (T[-1]-T[0])/len(T)
    fiono = np.zeros(len(Te))
    me = 9.109e-31 ## masse d'un electron en kg
    for iTe in range(0,len(Te)):
        MED = 2*np.sqrt(T*1.6e-19/math.pi)*(1/(Kb*Te[iTe]))**(3/2) * np.exp(-T/(Kb*Te[iTe]/1.602e-19))

        sumMED = np.sum(MED)*dE*1.6e-19
        # print("sum MED", sumMED)


        ###### Calcul de la fréquence d'ionisation. Utilisation de la méthode des rectangles à gauche pour calculer l'integral #####
        for i in range(0,len(T)): # Pour chaque energie de T
            fiono[iTe] += MED[i]*sigma[i]*np.sqrt(2*T[i]*1.6e-19/me)*100*dE*1.6e-19

    

    # print(optimization.curve_fit(func, Te_poly, fiono))
    x0=np.full(5,0.0)
    coefs=optimization.curve_fit(func, np.log(Te), fiono, x0)[0]
    print(mol+" coef=",coefs," fin")

    testpoly=np.zeros(len(Te))
    testpoly=coefs[0]

    for i in range(1,len(coefs)):
        testpoly+=coefs[i]*np.power(np.log(Te),i)

    plotting(mol,fiono,testpoly)
    plotting(mol,fiono,testpoly,True)
    plotting(mol,fiono)
    plotting(mol,fiono,log=True)

    return fiono
    


    

### Parametres pour O2 issu de l'article Hwang et al. 1996
U = np.array([79.73,90.92,59.89,71.84,84.88]) ## Kinetic energy in eV
B = np.array([46.19,29.82,19.64,19.79,12.07]) ## Electron binding energy in eV
N = np.array([2,2,4,2,2]) ## orbital occupation number
f_O2 = ei_ionisation_rate(U,B,N,"O2",len(U))
coef_O2 = [-352.134, 88.1318, -8.882784, 0.4082678, -0.0072026]
curve_fit_O2 = np.zeros(len(Te))
curve_fit_O2 = coef_O2[0]
for i in range(1,len(coef_O2)):
    curve_fit_O2+=coef_O2[i]*np.power(np.log(Te),i)

### Parametres pour H2O issu de l'article Hwang et al. 1996
U = np.array([70.71,48.36,59.52,61.91]) ## Kinetic energy in eV
B = np.array([36.88,19.83,15.57,12.61]) ## Electron binding energy in eV
N = np.array([2,2,2,2]) ## orbital occupation number
f_H2O = ei_ionisation_rate(U,B,N,"H2O",len(U))
coef_H2O = [-390.080,  98.8605, -10.001035, 0.4592265, -0.0080629]
curve_fit_H2O = np.zeros(len(Te))
curve_fit_H2O = coef_H2O[0]
for i in range(1,len(coef_H2O)):
    curve_fit_H2O+=coef_H2O[i]*np.power(np.log(Te),i)
                                      
### Parametres pour H2 issu de l'article Hwang et al. 1996
U = np.array([15.98]) ## Kinetic energy in eV
B = np.array([15.43]) ## Electron binding energy in eV
N = np.array([2]) ## orbital occupation number
f_H2 = ei_ionisation_rate(U,B,N,"H2",len(U))
coef_H2 = [-489.841, 126.3080, -12.813012, 0.5855403, -0.0101690]
curve_fit_H2 = np.zeros(len(Te))
curve_fit_H2 = coef_H2[0]
for i in range(1,len(coef_H2)):
    curve_fit_H2+=coef_H2[i]*np.power(np.log(Te),i)

plt.rcParams["figure.figsize"] = (8,6)

plt.plot(Te*Kb/1.602e-19,f_O2,"r",label="$\\nu^{ei}_{O_2}$")
plt.plot(Te*Kb/1.602e-19,np.exp(curve_fit_O2),"darkblue",ls="--",label="$\\nu^{ei}_{O_2}$ fit curve")
plt.plot(Te*Kb/1.602e-19,f_H2O,"b",label="$\\nu^{ei}_{H_2O}$")
plt.plot(Te*Kb/1.602e-19,np.exp(curve_fit_H2O),"darkred",ls="--",label="$\\nu^{ei}_{H_2O}$ fit curve")
plt.plot(Te*Kb/1.602e-19,f_H2,"green",label="$\\nu^{ei}_{H_2}$")
plt.plot(Te*Kb/1.602e-19,np.exp(curve_fit_H2),"purple",ls="--",label="$\\nu^{ei}_{H_2}$ fit curve")
plt.xlabel("Te [eV]")
plt.ylabel("$\\nu^{ei} [s^{-1}]$")
plt.xlim(0,1000)
# plt.legend(loc="upper right")
plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.125),
          ncol=3, fancybox=True, shadow=True)
plt.grid(True)
plt.savefig("frequence_impact_e-_O2_H2O_H2_en_fct_Te_Rubin_et_fit_curve.png")
plt.close('all')