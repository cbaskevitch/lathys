#################################
## Ce script reproduit la courbe du modele charge exchange 
## ionization cross sections de O+ - O2 de Lindsay et Stebbings 2005.
## resultat en cm2

import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

E = np.linspace(0.01, 10,num=1000) ## Energy in keV

## Courbe des sections efficaces pour l'etat stable
## O+(4S)-O2
a = [2.16e-16, 0.250, 2.85e-8, -0.103e-8, 0.144]
sigma = np.zeros(len(E))
sigma = a[0]*(1-np.exp(-a[1]/E))**2 + (a[2]-a[3]*np.log(E))**2 * (1-np.exp(-E/a[4]))**4

plt.plot(np.log10(E),sigma,c="r",label="Ground state O+ - O2 cross section")
print("sigma pour 250eV",a[0]*(1-np.exp(-a[1]/250e-3))**2 + (a[2]-a[3]*np.log(250e-3))**2 * (1-np.exp(-250e-3/a[4]))**4)

## Courbe des sections efficaces pour l'etat excite
## O+(2D,2P)-O2
a = [4.19e-8, 0.0315e-8]
sigma = np.zeros(len(E))
sigma = (a[0]-a[1]*np.log(E))**2

# plt.plot(np.log10(E),sigma,c="b",label="Excited state O+ - O2 cross section")

# plt.ylim(0,18)
plt.legend(loc="upper right")
plt.ylabel('sigma 10^-16[cm^2]')
plt.xlabel("E log10[keV]")
# plt.show()
plt.savefig("charge_exchange_O+-O2_cross_section_Lindsay_Stebbings_2005.png")