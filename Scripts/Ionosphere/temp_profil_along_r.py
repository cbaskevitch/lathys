from cmath import nan
from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import scipy.stats as st
import math
import sys, os

####
# plot un profil moyen de la temperature en altitude de 1Re à 2Re
# Moyenne sur la longitude et latitude (à voir si besoin de restraindre au cote ram uniquement)
####

thew_file = sys.argv[1]
Ojv_file = sys.argv[2]
Oiono_file = sys.argv[3]

ncid = Dataset(thew_file)
ncid2 = Dataset(Ojv_file)
ncid3 = Dataset(Oiono_file)
var_nc = ncid.variables
var_nc2 = ncid2.variables
var_nc3 = ncid3.variables
X_axis = var_nc["X_axis"][:]
Y_axis = var_nc["Y_axis"][:]
Z_axis = var_nc["Z_axis"][:]

centr      = var_nc['s_centr'][:]
radius     = var_nc['r_planet'][:]
r_iono     = var_nc['r_iono'][:]
gs         = var_nc['gstep'][:]
phys_length = var_nc['phys_length'][:]
Dnjv = var_nc2["Density"][:]
Dniono = var_nc3["Density"][:]
Te = var_nc["Temperature"][:] # temperature moyenne e- + ions

## Variables pour calcul temperature electron uniquement
betae = var_nc['betae'][:]
n0 = var_nc['phys_density'][:]
nsimjv = var_nc2["Density"][:]*1e6/n0
nsimiono = var_nc3["Density"][:]*1e6/n0
B0 = var_nc['phys_mag'][:]
mu = 4e-7*np.pi
e = 1.6e-19
kB = 1.38e-23
gamma = 5/3

print("B0 ",B0)
print("n0 ",n0)
print("betae ",betae)

nc = [len(Te[0][0]), len(Te[0]), len(Te)]

icentr = int(np.fix(centr[0]/gs[0]))-1
jcentr = int(np.fix(centr[1]/gs[1]))-1
kcentr = int(np.fix(centr[2]/gs[2]))-1
iminmax = [icentr-int(np.fix(3*radius/gs[0])), icentr+int(np.fix(3*radius/gs[0]))]
jminmax = [jcentr-int(np.fix(3*radius/gs[1])), jcentr+int(np.fix(3*radius/gs[1]))]
kminmax = [kcentr-int(np.fix(3*radius/gs[2])), kcentr+int(np.fix(3*radius/gs[2]))]

print(iminmax)
print(jminmax)
print(kminmax)

r_axis = np.arange(radius*phys_length,2*radius*phys_length,phys_length*gs[0])
print("r_axis ",r_axis)

count = np.zeros(len(r_axis)-1)
Tmoy = np.zeros(len(r_axis)-1)
Tejv = np.zeros(len(r_axis)-1)
Teiono = np.zeros(len(r_axis)-1)
nmoyjv =  np.zeros(len(r_axis)-1)
nmoyiono =  np.zeros(len(r_axis)-1)
pts_x = []
pts_y = []
pts_z = []


for ii in range(iminmax[0],iminmax[1]):#len(X_axis)):
    for jj in range(jminmax[0],jminmax[1]):#len(Y_axis)):
        for kk in range(kminmax[0],kminmax[1]):#len(Z_axis)):
            rb = np.sqrt(X_axis[ii]**2 + Y_axis[jj]**2 + Z_axis[kk]**2)
            if r_axis[0] <= rb and rb <= r_axis[-1]:
                for r in range(0,len(r_axis)-1):
                    if r_axis[r] <= rb and rb < r_axis[r+1]:
                        theta = np.arccos(Z_axis[kk]/rb)
                        phi=nan
                        if (X_axis[ii] != 0.0):
                            if X_axis[ii] > 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii])
                            elif Y_axis[jj] >= 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) + np.pi
                            elif Y_axis[jj] < 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) - np.pi
                        elif Y_axis[jj] > 0:
                            phi = np.pi/2
                        elif Y_axis[jj] < 0:
                            phi = -np.pi/2
                        phi = (phi+4*np.pi)%(2*np.pi)
                            
                        if np.pi/2.0 <= phi and phi <= 3*np.pi/2.0:    
                            # print(X_axis[ii],Y_axis[jj],Z_axis[kk],ii,jj,kk,rb,theta,phi)

                            count[r] += 1
                            Tmoy[r] += Te[kk,jj,ii]
                            nmoyjv[r] += Dnjv[kk,jj,ii]
                            nmoyiono[r] += Dniono[kk,jj,ii]
                            Tejv[r] += 1/e * betae*(nsimjv[kk,jj,ii])**(gamma-1) * B0**2/(2*mu*n0)

                            gamma_iono = 1/np.log10(nsimiono[kk,jj,ii])
                            gamma_iono = gamma_iono/np.sqrt(np.sqrt((1+gamma_iono**4)/gamma**4))
                            Teiono[r] += 1/e * betae*(nsimiono[kk,jj,ii])**(gamma_iono-1) * B0**2/(2*mu*n0)
                            if rb < r_axis[1]:
                                pts_x.append(X_axis[ii])
                                pts_y.append(Y_axis[jj])
                                pts_z.append(Z_axis[kk])
                            break

Tmoy = np.where(Tmoy != nan, Tmoy/count, 0)
Tejv = np.where(Tejv != nan, Tejv/count, 0)
nmoyjv = np.where(Tejv != nan, nmoyjv/count, 0)
Teiono = np.where(Teiono != nan, Teiono/count, 0)
Teiono = Teiono*1e-3
nmoyiono = np.where(Teiono != nan, nmoyiono/count, 0)
print(Tejv)

# plt.plot(r_axis[:-1],Tmoy,c="blue",label="$<Te>$")
# plt.plot(r_axis[:-1],Tejv,c="red",label="$Te_{jv}$")
plt.plot(r_axis[:-1],Teiono,c="green",label="$Te_{iono}$")
plt.grid(True)
plt.legend(loc="lower right")
plt.xlabel('altitude [$R_E$]')
plt.ylabel('Te [eV]')
plt.savefig("mean_temp_profile_along_r_gamma_iono.png")

plt.close('all')

sys.exit()

# plt.plot(r_axis[:-1],Tmoy*,c="blue",label="$<Te>$")
# plt.plot(r_axis[:-1],Tejv*nmoyjv,c="red",label="$Te_{jv}$")
# plt.plot(r_axis[:-1],Teiono*nmoyiono,c="green",label="$Te_{iono}$")
# plt.grid(True)
# plt.legend(loc="lower right")
# plt.xlabel('altitude [$R_E$]')
# plt.ylabel('Te [eV]')
# plt.savefig("comparaison_Tejv*nejv_Teion*neiono_profile_along_r_gamma_iono.png")

# plt.close('all')


### Production of ei

nO2 = np.zeros(len(r_axis)-1)
n0O2 = 2.5e7
H0=100
nO2 = n0O2*np.exp(-(r_axis[:-1]-r_axis[0])/H0)

coefs = [-352.134, 88.1318, -8.882784, 0.4082678, -0.0072026] # coefficients pour calculer la frequence d'ionisation.
                                                              # Déterminé grace au script O2+_ionisation_production

prod_ei = np.zeros(len(r_axis)-1)
prod_ei[:] = coefs[0]
for c in range(1,len(coefs)):
    prod_ei += coefs[c]*np.log(Tejv*e/kB)**c
prod_ei = nO2*nmoyjv*np.exp(prod_ei)

plt.plot(r_axis[:-1],prod_ei,c="blue",label="$\\nu_{ei}^{jv}$")

prod_ei[:] = coefs[0]
for c in range(1,len(coefs)):
    prod_ei += coefs[c]*np.log(Teiono*e/kB)**c
prod_ei = nO2*nmoyiono*np.exp(prod_ei)

plt.plot(r_axis[:-1],prod_ei,c="red",label="$\\nu_{ei}^{iono}$")

plt.plot(r_axis[:-1],nO2*1.7e-8,color="green",label="$h\\nu$")

plt.legend(loc="lower right")
plt.yscale('log')
plt.grid(True)
# plt.gca().yaxis.set_minor_formatter(tck.NullFormatter())
plt.xlabel('altitude [$R_E$]')
plt.ylabel('Production ($O_2^+$) [$cm^{-3}.s^{-1}$]')
plt.savefig("production_O2+_profile_along_r.png")

plt.close('all')

# -- Creation of axis values centered on the planet ( normalized to planet radius)
# X_mesh, Y_mesh, Z_mesh = np.meshgrid(X_axis,Y_axis,Z_axis)

# # planet center in cell number (NB: cell number start at 1
# icentr = int(np.fix(centr[0]/gs[0]))-1
# jcentr = int(np.fix(centr[1]/gs[1]))-1
# kcentr = int(np.fix(centr[2]/gs[2]))-1
# print(X_axis[:])
# print("centr ",icentr,jcentr,kcentr)
# icentr=np.where(X_axis == 0.0)
# jcentr=np.where(Y_axis==0.0)
# kcentr=np.where(Z_axis==0.0)
# print("centr ",icentr[0],jcentr[0],kcentr[0])

# r_mesh = np.sqrt(X_mesh**2 + Y_mesh**2 + Z_mesh**2)
# theta_mesh = np.arccos(Z_mesh/r_mesh)
# r_mesh = r_mesh/(radius*phys_length)
# # phi_mesh = np.zeros((nc[0],nc[1],nc[2]),dtype=float)

# phi_mesh = np.where(X_mesh != 0, (np.arctan(Y_mesh/X_mesh)+4*np.pi)%(2*np.pi), nan)

# for ii in range (0, len(X_mesh)):
#     for jj in range(0,len(Y_mesh[0])):
#         for kk in range(0,len(Z_mesh[0][0])):
#             if X_mesh[ii,jj,kk] > 0:
#                 print(np.arctan(Y_mesh[ii,jj,kk]/X_mesh[ii,jj,kk]))
#                 phi_mesh[ii,jj,kk] = np.arctan((Y_mesh[ii][jj][kk]/X_mesh[ii][jj][kk]))
#             elif X_mesh[ii,jj,kk] < 0 and Y_mesh[ii,jj,kk] >= 0:
#                 phi_mesh[ii,jj,kk] = np.arctan(Y_mesh[ii,jj,kk]/X_mesh[ii,jj,kk]) + np.pi
#             elif X_mesh[ii,jj,kk] < 0 and Y_mesh[ii,jj,kk] < 0:
#                 phi_mesh[ii,jj,kk] = np.arctan(Y_mesh[ii,jj,kk]/X_mesh[ii,jj,kk]) - np.pi
#             elif X_mesh[ii,jj,kk] == 0 and Y_mesh[ii,jj,kk] > 0:
#                 phi_mesh = np.pi/2
#             elif X_mesh[ii,jj,kk] == 0 and Y_mesh[ii,jj,kk] < 0:
#                 phi_mesh = -np.pi/2
#             else:
#                 phi_mesh = nan
# phi_mesh = (phi_mesh + 4*np.pi) % (2*np.pi)

# print("r_mesh ",r_mesh[:,jcentr,kcentr],"\n")
# print("theta_mesh ",theta_mesh[:,jcentr,:],"\n")
# print("phi_mesh",phi_mesh[:,:,kcentr])

# f=plt.figure(figsize=(20,20))
# ax = f.add_subplot(projection='3d')
# ax.plot_wireframe(r_mesh,theta_mesh,phi_mesh)
# plt.savefig("test.png")