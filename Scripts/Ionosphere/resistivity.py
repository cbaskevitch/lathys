import numpy as np
import matplotlib.pyplot as plt
import sys

Kb   = 1.38e-23  ## Constante de Boltzmann en m^2 kg s^-2 K-1
e_Cb = 1.602e-19 ## Charge elementaire en C (A.s)
Re = 1560.0 #km
ne = 2500e6 #m-3
B=500e-9 # Tesla
me = 9.1e-31 #1.66e-27#
amu_pmass = 1.660538782e-27
gyrofreq_e = e_Cb*B/me
gyrofreq_O2p = e_Cb*B/(16*amu_pmass)
# altitude
h = np.linspace(0,3*Re,int(3*Re)) # km

# O2
n0_O2 = 2.5e7 #cm-3
H0_O2 = 100   # km
nO2 = n0_O2*np.exp(-h/H0_O2)

# O2+
nO2p = np.zeros(len(h))
n0_O2p = 2500 #cm-3
H0_O2p = 240   #km
H1_O2p = 440   #km
n1_O2p = n0_O2p*np.exp(-300/H0_O2p)*np.exp(300/H1_O2p) #cm-3

# nO2p = n0*np.exp(-h/H0)
for i in range(0,len(h)):
    if h[i] <= 300:
        nO2p[i]=n0_O2p*np.exp(-h[i]/H0_O2p)
    else:
        nO2p[i]=n1_O2p*np.exp(-h[i]/H1_O2p)
# ne = nO2p*1e6

Te = 20.0*e_Cb/Kb # Kelvin

# momentum transfer collision frequencies electron-neutral from Kelley 1989
coll_freq_Kelley_en = 5.4e-10 * nO2 * np.sqrt(Te)

# momentum transfer collision frequencies electron-neutral from Shunk and Nagy 2004, Rubin et al. 2015
coll_freq_Shunk_en = 1.82e-10 * nO2 * (1 + 3.6e-2 * np.sqrt(Te)) * np.sqrt(Te)

coll_freq_Shunk_in = 2.6e-9 * nO2 * np.sqrt(1.59e-24/(16*amu_pmass))

conductivity_20eV = 1/18
coll_freq_ei = e_Cb**2 * nO2p*1e6 / (conductivity_20eV*16*amu_pmass)
plt.rcParams["figure.figsize"] = (8,6)

plt.plot(coll_freq_Kelley_en,h,c="blue",label="$ \\nu_{en}$ Te=20eV (Kelley, 1989)")
plt.plot(coll_freq_Shunk_en,h,c="blue",ls="--",label="$ \\nu_{en}$ Te=20eV (Shunk, 2004)")
plt.plot(coll_freq_Shunk_in,h,c="red",label="$ \\nu_{in}$ Te=20eV (Shunk, 2004)")
plt.plot(coll_freq_ei,h,c="green",label="$ \\nu_{ei}$")
plt.plot(gyrofreq_e*np.ones(len(h)),h,ls="-.",label="$\Omega_{e}$")
plt.plot(gyrofreq_O2p*np.ones(len(h)),h,ls="-.",label="$\Omega_{i}$")
plt.ylim(0,600)
plt.xlim(1e-3,1e5)
plt.xlabel("collision frequency [$s^{-1}$]")
plt.ylabel("altitude [km]")

plt.xscale("log")
# plt.grid(True,'both','both',ls=":",color="lightgrey")
plt.grid(visible=True, which='major', color='k', linestyle='-')
plt.grid(visible=True, which='minor', color='g', linestyle='-', alpha=0.2)
# plt.legend(loc="upper right")
plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
          ncol=3, fancybox=True, shadow=True)
plt.savefig("resistivity_coll_freq.png")
plt.close('all')





# conductivity_Kelley = e_Cb * nO2p * coll_freq_Kelley_en / (B * gyrofreq_e)


# resistivity_Kelley = coll_freq_Kelley_en * me / (e_Cb**2 * nO2p) #ne)
resistivity_Shunk = coll_freq_Shunk_en * me / (e_Cb**2 * nO2p) #ne)

# resis = -0.9*np.exp(h*1e3) + 5.5e5
# resis = 5.5e5 * 10**(-((np.log10(5.5)+1)*1e-5/6)*h*1e3)
resis = 1/conductivity_20eV*np.ones(int(len(h)/20)) + resistivity_Shunk[::20]

# plt.plot(resistivity_Kelley,h,label="$\eta$ Te=20eV (Kelley, 1989)")
# plt.plot(1/conductivity_Kelley,h,label="1/$\sigma$ Te=20eV (Kelley, 1989)")
plt.plot(resistivity_Shunk,h,label="$1/\sigma_{en}$ (Te=20eV, Shunk)")
plt.plot(np.ones(len(h))/conductivity_20eV,h,label="$1/\sigma_{ei}$ (Te=20eV)")
plt.plot(resis,h[::20],marker="o",color="red",fillstyle='none',linestyle="None",label="$\eta$ (Te=20eV)")
plt.ylim(0,600)
plt.xlim(1e1,1e6)
plt.xlabel("resistivity $[\Omega .m]$")
plt.ylabel("altitude [km]")

plt.xscale("log")
# plt.grid(True,'both','both',ls=":",color="lightgrey")
plt.grid(visible=True, which='major', color='k', linestyle='-')
plt.grid(visible=True, which='minor', color='g', linestyle='-', alpha=0.2)
# plt.legend(loc="upper right")
plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.125),
          ncol=3, fancybox=True, shadow=True)
plt.savefig("resistivity_ne_surf_2500cc.png")
plt.close('all')



conductivity_Shunk = 1/resistivity_Shunk
conductivity_in = e_Cb**2 * nO2p / (coll_freq_Shunk_in * 16 * amu_pmass)
conductivity_Kelley = e_Cb**2 * nO2p / (coll_freq_Kelley_en * me)

plt.plot(conductivity_Shunk,h,label="$\sigma_{en}$ (Te=20eV, Shunk)")
plt.plot(conductivity_Kelley,h,label="$\sigma_{en}$ (Te=20eV, Kelley)")
plt.plot(conductivity_in,h,label="$\sigma_{in}$")
plt.ylim(0,600)
plt.xlim(1e-9,1e-3)
plt.xlabel("Conductivity $[S/m]$")
plt.ylabel("altitude [km]")

plt.xscale("log")
# plt.grid(True,'both','both',ls=":",color="lightgrey")
plt.grid(visible=True, which='major', color='k', linestyle='-')
plt.grid(visible=True, which='minor', color='g', linestyle='-', alpha=0.2)
# plt.legend(loc="upper right")
plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.125),
          ncol=3, fancybox=True, shadow=True)
plt.savefig("conductivity.png")
plt.close('all')


sys.exit()

from netCDF4 import Dataset
import sys

ncfile = sys.argv[1]#src_dir + "/Atmw_" + rundate + "_t" + diagtime + '.nc'
ncid = Dataset(ncfile)
var_nc = ncid.variables

centr      = var_nc['s_centr'][:]
radius     = var_nc['r_planet'][:]
r_iono     = var_nc['r_iono'][:]
gs         = var_nc['gstep'][:]
phys_speed = var_nc['phys_speed'][:]
phys_length = var_nc['phys_length'][:]
resis_lat = var_nc["Resistivity"][:]
nc = [len(resis_lat[0][0]), len(resis_lat[0]), len(resis_lat)]
mu0 = 4*np.pi*1e-7

resis_lat = resis_lat*phys_length*phys_speed*mu0*1e6
icentr = int(np.fix(centr[0]/gs[0]))
jcentr = int(np.fix(centr[1]/gs[1]))
kcentr = int(np.fix(centr[2]/gs[2]))

resis_X=resis_lat[kcentr,jcentr,:]
X=np.arange(0,(nc[0])*gs[0],gs[0])/radius - centr[0]/radius

print(resis_X)

plt.plot(resis_X[icentr:],(X[icentr:]*phys_length*radius)-radius*phys_length,c="b",label="$\eta$ LatHyS")
plt.plot(resis,h,c="r",ls="--",label="$\eta$ Te=20eV linear func")

plt.ylim(0,Re)
plt.xlim(1e1,1e6)
plt.xlabel("resistivity [$\eta [\Omega .m]$]")
plt.ylabel("altitude [km]")

plt.xscale("log")
# plt.grid(True,'both','both',ls=":",color="lightgrey")
plt.grid(visible=True, which='major', color='k', linestyle='-')
plt.grid(visible=True, which='minor', color='g', linestyle='-', alpha=0.2)
plt.legend(loc="upper right")
plt.savefig("resistivity_lathys.png")
plt.close('all')
