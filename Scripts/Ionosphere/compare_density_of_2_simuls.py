from cmath import nan
from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import scipy.stats as st
import math
import sys, os

####
# plot un profil moyen de la temperature en altitude de 1Re à 2Re
# Moyenne sur la longitude et latitude (à voir si besoin de restraindre au cote ram uniquement)
####

O2eit50_file = sys.argv[1]
O2eit100_file = sys.argv[2]
O2eit200_file = sys.argv[3]

ncid = Dataset(O2eit50_file)
ncid2 = Dataset(O2eit100_file)
ncid3 = Dataset(O2eit200_file)
var_nc = ncid.variables
var_nc2 = ncid2.variables
var_nc3 = ncid3.variables
X_axis = var_nc["X_axis"][:]
Y_axis = var_nc["Y_axis"][:]
Z_axis = var_nc["Z_axis"][:]

phys_length = var_nc['phys_length'][:]
centr      = var_nc['s_centr'][:]
radius     = var_nc['r_planet'][:]
r_iono     = var_nc['r_iono'][:]
gs         = var_nc['gstep'][:]
Dneit50 = var_nc["Density"][:]
Dneit100 = var_nc2["Density"][:]
Dneit200 = var_nc3["Density"][:]


nc = [len(Dneit50[0][0]), len(Dneit50[0]), len(Dneit50)]

icentr = int(np.fix(centr[0]/gs[0]))-1
jcentr = int(np.fix(centr[1]/gs[1]))-1
kcentr = int(np.fix(centr[2]/gs[2]))-1
iminmax = [icentr-int(np.fix(3*radius/gs[0])), icentr+int(np.fix(3*radius/gs[0]))]
jminmax = [jcentr-int(np.fix(3*radius/gs[1])), jcentr+int(np.fix(3*radius/gs[1]))]
kminmax = [kcentr-int(np.fix(3*radius/gs[2])), kcentr+int(np.fix(3*radius/gs[2]))]

print(iminmax)
print(jminmax)
print(kminmax)

r_axis = np.arange(radius*phys_length,2*radius*phys_length,phys_length*gs[0])
print("r_axis ",r_axis)

count = np.zeros(len(r_axis)-1)
nmoyeit50 =  np.zeros(len(r_axis)-1)
nmoyeit100 =  np.zeros(len(r_axis)-1)
nmoyeit200 =  np.zeros(len(r_axis)-1)



for ii in range(iminmax[0],iminmax[1]):#len(X_axis)):
    for jj in range(jminmax[0],jminmax[1]):#len(Y_axis)):
        for kk in range(kminmax[0],kminmax[1]):#len(Z_axis)):
            
            
            # rate = coefs[0]
            # Tejv_tmp = 1/e * betae*(nsimjv[kk,jj,ii])**(gamma-1) * B0**2/(2*mu*n0)
            # for c in range(1,len(coefs)):
            #     rate += coefs[c]*np.log(Tejv_tmp*e/kB)**c
            # PROD_JV_TESt[kk,jj,ii] = Dnjv[kk,jj,ii]*np.exp(rate)


            rb = np.sqrt(X_axis[ii]**2 + Y_axis[jj]**2 + Z_axis[kk]**2)
            if r_axis[0] <= rb and rb <= r_axis[-1]:
                for r in range(0,len(r_axis)-1):
                    if r_axis[r] <= rb and rb < r_axis[r+1]:
                        theta = np.arccos(Z_axis[kk]/rb)
                        phi=nan
                        if (X_axis[ii] != 0.0):
                            if X_axis[ii] > 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii])
                            elif Y_axis[jj] >= 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) + np.pi
                            elif Y_axis[jj] < 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) - np.pi
                            elif Y_axis[jj] > 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) + np.pi
                        elif Y_axis[jj] > 0:
                            phi = np.pi/2
                        elif Y_axis[jj] < 0:
                            phi = -np.pi/2
                        phi = (phi+4*np.pi)%(2*np.pi)
                            
                        if np.pi/2.0 <= phi and phi <= 3*np.pi/2.0:    

                            count[r] += 1
                            nmoyeit50[r] += Dneit50[kk,jj,ii]
                            nmoyeit100[r] += Dneit100[kk,jj,ii]
                            nmoyeit200[r] += Dneit200[kk,jj,ii]





nmoyeit50           = nmoyeit50/count
nmoyeit100         = nmoyeit100/count
nmoyeit200       = nmoyeit200/count




### Density

plt.plot(r_axis[:-1]-r_axis[0],nmoyeit50,c="blue",label="$n_e$ 11_02_22 t=50")
plt.plot(r_axis[:-1]-r_axis[0],nmoyeit100,c="red",label="$n_e^{ei}$ 11_02_22 t=100")
plt.plot(r_axis[:-1]-r_axis[0],nmoyeit200,c="green",label="$n_e^{iono}$ 11_02_22 t=200")








O2eit50_file = sys.argv[4]
O2eit100_file = sys.argv[5]
O2eit200_file = sys.argv[6]

ncid = Dataset(O2eit50_file)
ncid2 = Dataset(O2eit100_file)
ncid3 = Dataset(O2eit200_file)
var_nc = ncid.variables
var_nc2 = ncid2.variables
var_nc3 = ncid3.variables
X_axis = var_nc["X_axis"][:]
Y_axis = var_nc["Y_axis"][:]
Z_axis = var_nc["Z_axis"][:]

phys_length = var_nc['phys_length'][:]
centr      = var_nc['s_centr'][:]
radius     = var_nc['r_planet'][:]
r_iono     = var_nc['r_iono'][:]
gs         = var_nc['gstep'][:]
Dneit50 = var_nc["Density"][:]
Dneit100 = var_nc2["Density"][:]
Dneit200 = var_nc3["Density"][:]


nc = [len(Dneit50[0][0]), len(Dneit50[0]), len(Dneit50)]

icentr = int(np.fix(centr[0]/gs[0]))-1
jcentr = int(np.fix(centr[1]/gs[1]))-1
kcentr = int(np.fix(centr[2]/gs[2]))-1
iminmax = [icentr-int(np.fix(3*radius/gs[0])), icentr+int(np.fix(3*radius/gs[0]))]
jminmax = [jcentr-int(np.fix(3*radius/gs[1])), jcentr+int(np.fix(3*radius/gs[1]))]
kminmax = [kcentr-int(np.fix(3*radius/gs[2])), kcentr+int(np.fix(3*radius/gs[2]))]

print(iminmax)
print(jminmax)
print(kminmax)

r_axis = np.arange(radius*phys_length,2*radius*phys_length,phys_length*gs[0])
print("r_axis ",r_axis)

count = np.zeros(len(r_axis)-1)
nmoyeit50 =  np.zeros(len(r_axis)-1)
nmoyeit100 =  np.zeros(len(r_axis)-1)
nmoyeit200 =  np.zeros(len(r_axis)-1)



for ii in range(iminmax[0],iminmax[1]):#len(X_axis)):
    for jj in range(jminmax[0],jminmax[1]):#len(Y_axis)):
        for kk in range(kminmax[0],kminmax[1]):#len(Z_axis)):
            
            
            # rate = coefs[0]
            # Tejv_tmp = 1/e * betae*(nsimjv[kk,jj,ii])**(gamma-1) * B0**2/(2*mu*n0)
            # for c in range(1,len(coefs)):
            #     rate += coefs[c]*np.log(Tejv_tmp*e/kB)**c
            # PROD_JV_TESt[kk,jj,ii] = Dnjv[kk,jj,ii]*np.exp(rate)


            rb = np.sqrt(X_axis[ii]**2 + Y_axis[jj]**2 + Z_axis[kk]**2)
            if r_axis[0] <= rb and rb <= r_axis[-1]:
                for r in range(0,len(r_axis)-1):
                    if r_axis[r] <= rb and rb < r_axis[r+1]:
                        theta = np.arccos(Z_axis[kk]/rb)
                        phi=nan
                        if (X_axis[ii] != 0.0):
                            if X_axis[ii] > 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii])
                            elif Y_axis[jj] >= 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) + np.pi
                            elif Y_axis[jj] < 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) - np.pi
                            elif Y_axis[jj] > 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) + np.pi
                        elif Y_axis[jj] > 0:
                            phi = np.pi/2
                        elif Y_axis[jj] < 0:
                            phi = -np.pi/2
                        phi = (phi+4*np.pi)%(2*np.pi)
                            
                        if np.pi/2.0 <= phi and phi <= 3*np.pi/2.0:    

                            count[r] += 1
                            nmoyeit50[r] += Dneit50[kk,jj,ii]
                            nmoyeit100[r] += Dneit100[kk,jj,ii]
                            nmoyeit200[r] += Dneit200[kk,jj,ii]





nmoyeit50           = nmoyeit50/count
nmoyeit100         = nmoyeit100/count
nmoyeit200       = nmoyeit200/count




### Density

plt.plot(r_axis[:-1]-r_axis[0],nmoyeit50,c="blue",ls="--",label="$n_e$ 07_04_22 t=50")
plt.plot(r_axis[:-1]-r_axis[0],nmoyeit100,c="red",ls="--",label="$n_e^{ei}$ 07_04_22 t=100")
plt.plot(r_axis[:-1]-r_axis[0],nmoyeit200,c="green",ls="--",label="$n_e^{iono}$ 07_04_22 t=200")



plt.yscale('log')
plt.grid(True)
plt.legend(loc="lower right")
plt.xlabel('altitude from surface [$km$]')
plt.ylabel('$n_e$ [$cm^{-3}$]')
plt.savefig("upstream_hemis_mean_density_ei_profile_along_r_comparision_between_11-02-22_07-04-22.png")

plt.close('all')