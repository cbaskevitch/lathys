from cmath import isnan, nan
from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import scipy.stats as st
import math
import sys, os

####
# plot un profil moyen de la temperature en altitude de 1Re à 2Re
# Moyenne sur la longitude et latitude (à voir si besoin de restraindre au cote ram uniquement)
# Calcul la temperature à partir des equations et la compare a celle des diag
####

thew_file = sys.argv[1]
Ojv_file = sys.argv[2]
Oiono_file = sys.argv[3]
O2_file = sys.argv[4]

ncid = Dataset(thew_file)
ncid2 = Dataset(Ojv_file)
ncid3 = Dataset(Oiono_file)
ncid4 = Dataset(O2_file)
var_nc = ncid.variables
var_nc2 = ncid2.variables
var_nc3 = ncid3.variables
var_nc4 = ncid4.variables
X_axis = var_nc["X_axis"][:]
Y_axis = var_nc["Y_axis"][:]
Z_axis = var_nc["Z_axis"][:]

centr      = var_nc['s_centr'][:]
radius     = var_nc['r_planet'][:]
r_iono     = var_nc['r_iono'][:]
gs         = var_nc['gstep'][:]
phys_length = var_nc['phys_length'][:]
phys_temp_e = var_nc['phys_temp_e'][:]
Dn = var_nc["Density"][:]
Dnjv = var_nc2["Density"][:]
Dniono = var_nc3["Density"][:]
Dn_O2 = var_nc4["Den_O2"][:]
Te = var_nc["Temperature"][:] # temperature moyenne e- + ions
# Tejv_compute_ei = var_nc["Temperature_e_jv"][:]
Tejv_compute_ei = var_nc["Temp_e_jv"][:]
Teiono_compute_ei = var_nc["Temp_e_iono"][:]
# prodjv_compute_ei = var_nc["ie_frequency"][:]
prodjv_compute_ei = var_nc["ei_prod"][:]
proddna_compute_ei = var_nc["ei_prod_dna"][:]

## Variables pour calcul temperature electron uniquement
betae = var_nc['betae'][:]
n0 = var_nc['phys_density'][:]
nsimjv = var_nc2["Density"][:]*1e6/n0
nsimiono = var_nc3["Density"][:]*1e6/n0
B0 = var_nc['phys_mag'][:]
mu = 4e-7*np.pi
e = 1.6e-19
kB = 1.38e-23
gamma = 5/3

print("B0 ",B0)
print("n0 ",n0)
print("betae ",betae)

nc = [len(Te[0][0]), len(Te[0]), len(Te)]

icentr = int(np.fix(centr[0]/gs[0]))-1
jcentr = int(np.fix(centr[1]/gs[1]))-1
kcentr = int(np.fix(centr[2]/gs[2]))-1
iminmax = [icentr-int(np.fix(3*radius/gs[0])), icentr+int(np.fix(3*radius/gs[0]))]
jminmax = [jcentr-int(np.fix(3*radius/gs[1])), jcentr+int(np.fix(3*radius/gs[1]))]
kminmax = [kcentr-int(np.fix(3*radius/gs[2])), kcentr+int(np.fix(3*radius/gs[2]))]

print(iminmax)
print(jminmax)
print(kminmax)

r_axis = np.arange(radius*phys_length,2*radius*phys_length,phys_length*gs[0])
print("r_axis ",r_axis)

count = np.zeros(len(r_axis)-1)
Tmoy = np.zeros(len(r_axis)-1)
Tejv = np.zeros(len(r_axis)-1)
Teiono = np.zeros(len(r_axis)-1)
Te_jv_compute_ei = np.zeros(len(r_axis)-1)
prod_jv_compute_ei = np.zeros(len(r_axis)-1)
Te_iono_compute_ei = np.zeros(len(r_axis)-1)
prod_dna_compute_ei = np.zeros(len(r_axis)-1)
Temoy_test = np.zeros(len(r_axis)-1)
nmoy =  np.zeros(len(r_axis)-1)
nmoyjv =  np.zeros(len(r_axis)-1)
nmoyiono =  np.zeros(len(r_axis)-1)
nmoyO2 =  np.zeros(len(r_axis)-1)


nO2 = np.zeros(len(r_axis)-1)
n0O2 = 2.5e7
H0=100
nO2 = n0O2*np.exp(-(r_axis[:-1]-r_axis[0])/H0)
coefs = [-352.134, 88.1318, -8.882784, 0.4082678, -0.0072026] # coefficients pour calculer la frequence d'ionisation.
                                                              # Déterminé grace au script O2+_ionisation_production
PROD_JV_TESt = np.zeros((nc[2],nc[1],nc[0]))
PROD_JV_TEStmoy =  np.zeros(len(r_axis)-1)

for ii in range(iminmax[0],iminmax[1]):#len(X_axis)):
    for jj in range(jminmax[0],jminmax[1]):#len(Y_axis)):
        for kk in range(kminmax[0],kminmax[1]):#len(Z_axis)):
            
            
            # rate = coefs[0]
            # Tejv_tmp = 1/e * betae*(nsimjv[kk,jj,ii])**(gamma-1) * B0**2/(2*mu*n0)
            # for c in range(1,len(coefs)):
            #     rate += coefs[c]*np.log(Tejv_tmp*e/kB)**c
            # PROD_JV_TESt[kk,jj,ii] = Dnjv[kk,jj,ii]*np.exp(rate)


            rb = np.sqrt(X_axis[ii]**2 + Y_axis[jj]**2 + Z_axis[kk]**2)
            if r_axis[0] <= rb and rb <= r_axis[-1]:
                for r in range(0,len(r_axis)-1):
                    if r_axis[r] <= rb and rb < r_axis[r+1]:
                        theta = np.arccos(Z_axis[kk]/rb)
                        phi=nan
                        if (X_axis[ii] != 0.0):
                            if X_axis[ii] > 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii])
                            elif Y_axis[jj] >= 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) + np.pi
                            elif Y_axis[jj] < 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) - np.pi
                            elif Y_axis[jj] > 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) + np.pi
                        elif Y_axis[jj] > 0:
                            phi = np.pi/2
                        elif Y_axis[jj] < 0:
                            phi = -np.pi/2
                        phi = (phi+4*np.pi)%(2*np.pi)
                            
                        if np.pi/2.0 <= phi and phi <= 3*np.pi/2.0:    
                            # print(X_axis[ii],Y_axis[jj],Z_axis[kk],ii,jj,kk,rb,theta,phi)

                            count[r] += 1
                            Tmoy[r] += Te[kk,jj,ii]
                            nmoy[r] += Dn[kk,jj,ii]
                            nmoyjv[r] += Dnjv[kk,jj,ii]
                            nmoyiono[r] += Dniono[kk,jj,ii]
                            nmoyO2[r] += Dn_O2[kk,jj,ii]
                            Tejv_tmp = 1/e * betae*(nsimjv[kk,jj,ii])**(gamma-1) * B0**2/(2*mu*n0)
                            Tejv[r] += Tejv_tmp
                            
                            gamma_iono = gamma
                            if nsimiono[kk,jj,ii] > 1.1:
                                gamma_iono = 1/np.log10(nsimiono[kk,jj,ii])
                                gamma_iono = gamma_iono/(np.sqrt(np.sqrt(1+gamma_iono**4/gamma**4)))
                            Teiono_tmp = 1/e * betae*(nsimiono[kk,jj,ii])**(gamma_iono-1) * B0**2/(2*mu*n0)
                            Teiono[r] += Teiono_tmp

                            
                            if Dniono[kk,jj,ii] > 0 and Dnjv[kk,jj,ii] > 0:
                                Temoy_test[r] += (Dnjv[kk,jj,ii]*Tejv_tmp + Dniono[kk,jj,ii]*Teiono_tmp*1e-3)/(Dnjv[kk,jj,ii]+Dniono[kk,jj,ii])
                                # te = betae/3.0
                                # Temoy_test[r] += (te*nsimjv[kk,jj,ii]**gamma + te*1e-3*nsimiono[kk,jj,ii]**gamma_iono)/(nsimjv[kk,jj,ii] + 1e-3*nsimiono[kk,jj,ii]) * phys_temp_e #<Te> = Pe/ne
                                # print(r_axis[r]-r_axis[0],Dniono[kk,jj,ii],nsimiono[kk,jj,ii],gamma_iono,Teiono,betae,B0**2/(2*mu*n0))

                            Te_jv_compute_ei[r] += Tejv_compute_ei[kk,jj,ii]
                            prod_jv_compute_ei[r] += prodjv_compute_ei[kk,jj,ii]

                            Te_iono_compute_ei[r] += Teiono_compute_ei[kk,jj,ii]
                            prod_dna_compute_ei[r] += proddna_compute_ei[kk,jj,ii]

                            PROD_JV_TEStmoy[r] += nO2[r]*PROD_JV_TESt[kk,jj,ii]




Tmoy           = Tmoy/count
nmoy           = nmoy/count
Tejv           = Tejv/count
nmoyjv         = nmoyjv/count
Teiono         = Teiono/count
Te_jv_compute_ei   = Te_jv_compute_ei/count
prod_jv_compute_ei = prod_jv_compute_ei/count
nmoyiono       = nmoyiono/count
nmoyO2       = nmoyO2/count
Te_iono_compute_ei   = Te_iono_compute_ei/count
prod_dna_compute_ei = prod_dna_compute_ei/count

PROD_JV_TEStmoy = PROD_JV_TEStmoy/count

Temoy_test = nmoyjv*Tejv/(nmoyjv+nmoyiono) + nmoyiono*Teiono*1e-3/(nmoyjv+nmoyiono)

print(Tmoy)
print(Temoy_test)
print(Teiono)

# print(Tecompute_ei)

### Temperature 

plt.plot(r_axis[:-1]-r_axis[0],Tmoy,c="blue",ls="--",label="$<Te^{sim}>$")
plt.plot(r_axis[:-1]-r_axis[0],Tejv,c="red",label="$Te_{jv}$")
plt.plot(r_axis[:-1]-r_axis[0],Teiono,c="green",label="$Te_{iono}$")
plt.plot(r_axis[:-1]-r_axis[0],Temoy_test,c="blue",label="$<Te>$")
plt.plot(r_axis[:-1]-r_axis[0],Te_jv_compute_ei,c="red",ls="--",label="$Te_{jv}^{sim}$")
# plt.plot(r_axis[:-1]-r_axis[0],Te_iono_compute_ei,c="green",ls="--",label="$Te_{iono}^{sim}$")
plt.grid(True)
plt.legend(loc="lower right")
plt.xlabel('altitude from surface [$km$]')
plt.ylabel('Te [eV]')
plt.savefig("upstream_hemis_mean_temp_profile_along_r_gamma_iono.png")

plt.close('all')

### Density

plt.plot(r_axis[:-1]-r_axis[0],nmoy,c="blue",label="$n_e$")
plt.plot(r_axis[:-1]-r_axis[0],nmoyjv,c="red",label="$n_e^{jv}$")
plt.plot(r_axis[:-1]-r_axis[0],nmoyiono,c="green",label="$n_e^{iono}$")
plt.grid(True)
plt.legend(loc="lower right")
plt.xlabel('altitude from surface [$km$]')
plt.ylabel('$n_e$ [$cm^{-3}$]')
plt.savefig("upstream_hemis_mean_density_profile_along_r_gamma_iono.png")

plt.close('all')

### Production of ei





prod_ei = np.zeros(len(r_axis)-1)
prod_ei[:] = coefs[0]
for c in range(1,len(coefs)):
    prod_ei += coefs[c]*np.log(Tmoy*e/kB)**c
prod_ei = nO2*nmoy*np.exp(prod_ei)

plt.plot(r_axis[:-1]-r_axis[0],prod_ei,c="blue",label="$Q^{ei}_{jv+iono}$")

prod_ei[:] = coefs[0]
for c in range(1,len(coefs)):
    prod_ei += coefs[c]*np.log(Tejv*e/kB)**c
prod_ei = nO2*nmoyjv*np.exp(prod_ei)

plt.plot(r_axis[:-1]-r_axis[0],prod_ei,c="red",label="$Q^{ei}_{jv}$")

prod_ei[:] = coefs[0]
for c in range(1,len(coefs)):
    for i in range(0,len(Teiono)):
        if Teiono[i] > 0.0:
            prod_ei[i] += coefs[c]*np.log(Teiono[i]*e/kB)**c
prod_ei = nO2*nmoyiono*np.exp(prod_ei)


plt.plot(r_axis[:-1]-r_axis[0],prod_ei,c="green",label="$Q_{ei}^{iono}$")
plt.plot(r_axis[:-1]-r_axis[0],prod_jv_compute_ei,c="red",ls="--",label="$Q^{ei sim}_{jv} $")
plt.plot(r_axis[:-1]-r_axis[0],nO2*1.7e-8,color="pink",label="$Q^{h\\nu}$")
plt.plot(r_axis[:-1]-r_axis[0],prod_dna_compute_ei,c="blue",ls="--",label="$Q^{ei sim}_{jv+iono}$")


prod_ei = np.zeros(len(r_axis)-1)
prod_ei[:] = coefs[0]
for c in range(1,len(coefs)):
    prod_ei += coefs[c]*np.log(Tejv*e/kB)**c
prod_ei = nmoyO2*nmoy*np.exp(prod_ei)

plt.plot(r_axis[:-1]-r_axis[0],prod_ei,c="blue",ls=":",label="$Q^{ei}_{jv+iono} TEST n_{O2}^{sim}$")

prod_ei[:] = coefs[0]
for c in range(1,len(coefs)):
    for i in range(0,len(Tejv)):
        if Tejv[i] > 0.0:
            prod_ei[i] += coefs[c]*np.log(Tejv[i]*e/kB)**c
prod_ei = nmoyO2*nmoyjv*np.exp(prod_ei)
plt.plot(r_axis[:-1]-r_axis[0],prod_ei,c="red",ls=":",label="$Q^{ei}_{jv} TEST n_{O2}^{sim}$")
plt.plot(r_axis[:-1]-r_axis[0],PROD_JV_TEStmoy,c="black",ls="--",label="$Q_{ei}^{jv} TEST$")

plt.legend(loc="upper right")
plt.yscale('log')
plt.grid(True,'both',color='lightgrey', ls="--")
plt.rc('axes', axisbelow=True)
plt.gca().yaxis.set_major_locator(tck.LogLocator(base=10.0,numticks=12))
plt.gca().yaxis.set_minor_locator(tck.LogLocator(base=10.0,subs=(0.2,0.4,0.6,0.8),numticks=12))
plt.gca().yaxis.set_minor_formatter(tck.NullFormatter())
plt.ylim(1e-6,1e2)
plt.xlabel('altitude from surface [$km$]')
plt.ylabel('Production ($O_2^+$) [$cm^{-3}.s^{-1}$]')
plt.savefig("upstream_hemis_mean_production_O2+_profile_along_r.png")

plt.close('all')


