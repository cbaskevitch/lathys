from cmath import nan
from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import scipy.stats as st
import math
import sys, os

####
# plot un profil moyen de la temperature en altitude de 1Re à 2Re
# Moyenne sur la longitude et latitude (à voir si besoin de restraindre au cote ram uniquement)
####

e = 1.6e-19
kB = 1.38e-23

# thew_file = sys.argv[1]
# Ojv_file = sys.argv[2]
Oiono_file = sys.argv[1]

# ncid = Dataset(thew_file)
# ncid2 = Dataset(Ojv_file)
ncid3 = Dataset(Oiono_file)
# var_nc = ncid.variables
# var_nc2 = ncid2.variables
var_nc = ncid3.variables
X_axis = var_nc["X_axis"][:]
Y_axis = var_nc["Y_axis"][:]
Z_axis = var_nc["Z_axis"][:]

centr      = var_nc['s_centr'][:]
radius     = var_nc['r_planet'][:]
r_iono     = var_nc['r_iono'][:]
gs         = var_nc['gstep'][:]
phys_length = var_nc['phys_length'][:]
# Dnjv = var_nc2["Density"][:]
# Dniono = var_nc3["Density"][:]
# Te = var_nc["Temperature"][:] # temperature moyenne e- + ions
# Tjv = var_nc2["Temperature"][:]
Tiono = var_nc["Temperature"][:]


nc = [len(Tiono[0][0]), len(Tiono[0]), len(Tiono)]

icentr = int(np.fix(centr[0]/gs[0]))-1
jcentr = int(np.fix(centr[1]/gs[1]))-1
kcentr = int(np.fix(centr[2]/gs[2]))-1
iminmax = [icentr-int(np.fix(3*radius/gs[0])), icentr+int(np.fix(3*radius/gs[0]))]
jminmax = [jcentr-int(np.fix(3*radius/gs[1])), jcentr+int(np.fix(3*radius/gs[1]))]
kminmax = [kcentr-int(np.fix(3*radius/gs[2])), kcentr+int(np.fix(3*radius/gs[2]))]

print(iminmax)
print(jminmax)
print(kminmax)

r_axis = np.arange(radius*phys_length,4*radius*phys_length,phys_length*gs[0])
print("r_axis ",r_axis)

count = np.zeros(len(r_axis)-1)
# Tmoy = np.zeros(len(r_axis)-1)
# Tejv = np.zeros(len(r_axis)-1)
Teiono = np.zeros(len(r_axis)-1)
# nmoyjv =  np.zeros(len(r_axis)-1)
# nmoyiono =  np.zeros(len(r_axis)-1)
pts_x = []
pts_y = []
pts_z = []


for ii in range(iminmax[0],iminmax[1]):#len(X_axis)):
    for jj in range(jminmax[0],jminmax[1]):#len(Y_axis)):
        for kk in range(kminmax[0],kminmax[1]):#len(Z_axis)):
            rb = np.sqrt(X_axis[ii]**2 + Y_axis[jj]**2 + Z_axis[kk]**2)
            if r_axis[0] <= rb and rb <= r_axis[-1]:
                for r in range(0,len(r_axis)-1):
                    if r_axis[r] <= rb and rb < r_axis[r+1]:
                        theta = np.arccos(Z_axis[kk]/rb)
                        phi=nan
                        if (X_axis[ii] != 0.0):
                            if X_axis[ii] > 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii])
                            elif Y_axis[jj] >= 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) + np.pi
                            elif Y_axis[jj] < 0:
                                phi = np.arctan(Y_axis[jj]/X_axis[ii]) - np.pi
                        elif Y_axis[jj] > 0:
                            phi = np.pi/2
                        elif Y_axis[jj] < 0:
                            phi = -np.pi/2
                        phi = (phi+4*np.pi)%(2*np.pi)
                            
                        if np.pi/2.0 <= phi and phi <= 3*np.pi/2.0:    
                            # print(X_axis[ii],Y_axis[jj],Z_axis[kk],ii,jj,kk,rb,theta,phi)

                            count[r] += 1
                            # Tmoy[r] += Te[kk,jj,ii]
                            # nmoyjv[r] += Dnjv[kk,jj,ii]
                            # nmoyiono[r] += Dniono[kk,jj,ii]
                            # Tejv[r] += Tjv[kk,jj,ii]
                            Teiono[r] += Tiono[kk,jj,ii]
                            if np.isnan(Tiono[kk,jj,ii]):
                                print(ii,jj,kk)
                            if rb < r_axis[1]:
                                pts_x.append(X_axis[ii])
                                pts_y.append(Y_axis[jj])
                                pts_z.append(Z_axis[kk])
                            break

# Tmoy = np.where(Tmoy != nan, Tmoy/count, 0)
# Tejv = np.where(Tejv != nan, Tejv/count, 0)
# nmoyjv = np.where(Tejv != nan, nmoyjv/count, 0)
print(Teiono,count)
Teiono = np.where(Teiono != nan, Teiono/count, 0)
# Teiono = Teiono*1e-3
# nmoyiono = np.where(Teiono != nan, nmoyiono/count, 0)

# print(Tmoy)
# print(Tejv)
print(Teiono)

r_axis[:] = r_axis[:] - r_axis[0]

# plt.plot(r_axis[:-1],Tmoy,c="blue",label="$<Te>$")
# plt.plot(r_axis[:-1],Tejv,c="red",label="$Te_{jv}$")
plt.plot(Teiono,r_axis[:-1],c="green",label="$Te_{iono}$")
plt.grid(True,'both','both',ls=":",color="lightgrey")
plt.legend(loc="upper right")
plt.ylabel('altitude [$R_E$]')
plt.xlabel('Te [eV]')
plt.savefig("mean_temp_profile_along_r.png")

plt.close('all')

sys.exit()

### Production of ei

nO2 = np.zeros(len(r_axis)-1)
n0O2 = 2.5e7
H0=100
nO2 = n0O2*np.exp(-(r_axis[:-1])/H0)

coefs = [-352.134, 88.1318, -8.882784, 0.4082678, -0.0072026] # coefficients pour calculer la frequence d'ionisation.
                                                              # Déterminé grace au script O2+_ionisation_production

prod_ei = np.zeros(len(r_axis)-1)
prod_ei[:] = coefs[0]
for c in range(1,len(coefs)):
    prod_ei += coefs[c]*np.log(Tejv*e/kB)**c
prod_ei = nO2*nmoyjv*np.exp(prod_ei)

plt.plot(r_axis[:-1],prod_ei,c="blue",label="$\\nu_{ei}^{jv}$")

prod_ei[:] = coefs[0]
for c in range(1,len(coefs)):
    prod_ei += coefs[c]*np.log(Teiono*e/kB)**c
prod_ei = nO2*nmoyiono*np.exp(prod_ei)

# plt.plot(r_axis[:-1],prod_ei,c="red",label="$\\nu_{ei}^{iono}$")
# plt.plot(r_axis[:-1],nO2*1.7e-8,color="green",label="$h\\nu$")

plt.plot(r_axis[:-1],prod_ei,c="red",label="$\\nu_{ei}^{iono}$")
plt.plot(r_axis[:-1],nO2*1.7e-8,color="green",label="$h\\nu$")

plt.xlim(0,600)
# plt.ylim(1e-5,1e3)
plt.legend(loc="lower right")
plt.yscale('log')
plt.grid(True,'both','both',ls=":",color="lightgrey")
# plt.gca().yaxis.set_minor_formatter(tck.NullFormatter())
plt.xlabel('altitude [$R_E$]')
plt.ylabel('Production ($O_2^+$) [$cm^{-3}.s^{-1}$]')
plt.savefig("production_O2+_profile_along_r.png")

plt.close('all')

