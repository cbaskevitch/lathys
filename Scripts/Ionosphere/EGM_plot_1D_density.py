from cmath import nan
from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import scipy.stats as st
import math
import sys, os

####
# plot un profil moyen de la temperature en altitude de 1Re à 2Re
# Moyenne sur la longitude et latitude (à voir si besoin de restraindre au cote ram uniquement)
####

e = 1.6e-19
kB = 1.38e-23

egm_file = sys.argv[1]

ncid = Dataset(egm_file)
var_nc = ncid.variables
altitude = var_nc["altitude"][:]*1e-5   # km
print(altitude)
latitude = var_nc["latitude"][:]   # rad
longitude = var_nc["longitude"][:] # rad

# radius     = var_nc['Planetary_Radius'][:]*1e-5 #km
radius     = var_nc['planet_radius'][:]*1e-5 #km
Dn_O2 = var_nc["density_O2"][:]

# len_alt = ncid.dimensions["altitude"].size
# len_lat = ncid.dimensions["latitude"].size
# len_lon = ncid.dimensions["longitude"].size
len_alt = ncid.dimensions["nr"].size
len_lat = ncid.dimensions["ntheta"].size
len_lon = ncid.dimensions["nphi"].size

print(len_alt,len_lat,len_lon)

r_axis = np.arange(radius,radius+800,50)
print("r_axis ",r_axis)

count = np.zeros(len(r_axis)-1)
Dn_O2_moy = np.zeros(len(r_axis)-1)


for ii in range(0,len_lon):
    for jj in range(0,len_lat):
        for kk in range(0,len_alt):
            # rb = altitude[ii,jj,kk]#-radius
            rb = altitude[kk]#-radius
            # print(r_axis[0],rb,r_axis[-1])
            if r_axis[0] <= rb and rb <= r_axis[-1]:
                for r in range(0,len(r_axis)-1):
                    if r_axis[r] <= rb and rb < r_axis[r+1]:
                        #if np.pi/2.0 <= phi and phi <= 3*np.pi/2.0:    
                        count[r] += 1
                        Dn_O2_moy[r] += Dn_O2[ii,jj,kk]
                            
# print(Teiono,count)
Dn_O2_moy = np.where(count != 0, Dn_O2_moy/count, 0)
print(Dn_O2_moy)

r_axis[:] = r_axis[:] - r_axis[0]

plt.plot(Dn_O2_moy,r_axis[:-1],c="blue")#,label="$T_{O_2}$")
plt.grid(True,'both','both',ls=":",color="lightgrey")
# plt.legend(loc="upper right")
plt.ylabel('altitude [km]')
plt.xlabel('$n_{O_2}$ [$cm_{-3}$]')
plt.savefig("mean_nO2_1D_profile_along_r.png")

plt.close('all')


