# Comparaison du profil de densite des neutres O2 dans la simulation avec la fonction analytique
# n(z)=n0*exp(-z/z0) (Rubin et al 2015 et Harris et al 2021)

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

# src_dir = sys.argv[1]
# dest_dir = sys.argv[2]
# rundate = sys.argv[3]
# diagtime = sys.argv[4]
egm_ncfile = sys.argv[1]#src_dir + "/Atmw_" + rundate + "_t" + diagtime + '.nc'
egm_ncid = Dataset(egm_ncfile)
egm_var_nc = egm_ncid.variables

egm_nO2 = egm_var_nc["density_O2"][:]
egm_altitude = egm_var_nc["altitude"][:]*1e-5   # km
egm_phi_upp = egm_var_nc["phi_upp"][:]
egm_phi_low = egm_var_nc["phi_low"][:]
egm_r_upp = egm_var_nc["altitude_upp"][:]*1e-5 #km
egm_r_low = egm_var_nc["altitude_low"][:]*1e-5 #km
egm_theta_upp = egm_var_nc["theta_upp"][:]
egm_theta_low = egm_var_nc["theta_low"][:]
egm_radius     = egm_var_nc['planet_radius'][:]*1e-5 #km

egm_nr = egm_var_nc["npt_alt"][0]
egm_ntheta = egm_var_nc["npt_lat"][0]
egm_nphi = egm_var_nc["npt_lon"][0]

r_axis = egm_altitude[:]#np.arange(radius,radius+800,50)
print("r_axis ",r_axis)



theta_ori = 90.0 #math.pi/2.0										#equatorial angle (pi/2)
theta = theta_ori*math.pi/180.0
dtheta = 5.0*math.pi/180.0									#interval of the equatorial angle for statistical mean, in rad
phi = 180*math.pi/180.0
dphi = 5*math.pi/180.0


tt=0
while not(egm_theta_low[tt] <= theta and theta < egm_theta_upp[tt]):
	tt += 1
theta1 = (egm_theta_upp[tt]+egm_theta_low[tt])/2 - dtheta
theta2 = (egm_theta_upp[tt]+egm_theta_low[tt])/2 + dtheta
if theta1 <= 0:
	theta1=0
else:
	tt=0
	while not(egm_theta_low[tt] <= theta1 and theta1 < egm_theta_upp[tt]):
		tt += 1
	theta1=tt
if theta2 > math.pi:
	theta2 = egm_ntheta
else:
	tt=0
	while  not(egm_theta_low[tt] <= theta2 and theta2 < egm_theta_upp[tt]):
		tt += 1
	theta2=tt

pp=0
while not(egm_phi_low[pp] <= phi and phi < egm_phi_upp[pp]):
    pp += 1
phi1 = (egm_phi_upp[pp]+egm_phi_low[pp])/2 - dphi
phi2 = (egm_phi_upp[pp]+egm_phi_low[pp])/2 + dphi
if phi1 <= 0:
    phi1=0
else:
    pp=0
    while not(egm_phi_low[pp] <= phi1 and phi1 < egm_phi_upp[pp]):
        pp += 1
    phi1=pp
if phi2 > 2*math.pi:
    phi2 = egm_nphi
else:
    pp=0
    while not(egm_phi_low[pp] <= phi2 and phi2 < egm_phi_upp[pp]):
        pp += 1
    phi2=pp

n_O2_egm = np.zeros(egm_nr)
sum_cells = np.zeros(egm_nr)
for p in range(phi1, phi2):
    if(egm_phi_upp[p]>=(phi-dphi) and egm_phi_low[p]<=(phi+dphi)):
        dens = 0
        for t in range(theta1, theta2):
            if(egm_theta_upp[t]>=(theta-dtheta) and egm_theta_low[t]<=(theta+dtheta)):
                for r in range(0, egm_nr):
                    Vcell = 1.0/3.0 * (egm_r_upp[r] ** 3 - egm_r_low[r] ** 3) * (egm_phi_upp[p] - egm_phi_low[p])
                    Vcell = Vcell * math.fabs(math.cos(egm_theta_low[t] - math.cos(egm_theta_upp[t])))
                    sum_cells[r] += Vcell
                    n_O2_egm[r] += egm_nO2[p][t][r]*Vcell #weight[specie][phi][t][r]/(Vcell*accumul_time_steps*dt)

n_O2_egm = np.divide(n_O2_egm,sum_cells)

n0=2.5e7 #cm-3
H0=100   #km
nO2_Harris=n0*np.exp(-(r_axis-egm_radius)/H0)
# nO2_Harris = np.where(x_ == 0, 0, nO2_Harris)

def read_lathys(file_name):
    lathys_ncfile = file_name#src_dir + "/Atmw_" + rundate + "_t" + diagtime + '.nc'
    lathys_ncid = Dataset(lathys_ncfile)
    lathys_var_nc = lathys_ncid.variables

    lathys_centr      = lathys_var_nc['s_centr'][:]
    lathys_radius     = lathys_var_nc['r_planet'][:]
    lathys_r_iono     = lathys_var_nc['r_iono'][:]
    lathys_gs         = lathys_var_nc['gstep'][:]
    lathys_phys_length = lathys_var_nc['phys_length'][:]
    lathys_nO2 = lathys_var_nc["Den_O2"][:]
    lathys_nc = [len(lathys_nO2[0][0]), len(lathys_nO2[0]), len(lathys_nO2)]

    icentr = int(np.fix(lathys_centr[0]/lathys_gs[0]))
    jcentr = int(np.fix(lathys_centr[1]/lathys_gs[1]))
    kcentr = int(np.fix(lathys_centr[2]/lathys_gs[2]))

    lathys_X=np.arange(0,(lathys_nc[0])*lathys_gs[0],lathys_gs[0])/lathys_radius - lathys_centr[0]/lathys_radius

    lathys_r_axis = lathys_X[icentr:]*lathys_phys_length*lathys_radius-lathys_radius*lathys_phys_length    

    return lathys_nO2[kcentr,jcentr,icentr:],lathys_r_axis

lathys_nO2_X_100km,lathys_100km_r_axis = read_lathys(sys.argv[2])
lathys_nO2_X_100km_subgrid,lathys_100km_r_axis = read_lathys(sys.argv[3])
lathys_nO2_X_50km,lathys_50km_r_axis = read_lathys(sys.argv[4])
lathys_nO2_X_50km_subgrid,lathys_50km_r_axis = read_lathys(sys.argv[5])


print(n_O2_egm)
print(nO2_Harris)
print(lathys_nO2_X_100km)
print(lathys_nO2_X_100km_subgrid)
print(lathys_100km_r_axis)

plt.plot(r_axis-egm_radius,n_O2_egm,c="b",label="$n_{O_2}$ EGM")
plt.plot(lathys_100km_r_axis,lathys_nO2_X_100km,c="g",label="$n_{O_2}$ LatHyS 100km")
plt.plot(lathys_100km_r_axis,lathys_nO2_X_100km_subgrid,c="g",ls="--",label="$n_{O_2}$ LatHyS 100km interpol. with sub grid")
plt.plot(lathys_50km_r_axis,lathys_nO2_X_50km,c="r",label="$n_{O_2}$ LatHyS 50km")
plt.plot(lathys_50km_r_axis,lathys_nO2_X_50km_subgrid,c="r",ls="--",label="$n_{O_2}$ LatHyS 50km interpol. with sub grid")
plt.plot(r_axis-egm_radius,nO2_Harris,ls="-.",c="black",label="$n_{O_2}$ Harris et al. 2021")
plt.legend(loc="upper right")
plt.xlabel("altitude [km]")
plt.ylabel("nO2 [cm^-3]")
plt.yscale('log')
plt.xlim(0,1000)
plt.ylim(1,1e8)
plt.savefig("profils_nO2_egm_vs_analytic_Harris2021.png")