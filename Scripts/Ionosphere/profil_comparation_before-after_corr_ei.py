from cmath import nan
from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import scipy.stats as st
import math
import sys, os


# 1D profil along x (y=0, z=0) between 2 runs

c = ["blue","red","blue","red"]
l = ["$n_{O_2^+}^{ei}$ avant corr. t=50","$n_{O_2^+}^{ei}$ avant corr. t=100","$n_{O_2^+}^{ei}$ apres corr. t=50","$n_{O_2^+}^{ei}$ apres corr. t=100"]
ls = ["-","-","--","--"]

for i in range(1,5):
    ncfile = sys.argv[i]
    ncid = Dataset(ncfile)
    var_nc = ncid.variables

    centr      = var_nc['s_centr'][:]
    radius     = var_nc['r_planet'][:]
    r_iono     = var_nc['r_iono'][:]
    gs         = var_nc['gstep'][:]
    phys_length = var_nc['phys_length'][:]
    n = var_nc["Density"][:]
    X_axis = var_nc["X_axis"][:]
    Y_axis = var_nc["Y_axis"][:]
    Z_axis = var_nc["Z_axis"][:]

    # nO2 = var_nc["Den_O2p"][:]*var_nc['phys_density'][:]/1e6
    
    nc = [len(n[0][0]), len(n[0]), len(n)]
    print(nc)
    print(len(X_axis),len(Y_axis),len(Z_axis))

    # icentr = int(np.fix(centr[0]/gs[0]))
    jcentr1 = int(np.fix(centr[1]/gs[1]))
    kcentr1 = int(np.fix(centr[2]/gs[2]))

    jcentr = 0 #np.where(-1 < Y_axis and Y_axis < 1)
    kcentr = 0 #np.where(-1 < Z_axis and Z_axis < 1)

    for y in Y_axis:
        if -50 < y and y < 50:
            jcentr=np.where(Y_axis==y)[0][0]

    for z in Z_axis:
        if -50 < z and z < 50:
            kcentr=np.where(Z_axis==z)[0][0]

    print(jcentr1,kcentr1)
    print(jcentr,kcentr)

    print(Y_axis[jcentr],Z_axis[kcentr])

    if jcentr==-1 or kcentr==-1:
        sys.exit()

    n_X=n[kcentr,jcentr,:]

    plt.plot(X_axis[:],n_X[:],c=c[i-1],label=l[i-1],ls=ls[i-1])
# plt.plot(X_axis[:],nO2_Harris,c="r",ls="--",label="nO2+ ref")
plt.legend(loc="upper center")
plt.xlabel("x [km]")
plt.ylabel("$n_{O_2^+}$ [$cm^{-3}$]")
plt.yscale('log')
plt.grid("True")
# plt.xlim(1.0,2.5)
plt.xlim(-3000,3000)
plt.savefig("profil_comparation_before-after_corr_ei_log_scale.png")