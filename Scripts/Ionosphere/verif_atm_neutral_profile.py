# Comparaison du profil de densite des neutres O2 dans la simulation avec la fonction analytique
# n(z)=n0*exp(-z/z0) (Rubin et al 2015 et Harris et al 2021)

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

# src_dir = sys.argv[1]
# dest_dir = sys.argv[2]
# rundate = sys.argv[3]
# diagtime = sys.argv[4]
ncfile = sys.argv[1]#src_dir + "/Atmw_" + rundate + "_t" + diagtime + '.nc'
ncid = Dataset(ncfile)
var_nc = ncid.variables

centr      = var_nc['s_centr'][:]
radius     = var_nc['r_planet'][:]
r_iono     = var_nc['r_iono'][:]
gs         = var_nc['gstep'][:]
phys_length = var_nc['phys_length'][:]
nO2 = var_nc["Den_O2"][:]
nc = [len(nO2[0][0]), len(nO2[0]), len(nO2)]

icentr = int(np.fix(centr[0]/gs[0]))
jcentr = int(np.fix(centr[1]/gs[1]))
kcentr = int(np.fix(centr[2]/gs[2]))

nO2_X=nO2[kcentr,jcentr,:]
X=np.arange(0,(nc[0])*gs[0],gs[0])/radius - centr[0]/radius

n0=2.5e7 #cm-3
H0=100   #km
x_ = np.arange(radius*phys_length,radius*phys_length*2,10)-radius*phys_length#X[icentr:]*phys_length*radius - radius*phys_length
x_ = np.where(x_ <= 0, 0, x_)
nO2_Harris=n0*np.exp(-x_/H0)
nO2_Harris = np.where(x_ == 0, 0, nO2_Harris)


plt.plot((X[icentr:]*phys_length*radius)-radius*phys_length,nO2_X[icentr:],c="b",label="$n_{O_2}$ LatHyS")
plt.plot(x_,nO2_Harris,ls="--",c="r",label="$n_{O_2}$ Harris et al. 2021")
plt.legend(loc="upper right")
plt.xlabel("x [km]")
plt.ylabel("nO2 [cm^-3]")
plt.yscale('log')
plt.xlim(0,2000)
plt.ylim(1e1,1e8)
plt.savefig("verif_nO2.png")