from netCDF4 import Dataset
import numpy as np
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.colors as mpcol
from matplotlib.patches import Polygon, Wedge
from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
import math
import sys, os
from datetime import datetime

file_ori = sys.argv[1]
nc = Dataset(file_ori)
var_nc = nc.variables
phi_upp = var_nc["phi_upp"][:]
phi_low = var_nc["phi_low"][:]
r_upp = var_nc["altitude_upp"][:]*1e-5 #km
r_low = var_nc["altitude_low"][:]*1e-5 #km
theta_upp = var_nc["theta_upp"][:]
theta_low = var_nc["theta_low"][:]
# weight = var_nc["Weight"][:]
# radius = var_nc['PLANETARY_RADIUS'][:]
subsol_long = var_nc["Subsol_Long"][0]
species = var_nc['species'][:]
# accumul_time_steps = var_nc['Accumul_time_steps'][:]
# dt = var_nc['dt'][:]
spe=sys.argv[2]
Dn = var_nc["density_"+spe][:]
radius     = var_nc['planet_radius'][:]*1e-5 #km
nr = var_nc["npt_alt"][0]
ntheta = var_nc["npt_lat"][0]
nphi = var_nc["npt_lon"][0]


theta_ori = 155.0 #math.pi/2.0										#equatorial angle (pi/2)
theta = (180.0-theta_ori)*math.pi/180.0
dtheta = 5.0*math.pi/180.0									#interval of the equatorial angle for statistical mean, in rad
dphi = 0*math.pi/180.0
# val_theta_cells = math.pi / len(theta_upp[0])			#theta value for one cell
val_phi_cells = 2.0*math.pi / len(phi_upp)
# theta1 = int((theta - dtheta)/val_theta_cells)
# theta2 = int((theta + dtheta)/val_theta_cells + 1.0)

tt=0
while not(theta_low[tt] <= theta and theta < theta_upp[tt]):
	tt += 1
theta1 = (theta_upp[tt]+theta_low[tt])/2 - dtheta
theta2 = (theta_upp[tt]+theta_low[tt])/2 + dtheta
if theta1 <= 0:
	theta1=0
else:
	tt=0
	while not(theta_low[tt] <= theta1 and theta1 < theta_upp[tt]):
		tt += 1
	theta1=tt
if theta2 > math.pi:
	theta2 = ntheta
else:
	tt=0
	while  not(theta_low[tt] <= theta2 and theta2 < theta_upp[tt]):
		tt += 1
	theta2=tt

subsol_long = subsol_long * 180.0 / math.pi
subsol_low = subsol_long - 90.0
subsol_upp = subsol_long + 90.0

dens_eq_plane=[]
for phi in range(0, nphi):
	sum_cells = np.zeros(shape=(nr), dtype=float)
	dens = np.zeros(shape=(nr), dtype=float)
	for t in range(theta1, theta2):
		if(theta_upp[t]>=(theta-dtheta) and theta_low[t]<=(theta+dtheta)):
			for r in range(0, nr):
				dr = r_upp[r] - r_low[r]
				dt = theta_upp[t] - theta_low[t]
				dp = phi_upp[phi] - phi_low[phi]
				Vcell = 0
				if theta_low[t] == 0:
					Vcell =  r_low[r]**2 * np.sin(theta_upp[t]) * dr * dt * dp
				else:
					Vcell = r_low[r]**2 * np.sin(theta_low[t]) * dr * dt * dp
				# Vcell = 1.0/3.0 * (r_upp[r] ** 3 - r_low[r] ** 3) * (phi_upp[phi] - phi_low[phi])
				# Vcell = Vcell * math.fabs(math.cos(theta_low[t] - math.cos(theta_upp[t])))
				sum_cells[r] = sum_cells[r] + Vcell
				dens[r] = dens[r] + Dn[phi][t][r]*Vcell #weight[specie][phi][t][r]/(Vcell*accumul_time_steps*dt)
				Phi = (phi_upp[phi]+phi_low[phi])/2.0								#interval of the equatorial angle for statistical mean, in rad
				phi1 = int((Phi - dphi)/val_phi_cells)
				phi2 = int((Phi + dphi)/val_phi_cells)
				for i in range(phi1, phi2):
					if (i < nphi-1) and i > 0 and i != phi:
						Vcell = 1.0/3.0 * (r_upp[r] ** 3 - r_low[r] ** 3) * (phi_upp[i] - phi_low[i])
						Vcell = Vcell * math.fabs(math.cos(theta_low[t] - math.cos(theta_upp[t])))
						dens[r] = dens[r] + Dn[phi][t][r]*Vcell #weight[specie][i][t][r]/(Vcell*accumul_time_steps*dt)
						sum_cells[r] = sum_cells[r] + Vcell
				# for i in range(r-1, r+1):
				# 	if (i < len(weight[specie][0][0])-1) and i > 0 and i != r:
				# 		Vcell1 = 1.0/3.0 * (r_upp[phi][t][i] ** 3 - r_low[phi][t][i] ** 3) * (phi_upp[phi][t][i] - phi_low[phi][t][i])
				# 		Vcell1 = Vcell1 * math.fabs(math.cos(theta_low[phi][t][i] - math.cos(theta_upp[phi][t][i])))
				# 		dens[r] = dens[r] + Vcell1 * weight[specie][phi][t][i]/(Vcell1*accumul_time_steps*dt)
				# 		sum_cells[r] = sum_cells[r] + Vcell1
	#np.savetxt("dens.txt", cumul_dens, delimiter=" ")
	dens = np.divide(dens,sum_cells)
	dens_eq_plane.append(dens)
dens_eq_plane = np.asarray(dens_eq_plane, dtype=float)
dens_eq_plane = np.log10(dens_eq_plane)
print("End computing density")

print("Start ploting figure")
fig, ax = plt.subplots(figsize=[8,6])
# fig.patch.set_facecolor('black')
# cmap = mpcol.LinearSegmentedColormap.from_list("", ["black","purple", "blue","cyan", "green", "yellow", "orange", "red"])
cmap = mpl.cm.get_cmap("jet")
print(np.amax(dens_eq_plane))
print(np.amin(dens_eq_plane))
# vmin_max = [[0.5, 3.5], [0.0, np.amax(dens_eq_plane)], [3.5, np.amax(dens_eq_plane)], [0.0, np.amax(dens_eq_plane)], [0.0, np.amax(dens_eq_plane)], [0.0, np.amax(dens_eq_plane)]]
if spe=="O2":
    # vmin_max = [-8.0, 8.0]#[2.0, 8.0]#16.0215]
    vmin_max = [0.0, 8.5]#[2.0, 8.0]#16.0215]
elif spe=="H2O":
    # vmin_max = [0.0, 16.0]
    vmin_max = [-5.5, 6.5]
norm = mpcol.Normalize(vmin=vmin_max[0], vmax=vmin_max[1])#np.amax(dens_eq_plane))
# norm = mpcol.Normalize(vmin=vmin_max[specie][0], vmax=vmin_max[specie][1])

# convertion en coordonnees cartesiennes
# x = r * sin theta * cos phi
# y = r * sin theta * sin phi
# z = r * cos theta
for r in range(0, nr):
	for phi in range(0, nphi):
		#coordonees bas gauche
		x_bg = r_upp[r] * np.cos(phi_upp[phi])
		y_bg = r_upp[r] * np.sin(phi_upp[phi])
		#coordonees haut gauche
		x_hg = r_upp[r] * np.cos(phi_low[phi])
		y_hg = r_upp[r] * np.sin(phi_low[phi])
		#coordonees bas droit
		x_bd = r_low[r] * np.cos(phi_upp[phi])
		y_bd = r_low[r] * np.sin(phi_upp[phi])
		#coordonees haut droit
		x_hd = r_low[r] * np.cos(phi_low[phi])
		y_hd = r_low[r] * np.sin(phi_low[phi])
		pts = np.array([[x_bg,y_bg], [x_hg,y_hg], [x_hd,y_hd], [x_bd,y_bd]])
		color = cmap(norm(dens_eq_plane[phi][r]))
		p = Polygon(pts, color=color, )
		# p = Polygon(pts, facecolor="white", edgecolor="black")
		ax = plt.gca()
		
		ax.add_patch(p)

# w = Wedge(0.0, radius, 0, 360, facecolor="black", edgecolor ="white")
w = Wedge(0.0, radius,  subsol_upp, subsol_low,color ="black")
ax = plt.gca()
ax.add_patch(w)
lim = radius*1.5
# if specie > 3:
# 	lim = radius*2.5
ax.set_xlim(0.0-lim,lim)
ax.set_ylim(0.0-lim,lim)

i = ax.imshow(dens_eq_plane, cmap=cmap, vmin=vmin_max[0], vmax=vmin_max[1])
cb = fig.colorbar(i)
cb.set_label("log10 "+spe+"/cm3")#, color="white")
# cb.ax.yaxis.set_tick_params(color="white")
# cb.outline.set_edgecolor("white")

plt.setp(plt.getp(cb.ax.axes, 'yticklabels'))#, color="white")
# plt.axis("off")

# plt.show()

tmp = file_ori.split("/")
file = tmp[len(tmp)-1][:-3]
file.replace('density_', '')
fig_title = "XY_"+spe+"_"+file+".png"
dir_name=""#"plot/"
plt.savefig(dir_name+fig_title,facecolor=fig.get_facecolor(), edgecolor='none')
print("End ploting figure")
print("Saved here : "+dir_name+fig_title)

print("end of script")
