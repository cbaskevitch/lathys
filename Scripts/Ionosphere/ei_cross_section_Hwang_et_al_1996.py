#################################
## Ce script reproduit la courbe du modele electron-impact 
## ionization cross sections de l'O2 de Hwang et al 1996.
## BEB : binary-encounter Bethe model
## resultat en cm2
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os



R = 13.61 ## eV
a0 = 0.5292 ## Angstrom

def ei_cross_section(U,B,N,mol,nb_orbitals,label):
    T = np.linspace(20, 1000,num=10000) ## High incident energy in eV
    sigma = np.zeros(len(T))
    sum_sigma = np.zeros(len(T))
    color = ["blue","red","green","yellow","pink", "black"]
    
    for i in range(0,nb_orbitals):
        t = T/B[i]
        u = U[i]/B[i]
        S = 4e-16*math.pi * a0*a0 * N[i] * R*R / (B[i]*B[i])

        sigma = S / (t+u+1) * (np.log(t)/2 * (1-1/(t*t)) + 1 - 1/t - np.log(t)/(t+1))
        sum_sigma += sigma

        plt.plot(np.log10(T),sigma,c=color[i],label=label[i])

    plt.plot(np.log10(T),sum_sigma,c=color[-1],label=label[-1])
    plt.legend(loc="upper right")
    plt.ylabel("sigma "+mol+" [$cm^2$]")
    plt.xlabel("T log10[eV]")
    plt.grid(True)
    # plt.show()
    plt.savefig("BEB_cross_section_"+mol+"_Hwang_et_al_1996.png")
    plt.close('all')

### Parametres pour O2 issu de l'article
U = np.array([79.73,90.92,59.89,71.84,84.88]) ## Kinetic energy in eV
B = np.array([46.19,29.82,19.64,19.79,12.07]) ## Electron binding energy in eV
N = np.array([2,2,4,2,2]) ## orbital occupation number
label = ["$MO = 2\sigma_g$", "$MO = 2\sigma_u$", "$MO = \pi_u$", "$MO = 3\sigma_g$", "$MO = \pi_g$", "Cumul"]
ei_cross_section(U,B,N,"O2",len(U),label)

### Parametres pour H2O issu de l'article
U = np.array([70.71,48.36,59.52,61.91]) ## Kinetic energy in eV
B = np.array([36.88,19.83,15.57,12.61]) ## Electron binding energy in eV
N = np.array([2,2,2,2]) ## orbital occupation number
label = ["$MO = 2a_1$", "$MO = 1b_2$", "$MO = 3a_1$", "$MO = 1b_1$", "Cumul"]
ei_cross_section(U,B,N,"H2O",len(U),label)

### Parametres pour H2 issu de l'article
U = np.array([15.98]) ## Kinetic energy in eV
B = np.array([15.43]) ## Electron binding energy in eV
N = np.array([2]) ## orbital occupation number
label = ["$MO = 1\sigma_g$","Cumul"]
ei_cross_section(U,B,N,"H2",len(U),label)