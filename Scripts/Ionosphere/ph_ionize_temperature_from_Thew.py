### Ce programme calcule la frequence d ionisation à partir 
### de la temperature du fichier Thew.

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

#constantes
Kb = 1.38e-23 ## Constante de Boltzmann en m^2 kg s^-2 K-1

src_dir = sys.argv[1]
dest_dir = sys.argv[2]
rundate = sys.argv[3]
diagtime = sys.argv[4]

ncfile = src_dir + "/Atmw_" + rundate + "_t" + diagtime + '.nc'
ncid = Dataset(ncfile)
var_nc = ncid.variables
centr      = var_nc['s_centr'][:]
radius     = var_nc['r_planet'][:]
r_iono     = var_nc['r_iono'][:]
gs         = var_nc['gstep'][:]
nO2 = var_nc["Den_O2"][:]



nc = [len(nO2[0][0]), len(nO2[0]), len(nO2)]

freq_iono = 1.70e-8 #s-1 (Rubin et al 2015)

prod_iono = np.zeros((nc[2],nc[1],nc[0]))
prod_iono = nO2*freq_iono


print(np.max(prod_iono))
# for i in range(0,nc[0]):
#     for j in range(0,nc[1]):
#         for k in range(0,nc[2]):
#             if prod_iono[k][j][i] > 1e-10:
#                 print(i,j,k,prod_iono[k][j][i])
    
# -- Creation of axis values centered on the planet ( normalized to planet radius)
X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)

# planet center in cell number (NB: cell number start at 1
icentr = int(np.fix(centr[0]/gs[0]))
jcentr = int(np.fix(centr[1]/gs[1]))
kcentr = int(np.fix(centr[2]/gs[2]))

prod_XY = np.zeros((nc[0],nc[1]))
prod_XY[:,:] = np.matrix.transpose(prod_iono[kcentr,:,:])

prod_XZ = np.zeros((nc[0],nc[2]))
prod_XZ[:,:] = np.matrix.transpose(prod_iono[:,jcentr,:])

prod_YZ = np.zeros((nc[1],nc[2]))
prod_YZ[:,:] = np.matrix.transpose(prod_iono[:,:,icentr])

alpha = 7.*np.pi/4.0
a=int(alpha*180/np.pi)
X_XY_mod = X_XY[:,:]*np.cos(alpha) + Y_XY[:,:]*np.sin(alpha)
Y_XY_mod = -X_XY[:,:]*np.sin(alpha) + Y_XY[:,:]*np.cos(alpha)
prod_XY_mod =np.zeros((nc[0],nc[1]))
prod_XY_mod[:,:] = prod_XY[:,:]
print(X_XY_mod)
for i in range(0,nc[0]):
    for j in range(0,nc[1]):
        # mod = np.sqrt(X_XY_mod[i,j]**2 + Y_XY_mod[i,j]**2)
        if X_XY_mod[i,j] > 0 and np.fabs(Y_XY_mod[i,j]) < 1:
        # if (X_XY_mod[i,j] > 0 and np.fabs(Y_XY_mod[i,j]) < 1):
        #     prod_XY_mod[i,j] = 0.3
        # elif (Y_XY_mod[i,j] > 0 and np.fabs(X_XY_mod[i,j])<1):
            # print(i,j)
            prod_XY_mod[i,j] = 10

# planet drawing
theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
xp = np.cos(theta)
yp = np.sin(theta)

fig_size = [[6,9.5],[6,7.5],[8,6],[10,6]] #differentes tailles de fenetres
figsize_Xnum = 2  #numero de la taille de la fenetre pour les plans XZ et XY
figsize_Ynum = 2  #numero de la taille de la fenetre pour les plans YZ
Xmin=X_XY[0][0]
Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1]
Ymin=Y_XY[0][0]
Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1]
Zmin=Z_XZ[0][0]
Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1]


min_val = 1e-10
max_val = 0.5 #5e-4

fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XY, Y_XY, np.log10(prod_XY_mod), vmin=np.log10(min_val), vmax=np.log10(max_val), cmap="hot",shading='auto')
if radius != 1:
    ax.plot(xp,yp,c="white")
    ax.plot(xp*r_iono/radius,yp*r_iono/radius,c="pink")

fig.colorbar(c, ax=ax)
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Ymin,Ymax)

titre = "photoionisation prod time: "+diagtime+" a=",a
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"/ph_prod_a="+str(a)+"_XY_Europa_"+rundate+"_t"+diagtime+".png")


fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XZ, Z_XZ, np.log10(prod_XZ), vmin=np.log10(min_val), vmax=np.log10(max_val), cmap="hot",shading='auto')
if radius != 1:
    ax.plot(xp,yp,c="black")
    ax.plot(xp*r_iono/radius,yp*r_iono/radius,c="pink")

fig.colorbar(c, ax=ax)
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Zmin,Zmax)

titre = "photoionisation prod time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"/ph_prod_XZ_Europa_"+rundate+"_t"+diagtime+".png")

fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
c = ax.pcolor(Y_YZ, Z_YZ, np.log10(prod_YZ), vmin=np.log10(min_val), vmax=np.log10(max_val), cmap="hot",shading='auto')
if radius != 1:
    ax.plot(xp,yp,c="black")
    ax.plot(xp*r_iono/radius,yp*r_iono/radius,c="pink")

fig.colorbar(c, ax=ax)
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "photoionisation prod time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"/ph_prod_YZ_Europa_"+rundate+"_t"+diagtime+".png")








sys.exit()


Te_XY = np.zeros((nc[0],nc[1]))
Te_XY[:,:] = np.matrix.transpose(Te[kcentr,:,:])

Te_XZ = np.zeros((nc[0],nc[2]))
Te_XZ[:,:] = np.matrix.transpose(Te[:,jcentr,:])

Te_YZ = np.zeros((nc[1],nc[2]))
Te_YZ[:,:] = np.matrix.transpose(Te[:,:,icentr])

min_val = 0.0
max_val = 30

fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XY, Y_XY, Te_XY, vmin=min_val, vmax=max_val, cmap="hot",shading='auto')
if radius != 1:
    ax.plot(xp,yp,c="white")

fig.colorbar(c, ax=ax)
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Ymin,Ymax)

titre = "Temperature [eV] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"/temperature_XY_Europa_"+rundate+"_t"+diagtime+".png")

fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XZ, Z_XZ, Te_XZ, vmin=min_val, vmax=max_val, cmap="hot",shading='auto')
if radius != 1:
    ax.plot(xp,yp,c="white")

fig.colorbar(c, ax=ax)
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Zmin,Zmax)

titre = "Temperature [eV] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"/temperature_XZ_Europa_"+rundate+"_t"+diagtime+".png")

fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
c = ax.pcolor(Y_YZ, Z_YZ, Te_YZ, vmin=min_val, vmax=max_val, cmap="hot",shading='auto')
if radius != 1:
    ax.plot(xp,yp,c="white")

fig.colorbar(c, ax=ax)
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "Temperature [eV] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"/temperature_YZ_Europa_"+rundate+"_t"+diagtime+".png")
