from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st
import math
import sys, os

### Compute and plot histogram of Vx, Vy and Vz distribution for different times. 
### @param src_dir : directory of p3 files
### @param dest_dir : destination directory of the results
### @param date : the date of the simulation
### @param time : the time of the simulation
### @return it returns Vx, Vy, Vz distribution of simulation <date> and <file> in csv files stored in src_dir and it plots histograms of the results

if len(sys.argv) < 5:
    print("py velocity_distribution_histo.py <src_dir> <dest_dir> <date> <time>")

#Largeur à mi-hauteur = 2*vth*sqrt(ln(2))
# m = 48*1.66*10**(-27)
# kB = 1.38*10**(-23)
# va = 2.4e5 #vitesse alfvènique
# beta = 0.00548
# n = 35.e6 
# mu0 = 4*math.pi*10**(-7)
# B = (77.6**2 + 140.7**2 + 441.3**2)**(1/2) * 10**(-9)
# T = beta * B**2 / (2*mu0*kB*n) #* 1/1.16e4
# vth = (2*kB*T/m)**(1/2)#/va
# print(vth)
# deltav = vth*0.832554*0.001  #vth * sqrt(ln(2))

# xmaxwell = np.linspace(-1.0, 1.0, 300)
# xmaxwell = xmaxwell*va*0.001

src_dir = sys.argv[1]
dest_dir = sys.argv[2]
date = sys.argv[3]
time = sys.argv[4]
orig0 = int(sys.argv[5]) # 1=hv ; 2=ei ; 3=load iono ; 4=ce

ncid = Dataset(src_dir+"/p3_000_"+date+"_t"+time+".nc")
var_nc = ncid.variables

nproc = var_nc["nproc"][:] # number of procs
nptot = var_nc["nptot"][:] # total number of particules of proc 0

# tbirth = var_nc["particule_tbirth"][:]
if orig0 !=5:
    orig = var_nc["particule_orig"][:]
    mask = np.where(orig == orig0, True, False)
else:
    orig = var_nc["particule_exc"][:]
    mask = np.where(orig > 0, True, False)
# mask = np.where(tbirth<50, True, False)

char = np.extract(mask, var_nc["particule_char"][:])
mass = np.extract(mask, var_nc["particule_mass"][:])
tbirth = np.extract(mask, var_nc["particule_tbirth"][:])
nptot += len(char)
pc = char
pm = mass
pbirth = tbirth
nptot = len(pm)
# pm = var_nc["particule_char"][:] # contains all values of vx of all procs
# vy = var_nc["particule_vy"][:] # contains all values of vx of all procs
# vz = var_nc["particule_vz"][:] # contains all values of vx of all procs
# nptot = var_nc["nptot"][:]

# values of V0
# vs = []
# vs.append(var_nc["vxs"][:]*va*0.001)
# vs.append(var_nc["vys"][:]*va*0.001)
# vs.append(var_nc["vzs"][:]*va*0.001)

print(nproc[0],nptot)
tot_orig=var_nc["nptot"][:][0]
for i in range(1,nproc[0]):
    if i < 10:
        ncid = Dataset(src_dir+"/p3_00"+str(i)+"_"+date+"_t"+time+".nc")
    elif i < 100:
        ncid = Dataset(src_dir+"/p3_0"+str(i)+"_"+date+"_t"+time+".nc")
    else:
        ncid = Dataset(src_dir+"/p3_"+str(i)+"_"+date+"_t"+time+".nc")
    var_nc = ncid.variables
    # nptot += var_nc["nptot"][:][0] # total number of particules of proc i
    
    # tbirth = var_nc["particule_tbirth"][:]
    if orig0 !=5:
        orig = var_nc["particule_orig"][:]
        exc = var_nc["particule_exc"][:]
        mask = np.where(orig == orig0, True, False)
        mask = np.where(exc > 0, False, mask)
    else:
        orig = var_nc["particule_exc"][:]
        mask = np.where(orig > 0, True, False)
    char = np.extract(mask, var_nc["particule_char"][:])
    mass = np.extract(mask, var_nc["particule_mass"][:])
    tbirth = np.extract(mask, var_nc["particule_tbirth"][:])
    nptot += len(char)
    pc = np.concatenate((pc,char),axis=None)
    pm = np.concatenate((pm,char),axis=None)
    pbirth = np.concatenate((pbirth,tbirth),axis=None)
    print("i=",i,nptot,len(char),char,var_nc["particule_orig"][:].min(),var_nc["particule_orig"][:].max())

    tot_orig+=var_nc["nptot"][:][0]

print("Nptot",tot_orig)

print("pm=",pm, "pm_min_max=",pm.min(), pm.max())
print("pc=",pm, "pc_min_max=",pc.min(), pc.max())
print("N=",len(pm),nptot)

if orig0 == 0:
    stro="jv"
elif orig0 == 1:
    stro="hv"
elif orig0==2:
    stro="ei"
elif orig0==3:
    stro="li"
elif orig0==4:
    stro="??"
elif orig0==5:
    stro="ce"
np.savetxt(src_dir+"/O2+_"+stro+"_weight_"+date+"_t"+time+".csv",pc,delimiter=',')
np.savetxt(src_dir+"/O2+_"+stro+"_mass_"+date+"_t"+time+".csv",pm,delimiter=',')
np.savetxt(src_dir+"/O2+_"+stro+"_tbirth_"+date+"_t"+time+".csv",pbirth,delimiter=',')




### particule char
bins=[0,0.5,1,1.5,2,2.5,3]
plt.hist(pc, bins = bins)#,density=True)#,weights=np.full(len(vx),np.sum(vx)))

# ymin, ymax = plt.ylim()

plt.ylabel('Number of particles')
plt.xlabel('Weight ')
plt.title("$O_{2("+stro+")}^+$ weight distribution "+date+" t="+time+"\nnb="+str(nptot))

plt.savefig(dest_dir+"/O2+_"+stro+"_weight_distribution_t"+time+".png")
plt.close('all')

### particule mass
plt.hist(pm, bins = bins)#,density=True)#,weights=np.full(len(vx),np.sum(vx)))

# ymin, ymax = plt.ylim()

plt.ylabel('Number of particles')
plt.xlabel('Mass ')
plt.title("$O_{2("+stro+")}^+$ mass distribution "+date+" t="+time+"\nnb="+str(nptot))

plt.savefig(dest_dir+"/O2+_"+stro+"_mass_distribution_t"+time+".png")
plt.close('all')

### particule tbirth
bins = 20
plt.hist(pbirth, bins = bins)#,density=True)#,weights=np.full(len(vx),np.sum(vx)))

# ymin, ymax = plt.ylim()

plt.ylabel('Number of particles')
plt.xlabel('Time of birth ')
plt.title("$O_{2("+stro+")}^+$ time of birth distribution "+date+" t="+time+"\nnb="+str(nptot))
plt.savefig(dest_dir+"/O2+_"+stro+"_tbirth_distribution_t"+time+".png")
plt.close('all')


