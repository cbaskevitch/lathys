import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

Kb   = 1.38e-23  ## Constante de Boltzmann en m^2 kg s^-2 K-1
e_Cb = 1.602e-19 ## Charge elementaire en C (A.s)

# altitude
h = np.linspace(0,600,600)

nO2  = np.zeros(len(h))
nO2p = np.zeros(len(h))

# O2+
n0 = 2500 #cm-3
H0 = 240   #km
H1 = 440   #km
n1 = n0*np.exp(-300/H0)*np.exp(300/H1) #cm-3

# nO2p = n0*np.exp(-h/H0)
for i in range(0,len(h)):
    if h[i] <= 300:
        nO2p[i]=n0*np.exp(-h[i]/H0)
    else:
        nO2p[i]=n1*np.exp(-h[i]/H1)

# O2
n0 = 2.5e7 #cm-3
H0 = 100   # km
nO2 = n0*np.exp(-h/H0)

Tr = 2.0*e_Cb/Kb # Kelvin

kO2_O2p = 2.59e-17 * nO2 * np.sqrt(Tr) *(1 - 0.073 * np.log10(Tr))**2 # m3.s-1

# print(nO2p)
nu_CX = kO2_O2p * nO2p*1e6

# print(kO2_O2p)
# print(nu_CX)

plt.plot(nu_CX,h,label="$\\nu_{CX}$ ($O_{2} + O_{2}^{+}$ Rubin, 2015)")
plt.ylim(0,600)
# plt.xlim(1e-1,1e3)
plt.xlabel("collision frequency [$s^{-1}$]")
plt.ylabel("altitude [km]")
plt.xscale("log")
plt.grid(True,'both','both',ls=":",color="lightgrey")
plt.legend(loc="upper right")
plt.savefig("CX_O2+_O2_Rubin2015.png")
