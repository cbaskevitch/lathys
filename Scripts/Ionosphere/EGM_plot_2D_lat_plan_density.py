from netCDF4 import Dataset
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.colors as mpcol
from matplotlib.patches import Polygon, Wedge, Arrow
from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
import math
import sys, os
from datetime import datetime

# if len(sys.argv) == 1:
# 	print "mettre en argument le fichier netcdf a traiter"
# 	sys.exit()
# elif len(sys.argv) > 4:
# 	print "Un seul fichier netdef"
# 	sys.exit()
# else:
# 	file_ori = sys.argv[1]
# 	# file_name1=sys.argv[2]
# 	# file_name2=sys.argv[3]
# 	if not os.path.exists(file_ori):# or not os.path.exists(file_name1):# or not os.path.exists(file_name2):
# 		print "fichier non existant"
# 		sys.exit()

file_ori = sys.argv[1]
spe=sys.argv[2]
nc = Dataset(file_ori)
var_nc = nc.variables
phi_upp = var_nc["phi_upp"][:]
phi_low = var_nc["phi_low"][:]
r_upp = var_nc["altitude_upp"][:]*1e-5 #km
r_low = var_nc["altitude_low"][:]*1e-5 #km
theta_upp = var_nc["theta_upp"][:]
theta_low = var_nc["theta_low"][:]
# weight = var_nc["Weight"][:]
# radius = var_nc['PLANETARY_RADIUS'][:]
subsol_long = var_nc["Subsol_Long"][0]
# species = var_nc['name_species'][:]
# accumul_time_steps = var_nc['Accumul_time_steps'][:]
# dt = var_nc['dt'][:]
Dn = var_nc["density_"+spe][:]
radius     = var_nc['planet_radius'][:]*1e-5 #km
nr = var_nc["npt_alt"][0]
ntheta = var_nc["npt_lat"][0]
nphi = var_nc["npt_lon"][0]


subsol_long = subsol_long * 180.0 / math.pi
# print subsol_long

subsol_low = subsol_long - 90.0
subsol_upp = subsol_long + 90.0
# specie = 4
lim = radius*2.
# if specie > 3:
#     lim = radius*2

# spe = ""
# i=0
# while species[specie][i] != " ":
#     spe = spe+species[specie][i]
#     i=i+1
# print "Start computing density of "+spe

phi_ori = [0, 180] # XZ
phi_ori = [270, 90] # YZ
# phi_ori = [230-180, 230] #math.pi/2.0										#equatorial angle (pi/2)
pos = {phi_ori[0] : -1, phi_ori[1] : 1}
print(pos)
# phi_ori2 = phi_ori-180

dphi = 5.0*math.pi/180.0									#interval of the equatorial angle for statistical mean, in rad
dtheta = 0*math.pi/180.0

# print "Start ploting figure"
fig, ax = plt.subplots(figsize=[8,6])
# fig.patch.set_facecolor('black')
# cmap = mpcol.LinearSegmentedColormap.from_list("", ["black","purple", "blue","cyan", "green", "yellow", "orange", "red"])
cmap = mpl.cm.get_cmap("jet")
if spe=="O2":
    # vmin_max = [-8.0, 8.0]#[2.0, 8.0]#16.0215]
    vmin_max = [0.5, 8.5]#[2.0, 8.0]#16.0215]
elif spe=="H2O":
    # vmin_max = [2.0, 16.0]
    vmin_max = [-4.0, 6.]
norm = mpcol.Normalize(vmin=vmin_max[0], vmax=vmin_max[1])#np.amax(dens_eq_plane))

for phi_deg in phi_ori:
    phi = phi_deg*math.pi/180.0

    pp=0
    while not(phi_low[pp] <= phi and phi < phi_upp[pp]):
        pp += 1
    phi1 = (phi_upp[pp]+phi_low[pp])/2 - dphi
    phi2 = (phi_upp[pp]+phi_low[pp])/2 + dphi
    if phi1 <= 0:
        phi1=0
    else:
        pp=0
        while not(phi_low[pp] <= phi1 and phi1 < phi_upp[pp]):
            pp += 1
        phi1=pp
    if phi2 > 2*math.pi:
        phi2 = nphi
    else:
        pp=0
        while not(phi_low[pp] <= phi2 and phi2 < phi_upp[pp]):
            pp += 1
        phi2=pp



    dens_eq_plane=[]
    for t in range(0, ntheta):
        sum_cells = np.zeros(shape=(nr), dtype=float)
        dens = np.zeros(shape=(nr), dtype=float)
        for p in range(phi1, phi2):
            if(phi_upp[p]>=(phi-dphi) and phi_low[p]<=(phi+dphi)):
                for r in range(0, nr):
                    dr = r_upp[r] - r_low[r]
                    dt = theta_upp[t] - theta_low[t]
                    dp = phi_upp[phi] - phi_low[phi]
                    Vcell = 0
                    if theta_low[t] == 0:
                        Vcell =  r_low[r]**2 * np.sin(theta_upp[t]) * dr * dt * dp
                    else:
                        Vcell = r_low[r]**2 * np.sin(theta_low[t]) * dr * dt * dp
                    # Vcell = 1.0/3.0 * (r_upp[r] ** 3 - r_low[r] ** 3) * (phi_upp[p] - phi_low[p])
                    # Vcell = Vcell * math.fabs(math.cos(theta_low[t] - math.cos(theta_upp[t])))
                    sum_cells[r] = sum_cells[r] + Vcell
                    dens[r] = dens[r] + Dn[p][t][r]*Vcell #weight[specie][p][t][r]/(accumul_time_steps*dt)
                    Phi = (phi_upp[p]+phi_low[p])/2.0								#interval of the equatorial angle for statistical mean, in rad
                    # phi1 = int((Phi - dphi)/val_phi_cells)
                    # phi2 = int((Phi + dphi)/val_phi_cells)
                    # for i in range(phi1, phi2):
                    # 	if (i < len(weight[specie])-1) and i > 0 and i != phi:
                    # 		Vcell = 1.0/3.0 * (r_upp[i][t][r] ** 3 - r_low[i][t][r] ** 3) * (phi_upp[i][t][r] - phi_low[i][t][r])
                    # 		Vcell = Vcell * math.fabs(math.cos(theta_low[i][t][r] - math.cos(theta_upp[i][t][r])))
                    # 		dens[r] = dens[r] + Vcell * weight[specie][i][t][r]/(Vcell*accumul_time_steps*dt)
                    # 		sum_cells[r] = sum_cells[r] + Vcell
                    # for i in range(r-1, r+1):
                    # 	if (i < len(weight[specie][0][0])-1) and i > 0 and i != r:
                    # 		Vcell1 = 1.0/3.0 * (r_upp[phi][t][i] ** 3 - r_low[phi][t][i] ** 3) * (phi_upp[phi][t][i] - phi_low[phi][t][i])
                    # 		Vcell1 = Vcell1 * math.fabs(math.cos(theta_low[phi][t][i] - math.cos(theta_upp[phi][t][i])))
                    # 		dens[r] = dens[r] + Vcell1 * weight[specie][phi][t][i]/(Vcell1*accumul_time_steps*dt)
                    # 		sum_cells[r] = sum_cells[r] + Vcell1
        #np.savetxt("dens.txt", cumul_dens, delimiter=" ")
        dens = np.divide(dens,sum_cells)
        dens_eq_plane.append(dens)
    dens_eq_plane = np.asarray(dens_eq_plane, dtype=float)
    dens_eq_plane = np.log10(dens_eq_plane)
    # print "End computing density"

    
    print(np.amax(dens_eq_plane))
    # print np.amin(dens_eq_plane)
    # vmin_max = [[0.5, 3.5], [0.0, np.amax(dens_eq_plane)], [3.5, np.amax(dens_eq_plane)], [0.0, np.amax(dens_eq_plane)], [0.0, np.amax(dens_eq_plane)], [0.0, np.amax(dens_eq_plane)]]

    # norm = mpcol.Normalize(vmin=vmin_max[specie][0], vmax=vmin_max[specie][1])

    # convertion en coordonnees cartesiennes
    # x = r * sin theta * cos phi
    # y = r * sin theta * sin phi
    # z = r * cos theta
    for r in range(0, nr):
        if(r_upp[r]<lim):
            for t in range(0, ntheta):
                #coordonees bas gauche
                x_bg = r_upp[r] * np.cos(theta_upp[t]+math.pi/2) * pos[phi_deg]#-1.0
                y_bg = r_upp[r] * np.sin(theta_upp[t]+math.pi/2) 
                #coordonees haut gauche
                x_hg = r_upp[r] * np.cos(theta_low[t]+math.pi/2) * pos[phi_deg]#-1.0
                y_hg = r_upp[r] * np.sin(theta_low[t]+math.pi/2) 
                #coordonees bas droit
                x_bd = r_low[r] * np.cos(theta_upp[t]+math.pi/2) * pos[phi_deg]#-1.0
                y_bd = r_low[r] * np.sin(theta_upp[t]+math.pi/2) 
                #coordonees haut droit
                x_hd = r_low[r] * np.cos(theta_low[t]+math.pi/2) * pos[phi_deg]#-1.0
                y_hd = r_low[r] * np.sin(theta_low[t]+math.pi/2) 

                pts = np.array([[x_bg,y_bg], [x_hg,y_hg], [x_hd,y_hd], [x_bd,y_bd]])
                color = cmap(norm(dens_eq_plane[t][r]))
                p = Polygon(pts, color=color, )
                ax = plt.gca()
                
                ax.add_patch(p)



#w = Wedge(0.0, radius, 0, 360, facecolor="black", edgecolor ="white")
#w = Wedge(0.0, radius, subsol_low, subsol_upp, color ="white")
#ax = plt.gca()
#ax.add_patch(w)
w = Wedge(0.0, radius, 0, 360, edgecolor ="white",facecolor="black")
ax = plt.gca()
ax.add_patch(w)

x = 0.0
y = 0.0
arrow = Arrow(x,y,0, radius,color="white")
ax.add_patch(arrow)
plt.text((x+radius/8), y+radius/2,"R",horizontalalignment='center',verticalalignment='center',color="white",fontsize=20)
# x = lim/2.0
# y = lim/2.0
# arrow = FancyArrowPatch(x,y,radius/2,0,color="white",width=5)
# ax.add_patch(arrow)

ax.set_xlim(0.0-lim,lim)
ax.set_ylim(0.0-lim,lim)

# i = ax.imshow(dens_eq_plane, cmap=cmap, vmin=vmin_max[0], vmax=vmin_max[1])
i = ax.imshow(dens_eq_plane, cmap="jet", vmin=vmin_max[0], vmax=vmin_max[1])
cb = fig.colorbar(i)
cb.set_label("log10 "+spe+"/cm3")#, color="white")
# cb.ax.yaxis.set_tick_params(color="white")
# cb.outline.set_edgecolor("white")

plt.setp(plt.getp(cb.ax.axes, 'yticklabels'))#, color="white")
# plt.axis("off")

# plt.show()

tmp = file_ori.split("/")
file = tmp[len(tmp)-1][:-3]
file.replace('density_', '')
fig_title = "long"+str(phi_ori[0])+"_"+str(phi_ori[1])+"_"+spe+"_"+file+".png"
dir_name=""#"plot/density_plot_lat_p230/"
plt.savefig(dir_name+fig_title,facecolor=fig.get_facecolor(), edgecolor='none')
# print "End ploting figure"
# print "Saved here : "+dir_name+fig_title

# print "end of script"
