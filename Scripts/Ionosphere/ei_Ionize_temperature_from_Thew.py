### Ce programme calcule la frequence d ionisation à partir 
### de la temperature du fichier Thew.

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

#constantes
Kb = 1.38e-23 ## Constante de Boltzmann en m^2 kg s^-2 K-1

src_dir = sys.argv[1]
dest_dir = sys.argv[2]
rundate = sys.argv[3]
diagtime = sys.argv[4]
ncfile = src_dir + "/Thew_" + rundate + "_t" + diagtime + '.nc'
ncid = Dataset(ncfile)
var_nc = ncid.variables

centr      = var_nc['s_centr'][:]
radius     = var_nc['r_planet'][:]
r_iono     = var_nc['r_iono'][:]
gs         = var_nc['gstep'][:]
Te = var_nc["Temperature"][:]
Dn = var_nc["Density"][:]
TeK = Te*1.602e-19/Kb
TeK = np.where(TeK <= 0, float('NaN'), TeK)

print("Te = ",Te)
print("TeK = ",TeK)

nc = [len(Te[0][0]), len(Te[0]), len(Te)]

coefs = [-352.134, 88.1318, -8.882784, 0.4082678, -0.0072026] # coefficients pour calculer la frequence d'ionisation.
                                                              # Déterminé grace au script O2+_ionisation_production

feq_iono = np.zeros((nc[2],nc[1],nc[0]))
feq_iono= coefs[0]
for i in range(1,len(coefs)):
    feq_iono += coefs[i]*np.power(np.log(TeK),i)
feq_iono=Dn*np.exp(feq_iono) #+ 2e-7 # 2e-7 correspond aux electrons suprathermiques : 2cc (Harris) et une temperature de 250eV donnant une frequence d'ionisation de 10^-7

# print(feq_iono)
# cpt=0
# for i in range(0,nc[0]):
#     for j in range(0,nc[1]):
#         for k in range(0,nc[2]):
#             r2 = ((i*gs[0]-centr[0])/radius)**2 + ((j*gs[1]-centr[1])/radius)**2+((k*gs[2]-centr[2])/radius)**2
#             # print(centr[0])
#             if r2 < (r_iono/radius)**2:
#                 #print("Te = ",Te[k][j][i]," freq = ",feq_iono[k][j][i])
#                 cpt+=1
#             else:
#                 feq_iono[k][j][i]=0
# print(cpt,nc[0]*nc[1]*nc[2])

# -- Creation of axis values centered on the planet ( normalized to planet radius)
X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)

# planet center in cell number (NB: cell number start at 1
icentr = int(np.fix(centr[0]/gs[0]))
jcentr = int(np.fix(centr[1]/gs[1]))
kcentr = int(np.fix(centr[2]/gs[2]))

feq_XY = np.zeros((nc[0],nc[1]))
feq_XY[:,:] = np.matrix.transpose(feq_iono[kcentr,:,:])

feq_XZ = np.zeros((nc[0],nc[2]))
feq_XZ[:,:] = np.matrix.transpose(feq_iono[:,jcentr,:])

feq_YZ = np.zeros((nc[1],nc[2]))
feq_YZ[:,:] = np.matrix.transpose(feq_iono[:,:,icentr])


# planet drawing
theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
xp = np.cos(theta)
yp = np.sin(theta)

fig_size = [[6,9.5],[6,7.5],[8,6],[10,6]] #differentes tailles de fenetres
figsize_Xnum = 1  #numero de la taille de la fenetre pour les plans XZ et XY
figsize_Ynum = 2  #numero de la taille de la fenetre pour les plans YZ
Xmin=X_XY[0][0]
Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1]
Ymin=Y_XY[0][0]
Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1]
Zmin=Z_XZ[0][0]
Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1]


min_val = 1e-7
max_val = 2e-6

fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XY, Y_XY, feq_XY, vmin=min_val, vmax=max_val, cmap="hot",shading='auto')
if radius != 1:
    ax.plot(xp,yp,c="white")
    ax.plot(xp*r_iono/radius,yp*r_iono/radius,c="pink")

fig.colorbar(c, ax=ax)
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Ymin,Ymax)

titre = "ei ionisation frequency time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"/ei_freq_ionisation_XY_Europa_"+rundate+"_t"+diagtime+".png")


fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XZ, Z_XZ, feq_XZ, vmin=min_val, vmax=max_val, cmap="hot",shading='auto')
if radius != 1:
    ax.plot(xp,yp,c="white")

fig.colorbar(c, ax=ax)
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Zmin,Zmax)

titre = "ei ionisation frequency time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"/ei_freq_ionisation_XZ_Europa_"+rundate+"_t"+diagtime+".png")

fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
c = ax.pcolor(Y_YZ, Z_YZ, feq_YZ, vmin=min_val, vmax=max_val, cmap="hot",shading='auto')
if radius != 1:
    ax.plot(xp,yp,c="white")

fig.colorbar(c, ax=ax)
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "ei ionisation frequency time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"/ei_freq_ionisation_YZ_Europa_"+rundate+"_t"+diagtime+".png")








# sys.exit()


Te_XY = np.zeros((nc[0],nc[1]))
Te_XY[:,:] = np.matrix.transpose(Te[kcentr,:,:])

Te_XZ = np.zeros((nc[0],nc[2]))
Te_XZ[:,:] = np.matrix.transpose(Te[:,jcentr,:])

Te_YZ = np.zeros((nc[1],nc[2]))
Te_YZ[:,:] = np.matrix.transpose(Te[:,:,icentr])

min_val = 0.0
max_val = 30

fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XY, Y_XY, Te_XY, vmin=min_val, vmax=max_val, cmap="hot",shading='auto')
if radius != 1:
    ax.plot(xp,yp,c="white")

fig.colorbar(c, ax=ax)
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Ymin,Ymax)

titre = "Temperature [eV] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Y [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"/temperature_XY_Europa_"+rundate+"_t"+diagtime+".png")

fig, ax = plt.subplots(figsize=fig_size[figsize_Xnum])
c = ax.pcolor(X_XZ, Z_XZ, Te_XZ, vmin=min_val, vmax=max_val, cmap="hot",shading='auto')
if radius != 1:
    ax.plot(xp,yp,c="white")

fig.colorbar(c, ax=ax)
ax.set_xlim(Xmin,Xmax)
ax.set_ylim(Zmin,Zmax)

titre = "Temperature [eV] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('X [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"/temperature_XZ_Europa_"+rundate+"_t"+diagtime+".png")

fig, ax = plt.subplots(figsize=fig_size[figsize_Ynum])
c = ax.pcolor(Y_YZ, Z_YZ, Te_YZ, vmin=min_val, vmax=max_val, cmap="hot",shading='auto')
if radius != 1:
    ax.plot(xp,yp,c="white")

fig.colorbar(c, ax=ax)
ax.set_xlim(Ymin,Ymax)
ax.set_ylim(Zmin,Zmax)

titre = "Temperature [eV] time: "+diagtime
plt.title(titre)#,'fontsize',12,'fontweight','b');
ax.set_xlabel('Y [R_E]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Z [R_E]')#,'fontsize',12,'fontweight','b');

plt.savefig(dest_dir+"/temperature_YZ_Europa_"+rundate+"_t"+diagtime+".png")
