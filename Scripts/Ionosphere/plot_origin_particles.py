

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import sys, os



src_dir = sys.argv[1]
dest_dir = sys.argv[2]
rundate = sys.argv[3]
diagtime = sys.argv[4]
ncfile = src_dir + "/p3_000_" + rundate + "_t" + diagtime + '.nc'
ncid = Dataset(ncfile)
var_nc = ncid.variables

centr      = var_nc['s_centr'][:]
radius     = var_nc['r_planet'][:]
r_iono     = var_nc['r_iono'][:]
gs         = var_nc['gstep'][:]
nptot      = var_nc['nptot'][:]
nproc      = var_nc['nproc'][:]

fig, ax = plt.subplots()
colormap = np.array(['r', 'g', 'b','black','pink','yellow'])

for i in range(0,nproc[0]):
    print(i)
    if i < 10:
        ncfile = src_dir + "/p3_00" + str(i) + "_" + rundate + "_t" + diagtime + '.nc'
    elif i < 100:
        ncfile = src_dir + "/p3_0" + str(i) + "_" + rundate + "_t" + diagtime + '.nc'
    else:
        ncfile = src_dir + "/p3_" + str(i) + "_" + rundate + "_t" + diagtime + '.nc'
    ncid = Dataset(ncfile)
    var_nc = ncid.variables
    px         = var_nc['particule_x'][:]/radius - centr[0]/radius
    py         = var_nc['particule_y'][:]/radius - centr[1]/radius
    pz         = var_nc['particule_z'][:]/radius - centr[2]/radius
    porig      = var_nc['particule_orig'][:]

    ax.scatter(px,py,color=colormap[porig])


# planet drawing
theta = np.divide(2.0*np.pi*np.arange(1,101, 1.0), 100.0)
xp = np.cos(theta)
yp = np.sin(theta)

ax.plot(xp,yp,c="white")

plt.savefig(dest_dir+"/test_plot_origin_particles_XY_"+rundate+"_t"+diagtime+".png")

