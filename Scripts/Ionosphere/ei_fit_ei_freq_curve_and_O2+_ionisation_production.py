### Ce programme calcule la production de O2+ par ionisation (Rubin 2015).
### Calcul le fit de la frequence d'ionisation par ei sous la forme exp(sum(ai * ln(Te)^i))
### 3 processus :
### photoionisation k1 : O2 + hmu -> O2+ + e-
### impact electronique k2 : O2 + e- -> O2+ + 2e-
### echange de charge k3 : O2 + O+ -> O2+ + O

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import math
import sys, os

Kb = 1.38e-23 ## Constante de Boltzmann en m^2 kg s^-2 K-1
Te = np.linspace(1,1500,10000)
Te = Te*1.602e-19/Kb#1.5e6  ## Electron temperature en K

###### Calcul des sections efficaces d'ionisation de Hwang et al 1996 ######
### Parametres pour O2 issu de l'article
U = np.array([79.73,90.92,59.89,71.84,84.88]) ## Kinetic energy in eV
B = np.array([46.19,29.82,19.64,19.79,12.07]) ## Electron binding energy in eV
N = np.array([2,2,4,2,2]) ## orbital occupation number

T = np.linspace(20, 1500,num=10000) ## High incident energy in eV
R = 13.61 ## eV
a0 = 0.5292 ## Angstrom

sigma = np.zeros(len(T))  ## section efficace en m^2
for i in range(0,len(U)):
    t = T/B[i]
    u = U[i]/B[i]
    S = 4e-16*math.pi * a0*a0 * N[i] * R*R / (B[i]*B[i])
    sigma += S / (t+u+1) * (np.log(t)/2 * (1-1/(t*t)) + 1 - 1/t - np.log(t)/(t+1))

###### Maxwellian energy distribution  ######
dE = (T[-1]-T[0])/len(T)
fiono = np.zeros(len(Te))
me = 9.109e-31 ## masse d'un electron en kg
for iTe in range(0,len(Te)):
    MED = 2*np.sqrt(T*1.6e-19/math.pi)*(1/(Kb*Te[iTe]))**(3/2) * np.exp(-T/(Kb*Te[iTe]/1.602e-19))

    sumMED = np.sum(MED)*dE*1.6e-19
    # print("sum MED", sumMED)


    ###### Calcul de la fréquence d'ionisation. Utilisation de la méthode des rectangles à gauche pour calculer l'integral #####
    for i in range(0,len(T)): # Pour chaque energie de T
        fiono[iTe] += MED[i]*sigma[i]*np.sqrt(2*T[i]*1.6e-19/me)*100*dE*1.6e-19

plt.plot(Te*Kb/1.602e-19,fiono,"r",label="$\\nu^{ei}$")
# plt.xlabel("Te [eV]")
# plt.ylabel("frequence s-1")
# plt.legend(loc="upper right")
# plt.title("nO2 = n0*exp(-r/H0) avec n0 = 2.5e7cm-3 et H0=100km (Harris)\n")
# plt.savefig("frequence_impact_e-_en_fct_Te_O2_Rubin2.png")
# plt.close("all")


####### Interpolation polynomiale de la courbe de la fréquence d'ionisation par impact électronique
####### Utilisation de Lagrange
from scipy.interpolate import lagrange,CubicSpline
from numpy.polynomial.polynomial import Polynomial
import scipy.optimize as optimization

# Te_poly = np.linspace(1,1000,100) ## temperatures utilisees pour calculer les coeficients à l'ordre 5 de l'interpolation polynomiale
# Te_poly = Te_poly*1.602e-19/Kb#1.5e6  ## Electron temperature en K
# fiono = np.zeros(len(Te_poly))
# for iTe in range(0,len(Te_poly)):
#     MED = 2*np.sqrt(T*1.6e-19/math.pi)*(1/(Kb*Te_poly[iTe]))**(3/2) * np.exp(-T/(Kb*Te_poly[iTe]/1.602e-19))

#     sumMED = np.sum(MED)*dE*1.6e-19
#     # print("sum MED", sumMED)


#     ###### Calcul de la fréquence d'ionisation. Utilisation de la méthode des rectangles à gauche pour calculer l'integral #####
#     for i in range(0,len(T)): # Pour chaque energie de T
#         fiono[iTe] += MED[i]*sigma[i]*np.sqrt(2*T[i]*1.6e-19/me)*100*dE*1.6e-19

def func(x,a0,a1,a2,a3,a4):
    return np.exp(a0 + a1*x + a2*x**2 + a3*x**3 + a4*x**4)

# print(optimization.curve_fit(func, Te_poly, fiono))
x0=np.full(5,0.0)
coefs=optimization.curve_fit(func, np.log(Te), fiono, x0)[0]
# poly = lagrange(Te_poly,fiono)
# poly = CubicSpline(Te_poly,fiono)
# print (poly)
# coefs=Polynomial(poly).coef
print("coef=",coefs," fin")
# Te2 = np.linspace(1,1500,100)
# Te2 = Te2*1.602e-19/Kb#1.5e6  ## Electron temperature en K
testpoly=np.zeros(len(Te))
# testpoly = poly(Te2)
# testpoly = poly(np.log(Te2))
# coefs=[-1233.29, 347.764, -37.4128, 1.79337,-0.0322777] #Valeur pour Mars ISSI 2008 meeting 2, input run A
coefs = [-352.134, 88.1318, -8.882784, 0.4082678, -0.0072026]
testpoly=coefs[0]

for i in range(1,len(coefs)):
    testpoly+=coefs[i]*np.power(np.log(Te),i)
# plt.plot(Te2*Kb/1.602e-19,testpoly,"b",label="test coefs")

# plt.plot(Te*Kb/1.602e-19,np.exp(testpoly),"b",label="Fit curve")


plt.xlabel("Te [eV]")
plt.ylabel("$\\nu^{ei} s^{-1}$")
# plt.legend(loc="upper right")
plt.grid(True)
plt.yscale('log')
plt.xscale('log')
# plt.gca().yaxis.get_ticklocs(minor=True)
# plt.gca().minorticks_on()
plt.gca().yaxis.set_major_locator(tck.LogLocator(base=10.0,numticks=12))
plt.gca().yaxis.set_minor_locator(tck.LogLocator(base=10.0,subs=(0.2,0.4,0.6,0.8),numticks=12))
plt.gca().yaxis.set_minor_formatter(tck.NullFormatter())
# plt.ylim(0,1e-8)
# plt.title("nO2 = n0*exp(-r/H0) avec n0 = 2.5e7cm-3 et H0=100km (Harris)\n")
plt.savefig("frequence_impact_e-_en_fct_Te_O2_Rubin.png")#_and_fit_curve.png")
plt.close("all")








sys.exit()



####### Resolution de l'equation k1*n(O2) + k2*n(O2)*ne - k3*ne*n(O2+) = dn(O2+)/dt = 0 (hyp à l'equilibre. Pour plus de détail, voir le document papier associé)
####### on pose x = ne = n(O2+)
####### k1*n(O2) + k2*n(O2)*x - k3*x^2 = 0
####### x(r) = (k2*n(O2)+sqrt(delta))/2k3 avec delta = (k2n(O2))^2 + 4k1n(O2)k3
####### (les valeurs de k1 et k2 viennent de Rubin 2015)

r = np.linspace(0,1000,num=10000)
n0 = 2.5e7 ### surface density of O2 in cm-3 (Harris 2021)
H0 = 100 ### en km echelle de hauteur (Harris 2021)
nO2 = np.zeros(len(r))
nO2 = n0*np.exp(-r/H0)  ### Eq 22 de Rubin 2015 sans le 2e terme de l'eq. Harris ne l'a pas pris
print("nO2=",nO2)
# n10 = 5.0e8 ### surface density of O2 in cm-3 (Rubin 2015)
# H10 = 20 ### en km echelle de hauteur (Rubin 2015)
# n11 = 5.0e4 ### surface density of O2 in cm-3 (Rubin)
# H11 = 500 ### en km echelle de hauteur (Rubin)
# nO21 = np.zeros(len(r))
# nO21 = n10*np.exp(-r/H10) + n11*np.exp(-r/H11)  ### Eq 22 de Rubin 2015 avec le 2e terme de l'eq.
plt.plot(r,nO2,"r",label="densite O2 avec param de Harris")
# plt.plot(r,np.log10(nO21),"b",label="densite O2 avec param de Rubin")
plt.xlabel("altitude [km]")
plt.ylabel("densite log[cm-3]")
plt.legend(loc="upper right")
# plt.title("nO2 = n0*exp(-r/H0) avec n0 = 2.5e7cm-3 et H0=100km (Harris)\n")
# plt.savefig("densite_O2_Rubin_vs_Harris.png")
plt.savefig("densite_O2_Harris.png")
plt.close("all")

#param de Harris
k1 = 1.7e-8#/(5.2*5.2) ## s-1 frequence de photoionisation
k2=fiono[0]    ## s-1 frequence d'ionisation par impact electronique
k3 = 2.4e-7 * (300/(Te[1]))**0.7  ## cm3.s-1 Shunk and Nagy 2009
print("k1 ",k1)
print("k2 ",k2)
print("k3 ",k3)
delta = np.zeros(len(r))
delta = (k2*nO2)*(k2*nO2) + 4*k1*nO2*k3
print("delta")
print(delta)
print("sqrt delta")
print(np.sqrt(delta))
print("ke*nO2")
print(k2*nO2*k2*nO2)

x = np.zeros(len(r)) ## densite nO2+
x = (k2*nO2 + np.sqrt(delta))/(2*k3)
plt.plot(r,x,"r",label="densite O2 avec param de Harris")

#param de Rubin
# delta1 = np.zeros(len(r))
# delta1 = (k2*nO21)*(k2*nO21) + 4*k1*nO21*k3
# x1 = np.zeros(len(r)) ## densite nO2+
# x1 = (k2*nO21 + np.sqrt(delta1))/(2*k3)
# plt.plot(r,np.log10(x1),"b",label="densite O2 avec param de Rubin")


# plt.xlabel("altitude [km]")
# plt.ylabel("densite O2+ log[cm-3]")
# plt.legend(loc="upper right")
# # plt.legend("nO2 = n0*exp(-r/H0) avec n0 = 2.5e7cm-3 et H0=100km (Harris)")
# plt.savefig("densite_O2+_Rubin_vs_Harris.png")
plt.savefig("densite_avec_param_perte_O2+_Harris.png")
# plt.close("all")



######### Comparaison k1n(O2) et k2n(O2)n(O2+)
k1nO2 = np.zeros(len(r))
k1nO2 = k1*nO2

k2nO2x = np.zeros(len(r))
k2nO2x = k2*nO2*x

plt.plot(r,k1nO2,"r",label="K1*nO2")
plt.plot(r,k2nO2x,"b",label="K2*nO2*nO2+")
plt.xlabel("altitude [km]")
plt.legend(loc="upper right")
plt.savefig("comparaison_k1nO2_k2nO2nO2+.png")
plt.close("all")
