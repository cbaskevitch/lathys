from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import sys, os


src_dir = sys.argv[1]
dest_dir = sys.argv[2]
date = sys.argv[3]
time = sys.argv[4]

ncid = Dataset(src_dir+"/p3_000_"+date+"_t"+time+".nc")
var_nc = ncid.variables

nproc = var_nc["nproc"][:] # number of procs
nptot = var_nc["nptot"][:] # total number of particules of proc 0

orig = var_nc["particule_orig"][:]
mask = np.where(orig == 3, True, False) # loading ionosphere
vx = np.extract(mask, var_nc["particule_vx"][:])
vy = np.extract(mask, var_nc["particule_vy"][:])
vz = np.extract(mask, var_nc["particule_vz"][:])

npli = 0 # nb of loading iono
npf = 0 # nb of frozen particles
npli += len(vx)

vxli = vx
vyli = vy
vzli = vz

mask = np.where(orig == 5, True, False) # loading ionosphere
vx = np.extract(mask, var_nc["particule_vx"][:])
vy = np.extract(mask, var_nc["particule_vy"][:])
vz = np.extract(mask, var_nc["particule_vz"][:])

npf += len(vx)
vxf = vx
vyf = vy
vzf = vz

for i in range(1,nproc[0]):
    if i < 10:
        ncid = Dataset(src_dir+"/p3_00"+str(i)+"_"+date+"_t"+time+".nc")
    elif i < 100:
        ncid = Dataset(src_dir+"/p3_0"+str(i)+"_"+date+"_t"+time+".nc")
    else:
        ncid = Dataset(src_dir+"/p3_"+str(i)+"_"+date+"_t"+time+".nc")
    var_nc = ncid.variables


    orig = var_nc["particule_orig"][:]
    mask = np.where(orig == 3, True, False) # loading ionosphere
    vx = np.extract(mask, var_nc["particule_vx"][:])
    vy = np.extract(mask, var_nc["particule_vy"][:])
    vz = np.extract(mask, var_nc["particule_vz"][:])

    npli += len(vx)
    vxli = np.concatenate((vxli,vx),axis=None)
    vyli = np.concatenate((vyli,vy),axis=None)
    vzli = np.concatenate((vzli,vz),axis=None)

    mask = np.where(orig == 5, True, False) # loading ionosphere
    vx = np.extract(mask, var_nc["particule_vx"][:])
    vy = np.extract(mask, var_nc["particule_vy"][:])
    vz = np.extract(mask, var_nc["particule_vz"][:])

    npf += len(vx)
    vxf = np.concatenate((vxf,vx),axis=None)
    vyf = np.concatenate((vyf,vy),axis=None)
    vzf = np.concatenate((vzf,vz),axis=None)

print("loading iono : ",npli," ; ","{:4.2f}".format(npli/(npli+npf)), "% ; <vx> = ",np.sum(vxli)/npli, " ; <vy> = ",np.sum(vyli)/npli, " ; <vz> = ",np.sum(vzli)/npli)
print("frozen iono  : ",npf," ; ","{:4.2f}".format(npf/(npli+npf)), "% ; <vx> = ",np.sum(vxf)/npf, " ; <vy> = ",np.sum(vyf)/npf, " ; <vz> = ",np.sum(vzf)/npf)
print("total parts  : ", npli+npf)

