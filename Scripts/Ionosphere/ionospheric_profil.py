from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

# X=np.linspace(0,600,10000)

# n0=2500 #cm-3
# H0=240   #km
# H1=440   #km
# n1=n0*np.exp(-300/H0)*np.exp(300/H1) #cm-3 n1=1416.39
# print(n1)
# nO2_Harris=np.zeros(len(X))
# # nO2_Harris = np.where(X <= 600, n1*np.exp(-X/H1), nO2_Harris)
# # nO2_Harris = np.where(X <= 300, n0*np.exp(-X/H0), nO2_Harris)

# for i in range(0,len(X)):
#     if X[i] <= 300:
#         nO2_Harris[i]=n0*np.exp(-X[i]/H0)
#     else:
#         nO2_Harris[i]=n1*np.exp(-X[i]/H1)

# # print(nO2_Harris)
# # print(X)

# plt.plot(X,nO2_Harris,c="r")
# # plt.legend(loc="upper right")
# plt.xlabel('x [$R_E$]')
# plt.ylabel('$n_{O_2^+}$ [$cm^{-3}$]')

# plt.savefig("ionospheric_profile_n0-2500_H0-240_H1-440.png")
# plt.close()



ncfile = sys.argv[1]
ncid = Dataset(ncfile)
var_nc = ncid.variables

centr      = var_nc['s_centr'][:]
radius     = var_nc['r_planet'][:]
r_iono     = var_nc['r_iono'][:]
gs         = var_nc['gstep'][:]
phys_length = var_nc['phys_length'][:]
nO2 = var_nc["Density"][:]
X_axis = var_nc["X_axis"][:]
Y_axis = var_nc["Y_axis"][:]
Z_axis = var_nc["Z_axis"][:]

# nO2 = var_nc["Den_O2p"][:]*var_nc['phys_density'][:]/1e6
nO2 = var_nc["ie_frequency"][:]
nc = [len(nO2[0][0]), len(nO2[0]), len(nO2)]
print(nc)
print(len(X_axis),len(Y_axis),len(Z_axis))

# icentr = int(np.fix(centr[0]/gs[0]))
jcentr1 = int(np.fix(centr[1]/gs[1]))
kcentr1 = int(np.fix(centr[2]/gs[2]))

jcentr = 0 #np.where(-1 < Y_axis and Y_axis < 1)
kcentr = 0 #np.where(-1 < Z_axis and Z_axis < 1)

for y in Y_axis:
    if -50 < y and y < 50:
        jcentr=np.where(Y_axis==y)[0][0]

for z in Z_axis:
    if -50 < z and z < 50:
        kcentr=np.where(Z_axis==z)[0][0]

print(jcentr1,kcentr1)
print(jcentr,kcentr)

print(Y_axis[jcentr],Z_axis[kcentr])

if jcentr==-1 or kcentr==-1:
    sys.exit()

# jcentr = 0
# kcentr = 0
# test = (jcentr*gs[1] - (centr[1]+0.5*gs[1]))/radius
# while not (-0.05 < test and test < 0.05):
#     jcentr += 1
#     test = (jcentr*gs[1] - (centr[1]+0.5*gs[1]))/radius
# test = (kcentr*gs[2] - (centr[2]+0.5*gs[2]))/radius
# while not (0.05 < test and test < 0.05):
#     kcentr += 1
#     test = (kcentr*gs[2] - (centr[2]+0.5*gs[2]))/radius
# kcentr += 1
# test = (kcentr*gs[2] - (centr[2]+0.5*gs[2]))/radius

# Y1 = np.arange(0,(nc[1])*gs[1],gs[1])
# Y1 = np.divide(Y1, radius) - np.divide((centr[1]+0.5*gs[1])*np.ones(nc[1]), radius)






nO2_X=nO2[kcentr,jcentr,:]
X=np.arange(0,nc[0]*gs[0],gs[0])/radius - centr[0]/radius
#print(X*radius*phys_length)

n0=2500 #cm-3
H0=240   #km
H1=440   #km
n1=n0*np.exp(-300/H0)*np.exp(300/H1) #cm-3

x_ = X[:]*phys_length*radius - radius*phys_length
x_ = np.where(x_ <= 0, 0, x_)
nO2_Harris=np.zeros(len(x_))
# nO2_ = np.where(x_ <= 600/radius, n1*np.exp(-x_/H1), nO2_)
# nO2_ = np.where(x_ <= 300/radius, n0*np.exp(-x_/H0), nO2_)

for i in range(0,len(x_)):
    if x_[i] <= 300:
        nO2_Harris[i]=n0*np.exp(-x_[i]/H0)
    else:
        nO2_Harris[i]=n1*np.exp(-x_[i]/H1)


plt.plot(X_axis[:],nO2_X[:],c="b",label="nO2+ simul")
plt.plot(X_axis[:],nO2_Harris,c="r",ls="--",label="nO2+ ref")
plt.legend(loc="upper right")
plt.xlabel("x [R_E]")
plt.ylabel("nO2+ [cm^-3]")
# plt.xlim(1.0,2.5)
plt.xlim(1500,3000)
plt.savefig("verif_nO2+.png")