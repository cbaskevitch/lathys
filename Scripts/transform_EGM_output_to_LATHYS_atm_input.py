from netCDF4 import Dataset
import numpy as np
import sys, os

kb = 1.380622e-16
amu = 1.660537e-24
mH  = 1.00794
mD  = 2.01410
mHe = 4.002602
mO  = 15.9994
mC  = 12.0107
mN  = 14.0067
mNa = 22.98977
mK  = 39.0983
mCa = 40.078
mAr40 = 39.948
mAr38 = 37.948
mAr36 = 35.948
mMg = 24.305
mAl = 26.981
mNe = 20.1797  

nc = Dataset(sys.argv[1])
var_nc = nc.variables
phi_upp = var_nc["phi_upp"][:]
phi_low = var_nc["phi_low"][:]
r_upp = var_nc["r_upp"][:]
r_low = var_nc["r_low"]
theta_upp = var_nc["theta_upp"][:]
theta_low = var_nc["theta_low"][:]
weight = var_nc["Weight"][:]
Ur = var_nc["Ur"][:]
Utheta = var_nc["Utheta"][:]
Uphi = var_nc["Uphi"][:]
U2 = var_nc["U2"][:]
radius = var_nc['PLANETARY_RADIUS'][:]
subsol_long = var_nc["Subsol_Long"][0]
species = var_nc['name_species'][:]
nb_time_steps = var_nc['nb_time_steps'][:]
accumul_time_steps = var_nc['Accumul_time_steps'][:]
dt = var_nc['dt'][:]

spe_str=[]
for s in species:
    if s.size != 0:
        res=""
        if type(s[0]) is np.bytes_:
            for a in s[0:4]:
                res += a.decode('UTF-8')
        spe_str.append(res)

density = np.zeros(np.shape(weight))
temp = np.zeros(np.shape(weight))
# print("len dens, weight",np.shape(density),np.shape(weight))
# print(np.shape(r_upp))

for ispe in range(0,len(weight)):
    # dV = r*r * sin(theta) * dr * dtheta * dphi
    dr = r_upp[:,:,:] - r_low[:,:,:]
    dtheta = theta_upp[:,:,:] - theta_low[:,:,:]
    dphi = phi_upp[:,:,:] - phi_low[:,:,:]
    Vcell = np.where(theta_low == 0, r_low[:,:,:]**2 * np.sin(theta_upp[:,:,:]) * dr * dtheta * dphi,r_low[:,:,:]**2 * np.sin(theta_low[:,:,:]) * dr * dtheta * dphi)
    # Vcell = r_low[:,:,:]**2 * np.sin(theta_low[:,:,:]) * dr * dtheta * dphi
    # Vcell = 1.0/3.0 * (r_upp[:][:][:] ** 3 - r_low[:][:][:] ** 3) * (phi_upp[:][:][:] - phi_low[:][:][:])
    # Vcell = Vcell * (np.cos(theta_low[:][:][:]) - np.cos(theta_upp[:][:][:]))
    print(Vcell[0,0,0])

    # density[ispe,:,:,:] = weight[ispe,:,:,:]/(Vcell * nb_time_steps * dt)
    density[ispe,:,:,:] = weight[ispe,:,:,:]/(Vcell * accumul_time_steps * dt)

    Ur[ispe,:,:,:] = np.where(weight[ispe,:,:,:] > 0, Ur[ispe,:,:,:]/weight[ispe,:,:,:], 0)
    Utheta[ispe,:,:,:] = np.where(weight[ispe,:,:,:] > 0, Utheta[ispe,:,:,:]/weight[ispe,:,:,:], 0)
    Uphi[ispe,:,:,:] = np.where(weight[ispe,:,:,:] > 0, Uphi[ispe,:,:,:]/weight[ispe,:,:,:], 0)
    U2[ispe,:,:,:] = np.where(weight[ispe,:,:,:] > 0, U2[ispe,:,:,:]/weight[ispe,:,:,:], 0)

    if spe_str[ispe].rstrip() == "H":
        mass0 = mH
    elif spe_str[ispe].rstrip() == "H2":
        mass0 = mH*2
    elif spe_str[ispe].rstrip() == "He":
        mass0 = mHe
    elif spe_str[ispe].rstrip() == "O":
        mass0 = mO
    elif spe_str[ispe].rstrip() == "O2":
        mass0 = mO*2
    elif spe_str[ispe].rstrip() == "OH":
        mass0 = mO+mH
    elif spe_str[ispe].rstrip() == "H2O":
        mass0 = 2*mH+mO
    elif spe_str[ispe].rstrip() == "Ca":
        mass0 = mCa
    elif spe_str[ispe].rstrip() == "Na":
        mass0 = mNa
    elif spe_str[ispe].rstrip() == "K":
        mass0 = mK
    elif spe_str[ispe].rstrip() == "Mg":
        mass0 = mMg
    elif spe_str[ispe].rstrip() == "Al":
        mass0 = mAl
    elif spe_str[ispe].rstrip() == "C":
        mass0 = mC
    elif spe_str[ispe].rstrip() == "N":
        mass0 = mN
    elif spe_str[ispe].rstrip() == "Ar":
        mass0 = mAr40
    elif spe_str[ispe].rstrip() == "Ar40":
        mass0 = mAr40
    elif spe_str[ispe].rstrip() == "Ar38":
        mass0 = mAr38
    elif spe_str[ispe].rstrip() == "Ar36":
        mass0 = mAr36
    elif spe_str[ispe].rstrip() == "Ne":
        mass0 = mNe
    elif spe_str[ispe].rstrip() == "D":
        mass0 = mD
    elif spe_str[ispe].rstrip() == "HD":
        mass0 = mH+mD
    elif spe_str[ispe].rstrip() == "Ojv":
        mass0 = mO
    elif spe_str[ispe].rstrip() == "Hjv":
        mass0 = mH
    else:
        print("species not recognised",spe_str[ispe])
        sys.exit()

    temp[ispe,:,:,:] = 1/(2.0*kb) * mass0 * amu * (U2[ispe,:,:,:] - np.sqrt(Ur[ispe,:,:,:]**2 + Utheta[ispe,:,:,:]**2 + Uphi[ispe,:,:,:]**2))
    # print(temp[ispe,:,:,:])

# print(species)
# print(density[5])

EGM_nc = Dataset("EGM_testVcell_"+sys.argv[1].split('/')[-1], "w", format="NETCDF4",diskless=True,persist=True)
# EGM_nc = Dataset("EGM_testacctimestep_"+sys.argv[1].split('/')[-1], "w", format="NETCDF4",diskless=True,persist=True)

input_dims = nc.dimensions
# alt = EGM_nc.createDimension("altitude",input_dims["nr"].size)
# lat = EGM_nc.createDimension("latitude",input_dims["ntheta"].size)
# lon = EGM_nc.createDimension("longitude",input_dims["nphi"].size)
# nspe = EGM_nc.createDimension("species",input_dims["nspecies"].size)
# comp= EGM_nc.createDimension("component",3)
# dim_str = EGM_nc.createDimension("dim_string",1)
# dim_str = EGM_nc.createDimension("dim_string",1)
# dim_str = EGM_nc.createDimension("dim_string",1)
# dim_str = EGM_nc.createDimension("dim_string",1)
        # component = 3 ;
        # species = 7 ;
        # dim_string = 1 ;
        # dim_int = 1 ;
        # dim_real = 1 ;
        # dim_char = 30 ;
        # Files = 5 ;
        # Length_Files = 140 ;
        # Length_Authors = 60 ;
        # Dim_Parameters = 14 ;

output_dims = []
for dims in input_dims.keys():
    if "local" in dims:
        continue
    output_dims.append(EGM_nc.createDimension(dims,input_dims[dims].size))



npt_alt = EGM_nc.createVariable("npt_alt","i4",("dim_scalar"))
npt_alt.title = "Nb of cells along radial"
npt_alt.units = ""
npt_alt[:] = input_dims["nr"].size
npt_lat = EGM_nc.createVariable("npt_lat","i4",("dim_scalar"))
npt_lat.title = "Nb of cells along latitude"
npt_lat.units = ""
npt_lat[:] = input_dims["ntheta"].size
npt_lon = EGM_nc.createVariable("npt_lon","i4",("dim_scalar"))
npt_lon.title = "Nb of cells along longitude"
npt_lon.units = ""
npt_lon[:] = input_dims["nphi"].size

alt_low = EGM_nc.createVariable("altitude_low","f8",("nr"))
alt_low.title = "Distance to planet center of the lower cell boundary"
alt_low.units = "cm"
alt_low[:] = r_low[0,0,:]
alt_upp = EGM_nc.createVariable("altitude_upp","f8",("nr"))
alt_upp.title = "Distance to planet center of the upper cell boundary"
alt_upp.units = "cm"
alt_upp[:] = r_upp[0,0,:]
alt_mid = EGM_nc.createVariable("altitude","f8",("nr"))
alt_mid.title = "Distance to planet center of the cell center"
alt_mid.units = "cm"
alt_mid[:] = (r_low[0,0,:]+r_upp[0,0,:])/2.0

lat_low = EGM_nc.createVariable("theta_low","f8",("ntheta"))
lat_low.title = "Polar Angle of the lower cell boundary"
lat_low.units = "rad"
lat_low[:] = theta_low[0,:,0]
lat_upp = EGM_nc.createVariable("theta_upp","f8",("ntheta"))
lat_upp.title = "Polar Angle of the upper cell boundary"
lat_upp.units = "rad"
lat_upp[:] = theta_upp[0,:,0]
lat_mid = EGM_nc.createVariable("latitude","f8",("ntheta"))
lat_mid.title = "Latitude of the cell center"
lat_mid.units = "rad"
lat_mid[:] = (theta_low[0,:,0]+theta_upp[0,:,0])/2.0

lon_low = EGM_nc.createVariable("phi_low","f8",("nphi"))
lon_low.title = "Azimuth angle of the lower cell boundary"
lon_low.units = "rad"
lon_low[:] = phi_low[:,0,0]
lon_upp = EGM_nc.createVariable("phi_upp","f8",("nphi"))
lon_upp.title = "Azimuth angle of the upper cell boundary"
lon_upp.units = "rad"
lon_upp[:] = phi_upp[:,0,0]
lon_mid = EGM_nc.createVariable("longitude","f8",("nphi"))
lon_mid.title = "Longitude of the cell center"
lon_mid.units = "rad"
lon_mid[:] = (phi_low[:,0,0]+phi_upp[:,0,0])/2.0

spe = EGM_nc.createVariable("species","str",("nspecies"))
spe.title = "Name of species"
spe.units = ""
for i in range(0,input_dims["nspecies"].size):
    spe[i] = spe_str[i]

for i in range(0,input_dims["nspecies"].size):
    spe = spe_str[i].rstrip()
    w = EGM_nc.createVariable("density_"+spe,"f8",("nphi","ntheta","nr"))
    w.title = "Cell density for species "+spe
    w.units = "cm-3"
    w[:,:,:] = density[i,:,:,:]

for i in range(0,input_dims["nspecies"].size):
    spe = spe_str[i].rstrip()
    w = EGM_nc.createVariable("Temperature_"+spe,"f8",("nphi","ntheta","nr"))
    w.title = "Cell Temperature for species "+spe
    w.units = "K"
    w[:,:,:] = temp[i,:,:,:]

for i in range(0,input_dims["nspecies"].size):
    spe = spe_str[i].rstrip()
    w = EGM_nc.createVariable("Ur_"+spe,"f8",("nphi","ntheta","nr"))
    w.title = "Cell radial velocity for species "+spe
    w.units = "cm.s-1"
    w[:,:,:] = Ur[i,:,:,:]
for i in range(0,input_dims["nspecies"].size):
    spe = spe_str[i].rstrip()
    w = EGM_nc.createVariable("Utheta_"+spe,"f8",("nphi","ntheta","nr"))
    w.title = "Cell latitudinal velocity for species "+spe
    w.units = "cm.s-1"
    w[:,:,:] = Utheta[i,:,:,:]
for i in range(0,input_dims["nspecies"].size):
    spe = spe_str[i].rstrip()
    w = EGM_nc.createVariable("Uphi_"+spe,"f8",("nphi","ntheta","nr"))
    w.title = "Cell longitudinal velocity for species "+spe
    w.units = "cm.s-1"
    w[:,:,:] = Uphi[i,:,:,:]


planet = EGM_nc.createVariable("planet_name","str",("dim_scalar"))
planet.title = "Name of planetary object"
planet.units = ""
planet[0] = "Europa"

r_planet = EGM_nc.createVariable("planet_radius","i4",("dim_scalar"))
r_planet.title = "Radius of the planet/satellite"
r_planet.units = ""
r_planet[0] = radius

coord_sys = EGM_nc.createVariable("coordinate_system","str",("dim_scalar"))
coord_sys.title = "Coordinate system of the simulation"
coord_sys.units = ""
res=""
for a in var_nc["COORDINATE_SYSTEM"][0,:]:
    res += a.decode('UTF-8')
coord_sys[0] = res

date = EGM_nc.createVariable("Date","str",("dim_scalar"))
date.title = "Date of the simulation EGM"
date.units = ""
date[0]="07_09_2022"

anglep = EGM_nc.createVariable("Anglep","f8",("dim_scalar"))
anglep.title = "TAA or Phase Angle"
anglep.units = "rad"
anglep[0] = var_nc["Anglep"][:]

subsol_long = EGM_nc.createVariable("Subsol_Long","f8",("dim_scalar"))
subsol_long.title = "Subsolar Longitude"
subsol_long.units = "rad"
subsol_long[0] = var_nc["Subsol_Long"][:]

rot_SLT = EGM_nc.createVariable("Rotation_SLT","f8",("dim_scalar"))
rot_SLT.title = "Rotation in Solar Local Time"
rot_SLT.units = "rad"
rot_SLT[0] = var_nc["Rotation_SLT"][:]