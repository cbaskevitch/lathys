######################
# Plot_magnetic_field_europa.py
# ---------------------
# This routine creates figures for articles and conf
# B field, Density and the flyby
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os
import astropy.io.votable as astrovot

# mission = "GALILEO_E4"
# mission = "JUICE_E1"
mission = "JUICE_E2"

# species = ["Ojv", "O2pl","O2ei","O2ce","O2ph", "H2Opl","H2Oei","H2Oce","H2Oph", "H2pl", "H2ce", "H2ei","H2ph"]
species = ["Ojv", "O2pl","O2ei","O2ce","O2ph", "H2Opl", "H2pl"]

date = sys.argv[1]
time = sys.argv[2]

var_nc = []
Ux = []
Ne = []

for spe in species:
    file = spe+"_"+date+"_t"+time+".nc"
    if os.path.isfile(file):
        var_nc.append(Dataset(file).variables)
        Ux.append(var_nc[-1]['Ux'][:])
        Ne.append(var_nc[-1]['Density'][:])
    else:
        print("File "+file+" doesn t exists")
        sys.exit()


ftraj = sys.argv[3]

# planetname = var_nc['planetname'][:]
centr      = var_nc[0]['s_centr'][:]
radius     = var_nc[0]['r_planet'][:]
gs         = var_nc[0]['gstep'][:]
# nptot      = var_nc['nptot'][:]
# nrm        = var_nc['phys_mag'][:]
nrm_len    = var_nc[0]['phys_length'][:]

nc = [len(Ne[0][0][0]), len(Ne[0][0]), len(Ne[0])]
print("nc = ",nc[0],nc[1],nc[2])

## get traj values
traj = np.genfromtxt(ftraj,delimiter=";",dtype=str)

x_traj = np.zeros(len(traj)-1)
y_traj = np.zeros(len(traj)-1)
z_traj = np.zeros(len(traj)-1)
x640_traj = 0
y640_traj = 0
z640_traj = 0
x700_traj = 0
y700_traj = 0
z700_traj = 0
for i in range(1,len(traj)):
    x_traj[i-1] = float(traj[i][1])
    y_traj[i-1] = float(traj[i][2])
    z_traj[i-1] = float(traj[i][3])

    if mission == "GALILEO_E4":
        if "06:40:00" in traj[i][0]:
            x640_traj = float(traj[i][1])
            y640_traj = float(traj[i][2])
            z640_traj = float(traj[i][3])
        if "07:00:00" in traj[i][0]:
            x700_traj = float(traj[i][1])
            y700_traj = float(traj[i][2])
            z700_traj = float(traj[i][3])
    elif mission == "JUICE_E1":
        if "16:10:00" in traj[i][0]:
            x640_traj = float(traj[i][1])
            y640_traj = float(traj[i][2])
            z640_traj = float(traj[i][3])
        if "16:30:00" in traj[i][0]:
            x700_traj = float(traj[i][1])
            y700_traj = float(traj[i][2])
            z700_traj = float(traj[i][3])
    elif mission == "JUICE_E2":
        if "22:10:00" in traj[i][0]:
            x640_traj = float(traj[i][1])
            y640_traj = float(traj[i][2])
            z640_traj = float(traj[i][3])
        if "22:30:00" in traj[i][0]:
            x700_traj = float(traj[i][1])
            y700_traj = float(traj[i][2])
            z700_traj = float(traj[i][3])

r = np.zeros(len(traj)-1)
for i in range(1,len(traj)):
    r[i-1] = np.sqrt(float(traj[i][1])**2 + float(traj[i][2])**2 + float(traj[i][3])**2)
    
time_CA = np.argmin(r)
# print(time_CA,np.min(r)*1560-1560,r[time_CA]*1560-1560)

x_CA = float(traj[time_CA+1][1])
y_CA = float(traj[time_CA+1][2])
z_CA = float(traj[time_CA+1][3])
    


# -- Creation of axis values centered on the planet ( normalized to planet radius)
X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)



# planet center in cell number (NB: cell number start at 0
icentr = int(np.fix(centr[0]/gs[0]))-1
jcentr = int(np.fix(centr[1]/gs[1]))-1
kcentr = int(np.fix(centr[2]/gs[2]))-1
iwake = int(icentr + np.fix(1.5*radius/gs[0]))

def get_XY_plan(cube,nc,k):
    C_XY        = np.zeros((nc[0],nc[1]))
    C_XY[:,:]   = np.matrix.transpose(cube[k,:,:])
    return C_XY

def get_XZ_plan(cube,nc,j):
    C_XZ        = np.zeros((nc[0],nc[2]))
    C_XZ[:,:]   = np.matrix.transpose(cube[:,j,:])
    return C_XZ

def get_YZ_plan(cube,nc,i):
    C_YZ        = np.zeros((nc[1],nc[2]))
    C_YZ[:,:]   = np.matrix.transpose(cube[:,:,i])
    return C_YZ


## Field lines
Xmin=X_XY[0][0]
Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1]
X_norm = np.linspace(0 , (nc[0]-1) * gs[0] , num=nc[0]) / radius - (centr[0]+0.5*gs[0])/radius
Ymin=Y_XY[0][0]
Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1]
Y_norm = np.linspace(0 , (nc[1]-1) * gs[1] , num=nc[1])/ radius - (centr[1]+0.5*gs[1])/radius
Zmin=Z_XZ[0][0]
Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1]
Z_norm = np.linspace(0 , (nc[2]-1) * gs[2] , num=nc[2])/ radius - (centr[2]+0.5*gs[2])/radius

fig_size = [[6,9.5],[6,7.5],[8,6],[10,6]] #differentes tailles de fenetres
figsize_Xnum = 2  #numero de la taille de la fenetre pour les plans XZ et XY
figsize_Ynum = 2  #numero de la taille de la fenetre pour les plans YZ


colmap = ["jet","Greens_r","viridis"]
ncol = 2  # numero de la colormap

lim_X = {"min":-4.0, "max":4}
lim_Y = {"min":-4.0, "max":4}
lim_Z = {"min":-4.0, "max":4}

if mission=="GALILEO_E4":
    lim_Ne={"min":0, "max":3}
    lim_U={"min":0.0, "max":200}

    txt_traj = ["06:40","07:00","CA"]
elif mission=="JUICE_E1":
    lim_Ne={"min":0, "max":3}
    lim_U={"min":0.0, "max":150}

    txt_traj = ["16:10","16:30","CA"]

elif mission=="JUICE_E2":
    lim_Ne={"min":0, "max":5}
    lim_U={"min":0.0, "max":150}

    txt_traj = ["22:10","22:30","CA"]

def plot_fig(x_axis,y_axis,plan,x_traj,y_traj,pts_traj,txt_traj,lim_plan,lim_x,lim_y,cmap,fig_name="",fig_title="",ax_labels={"x":"","y":""},fig_size=[6,9.5],is_streamplot=False,X_norm=None,Y_norm=None,StreamX=None,StreamY=None):
    # planet drawing
    theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
    xp = np.cos(theta)
    yp = np.sin(theta)

    fig, ax = plt.subplots(figsize=fig_size)
    c = ax.pcolor(x_axis, y_axis, plan, vmin=lim_plan["min"], vmax=lim_plan["max"], cmap=cmap,shading='auto')
    # c = ax.contourf(x_axis, y_axis, plan, vmin=lim_plan["min"], vmax=lim_plan["max"], cmap=cmap,shading='auto')
    
    axcb = fig.colorbar(c, ax=ax)
    axcb.set_label(fig_title,size="large")
    axcb.minorticks_on()
    axcb.ax.tick_params(labelsize="large")

    if is_streamplot:
        ax.streamplot(np.transpose(X_norm),Y_norm,np.transpose(StreamX),np.transpose(StreamY),color="white",density=4, arrowstyle='->', arrowsize=1.,linewidth=1)
    ax.plot(xp,yp,c="black")
    ax.fill(xp,yp,c="white")
    ax.plot(x_traj,y_traj,color="black")
    for pts,txt in zip(pts_traj,txt_traj):
        print(pts)
        ax.scatter(pts[0],pts[1],color="black",zorder=3)
        plt.text(pts[0]+0.15,pts[1]+0.15,txt,color="black",fontsize="large")
    
    ax.set_xlim(lim_x["min"],lim_x["max"])
    ax.set_ylim(lim_y["min"],lim_y["max"])

    ax.tick_params(axis='x', labelsize="large")
    ax.tick_params(axis='y', labelsize="large")
    # plt.title(fig_title)#,'fontsize',12,'fontweight','b');
    ax.set_xlabel(ax_labels["x"],fontsize='large')
    ax.set_ylabel(ax_labels["y"])

    plt.savefig(fig_name)

    plt.close('all')


for i in range(0,len(species)):
    Ux_XY        = get_XY_plan(Ux[i],nc,kcentr)
    Ux_XZ        = get_XZ_plan(Ux[i],nc,jcentr)
    Ux_YZ        = get_YZ_plan(Ux[i],nc,icentr)

    Dn_XY = get_XY_plan(Ne[i],nc,kcentr)
    Dn_XZ = get_XZ_plan(Ne[i],nc,jcentr)
    Dn_YZ = get_YZ_plan(Ne[i],nc,icentr)

    ## density
    # pts_traj = [[x640_traj,y640_traj],[x700_traj,y700_traj],[x_CA,y_CA]]
    # ax_labels = {"x":"X [$R_E$]", "y":"Y [$R_E$]"}
    # fig_title = "Density "+species[i]+" log[$cm^{-3}$]"# time: "+time
    # fig_name = "Dn_"+species[i]+"_XY_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
    # plot_fig(X_XY,Y_XY,np.log10(Dn_XY),x_traj,y_traj,pts_traj,txt_traj,lim_Ne,lim_X,lim_Y,colmap[ncol],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum])

    # pts_traj = [[x640_traj,z640_traj],[x700_traj,z700_traj],[x_CA,z_CA]]
    # ax_labels = {"x":"X [$R_E$]", "y":"Z [$R_E$]"}
    # fig_title = "Density "+species[i]+" log[$cm^{-3}$]"# time: "+time
    # fig_name = "Dn_"+species[i]+"_XZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
    # plot_fig(X_XZ,Z_XZ,np.log10(Dn_XZ),x_traj,z_traj,pts_traj,txt_traj,lim_Ne,lim_X,lim_Z,colmap[ncol],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum])

    # pts_traj = [[y640_traj, z640_traj],[y700_traj,z700_traj],[y_CA,z_CA]]
    # ax_labels = {"x":"Y [$R_E$]", "y":"Z [$R_E$]"}
    # fig_title = "Density "+species[i]+" log[$cm^{-3}$]"# time: "+time
    # fig_name = "Dn_"+species[i]+"_YZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
    # plot_fig(Y_YZ,Z_YZ,np.log10(Dn_YZ),y_traj,z_traj,pts_traj,txt_traj,lim_Ne,lim_Y,lim_Z,colmap[ncol],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum])




    ## Ux
    pts_traj = [[x640_traj,y640_traj],[x700_traj,y700_traj],[x_CA,y_CA]]
    ax_labels = {"x":"X [$R_E$]", "y":"Y [$R_E$]"}
    fig_title = "Ux "+species[i]+" [$km.s^{-1}$]"# time: "+time
    fig_name = "Ux_"+species[i]+"_XY_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
    plot_fig(X_XY,Y_XY,Ux_XY,x_traj,y_traj,pts_traj,txt_traj,lim_U,lim_X,lim_Y,colmap[ncol],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum])

    pts_traj = [[x640_traj,z640_traj],[x700_traj,z700_traj],[x_CA,z_CA]]
    ax_labels = {"x":"X [$R_E$]", "y":"Z [$R_E$]"}
    fig_title = "Ux "+species[i]+" [$km.s^{-1}$]"# time: "+time
    fig_name = "Ux_"+species[i]+"_XZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
    plot_fig(X_XZ,Z_XZ,Ux_XZ,x_traj,z_traj,pts_traj,txt_traj,lim_U,lim_X,lim_Z,colmap[ncol],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum])

    pts_traj = [[y640_traj, z640_traj],[y700_traj,z700_traj],[y_CA,z_CA]]
    ax_labels = {"x":"Y [$R_E$]", "y":"Z [$R_E$]"}
    fig_title = "Ux "+species[i]+" [$km.s^{-1}$]"# time: "+time
    fig_name = "Ux_"+species[i]+"_YZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
    plot_fig(Y_YZ,Z_YZ,Ux_YZ,y_traj,z_traj,pts_traj,txt_traj,lim_U,lim_Y,lim_Z,colmap[ncol],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum])