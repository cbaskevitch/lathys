import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import matplotlib.dates as md
import datetime
import astropy.io.votable as astrovot
import sys, os


### Get V values of Galileo flyby from AMDA VOTable ###
print(sys.argv[1])
GVxdata=np.genfromtxt(sys.argv[1],delimiter=";",dtype=str)
print(sys.argv[2])
GVydata=np.genfromtxt(sys.argv[2],delimiter=";",dtype=str)
print(sys.argv[3])
GVzdata=np.genfromtxt(sys.argv[3],delimiter=";",dtype=str)



### Get V values from xml file generated by lathys interpol_field ###
print(sys.argv[4])
votable = astrovot.parse(sys.argv[4])
Stable = votable.get_first_table()
Sdata=np.ma.getdata(Stable.array)

print(sys.argv[5])
votable = astrovot.parse(sys.argv[5])
SOjvtable = votable.get_first_table()
SOjvdata=np.ma.getdata(SOjvtable.array)

print(sys.argv[6])
votable = astrovot.parse(sys.argv[6])
SO2pltable = votable.get_first_table()
SO2pldata=np.ma.getdata(SO2pltable.array)


x   = []  #axe x -> temps lors du survol E4 de Galileo
GVx = []  # axe y -> Valeur de Vx mesurees par Galileo
GVy = []  # axe y -> Valeur de Vy mesurees par Galileo
GVz = []  # axe y -> Valeur de Vz mesurees par Galileo
SVx = []  # axe y -> Valeur de Vx dans la simulation
SVy = []  # axe y -> Valeur de Vy dans la simulation
SVz = []  # axe y -> Valeur de Vz dans la simulation
SOjv_Vx = []  # axe y -> Valeur de Vx dans la simulation
SOjv_Vy = []  # axe y -> Valeur de Vy dans la simulation
SOjv_Vz = []  # axe y -> Valeur de Vz dans la simulation
SO2pl_Vx = []  # axe y -> Valeur de Vx dans la simulation
SO2pl_Vy = []  # axe y -> Valeur de Vy dans la simulation
SO2pl_Vz = []  # axe y -> Valeur de Vz dans la simulation




x_Vx=[]
x_Vy=[]
x_Vz=[]
for i in range(0,len(GVxdata)):
    GVx.append(float(GVxdata[i][1]))
    GVy.append(float(GVydata[i][1]))
    GVz.append(float(GVzdata[i][1]))
    x_Vx.append(GVxdata[i][0][:-4])
    x_Vy.append(GVydata[i][0][:-4])
    x_Vz.append(GVzdata[i][0][:-4])
dates = [datetime.datetime.strptime(elem, '%Y-%m-%d %H:%M:%S') for elem in x_Vx]
datenums_GVx=md.date2num(dates)
dates = [datetime.datetime.strptime(elem, '%Y-%m-%d %H:%M:%S') for elem in x_Vy]
datenums_GVy=md.date2num(dates)
dates = [datetime.datetime.strptime(elem, '%Y-%m-%d %H:%M:%S') for elem in x_Vz]
datenums_GVz=md.date2num(dates)


for i in range (0,len(Sdata)):
    test = 0
    for t in range(0,10):
        if i+t < len(Sdata):
            test += Sdata[i+t][4] + Sdata[i+t][5] + Sdata[i+t][6]
    if test != 0:
        SVx.append(Sdata[i][4])
        SVy.append(Sdata[i][5])
        SVz.append(Sdata[i][6])

        SOjv_Vx.append(SOjvdata[i][4])
        SOjv_Vy.append(SOjvdata[i][5])
        SOjv_Vz.append(SOjvdata[i][6])

        SO2pl_Vx.append(SO2pldata[i][4])
        SO2pl_Vy.append(SO2pldata[i][5])
        SO2pl_Vz.append(SO2pldata[i][6])
    else:
        SVx.append(np.nan)
        SVy.append(np.nan)
        SVz.append(np.nan)
        SOjv_Vx.append(np.nan)
        SOjv_Vy.append(np.nan)
        SOjv_Vz.append(np.nan)
        SO2pl_Vx.append(np.nan)
        SO2pl_Vy.append(np.nan)
        SO2pl_Vz.append(np.nan)
    
    x.append(Sdata[i][0][:-4].replace("T"," "))


dates = [datetime.datetime.strptime(elem, '%Y-%m-%d %H:%M:%S') for elem in x]
datenums=md.date2num(dates)

r = np.zeros(len(Sdata))
Y = np.zeros(len(Sdata))
for i in range(0,len(Sdata)):
    r[i] = np.sqrt(Sdata[i][1]**2 + Sdata[i][2]**2 + Sdata[i][3]**2)
    Y[i] = Sdata[i][2]
time_CA = np.argmin(r)
# print(time_CA,np.min(r)*1560-1560,r[time_CA]*1560-1560)

id_Y_inf_1 = np.where(np.abs(Y)<1.0)

ylim = [-5.0,230.0]

plt.figure(figsize=(10, 14))
plt.xlim(datenums[0], datenums[-1])
xfmt = md.DateFormatter('%H:%M:%S')
plt.subplots_adjust(bottom=0.2)
plt.title("E4 flyby 1996/12/19",size=17)

### Vx
ax1 = plt.subplot(311)#, sharex=ax0)
ax1.xaxis.set_major_formatter(xfmt)
ax1.yaxis.set_major_locator(MultipleLocator(40))
ax1.yaxis.set_tick_params(labelsize=16)
plt.fill_between(datenums[id_Y_inf_1[0][0]:id_Y_inf_1[0][-1]],ylim[0],ylim[1],color="lightgrey")
plt.scatter(datenums_GVx,GVx,c="black",label="PLS",marker=".")
plt.plot(datenums,SVx,c="red",label="Mag. $O^+$ + Iono. $O_2^+$")
plt.plot(datenums,SOjv_Vx,c="blue",label="Mag. $O^+$")
plt.plot(datenums,SO2pl_Vx,c="green",label="Iono. $O_2^+$")
plt.vlines(datenums[time_CA],ylim[0],ylim[1],color="black")

plt.ylim(ylim[0],ylim[1])
plt.ylabel("Ux [$km.s^{-1}$]",size=17)
plt.setp(ax1.get_xticklabels(), visible=False)
ax1.legend(loc="upper right",fontsize=16)#,bbox_to_anchor=(1.12, 1.07))
plt.grid(True)

ylim = [-55.0,75.0]
ax2 = plt.subplot(312, sharex=ax1)
plt.fill_between(datenums[id_Y_inf_1[0][0]:id_Y_inf_1[0][-1]],ylim[0],ylim[1],color="lightgrey")
plt.scatter(datenums_GVy,GVy,c="black",label="PLS",marker=".")
plt.plot(datenums,SVy,c="red",label="Mag. $O^+$ + Iono. $O_2^+$")
plt.plot(datenums,SOjv_Vy,c="blue",label="Mag. $O^+$")
plt.plot(datenums,SO2pl_Vy,c="green",label="Iono. $O_2^+$")
plt.vlines(datenums[time_CA],ylim[0],ylim[1],color="black")

plt.ylim(ylim[0],ylim[1])
plt.ylabel("Uy [$km.s^{-1}$]",size=17)
plt.setp(ax2.get_xticklabels(), visible=False)
ax2.yaxis.set_tick_params(labelsize=16)
ax2.yaxis.set_major_locator(MultipleLocator(20))

plt.grid(True)

ylim = [-55.0,75.0]
ax3 = plt.subplot(313, sharex=ax1)
plt.fill_between(datenums[id_Y_inf_1[0][0]:id_Y_inf_1[0][-1]],ylim[0],ylim[1],color="lightgrey")
plt.scatter(datenums_GVz,GVz,c="black",label="PLS",marker=".")
plt.plot(datenums,SVz,c="red",label="Mag. $O^+$ + Iono. $O_2^+$")
plt.plot(datenums,SOjv_Vz,c="blue",label="Mag. $O^+$")
plt.plot(datenums,SO2pl_Vz,c="green",label="Iono. $O_2^+$")
plt.vlines(datenums[time_CA],ylim[0],ylim[1],color="black")

plt.ylim(ylim[0],ylim[1])
plt.ylabel("Uz [$km.s^{-1}$]",size=17)
plt.xlabel("Time [h:m:s]",size=17)
plt.setp(ax3.get_xticklabels(), fontsize=16)
ax3.xaxis.set_tick_params(rotation=30, labelsize=16)
ax3.yaxis.set_major_locator(MultipleLocator(20))
ax3.yaxis.set_tick_params(labelsize=16)
plt.grid(True)



plt.savefig("conf_2022_comparaison_U_Galileo_PLS_simul.png")