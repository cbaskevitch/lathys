import numpy as np
import sys, os

file = sys.argv[1]
file_name=sys.argv[2]

df=np.genfromtxt(file, delimiter=';',dtype=float)

out=[]
for i in range(0,len(df)):
    hh = int(df[i][0])
    mm = (df[i][0]-int(df[i][0]))*60
    ss = (mm - int(mm))*60
    mm = int(mm)
    out.append(["1996-12-19 %d:%d:%.3f"% (hh, mm,ss), df[i][1]])

np.savetxt(file_name+"_Galileo_E4_flyby_Harris2021_Digitalized_data.txt",out,delimiter=";",fmt="%s")
