import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import matplotlib.dates as md
import datetime
import astropy.io.votable as astrovot
import pandas as pd
import sys, os

radius = 1#1560 
phys_length = 1#202.9337
### Get traj xyz
ftraj_E1 = sys.argv[3]
print("traj file : ",ftraj_E1)
## get traj values
traj_E1 = np.genfromtxt(ftraj_E1,delimiter=";",dtype=str)
x_traj_E1 = np.zeros(len(traj_E1)-1)
y_traj_E1 = np.zeros(len(traj_E1)-1)
z_traj_E1 = np.zeros(len(traj_E1)-1)
for i in range(1,len(traj_E1)):
    x_traj_E1[i-1] = float(traj_E1[i][1])*radius/phys_length
    y_traj_E1[i-1] = float(traj_E1[i][2])*radius/phys_length
    z_traj_E1[i-1] = float(traj_E1[i][3])*radius/phys_length

ftraj_E2 = sys.argv[4]
print("traj file : ",ftraj_E2)
## get traj values
traj_E2 = np.genfromtxt(ftraj_E2,delimiter=";",dtype=str)
x_traj_E2 = np.zeros(len(traj_E2)-1)
y_traj_E2 = np.zeros(len(traj_E2)-1)
z_traj_E2 = np.zeros(len(traj_E2)-1)
for i in range(1,len(traj_E2)):
    x_traj_E2[i-1] = float(traj_E2[i][1])*radius/phys_length
    y_traj_E2[i-1] = float(traj_E2[i][2])*radius/phys_length
    z_traj_E2[i-1] = float(traj_E2[i][3])*radius/phys_length

# mission = "JUICE E1"
# mission = "JUICE E2"

labels = ["JUICE E1","JUICE E2"]
ls=["-","--"]

# if mission == "JUICE E1":
# labels_E1 = ["LatHyS JUICE E1 t=300","LatHyS JUICE E1 t=600"]
Bx0_E1 = -71.0
By0_E1 = -86.0
Bz0_E1 = -460.0
Bx0_ind_E1 = -Bx0_E1/2
By0_ind_E1 = -By0_E1/2
Bz0_ind_E1 = 0
# Gdata = np.loadtxt(sys.argv[1],delimiter=" ",skiprows=4)
Gdata_E1 = pd.read_csv(sys.argv[1],delimiter=" ",skiprows=4,header=None,names=("date","Bx","By","Bz","Btot"))
print(Gdata_E1)
# elif mission == "JUICE E2":
# labels_E2 = ["LatHyS JUICE E2 t=300","LatHyS JUICE E2 t=600"]
Bx0_E2 = 40.0
By0_E2 = 145.0
Bz0_E2 = -461.0
Bx0_ind_E2 = -Bx0_E2/2
By0_ind_E2 = -By0_E2/2
Bz0_ind_E2 = 0
# Gdata = np.loadtxt(sys.argv[1],delimiter=" ",skiprows=4)
Gdata_E2 = pd.read_csv(sys.argv[2],delimiter=" ",skiprows=4,header=None,names=("date","Bx","By","Bz","Btot"))
print(Gdata_E2)
# else:
#     print("Mission unknown")
#     sys.exit()

GBx_E1 = []  # axe y -> Valeur de Bx mesurees par Galileo
GBy_E1 = []  # axe y -> Valeur de By mesurees par Galileo
GBz_E1 = []  # axe y -> Valeur de Bz mesurees par Galileo


GBx_E2 = []  # axe y -> Valeur de Bx mesurees par Galileo
GBy_E2 = []  # axe y -> Valeur de By mesurees par Galileo
GBz_E2 = []  # axe y -> Valeur de Bz mesurees par Galileo


# Sden_plasma = []
# Sden_iono = []


x_E1=[]   # Recupere l indice de l'heure (dans les donnes de Galileo) ou commence les donnees de la simulation afin d'avoir la meme taille de tableau
for index, row in Gdata_E1.iterrows():
    GBx_E1.append(row["Bx"])
    GBy_E1.append(row["By"])
    GBz_E1.append(row["Bz"])

    x_E1.append(row["date"].replace("T"," "))#.replace(".000",""))

x_E2=[]   # Recupere l indice de l'heure (dans les donnes de Galileo) ou commence les donnees de la simulation afin d'avoir la meme taille de tableau
for index, row in Gdata_E2.iterrows():
    GBx_E2.append(row["Bx"])
    GBy_E2.append(row["By"])
    GBz_E2.append(row["Bz"])

    x_E2.append(row["date"].replace("T"," "))#.replace(".000",""))


dates = [datetime.datetime.strptime(elem, '%Y-%m-%d %H:%M:%S') for elem in x_E1]
Gdatenums_E1=md.date2num(dates)
dates = [datetime.datetime.strptime(elem, '%Y-%m-%d %H:%M:%S') for elem in x_E2]
Gdatenums_E2=md.date2num(dates)

# Calcul de la moyenne glissante
N = int(1*2) # attention, ça dépend de la frequence des points. ici 1s est faite en 2 points

GBx_moy_E1 = np.zeros(len(GBx_E1))  # axe y -> Valeurs moyennes de Bx mesurees par Galileo avec une fenetre glissante de 50s
GBy_moy_E1 = np.zeros(len(GBx_E1))  # axe y -> Valeurs moyennes de By mesurees par Galileo avec une fenetre glissante de 50s
GBz_moy_E1 = np.zeros(len(GBx_E1))  # axe y -> Valeurs moyennes de Bz mesurees par Galileo avec une fenetre glissante de 50s
for i in range(0,len(GBx_E1)):
    count = 0
    for k in range(0,int(N/2)):
        id = i-k
        if id > -1:
            GBx_moy_E1[i] += Gdata_E1.loc[id,"Bx"]
            GBy_moy_E1[i] += Gdata_E1.loc[id,"By"]
            GBz_moy_E1[i] += Gdata_E1.loc[id,"Bz"]
            count+=1
    for k in range(1,int(N/2)+1):
        id = i+k
        if id < len(Gdata_E1):
            GBx_moy_E1[i] += Gdata_E1.loc[id,"Bx"]
            GBy_moy_E1[i] += Gdata_E1.loc[id,"By"]
            GBz_moy_E1[i] += Gdata_E1.loc[id,"Bz"]
            count+=1
    GBx_moy_E1[i] /= count
    GBy_moy_E1[i] /= count
    GBz_moy_E1[i] /= count

GBx_moy_E2 = np.zeros(len(GBx_E2))  # axe y -> Valeurs moyennes de Bx mesurees par Galileo avec une fenetre glissante de 50s
GBy_moy_E2 = np.zeros(len(GBx_E2))  # axe y -> Valeurs moyennes de By mesurees par Galileo avec une fenetre glissante de 50s
GBz_moy_E2 = np.zeros(len(GBx_E2))  # axe y -> Valeurs moyennes de Bz mesurees par Galileo avec une fenetre glissante de 50s
for i in range(0,len(GBx_E2)):
    count = 0
    for k in range(0,int(N/2)):
        id = i-k
        if id > -1:
            GBx_moy_E2[i] += Gdata_E2.loc[id,"Bx"]
            GBy_moy_E2[i] += Gdata_E2.loc[id,"By"]
            GBz_moy_E2[i] += Gdata_E2.loc[id,"Bz"]
            count+=1
    for k in range(1,int(N/2)+1):
        id = i+k
        if id < len(Gdata_E2):
            GBx_moy_E2[i] += Gdata_E2.loc[id,"Bx"]
            GBy_moy_E2[i] += Gdata_E2.loc[id,"By"]
            GBz_moy_E2[i] += Gdata_E2.loc[id,"Bz"]
            count+=1
    GBx_moy_E2[i] /= count
    GBy_moy_E2[i] /= count
    GBz_moy_E2[i] /= count


id = N # attention, ça dépend de la frequence des points. ici 1s est faite en 2 points
x1_E1 = float(x_E1[id][11:13])*3600.0 + float(x_E1[id][14:16])*60.0 + float(x_E1[id][17:19])
x2_E1 = float(x_E1[-id][11:13])*3600.0 + float(x_E1[-id][14:16])*60.0 + float(x_E1[-id][17:19])
Bx_background_E1 = np.polyfit([x1_E1, x2_E1],[GBx_moy_E1[id], GBx_moy_E1[-id]],1)
By_background_E1 = np.polyfit([x1_E1, x2_E1],[GBy_moy_E1[id], GBy_moy_E1[-id]],1)
Bz_background_E1 = np.polyfit([x1_E1, x2_E1],[GBz_moy_E1[id], GBz_moy_E1[-id]],1)

x1_E2 = float(x_E2[id][11:13])*3600.0 + float(x_E2[id][14:16])*60.0 + float(x_E2[id][17:19])
x2_E2 = float(x_E2[-id][11:13])*3600.0 + float(x_E2[-id][14:16])*60.0 + float(x_E2[-id][17:19])
Bx_background_E2 = np.polyfit([x1_E2, x2_E2],[GBx_moy_E2[id], GBx_moy_E2[-id]],1)
By_background_E2 = np.polyfit([x1_E2, x2_E2],[GBy_moy_E2[id], GBy_moy_E2[-id]],1)
Bz_background_E2 = np.polyfit([x1_E2, x2_E2],[GBz_moy_E2[id], GBz_moy_E2[-id]],1)

Xx_E1 = []
Yx_E1 = []
Yy_E1 = []
Yz_E1 = []
for i in range(0,len(x_E1)):
    Xx_E1.append(float(x_E1[i][11:13])*3600.0 + float(x_E1[i][14:16])*60.0 + float(x_E1[i][17:19]))
    Yx_E1.append(Bx_background_E1[0]*Xx_E1[i] + Bx_background_E1[1])
    Yy_E1.append(By_background_E1[0]*Xx_E1[i] + By_background_E1[1])
    Yz_E1.append(Bz_background_E1[0]*Xx_E1[i] + Bz_background_E1[1])

Xx_E2 = []
Yx_E2 = []
Yy_E2 = []
Yz_E2 = []
for i in range(0,len(x_E2)):
    Xx_E2.append(float(x_E2[i][11:13])*3600.0 + float(x_E2[i][14:16])*60.0 + float(x_E2[i][17:19]))
    Yx_E2.append(Bx_background_E2[0]*Xx_E2[i] + Bx_background_E2[1])
    Yy_E2.append(By_background_E2[0]*Xx_E2[i] + By_background_E2[1])
    Yz_E2.append(Bz_background_E2[0]*Xx_E2[i] + Bz_background_E2[1])

##### Calcul du champ induit le long de la traj
GBx_ind_E1 = np.zeros(len(GBx_moy_E1))
GBy_ind_E1 = np.zeros(len(GBx_moy_E1))
GBz_ind_E1 = np.zeros(len(GBx_moy_E1))
GBx_ind_E1[:] = Yx_E1[:]
GBy_ind_E1[:] = Yy_E1[:]
GBz_ind_E1[:] = Yz_E1[:]

bme = np.sqrt(Bx0_ind_E1**2 + By0_ind_E1**2 + Bz0_ind_E1**2)
# Initialisation
# Dipolar Moment in planetary units (Spe%ref%mag) mu0/4pi*M
mom_dip_u1 = Bx0_ind_E1
mom_dip_u2 = By0_ind_E1
mom_dip_u3 = Bz0_ind_E1
mom_dip = [mom_dip_u1*(radius/phys_length)**3, mom_dip_u2*(radius/phys_length)**3, mom_dip_u3*(radius/phys_length)**3]

for i in range(0,len(x_traj_E1)):
    distance = np.sqrt(x_traj_E1[i]**2 + y_traj_E1[i]**2 + z_traj_E1[i]**2)
    b_dip_1 = (3*x_traj_E1[i]*(x_traj_E1[i]*mom_dip[0] + y_traj_E1[i]*mom_dip[1] + z_traj_E1[i]*mom_dip[2])/distance**2-mom_dip[0]) / distance**3
    b_dip_2 = (3*y_traj_E1[i]*(x_traj_E1[i]*mom_dip[0] + y_traj_E1[i]*mom_dip[1] + z_traj_E1[i]*mom_dip[2])/distance**2-mom_dip[1]) / distance**3
    b_dip_3 = (3*z_traj_E1[i]*(x_traj_E1[i]*mom_dip[0] + y_traj_E1[i]*mom_dip[1] + z_traj_E1[i]*mom_dip[2])/distance**2-mom_dip[2]) / distance**3
    GBx_ind_E1[i] += b_dip_1
    GBy_ind_E1[i] += b_dip_2
    GBz_ind_E1[i] += b_dip_3

GBtot_ind_E1 = np.sqrt(GBx_ind_E1**2 + GBy_ind_E1**2 + GBz_ind_E1**2)

GBx_ind_E2 = np.zeros(len(GBx_moy_E2))
GBy_ind_E2 = np.zeros(len(GBx_moy_E2))
GBz_ind_E2 = np.zeros(len(GBx_moy_E2))
GBx_ind_E2[:] = Yx_E2[:]
GBy_ind_E2[:] = Yy_E2[:]
GBz_ind_E2[:] = Yz_E2[:]

bme = np.sqrt(Bx0_ind_E2**2 + By0_ind_E2**2 + Bz0_ind_E2**2)
# Initialisation
# Dipolar Moment in planetary units (Spe%ref%mag) mu0/4pi*M
mom_dip_u1 = Bx0_ind_E2
mom_dip_u2 = By0_ind_E2
mom_dip_u3 = Bz0_ind_E2
mom_dip = [mom_dip_u1*(radius/phys_length)**3, mom_dip_u2*(radius/phys_length)**3, mom_dip_u3*(radius/phys_length)**3]

for i in range(0,len(x_traj_E2)):
    distance = np.sqrt(x_traj_E2[i]**2 + y_traj_E2[i]**2 + z_traj_E2[i]**2)
    b_dip_1 = (3*x_traj_E2[i]*(x_traj_E2[i]*mom_dip[0] + y_traj_E2[i]*mom_dip[1] + z_traj_E2[i]*mom_dip[2])/distance**2-mom_dip[0]) / distance**3
    b_dip_2 = (3*y_traj_E2[i]*(x_traj_E2[i]*mom_dip[0] + y_traj_E2[i]*mom_dip[1] + z_traj_E2[i]*mom_dip[2])/distance**2-mom_dip[1]) / distance**3
    b_dip_3 = (3*z_traj_E2[i]*(x_traj_E2[i]*mom_dip[0] + y_traj_E2[i]*mom_dip[1] + z_traj_E2[i]*mom_dip[2])/distance**2-mom_dip[2]) / distance**3
    GBx_ind_E2[i] += b_dip_1
    GBy_ind_E2[i] += b_dip_2
    GBz_ind_E2[i] += b_dip_3

GBtot_ind_E2 = np.sqrt(GBx_ind_E2**2 + GBy_ind_E2**2 + GBz_ind_E2**2)



fig = plt.figure(figsize=(10, 14))
xfmt = md.DateFormatter('%H:%M:%S')
plt.subplots_adjust(bottom=0.2)
plt.title("E4 flyby 1996/12/19",size=17)

### Bx
ax1 = plt.subplot(411)#, sharex=ax0)
plt.ylabel("Bx E1 [nT]",size=17)
plt.setp(ax1.get_xticklabels(), visible=False)
plt.grid(True)

ax12 = ax1.twinx()
plt.ylabel("Bx E2 [nT]",size=17)
plt.setp(ax12.get_xticklabels(), visible=False)
ax12.yaxis.set_tick_params(labelsize=16)
ax12.yaxis.set_major_locator(MultipleLocator(20))
plt.grid(True)

ax2 = plt.subplot(412, sharex=ax1)
plt.ylabel("By E1 [nT]",size=17)
plt.setp(ax2.get_xticklabels(), visible=False)
ax2.yaxis.set_tick_params(labelsize=16)
ax2.yaxis.set_major_locator(MultipleLocator(20))
plt.grid(True)

ax22 = ax2.twinx()
plt.ylabel("By E2 [nT]",size=17)
plt.setp(ax22.get_xticklabels())
ax22.yaxis.set_tick_params(labelsize=16)
ax22.yaxis.set_major_locator(MultipleLocator(20))
plt.grid(True)

ax3 = plt.subplot(413, sharex=ax1)
plt.ylabel("Bz E1 [nT]",size=17)
# plt.xlabel("Time [h:m:s]",size=17)
plt.setp(ax3.get_xticklabels(), visible=False)
# ax3.xaxis.set_tick_params(rotation=30, labelsize=16)
ax3.yaxis.set_major_locator(MultipleLocator(20))
ax3.yaxis.set_tick_params(labelsize=16)
plt.grid(True)

ax32 = ax3.twinx()
plt.ylabel("Bz E2 [nT]",size=17)
# plt.xlabel("Time [h:m:s]",size=17)
plt.setp(ax32.get_xticklabels(), visible=False)
ax32.yaxis.set_major_locator(MultipleLocator(20))
ax32.yaxis.set_tick_params(labelsize=16)
plt.grid(True)

ax4 = plt.subplot(414, sharex=ax1)
plt.ylabel("||B|| E1 [nT]",size=17)
plt.xlabel("Time [minutes from CA]",size=17)
plt.setp(ax4.get_xticklabels(), fontsize=16)
# ax4.xaxis.set_tick_params(rotation=30, labelsize=16)
ax4.yaxis.set_major_locator(MultipleLocator(20))
ax4.yaxis.set_tick_params(labelsize=16)
plt.grid(True)

ax42 = ax4.twinx()
plt.ylabel("||B|| E2 [nT]",size=17)
# plt.xlabel("Time [h:m:s]",size=17)
plt.setp(ax42.get_xticklabels(), fontsize=16)
ax42.yaxis.set_major_locator(MultipleLocator(20))
ax42.yaxis.set_tick_params(labelsize=16)
plt.grid(True)


### Get B field values from xml file generated by lathys interpol_field ###
for irun in range(0,2):
    print(sys.argv[5+irun])
    votable = astrovot.parse(sys.argv[5+irun])
    Stable = votable.get_first_table()
    S1data=np.ma.getdata(Stable.array)

    if irun == 0:
        Bx0 = Bx0_E1
        By0 = By0_E1
        Bz0 = Bz0_E1
        Bx_background = Bx_background_E1
        By_background = By_background_E1
        Bz_background = Bz_background_E1
    else:
        Bx0 = Bx0_E2
        By0 = By0_E2
        Bz0 = Bz0_E2
        Bx_background = Bx_background_E2
        By_background = By_background_E2
        Bz_background = Bz_background_E2


    S1Bx = []  # axe y -> Valeur de Bx dans la simulation
    S1By = []  # axe y -> Valeur de By dans la simulation
    S1Bz = []  # axe y -> Valeur de Bz dans la simulation
    S1Btot = []

    x1=[]
    x_time = []
    for i in range (0,len(S1data)):
        x1.append(S1data[i][0].replace("T"," ").replace(".0",""))
        if S1data[i][4] != 0 or S1data[i][5] != 0 or S1data[i][6] != 0:
            xtmp = float(x1[i][11:13])*3600.0 + float(x1[i][14:16])*60.0 + float(x1[i][17:19])
            x_time.append(xtmp)
            S1Bx.append(S1data[i][4] - Bx0 + Bx_background[0]*xtmp + Bx_background[1])
            S1By.append(S1data[i][5] - By0 + By_background[0]*xtmp + By_background[1])
            S1Bz.append(S1data[i][6] - Bz0 + Bz_background[0]*xtmp + Bz_background[1])
        else:
            S1Bx.append(np.nan)
            S1By.append(np.nan)
            S1Bz.append(np.nan)

        S1Btot.append(np.sqrt(S1Bx[i]*S1Bx[i] + S1By[i]*S1By[i] + S1Bz[i]*S1Bz[i]))


    r1 = np.zeros(len(S1data))
    Y1 = np.zeros(len(S1data))
    for i in range(0,len(S1data)):
        r1[i] = np.sqrt(S1data[i][1]**2 + S1data[i][2]**2 + S1data[i][3]**2)
        Y1[i] = S1data[i][2]
    time_CA = np.argmin(r1)


    id_Y1_inf_1 = np.where(np.abs(Y1)<1.0)

    # dates = [datetime.datetime.strptime(elem, '%Y-%m-%d %H:%M:%S') for elem in x1]
    # datenums1=md.date2num(dates)
    datenums1=np.array(x_time)
    datenums1 -= datenums1[time_CA]
    datenums1 /= 60
    # print(datenums1,datenums1[time_CA])

    if irun == 0 :
        ax1.plot(datenums1,S1Bx,c="red",label=labels[irun])
        ax1.plot(datenums1,GBx_ind_E1,c="red",ls="--",label="$B_J$ + induced JUICE E1")
        ax2.plot(datenums1,S1By,c="red",label=labels[irun])
        ax2.plot(datenums1,GBy_ind_E1,c="red",ls="--",label="$B_J$ + induced JUICE E1")
        ax3.plot(datenums1,S1Bz,c="red",label=labels[irun])
        # ax3.plot(datenums1,Yz_E1,c="green")
        ax3.plot(datenums1,GBz_ind_E1,c="red",ls="--",label="$B_J$ + induced JUICE E1")
        ax4.plot(datenums1,S1Btot,c="red",label=labels[irun])
        ax4.plot(datenums1,GBtot_ind_E1,c="red",ls="--",label="$B_J$ + induced JUICE E1")
    else:
        ax12.plot(datenums1,S1Bx,c="black",label=labels[irun])
        ax12.plot(datenums1,GBx_ind_E2,c="black",ls="--",label="$B_J$ + induced JUICE E2")
        ax22.plot(datenums1,S1By,c="black",label=labels[irun])
        ax22.plot(datenums1,GBy_ind_E2,c="black",ls="--",label="$B_J$ + induced JUICE E2")
        ax32.plot(datenums1,S1Bz,c="black",label=labels[irun])
        ax32.plot(datenums1,GBz_ind_E2,c="black",ls="--",label="$B_J$ + induced JUICE E2")
        ax42.plot(datenums1,S1Btot,c="black",label=labels[irun])
        ax42.plot(datenums1,GBtot_ind_E2,c="black",ls="--",label="$B_J$ + induced JUICE E2")
    ax1.vlines(datenums1[time_CA],-170.0,-50.0,color="black")
    ax2.vlines(datenums1[time_CA],-140.0,10.0,color="black")
    ax3.vlines(datenums1[time_CA],-550.0,-350.0,color="black")
    ax4.vlines(datenums1[time_CA],360.0,580.0,color="black")

# ax1.xaxis.set_major_formatter(xfmt)
ax1.yaxis.set_major_locator(MultipleLocator(20))
ax1.yaxis.set_tick_params(labelsize=16)


ax1.set_ylim(-170,-50.0)
ax12.set_ylim(-20.0,100.0)
# ax1.legend(loc="lower right",fontsize=16)#,bbox_to_anchor=(1.12, 1.07))
ax2.set_ylim(-140.0,10.0)
ax22.set_ylim(50.0,200.0)
ax3.set_ylim(-550.0,-350.0)
ax32.set_ylim(-550.0,-350.0)
ax4.set_ylim(360.0,580.0)
ax42.set_ylim(360.0,580.0)

ax2.legend(loc='upper center', bbox_to_anchor=(0.5, 1.21),
          ncol=2, fancybox=True, shadow=True,fontsize=16)
ax32.legend(loc='upper center', bbox_to_anchor=(0.5, 1.21),
          ncol=2, fancybox=True, shadow=True,fontsize=16)

# plt.xlim(Gdatenums[0], Gdatenums[-1])

fig.tight_layout(rect=[0, 0, 1, 1])

plt.savefig("test_conf_2023_comparaison_B_field_JUICE_sim.png")