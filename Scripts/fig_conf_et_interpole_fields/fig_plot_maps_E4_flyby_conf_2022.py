######################
# Plot_magnetic_field_europa.py
# ---------------------
# This routine creates figures for articles and conf
# B field, Density and the flyby
#
# C. Baskevitch
# UVSQ-LATMOS
# claire.baskevitch@latmos.ipsl.fr
# November 2020
######################

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os
import astropy.io.votable as astrovot

# mission = "GALILEO_E4"
# mission = "JUICE_E1"
mission = "JUICE_E2"

ncidB = Dataset(sys.argv[1])
var_ncB = ncidB.variables

ncidU = Dataset(sys.argv[2])
var_ncU = ncidU.variables

ncidNe = Dataset(sys.argv[2])
var_ncNe = ncidNe.variables

ncidE = Dataset(sys.argv[3])
var_ncE = ncidE.variables

ftraj = sys.argv[4]

date = sys.argv[5]
time = sys.argv[6]



# planetname = var_nc['planetname'][:]
centr      = var_ncB['s_centr'][:]
radius     = var_ncB['r_planet'][:]
gs         = var_ncB['gstep'][:]
# nptot      = var_nc['nptot'][:]
Bx         = var_ncB['Bx'][:]
By         = var_ncB['By'][:]
Bz         = var_ncB['Bz'][:]

Jx         = var_ncB['Jx'][:]
Jy         = var_ncB['Jy'][:]
Jz         = var_ncB['Jz'][:]
Jtot = np.sqrt(Jx*Jx + Jy*Jy + Jz*Jz)

Ex         = var_ncE['Ex'][:]
Ey         = var_ncE['Ey'][:]
Ez         = var_ncE['Ez'][:]

Ux         = var_ncU['Ux'][:]
Uy         = var_ncU['Uy'][:]
Uz         = var_ncU['Uz'][:]
Ne         = var_ncNe['Density'][:]
# nrm        = var_nc['phys_mag'][:]
nrm_len    = var_ncB['phys_length'][:]

nc = [len(Bx[0][0]), len(Bx[0]), len(Bx)]
print("nc = ",nc[0],nc[1],nc[2])

## get traj values
traj = np.genfromtxt(ftraj,delimiter=";",dtype=str)

x_traj = np.zeros(len(traj)-1)
y_traj = np.zeros(len(traj)-1)
z_traj = np.zeros(len(traj)-1)
x640_traj = 0
y640_traj = 0
z640_traj = 0
x700_traj = 0
y700_traj = 0
z700_traj = 0
for i in range(1,len(traj)):
    x_traj[i-1] = float(traj[i][1])
    y_traj[i-1] = float(traj[i][2])
    z_traj[i-1] = float(traj[i][3])

    if mission == "GALILEO_E4":
        if "06:40:00" in traj[i][0]:
            x640_traj = float(traj[i][1])
            y640_traj = float(traj[i][2])
            z640_traj = float(traj[i][3])
        if "07:00:00" in traj[i][0]:
            x700_traj = float(traj[i][1])
            y700_traj = float(traj[i][2])
            z700_traj = float(traj[i][3])
    elif mission == "JUICE_E1":
        if "16:10:00" in traj[i][0]:
            x640_traj = float(traj[i][1])
            y640_traj = float(traj[i][2])
            z640_traj = float(traj[i][3])
        if "16:30:00" in traj[i][0]:
            x700_traj = float(traj[i][1])
            y700_traj = float(traj[i][2])
            z700_traj = float(traj[i][3])
    elif mission == "JUICE_E2":
        if "22:10:00" in traj[i][0]:
            x640_traj = float(traj[i][1])
            y640_traj = float(traj[i][2])
            z640_traj = float(traj[i][3])
        if "22:30:00" in traj[i][0]:
            x700_traj = float(traj[i][1])
            y700_traj = float(traj[i][2])
            z700_traj = float(traj[i][3])

r = np.zeros(len(traj)-1)
for i in range(1,len(traj)):
    r[i-1] = np.sqrt(float(traj[i][1])**2 + float(traj[i][2])**2 + float(traj[i][3])**2)
    
time_CA = np.argmin(r)
# print(time_CA,np.min(r)*1560-1560,r[time_CA]*1560-1560)

x_CA = float(traj[time_CA+1][1])
y_CA = float(traj[time_CA+1][2])
z_CA = float(traj[time_CA+1][3])
    

# -- Creation of axis values centered on the planet ( normalized to planet radius)
X_XY, Y_XY = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[1])*gs[1],gs[1]))
X_XY = np.divide(np.matrix.transpose(X_XY), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[1])), radius)
Y_XY = np.divide(np.matrix.transpose(Y_XY), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[0],nc[1])), radius)

X_XZ, Z_XZ = np.meshgrid(np.arange(0,(nc[0])*gs[0],gs[0]),np.arange(0,(nc[2])*gs[2],gs[2]))
X_XZ = np.divide(np.matrix.transpose(X_XZ), radius) - np.divide((centr[0]+0.5*gs[0])*np.ones((nc[0],nc[2])), radius)
Z_XZ = np.divide(np.matrix.transpose(Z_XZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[0],nc[2])), radius)

Y_YZ, Z_YZ = np.meshgrid(np.arange(0,(nc[1])*gs[1],gs[1]),np.arange(0,(nc[2])*gs[2],gs[2]))
Y_YZ = np.divide(np.matrix.transpose(Y_YZ), radius) - np.divide((centr[1]+0.5*gs[1])*np.ones((nc[1],nc[2])), radius)
Z_YZ = np.divide(np.matrix.transpose(Z_YZ), radius) - np.divide((centr[2]+0.5*gs[2])*np.ones((nc[1],nc[2])), radius)



# planet center in cell number (NB: cell number start at 0
icentr = int(np.fix(centr[0]/gs[0]))-1
jcentr = int(np.fix(centr[1]/gs[1]))-1
kcentr = int(np.fix(centr[2]/gs[2]))-1
iwake = int(icentr + np.fix(1.5*radius/gs[0]))

def get_XY_plan(cube,nc,k):
    C_XY        = np.zeros((nc[0],nc[1]))
    C_XY[:,:]   = np.matrix.transpose(cube[k,:,:])
    return C_XY

def get_XZ_plan(cube,nc,j):
    C_XZ        = np.zeros((nc[0],nc[2]))
    C_XZ[:,:]   = np.matrix.transpose(cube[:,j,:])
    return C_XZ

def get_YZ_plan(cube,nc,i):
    C_YZ        = np.zeros((nc[1],nc[2]))
    C_YZ[:,:]   = np.matrix.transpose(cube[:,:,i])
    return C_YZ

Bx_XY = get_XY_plan(Bx,nc,kcentr)
By_XY = get_XY_plan(By,nc,kcentr)
Bz_XY = get_XY_plan(Bz,nc,kcentr)

Bx_XZ = get_XZ_plan(Bx,nc,jcentr)
By_XZ = get_XZ_plan(By,nc,jcentr)
Bz_XZ = get_XZ_plan(Bz,nc,jcentr)

Bx_YZ_term        = get_YZ_plan(Bx,nc,icentr)
By_YZ_term        = get_YZ_plan(By,nc,icentr)
Bz_YZ_term        = get_YZ_plan(Bz,nc,icentr)

Jx_XY = get_XY_plan(Jx,nc,kcentr)
Jy_XY = get_XY_plan(Jy,nc,kcentr)
Jz_XY = get_XY_plan(Jz,nc,kcentr)
Jtot_XY = get_XY_plan(Jtot,nc,kcentr)

Jx_XZ = get_XZ_plan(Jx,nc,jcentr)
Jy_XZ = get_XZ_plan(Jy,nc,jcentr)
Jz_XZ = get_XZ_plan(Jz,nc,jcentr)
Jtot_XZ = get_XZ_plan(Jtot,nc,jcentr)

Jx_YZ_term        = get_YZ_plan(Jx,nc,icentr)
Jy_YZ_term        = get_YZ_plan(Jy,nc,icentr)
Jz_YZ_term        = get_YZ_plan(Jz,nc,icentr)
Jtot_YZ_term        = get_YZ_plan(Jtot,nc,icentr)

Ex_XY = get_XY_plan(Ex,nc,kcentr)
Ey_XY = get_XY_plan(Ey,nc,kcentr)
Ez_XY = get_XY_plan(Ez,nc,kcentr)

Ex_XZ = get_XZ_plan(Ex,nc,jcentr)
Ey_XZ = get_XZ_plan(Ey,nc,jcentr)
Ez_XZ = get_XZ_plan(Ez,nc,jcentr)

Ex_YZ_term        = get_YZ_plan(Ex,nc,icentr)
Ey_YZ_term        = get_YZ_plan(Ey,nc,icentr)
Ez_YZ_term        = get_YZ_plan(Ez,nc,icentr)

Ux_XY        = get_XY_plan(Ux,nc,kcentr)
Ux_XZ        = get_XZ_plan(Ux,nc,jcentr)
Ux_YZ        = get_YZ_plan(Ux,nc,icentr)

Uy_XY        = get_XY_plan(Uy,nc,kcentr)
Uy_XZ        = get_XZ_plan(Uy,nc,jcentr)
Uy_YZ        = get_YZ_plan(Uy,nc,icentr)

Uz_XY        = get_XY_plan(Uz,nc,kcentr)
Uz_XZ        = get_XZ_plan(Uz,nc,jcentr)
Uz_YZ        = get_YZ_plan(Uz,nc,icentr)

Dn_XY = get_XY_plan(Ne,nc,kcentr)
Dn_XZ = get_XZ_plan(Ne,nc,jcentr)
Dn_YZ = get_YZ_plan(Ne,nc,icentr)


## Field lines
Xmin=X_XY[0][0]
Xmax=X_XY[len(X_XY)-1][len(X_XY[0])-1]
X_norm = np.linspace(0 , (nc[0]-1) * gs[0] , num=nc[0]) / radius - (centr[0]+0.5*gs[0])/radius
Ymin=Y_XY[0][0]
Ymax=Y_XY[len(Y_XY)-1][len(Y_XY[0])-1]
Y_norm = np.linspace(0 , (nc[1]-1) * gs[1] , num=nc[1])/ radius - (centr[1]+0.5*gs[1])/radius
Zmin=Z_XZ[0][0]
Zmax=Z_XZ[len(Z_XZ)-1][len(Z_XZ[0])-1]
Z_norm = np.linspace(0 , (nc[2]-1) * gs[2] , num=nc[2])/ radius - (centr[2]+0.5*gs[2])/radius

fig_size = [[6,9.5],[6,7.5],[8,6],[10,6]] #differentes tailles de fenetres
figsize_Xnum = 2  #numero de la taille de la fenetre pour les plans XZ et XY
figsize_Ynum = 2  #numero de la taille de la fenetre pour les plans YZ


colmap = ["jet","seismic","viridis"]
ncol = 2  # numero de la colormap
ncolB=1
ncolJtot=0

lim_X = {"min":-4.0, "max":4}
lim_Y = {"min":-4.0, "max":4}
lim_Z = {"min":-4.0, "max":4}

if mission=="GALILEO_E4":
    # lim_Bx={"min":0.0, "max":150}
    # lim_By={"min":-250.0, "max":-50}
    # lim_Bz={"min":-480.0, "max":-300}
    lim_Bx={"min":-60.0, "max":220}
    lim_By={"min":-280.0, "max":0}
    lim_Bz={"min":-510.0, "max":-290}

    lim_Jx={"min":-30.0, "max":30}
    lim_Jy={"min":-30.0, "max":30}
    lim_Jz={"min":-30.0, "max":30}
    lim_Jtot={"min":0.0, "max":40}

    lim_Ex={"min":-100.0, "max":100}
    lim_Ey={"min":-150.0, "max":50}
    lim_Ez={"min":-50.0, "max":100}

    lim_Ne={"min":0, "max":5}
    lim_U={"min":-25.0, "max":250}

    txt_traj = ["06:40","07:00","CA"]
elif mission=="JUICE_E1":
    #EGM
    lim_Bx={"min":-200.0, "max":0}
    lim_By={"min":-150.0, "max":100}
    lim_Bz={"min":-550.0, "max":-350}
    #HARRIS
    lim_Bx={"min":-250.0, "max":100}
    lim_By={"min":-150.0, "max":50}
    lim_Bz={"min":-600.0, "max":-300}

    lim_Jx={"min":-30.0, "max":30}
    lim_Jy={"min":-30.0, "max":30}
    lim_Jz={"min":-30.0, "max":30}
    lim_Jtot={"min":0.0, "max":40}

    lim_Ex={"min":-100.0, "max":100}
    lim_Ey={"min":-150.0, "max":50}
    lim_Ez={"min":-50.0, "max":100}
    
    lim_Ne={"min":1, "max":4}
    lim_U={"min":-50.0, "max":150}

    txt_traj = ["16:10","16:30","CA"]
elif mission=="JUICE_E2":
    #EGM
    lim_Bx={"min":-100.0, "max":200}
    lim_By={"min":50.0, "max":250}
    lim_Bz={"min":-550.0, "max":-300}
    #HARRIS
    lim_Bx={"min":-150.0, "max":200}
    lim_By={"min":50.0, "max":250}
    lim_Bz={"min":-600.0, "max":-300}

    lim_Jx={"min":-30.0, "max":30}
    lim_Jy={"min":-30.0, "max":30}
    lim_Jz={"min":-30.0, "max":30}
    lim_Jtot={"min":0.0, "max":40}

    lim_Ex={"min":-100.0, "max":100}
    lim_Ey={"min":-150.0, "max":50}
    lim_Ez={"min":-50.0, "max":100}

    lim_Ne={"min":1, "max":4.5}
    lim_U={"min":0.0, "max":150}

    txt_traj = ["22:10","22:30","CA"]


def plot_fig(x_axis,y_axis,plan,x_traj,y_traj,pts_traj,txt_traj,lim_plan,lim_x,lim_y,cmap,fig_name="",fig_title="",ax_labels={"x":"","y":""},fig_size=[6,9.5],is_streamplot=False,X_norm=None,Y_norm=None,StreamX=None,StreamY=None):
    # planet drawing
    theta = np.divide(2.0*math.pi*np.arange(1,101, 1.0), 100.0)
    xp = np.cos(theta)
    yp = np.sin(theta)

    fig, ax = plt.subplots(figsize=fig_size)
    # contourf for B
    if "Density" not in fig_title and "J" not in fig_title and "Ux" not in fig_title:
        c = ax.contourf(x_axis, y_axis, plan, np.linspace(lim_plan["min"],lim_plan["max"],int((lim_plan["max"]-lim_plan["min"])/2)), vmin=lim_plan["min"], vmax=lim_plan["max"], cmap=cmap,shading='auto')
    else:
        c = ax.pcolor(x_axis, y_axis, plan, vmin=lim_plan["min"], vmax=lim_plan["max"], cmap=cmap,shading='auto')
    axcb = fig.colorbar(c, ax=ax)
    axcb.set_label(fig_title,size="large")
    axcb.minorticks_on()
    axcb.ax.tick_params(labelsize="large")

    if is_streamplot:
        ax.streamplot(np.transpose(X_norm),Y_norm,np.transpose(StreamX),np.transpose(StreamY),color="lightgrey",density=4, arrowstyle='->', arrowsize=1.,linewidth=1)
    ax.plot(xp,yp,c="black",zorder=2)
    ax.fill(xp,yp,c="white",zorder=2)
    ax.plot(x_traj,y_traj,color="black")
    for pts,txt in zip(pts_traj,txt_traj):
        print(pts)
        ax.scatter(pts[0],pts[1],color="black",zorder=3)
        plt.text(pts[0]+0.15,pts[1]+0.15,txt,color="black",fontsize="large")
    
    ax.set_xlim(lim_x["min"],lim_x["max"])
    ax.set_ylim(lim_y["min"],lim_y["max"])

    ax.tick_params(axis='x', labelsize="large")
    ax.tick_params(axis='y', labelsize="large")
    # plt.title(fig_title)#,'fontsize',12,'fontweight','b');
    ax.set_xlabel(ax_labels["x"],fontsize='large')
    ax.set_ylabel(ax_labels["y"])

    plt.savefig(fig_name)

    plt.close('all')


# ## Bx
# pts_traj = [[x640_traj,y640_traj],[x700_traj,y700_traj],[x_CA,y_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Y [$R_E$]"}
# fig_title = "Bx [$nT$]"# time: "+time
# fig_name = "Bx_XY_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XY,Y_XY,Bx_XY,x_traj,y_traj,pts_traj,txt_traj,lim_Bx,lim_X,lim_Y,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Y_norm,Bx_XY,By_XY)

# pts_traj = [[x640_traj,z640_traj],[x700_traj,z700_traj],[x_CA,z_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Bx [$nT$]"# time: "+time
# fig_name = "Bx_XZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XZ,Z_XZ,Bx_XZ,x_traj,z_traj,pts_traj,txt_traj,lim_Bx,lim_X,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Z_norm,Bx_XZ,Bz_XZ)


# pts_traj = [[y640_traj, z640_traj],[y700_traj,z700_traj],[y_CA,z_CA]]
# ax_labels = {"x":"Y [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Bx [$nT$]"# time: "+time
# fig_name = "Bx_YZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(Y_YZ,Z_YZ,Bx_YZ_term,y_traj,z_traj,pts_traj,txt_traj,lim_Bx,lim_Y,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Ynum],True,Y_norm,Z_norm,By_YZ_term,Bz_YZ_term)

# ## By
# pts_traj = [[x640_traj,y640_traj],[x700_traj,y700_traj],[x_CA,y_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Y [$R_E$]"}
# fig_title = "By [$nT$]"# time: "+time
# fig_name = "By_XY_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XY,Y_XY,By_XY,x_traj,y_traj,pts_traj,txt_traj,lim_By,lim_X,lim_Y,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Y_norm,Bx_XY,By_XY)

# pts_traj = [[x640_traj,z640_traj],[x700_traj,z700_traj],[x_CA,z_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "By [$nT$]"# time: "+time
# fig_name = "By_XZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XZ,Z_XZ,By_XZ,x_traj,z_traj,pts_traj,txt_traj,lim_By,lim_X,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Z_norm,Bx_XZ,Bz_XZ)


# pts_traj = [[y640_traj, z640_traj],[y700_traj,z700_traj],[y_CA,z_CA]]
# ax_labels = {"x":"Y [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "By [$nT$]"# time: "+time
# fig_name = "By_YZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(Y_YZ,Z_YZ,By_YZ_term,y_traj,z_traj,pts_traj,txt_traj,lim_By,lim_Y,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Ynum],True,Y_norm,Z_norm,By_YZ_term,Bz_YZ_term)

# ## Bz
# pts_traj = [[x640_traj,y640_traj],[x700_traj,y700_traj],[x_CA,y_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Y [$R_E$]"}
# fig_title = "Bz [$nT$]"# time: "+time
# fig_name = "Bz_XY_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XY,Y_XY,Bz_XY,x_traj,y_traj,pts_traj,txt_traj,lim_Bz,lim_X,lim_Y,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Y_norm,Bx_XY,By_XY)

# pts_traj = [[x640_traj,z640_traj],[x700_traj,z700_traj],[x_CA,z_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Bz [$nT$]"# time: "+time
# fig_name = "Bz_XZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XZ,Z_XZ,Bz_XZ,x_traj,z_traj,pts_traj,txt_traj,lim_Bz,lim_X,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Z_norm,Bx_XZ,Bz_XZ)


# pts_traj = [[y640_traj, z640_traj],[y700_traj,z700_traj],[y_CA,z_CA]]
# ax_labels = {"x":"Y [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Bz [$nT$]"# time: "+time
# fig_name = "Bz_YZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(Y_YZ,Z_YZ,Bz_YZ_term,y_traj,z_traj,pts_traj,txt_traj,lim_Bz,lim_Y,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Ynum],True,Y_norm,Z_norm,By_YZ_term,Bz_YZ_term)

# #######################################


# ## Jx
# pts_traj = [[x640_traj,y640_traj],[x700_traj,y700_traj],[x_CA,y_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Y [$R_E$]"}
# fig_title = "Jx [$nA.m^{-2}$]"# time: "+time
# fig_name = "Jx_XY_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XY,Y_XY,Jx_XY,x_traj,y_traj,pts_traj,txt_traj,lim_Jx,lim_X,lim_Y,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Y_norm,Ux_XY,Uy_XY)

# pts_traj = [[x640_traj,z640_traj],[x700_traj,z700_traj],[x_CA,z_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Jx [$nA.m^{-2}$]"# time: "+time
# fig_name = "Jx_XZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XZ,Z_XZ,Jx_XZ,x_traj,z_traj,pts_traj,txt_traj,lim_Jx,lim_X,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Z_norm,Ux_XZ,Uz_XZ)


# pts_traj = [[y640_traj, z640_traj],[y700_traj,z700_traj],[y_CA,z_CA]]
# ax_labels = {"x":"Y [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Jx [$nA.m^{-2}$]"# time: "+time
# fig_name = "Jx_YZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(Y_YZ,Z_YZ,Jx_YZ_term,y_traj,z_traj,pts_traj,txt_traj,lim_Jx,lim_Y,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Ynum],True,Y_norm,Z_norm,Uy_YZ,Uz_YZ)

# ## Jy
# pts_traj = [[x640_traj,y640_traj],[x700_traj,y700_traj],[x_CA,y_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Y [$R_E$]"}
# fig_title = "Jy [$nA.m^{-2}$]"# time: "+time
# fig_name = "Jy_XY_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XY,Y_XY,Jy_XY,x_traj,y_traj,pts_traj,txt_traj,lim_Jy,lim_X,lim_Y,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Y_norm,Ux_XY,Uy_XY)

# pts_traj = [[x640_traj,z640_traj],[x700_traj,z700_traj],[x_CA,z_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Jy [$nA.m^{-2}$]"# time: "+time
# fig_name = "Jy_XZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XZ,Z_XZ,Jy_XZ,x_traj,z_traj,pts_traj,txt_traj,lim_Jy,lim_X,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Z_norm,Ux_XZ,Uz_XZ)


# pts_traj = [[y640_traj, z640_traj],[y700_traj,z700_traj],[y_CA,z_CA]]
# ax_labels = {"x":"Y [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Jy [$nA.m^{-2}$]"# time: "+time
# fig_name = "Jy_YZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(Y_YZ,Z_YZ,Jy_YZ_term,y_traj,z_traj,pts_traj,txt_traj,lim_Jy,lim_Y,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Ynum],True,Y_norm,Z_norm,Uy_YZ,Uz_YZ)

# ## Jz
# pts_traj = [[x640_traj,y640_traj],[x700_traj,y700_traj],[x_CA,y_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Y [$R_E$]"}
# fig_title = "Jz [$nA.m^{-2}$]"# time: "+time
# fig_name = "Jz_XY_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XY,Y_XY,Jz_XY,x_traj,y_traj,pts_traj,txt_traj,lim_Jz,lim_X,lim_Y,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Y_norm,Ux_XY,Uy_XY)

# pts_traj = [[x640_traj,z640_traj],[x700_traj,z700_traj],[x_CA,z_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Jz [$nA.m^{-2}$]"# time: "+time
# fig_name = "Jz_XZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XZ,Z_XZ,Jz_XZ,x_traj,z_traj,pts_traj,txt_traj,lim_Jz,lim_X,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Z_norm,Ux_XZ,Uz_XZ)


# pts_traj = [[y640_traj, z640_traj],[y700_traj,z700_traj],[y_CA,z_CA]]
# ax_labels = {"x":"Y [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Jz [$nA.m^{-2}$]"# time: "+time
# fig_name = "Jz_YZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(Y_YZ,Z_YZ,Jz_YZ_term,y_traj,z_traj,pts_traj,txt_traj,lim_Jz,lim_Y,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Ynum],True,Y_norm,Z_norm,Uy_YZ,Uz_YZ)

# ## Jtot
# pts_traj = [[x640_traj,y640_traj],[x700_traj,y700_traj],[x_CA,y_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Y [$R_E$]"}
# fig_title = "Jtot [$nA.m^{-2}$]"# time: "+time
# fig_name = "Jtot_XY_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XY,Y_XY,Jtot_XY,x_traj,y_traj,pts_traj,txt_traj,lim_Jtot,lim_X,lim_Y,colmap[ncolJtot],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Y_norm,Ux_XY,Uy_XY)

# pts_traj = [[x640_traj,z640_traj],[x700_traj,z700_traj],[x_CA,z_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Jtot [$nA.m^{-2}$]"# time: "+time
# fig_name = "Jtot_XZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XZ,Z_XZ,Jtot_XZ,x_traj,z_traj,pts_traj,txt_traj,lim_Jtot,lim_X,lim_Z,colmap[ncolJtot],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Z_norm,Ux_XZ,Uz_XZ)


# pts_traj = [[y640_traj, z640_traj],[y700_traj,z700_traj],[y_CA,z_CA]]
# ax_labels = {"x":"Y [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Jtot [$nA.m^{-2}$]"# time: "+time
# fig_name = "Jtot_YZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(Y_YZ,Z_YZ,Jtot_YZ_term,y_traj,z_traj,pts_traj,txt_traj,lim_Jtot,lim_Y,lim_Z,colmap[ncolJtot],fig_name,fig_title,ax_labels,fig_size[figsize_Ynum],True,Y_norm,Z_norm,Uy_YZ,Uz_YZ)

# #######################################





# ## Ex
# pts_traj = [[x640_traj,y640_traj],[x700_traj,y700_traj],[x_CA,y_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Y [$R_E$]"}
# fig_title = "Ex [$mV/m$]"# time: "+time
# fig_name = "Ex_XY_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XY,Y_XY,Ex_XY,x_traj,y_traj,pts_traj,txt_traj,lim_Ex,lim_X,lim_Y,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Y_norm,Ex_XY,Ey_XY)

# pts_traj = [[x640_traj,z640_traj],[x700_traj,z700_traj],[x_CA,z_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Ex [$mV/m$]"# time: "+time
# fig_name = "Ex_XZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XZ,Z_XZ,Ex_XZ,x_traj,z_traj,pts_traj,txt_traj,lim_Ex,lim_X,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Z_norm,Ex_XZ,Ez_XZ)


# pts_traj = [[y640_traj, z640_traj],[y700_traj,z700_traj],[y_CA,z_CA]]
# ax_labels = {"x":"Y [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Ex [$mV/m$]"# time: "+time
# fig_name = "Ex_YZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(Y_YZ,Z_YZ,Ex_YZ_term,y_traj,z_traj,pts_traj,txt_traj,lim_Ex,lim_Y,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Ynum],True,Y_norm,Z_norm,Ey_YZ_term,Ez_YZ_term)

# ## Ey
# pts_traj = [[x640_traj,y640_traj],[x700_traj,y700_traj],[x_CA,y_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Y [$R_E$]"}
# fig_title = "Ey [$mV/m$]"# time: "+time
# fig_name = "Ey_XY_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XY,Y_XY,Ey_XY,x_traj,y_traj,pts_traj,txt_traj,lim_Ey,lim_X,lim_Y,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Y_norm,Ex_XY,Ey_XY)

# pts_traj = [[x640_traj,z640_traj],[x700_traj,z700_traj],[x_CA,z_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Ey [$mV/m$]"# time: "+time
# fig_name = "Ey_XZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XZ,Z_XZ,Ey_XZ,x_traj,z_traj,pts_traj,txt_traj,lim_Ey,lim_X,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Z_norm,Ex_XZ,Ez_XZ)


# pts_traj = [[y640_traj, z640_traj],[y700_traj,z700_traj],[y_CA,z_CA]]
# ax_labels = {"x":"Y [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Ey [$mV/m$]"# time: "+time
# fig_name = "Ey_YZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(Y_YZ,Z_YZ,Ey_YZ_term,y_traj,z_traj,pts_traj,txt_traj,lim_Ey,lim_Y,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Ynum],True,Y_norm,Z_norm,Ey_YZ_term,Ez_YZ_term)

# ## Ez
# pts_traj = [[x640_traj,y640_traj],[x700_traj,y700_traj],[x_CA,y_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Y [$R_E$]"}
# fig_title = "Ez [$mV/m$]"# time: "+time
# fig_name = "Ez_XY_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XY,Y_XY,Ez_XY,x_traj,y_traj,pts_traj,txt_traj,lim_Ez,lim_X,lim_Y,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Y_norm,Ex_XY,Ey_XY)

# pts_traj = [[x640_traj,z640_traj],[x700_traj,z700_traj],[x_CA,z_CA]]
# ax_labels = {"x":"X [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Ez [$mV/m$]"# time: "+time
# fig_name = "Ez_XZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(X_XZ,Z_XZ,Ez_XZ,x_traj,z_traj,pts_traj,txt_traj,lim_Ez,lim_X,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Z_norm,Ex_XZ,Ez_XZ)


# pts_traj = [[y640_traj, z640_traj],[y700_traj,z700_traj],[y_CA,z_CA]]
# ax_labels = {"x":"Y [$R_E$]", "y":"Z [$R_E$]"}
# fig_title = "Ez [$mV/m$]"# time: "+time
# fig_name = "Ez_YZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
# plot_fig(Y_YZ,Z_YZ,Ez_YZ_term,y_traj,z_traj,pts_traj,txt_traj,lim_Ez,lim_Y,lim_Z,colmap[ncolB],fig_name,fig_title,ax_labels,fig_size[figsize_Ynum],True,Y_norm,Z_norm,Ey_YZ_term,Ez_YZ_term)





#####################################################


## density
pts_traj = [[x640_traj,y640_traj],[x700_traj,y700_traj],[x_CA,y_CA]]
# txt_traj = ["06:40","07:00","CA"]
ax_labels = {"x":"X [$R_E$]", "y":"Y [$R_E$]"}
fig_title = "Density $n_e$ log[$cm^{-3}$]"# time: "+time
fig_name = "Dn_ne_XY_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
plot_fig(X_XY,Y_XY,np.log10(Dn_XY),x_traj,y_traj,pts_traj,txt_traj,lim_Ne,lim_X,lim_Y,colmap[0],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum])

pts_traj = [[x640_traj,z640_traj],[x700_traj,z700_traj],[x_CA,z_CA]]
# txt_traj = ["06:40","07:00","CA"]
ax_labels = {"x":"X [$R_E$]", "y":"Z [$R_E$]"}
fig_title = "Density $n_e$ log[$cm^{-3}$]"# time: "+time
fig_name = "Dn_ne_XZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
plot_fig(X_XZ,Z_XZ,np.log10(Dn_XZ),x_traj,z_traj,pts_traj,txt_traj,lim_Ne,lim_X,lim_Z,colmap[0],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum])

pts_traj = [[y640_traj, z640_traj],[y700_traj,z700_traj],[y_CA,z_CA]]
# txt_traj = ["06:40","07:00","CA"]
ax_labels = {"x":"Y [$R_E$]", "y":"Z [$R_E$]"}
fig_title = "Density $n_e$ log[$cm^{-3}$]"# time: "+time
fig_name = "Dn_ne_YZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
plot_fig(Y_YZ,Z_YZ,np.log10(Dn_YZ),y_traj,z_traj,pts_traj,txt_traj,lim_Ne,lim_Y,lim_Z,colmap[0],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum])




# ## Ux
pts_traj = [[x640_traj,y640_traj],[x700_traj,y700_traj],[x_CA,y_CA]]
# txt_traj = ["06:40","07:00","CA"]
ax_labels = {"x":"X [$R_E$]", "y":"Y [$R_E$]"}
fig_title = "Ux [$km.s^{-1}$]"# "+time
fig_name = "Ux_XY_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
plot_fig(X_XY,Y_XY,Ux_XY,x_traj,y_traj,pts_traj,txt_traj,lim_U,lim_X,lim_Y,colmap[ncol],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Y_norm,Ux_XY,Uy_XY)

pts_traj = [[x640_traj,z640_traj],[x700_traj,z700_traj],[x_CA,z_CA]]
# txt_traj = ["06:40","07:00","CA"]
ax_labels = {"x":"X [$R_E$]", "y":"Z [$R_E$]"}
fig_title = "Ux [$km.s^{-1}$]"
fig_name = "Ux_XZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
plot_fig(X_XZ,Z_XZ,Ux_XZ,x_traj,z_traj,pts_traj,txt_traj,lim_U,lim_X,lim_Z,colmap[ncol],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,X_norm,Z_norm,Ux_XZ,Uz_XZ)

pts_traj = [[y640_traj, z640_traj],[y700_traj,z700_traj],[y_CA,z_CA]]
# txt_traj = ["06:40","07:00","CA"]
ax_labels = {"x":"Y [$R_E$]", "y":"Z [$R_E$]"}
fig_title = "Ux [$km.s^{-1}$]"# time: "+time
fig_name = "Ux_YZ_Europa_"+mission+"_flyby_"+date+"_t"+time+".png"
plot_fig(Y_YZ,Z_YZ,Ux_YZ,y_traj,z_traj,pts_traj,txt_traj,lim_U,lim_Y,lim_Z,colmap[ncol],fig_name,fig_title,ax_labels,fig_size[figsize_Xnum],True,Y_norm,Z_norm,Uy_YZ,Uz_YZ)