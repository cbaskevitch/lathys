import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import matplotlib.dates as md
import datetime
import astropy.io.votable as astrovot
import pandas as pd
import sys, os

labels = ["$n_{e}$ JUICE 7E1","$n_{O^+_{jov}}$ JUICE 7E1","$n_{e}$ JUICE 8E2","$n_{O^+_{jov}}$ JUICE 8E2"]
ls=["-","--"]


fig = plt.figure(figsize=(14, 7))
xfmt = md.DateFormatter('%H:%M:%S')
plt.subplots_adjust(bottom=0.2)
plt.title("E4 flyby 1996/12/19",size=17)

### Bx
ax1 = plt.subplot()#, sharex=ax0)
plt.ylabel("Density E1 [$cm^{-3}$]",size=17)
plt.xlabel("Time [minutes from CA]",size=17)
plt.setp(ax1.get_xticklabels(), fontsize=16)
plt.grid(True)

ax12 = ax1.twinx()
plt.ylabel("Density E2 [$cm^{-3}$]",size=17)
plt.setp(ax12.get_xticklabels(), visible=False)
ax12.yaxis.set_tick_params(labelsize=16)
ax12.yaxis.set_major_locator(MultipleLocator(20))
plt.grid(True)


### Get B field values from xml file generated by lathys interpol_field ###
for irun in range(0,3,2):
    print(sys.argv[1+irun])
    votable = astrovot.parse(sys.argv[1+irun])
    Stable = votable.get_first_table()
    SOjvdata=np.ma.getdata(Stable.array)

    votable = astrovot.parse(sys.argv[1+irun+1])
    Stable = votable.get_first_table()
    SO2pldata=np.ma.getdata(Stable.array)


    SOjv = []  # axe y -> Valeur de Bx dans la simulation
    SO2pl = []  # axe y -> Valeur de By dans la simulation

    x1=[]
    x_time = []
    for i in range (0,len(SOjvdata)):
        x1.append(SOjvdata[i][0].replace("T"," ").replace(".0",""))
        if SOjvdata[i][4] != 0:
            xtmp = float(x1[i][11:13])*3600.0 + float(x1[i][14:16])*60.0 + float(x1[i][17:19])
            x_time.append(xtmp)
            SOjv.append(SOjvdata[i][4])
        else:
            SOjv.append(np.nan)
        
        if SO2pldata[i][4] != 0:
            SO2pl.append(SO2pldata[i][4])
        else:
            SO2pl.append(np.nan)


    r1 = np.zeros(len(SOjvdata))
    Y1 = np.zeros(len(SOjvdata))
    for i in range(0,len(SOjvdata)):
        r1[i] = np.sqrt(SOjvdata[i][1]**2 + SOjvdata[i][2]**2 + SOjvdata[i][3]**2)
    time_CA = np.argmin(r1)


    id_Y1_inf_1 = np.where(np.abs(Y1)<1.0)

    # dates = [datetime.datetime.strptime(elem, '%Y-%m-%d %H:%M:%S') for elem in x1]
    # datenums1=md.date2num(dates)
    datenums1=np.array(x_time)
    datenums1 -= datenums1[time_CA]
    datenums1 /= 60
    # print(datenums1,datenums1[time_CA])

    if irun == 0 :
        ax1.plot(datenums1,SOjv,c="red",label=labels[irun])
        ax1.plot(datenums1,SO2pl,c="red",ls="--",label=labels[irun+1])
    else:
        ax12.plot(datenums1,SOjv,c="black",label=labels[irun])
        ax12.plot(datenums1,SO2pl,c="black",ls="--",label=labels[irun+1])
    # ax1.vlines(datenums1[time_CA],0,150.0,color="black")

# ax1.xaxis.set_major_formatter(xfmt)
ax1.yaxis.set_major_locator(MultipleLocator(20))
ax1.yaxis.set_tick_params(labelsize=16)


ax1.set_ylim(1,1e4)
ax12.set_ylim(1,1e4)
ax1.set_yscale('log')
ax12.set_yscale('log')


ax1.legend(loc='upper center', bbox_to_anchor=(0.5, 1.21),
          ncol=2, fancybox=True, shadow=True,fontsize=16)
ax12.legend(loc='upper center', bbox_to_anchor=(0.5, 1.11),
          ncol=2, fancybox=True, shadow=True,fontsize=16)

# plt.xlim(Gdatenums[0], Gdatenums[-1])

fig.tight_layout(rect=[0, 0, 1, 1])

plt.savefig("test_conf_2023_comparaison_density_field_JUICE_sim.png")