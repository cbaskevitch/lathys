import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import matplotlib.dates as md
import datetime
import astropy.io.votable as astrovot
import pandas as pd
import sys, os

radius = 1#1560 
phys_length = 1#202.9337

n_run=int(sys.argv[1])

### Get traj xyz
ftraj = sys.argv[3]
print("traj file : ",ftraj)
## get traj values
traj = np.genfromtxt(ftraj,delimiter=";",dtype=str)
x_traj = np.zeros(len(traj)-1)
y_traj = np.zeros(len(traj)-1)
z_traj = np.zeros(len(traj)-1)
for i in range(1,len(traj)):
    x_traj[i-1] = float(traj[i][1])*radius/phys_length
    y_traj[i-1] = float(traj[i][2])*radius/phys_length
    z_traj[i-1] = float(traj[i][3])*radius/phys_length


mission = "GALILEO"
# mission = "JUICE_E1"
# mission = "JUICE_E2"

# labels = ["LatHyS anal. atm. + iono.","LatHyS EGM atm."]

if mission=="GALILEO":
    # labels = ["LatHyS GALILEO E4 t=300","LatHyS GALILEO E4 t=600"]
    # labels = ["Harris & Kliore profiles","Harris profile & build ionosphere","EGM & build ionosphere"]
    # labels = ["RUN A1","RUN A2","RUN A3"]
    labels = ["RUN A1 $t=300\Omega_{O^+}$","RUN A1 $t=600\Omega_{O^+}$"]
    ### Get B field values of Galileo flyby from AMDA VOTable ###
    Bx0 = 65.0
    By0 = -173.0
    Bz0 = -412.0
    phys_mag = 450

    print(sys.argv[2])
    votable = astrovot.parse(sys.argv[2])
    Gtable = votable.get_first_table()
    Gdata=np.ma.getdata(Gtable.array)
elif mission == "JUICE_E1":
    # labels = ["LatHyS JUICE E1 t=300","LatHyS JUICE E1 t=600"]
    labels = ["RUN B1","RUN B2"]
    Bx0 = -71.0
    By0 = -86.0
    Bz0 = -460.0
    phys_mag = 473.33
    # Gdata = np.loadtxt(sys.argv[1],delimiter=" ",skiprows=4)
    Gdata = pd.read_csv(sys.argv[2],delimiter=" ",skiprows=4,header=None,names=("date","Bx","By","Bz","Btot"))
    print(Gdata)
elif mission == "JUICE_E2":
    # labels = ["LatHyS JUICE E2 t=300","LatHyS JUICE E2 t=600"]
    labels = ["RUN C1","RUN C2"]
    Bx0 = 40.0
    By0 = 145.0
    Bz0 = -461.0
    phys_mag = 484.92
    # Gdata = np.loadtxt(sys.argv[1],delimiter=" ",skiprows=4)
    Gdata = pd.read_csv(sys.argv[2],delimiter=" ",skiprows=4,header=None,names=("date","Bx","By","Bz","Btot"))
    print(Gdata)
else:
    print("Mission unknown")
    sys.exit()

# champ induit
Bx0_ind = -Bx0/2
By0_ind = -By0/2
Bz0_ind = 0

GBx = []  # axe y -> Valeur de Bx mesurees par Galileo
GBy = []  # axe y -> Valeur de By mesurees par Galileo
GBz = []  # axe y -> Valeur de Bz mesurees par Galileo
GBtot = []  # axe y -> Valeur de Bz mesurees par Galileo
Gxyz = []
# Sden_plasma = []
# Sden_iono = []


x=[]   # Recupere l indice de l'heure (dans les donnes de Galileo) ou commence les donnees de la simulation afin d'avoir la meme taille de tableau
if mission=="GALILEO":
    for i in range(0,len(Gdata)):
        GBx.append(Gdata[i][1][0])
        GBy.append(Gdata[i][1][1])
        GBz.append(Gdata[i][1][2])
        GBtot.append(np.sqrt(Gdata[i][1][0]**2 + Gdata[i][1][1]**2 + Gdata[i][1][2]**2))
        Gxyz.append([])
        x.append(Gdata[i][0][:-5].replace("T"," "))#.replace(".000",""))
elif "JUICE" in mission:
    for index, row in Gdata.iterrows():
        GBx.append(row["Bx"])
        GBy.append(row["By"])
        GBz.append(row["Bz"])

        x.append(row["date"].replace("T"," "))#.replace(".000",""))


dates = [datetime.datetime.strptime(elem, '%Y-%m-%d %H:%M:%S') for elem in x]
Gdatenums=md.date2num(dates)

# Calcul de la moyenne glissante
if mission == "GALILEO" :
    N = int(50*3) # attention, ça dépend de la frequence des points. ici 1s est faite en 3 points
elif "JUICE" in mission:
    N = int(1*2) # attention, ça dépend de la frequence des points. ici 1s est faite en 2 points

GBx_moy = np.zeros(len(GBx))  # axe y -> Valeurs moyennes de Bx mesurees par Galileo avec une fenetre glissante de 50s
GBy_moy = np.zeros(len(GBx))  # axe y -> Valeurs moyennes de By mesurees par Galileo avec une fenetre glissante de 50s
GBz_moy = np.zeros(len(GBx))  # axe y -> Valeurs moyennes de Bz mesurees par Galileo avec une fenetre glissante de 50s
for i in range(0,len(GBx)):
    count = 0
    for k in range(0,int(N/2)):
        id = i-k
        if id > -1:
            if mission == "GALILEO" :
                GBx_moy[i] += Gdata[id][1][0]
                GBy_moy[i] += Gdata[id][1][1]
                GBz_moy[i] += Gdata[id][1][2]
            elif "JUICE" in mission:
                GBx_moy[i] += Gdata.loc[id,"Bx"]
                GBy_moy[i] += Gdata.loc[id,"By"]
                GBz_moy[i] += Gdata.loc[id,"Bz"]
            count+=1
    for k in range(1,int(N/2)+1):
        id = i+k
        if id < len(Gdata):
            if mission == "GALILEO" :
                GBx_moy[i] += Gdata[id][1][0]
                GBy_moy[i] += Gdata[id][1][1]
                GBz_moy[i] += Gdata[id][1][2]
            elif "JUICE" in mission:
                GBx_moy[i] += Gdata.loc[id,"Bx"]
                GBy_moy[i] += Gdata.loc[id,"By"]
                GBz_moy[i] += Gdata.loc[id,"Bz"]
            count+=1
    GBx_moy[i] /= count
    GBy_moy[i] /= count
    GBz_moy[i] /= count
    

GBtot_moy = np.sqrt(GBx_moy*GBx_moy + GBy_moy*GBy_moy + GBz_moy*GBz_moy)


id = N # attention, ça dépend de la frequence des points. ici 1s est faite en 2 points
x1 = float(x[id][11:13])*3600.0 + float(x[id][14:16])*60.0 + float(x[id][17:19])
x2 = float(x[-id][11:13])*3600.0 + float(x[-id][14:16])*60.0 + float(x[-id][17:19])
Bx_background = np.polyfit([x1, x2],[GBx_moy[id], GBx_moy[-id]],1)
By_background = np.polyfit([x1, x2],[GBy_moy[id], GBy_moy[-id]],1)
Bz_background = np.polyfit([x1, x2],[GBz_moy[id], GBz_moy[-id]],1)

Xx = []
Yx = []
Yy = []
Yz = []
for i in range(0,len(x)):
    Xx.append(float(x[i][11:13])*3600.0 + float(x[i][14:16])*60.0 + float(x[i][17:19]))
    Yx.append(Bx_background[0]*Xx[i] + Bx_background[1])
    Yy.append(By_background[0]*Xx[i] + By_background[1])
    Yz.append(Bz_background[0]*Xx[i] + Bz_background[1])


##### Calcul du champ induit le long de la traj
GBx_ind = np.zeros(len(GBx_moy))
GBy_ind = np.zeros(len(GBx_moy))
GBz_ind = np.zeros(len(GBx_moy))
GBx_ind[:] = Yx[:]
GBy_ind[:] = Yy[:]
GBz_ind[:] = Yz[:]

bme = np.sqrt(Bx0_ind**2 + By0_ind**2 + Bz0_ind**2)
# Initialisation
# Dipolar Moment in planetary units (Spe%ref%mag) mu0/4pi*M
mom_dip_u1 = Bx0_ind
mom_dip_u2 = By0_ind
mom_dip_u3 = Bz0_ind
mom_dip = [mom_dip_u1*(radius/phys_length)**3, mom_dip_u2*(radius/phys_length)**3, mom_dip_u3*(radius/phys_length)**3]

for i in range(0,len(x_traj)):
    distance = np.sqrt(x_traj[i]**2 + y_traj[i]**2 + z_traj[i]**2)
    b_dip_1 = (3*x_traj[i]*(x_traj[i]*mom_dip[0] + y_traj[i]*mom_dip[1] + z_traj[i]*mom_dip[2])/distance**2-mom_dip[0]) / distance**3
    b_dip_2 = (3*y_traj[i]*(x_traj[i]*mom_dip[0] + y_traj[i]*mom_dip[1] + z_traj[i]*mom_dip[2])/distance**2-mom_dip[1]) / distance**3
    b_dip_3 = (3*z_traj[i]*(x_traj[i]*mom_dip[0] + y_traj[i]*mom_dip[1] + z_traj[i]*mom_dip[2])/distance**2-mom_dip[2]) / distance**3
    GBx_ind[i] += b_dip_1
    GBy_ind[i] += b_dip_2
    GBz_ind[i] += b_dip_3

GBtot_ind = np.sqrt(GBx_ind**2 + GBy_ind**2 + GBz_ind**2)



#### initialisation de la figure

fig = plt.figure(figsize=(10, 14))
xfmt = md.DateFormatter('%H:%M:%S')
plt.subplots_adjust(bottom=0.2)
plt.title("E4 flyby 1996/12/19",size=17)

### Bx
ax1 = plt.subplot(411)#, sharex=ax0)
plt.ylabel("Bx [nT]",size=17)
plt.setp(ax1.get_xticklabels(), visible=False)
plt.grid(True)

ax2 = plt.subplot(412, sharex=ax1)
plt.ylabel("By [nT]",size=17)
plt.setp(ax2.get_xticklabels(), visible=False)
ax2.yaxis.set_tick_params(labelsize=16)
ax2.yaxis.set_major_locator(MultipleLocator(20))
plt.grid(True)

ax3 = plt.subplot(413, sharex=ax1)
plt.ylabel("Bz [nT]",size=17)
# plt.xlabel("Time [h:m:s]",size=17)
plt.setp(ax3.get_xticklabels(), visible=False)
# ax3.xaxis.set_tick_params(rotation=30, labelsize=16)
ax3.yaxis.set_major_locator(MultipleLocator(20))
ax3.yaxis.set_tick_params(labelsize=16)
plt.grid(True)

ax4 = plt.subplot(414, sharex=ax1)
plt.ylabel("||B|| [nT]",size=17)
plt.xlabel("Time [h:m:s]",size=17)
plt.setp(ax4.get_xticklabels(), fontsize=16)
ax4.xaxis.set_tick_params(rotation=30, labelsize=16)
ax4.yaxis.set_major_locator(MultipleLocator(20))
ax4.yaxis.set_tick_params(labelsize=16)
plt.grid(True)


color=["red","blue","green"]
# ls=["-","--","-."]
ls=["-","-","-"]


for irun in range (4,4+n_run):
### Get B field values from xml file generated by lathys interpol_field ###
    print(sys.argv[irun])
    votable = astrovot.parse(sys.argv[irun])
    Stable = votable.get_first_table()
    S1data=np.ma.getdata(Stable.array)

    S1Bx = []  # axe y -> Valeur de Bx dans la simulation
    S1By = []  # axe y -> Valeur de By dans la simulation
    S1Bz = []  # axe y -> Valeur de Bz dans la simulation
    S1Btot = []  # axe y -> Valeur de Bz dans la simulation

    x1=[]
    for i in range (0,len(S1data)):
        if mission == "GALILEO" :
            x1.append(S1data[i][0][:-4].replace("T"," "))#.replace(".000",""))
            if S1data[i][4] != 0 or S1data[i][5] != 0 or S1data[i][6] != 0:
                xtmp = float(x1[i][11:13])*3600.0 + float(x1[i][14:16])*60.0 + float(x1[i][17:19])
                S1Bx.append(S1data[i][4] - Bx0 + Bx_background[0]*xtmp + Bx_background[1])
                S1By.append(S1data[i][5] - By0 + By_background[0]*xtmp + By_background[1])
                S1Bz.append(S1data[i][6] - Bz0 + Bz_background[0]*xtmp + Bz_background[1])
            else:
                S1Bx.append(np.nan)
                S1By.append(np.nan)
                S1Bz.append(np.nan)
        elif "JUICE" in mission:
            x1.append(S1data[i][0].replace("T"," ").replace(".0",""))
            if S1data[i][4] != 0 or S1data[i][5] != 0 or S1data[i][6] != 0:
                xtmp = float(x1[i][11:13])*3600.0 + float(x1[i][14:16])*60.0 + float(x1[i][17:19])
                S1Bx.append(S1data[i][4] - Bx0 + Bx_background[0]*xtmp + Bx_background[1])
                S1By.append(S1data[i][5] - By0 + By_background[0]*xtmp + By_background[1])
                S1Bz.append(S1data[i][6] - Bz0 + Bz_background[0]*xtmp + Bz_background[1])
            else:
                S1Bx.append(np.nan)
                S1By.append(np.nan)
                S1Bz.append(np.nan)
        
        S1Btot.append(np.sqrt(S1Bx[i]*S1Bx[i] + S1By[i]*S1By[i] + S1Bz[i]*S1Bz[i]))


    r1 = np.zeros(len(S1data))
    Y1 = np.zeros(len(S1data))
    for i in range(0,len(S1data)):
        r1[i] = np.sqrt(S1data[i][1]**2 + S1data[i][2]**2 + S1data[i][3]**2)
        Y1[i] = S1data[i][2]
    time_CA = np.argmin(r1)


    id_Y1_inf_1 = np.where(np.abs(Y1)<1.0)


    dates = [datetime.datetime.strptime(elem, '%Y-%m-%d %H:%M:%S') for elem in x1]
    datenums1=md.date2num(dates)


    ax1.plot(datenums1,S1Bx,c=color[irun-4],ls=ls[irun-4],label=labels[irun-4])
    if mission == "GALILEO" :
        ax1.fill_between(datenums1[id_Y1_inf_1[0][0]:id_Y1_inf_1[0][-1]],-12.0,125.0,color="lightgrey")
        ax1.vlines(datenums1[time_CA],-12.0,125.0,color="black")
    elif mission == "JUICE_E1" :
        ax1.vlines(datenums1[time_CA],-170.0,-50.0,color="black")
        ax1.set_ylim(-170.0,-50.0)
    elif mission == "JUICE_E2" :
        ax1.vlines(datenums1[time_CA],-20.0,100.0,color="black")
        ax1.set_ylim(-20.0,100.0)

    ax2.plot(datenums1,S1By,c=color[irun-4],ls=ls[irun-4],label=labels[irun-4])
    if mission == "GALILEO" :
        ax2.fill_between(datenums1[id_Y1_inf_1[0][0]:id_Y1_inf_1[0][-1]],-225.0,-95.0,color="lightgrey")
        ax2.vlines(datenums1[time_CA],-225.0,-95.0,color="black")
    elif mission == "JUICE_E1" :
        ax2.vlines(datenums1[time_CA],-140.0,10.0,color="black")
        ax2.set_ylim(-140.0,10.0)
    elif mission == "JUICE_E2" :
        ax2.vlines(datenums1[time_CA],50.0,200.0,color="black")
        ax2.set_ylim(50.0,200.0)


    ax3.plot(datenums1,S1Bz,c=color[irun-4],ls=ls[irun-4],label=labels[irun-4])
    if mission == "GALILEO" :
        ax3.fill_between(datenums1[id_Y1_inf_1[0][0]:id_Y1_inf_1[0][-1]],-475.0,-340.0,color="lightgrey")
        ax3.vlines(datenums1[time_CA],-475.0,-340.0,color="black")
    elif mission == "JUICE_E1":
        ax3.vlines(datenums1[time_CA],-550.0,-350.0,color="black")
        ax3.set_ylim(-550.0,-350.0)
    elif mission == "JUICE_E2":
        ax3.vlines(datenums1[time_CA],-550.0,-350.0,color="black")
        ax3.set_ylim(-550.0,-350.0)

    ax4.plot(datenums1,S1Btot,c=color[irun-4],ls=ls[irun-4],label=labels[irun-4])
    if mission == "GALILEO" :
        ax4.fill_between(datenums1[id_Y1_inf_1[0][0]:id_Y1_inf_1[0][-1]],300.0,500.0,color="lightgrey")
        ax4.vlines(datenums1[time_CA],300.0,500.0,color="black")
    elif mission == "JUICE_E1":
        ax4.vlines(datenums1[time_CA],360.0,580.0,color="black")
        ax4.set_ylim(360.0,580.0)
    elif mission == "JUICE_E2":
        ax4.vlines(datenums1[time_CA],360.0,580.0,color="black")
        ax4.set_ylim(360.0,580.0)

ax1.xaxis.set_major_formatter(xfmt)
ax1.yaxis.set_major_locator(MultipleLocator(20))
ax1.yaxis.set_tick_params(labelsize=16)

if mission == "GALILEO" :
    ax1.scatter(Gdatenums,GBx,c="grey",label="MAG",marker=".")
    ax1.plot(Gdatenums,GBx_moy,c="black",label="MAG smoothed")
    ax1.plot(Gdatenums,GBx_ind,c="black",ls="--",label="$B_J$ + induced")
    # ax1.plot(Gdatenums,Yx,c="red",ls="--",label="$B_J$")
    ax1.set_ylim(0.0,125.0)

    ax2.scatter(Gdatenums,GBy,c="grey",label="MAG",marker=".")
    ax2.plot(Gdatenums,GBy_moy,c="black",label="MAG smoothed")
    ax2.plot(Gdatenums,GBy_ind,c="black",ls="--",label="$B_J$ + induced")
    # ax2.plot(Gdatenums,Yy,c="red",ls="--",label="$B_J$")
    ax2.set_ylim(-220.0,-95.0)
    # ax2.legend(loc="upper right",fontsize=16)#,bbox_to_anchor=(1.12, 1.07))
    ax2.legend(loc="upper right",fontsize=16, bbox_to_anchor=(1, 1.15),
         fancybox=True, shadow=True)#,bbox_to_anchor=(1.12, 1.07))

    ax3.scatter(Gdatenums,GBz,c="grey",label="MAG",marker=".")
    ax3.plot(Gdatenums,GBz_moy,c="black",label="MAG smoothed")
    ax3.plot(Gdatenums,GBz_ind,c="black",ls="--",label="$B_J$ + induced")
    # ax3.plot(Gdatenums,Yz,c="red",ls="--",label="$B_J$")
    ax3.set_ylim(-460.0,-340.0)

    ax4.scatter(Gdatenums,GBtot,c="grey",label="MAG",marker=".")
    ax4.plot(Gdatenums,GBtot_moy,c="black",label="MAG smoothed")
    ax4.plot(Gdatenums,GBtot_ind,c="black",ls="--",label="$B_J$ + induced")
    ax4.set_ylim(350.0,500.0)
elif "JUICE" in mission:
    # ax1.plot(Gdatenums,GBx_moy,c="black",label="bg Bx field (Cornnerney et al. 2018)")
    ax1.plot(Gdatenums,GBx_ind,c="black",ls="-",label="$B_J$ + induced")
    # ax1.set_ylim(0.0,100.0)
    ax1.legend(loc="lower right",fontsize=16)#,bbox_to_anchor=(1.12, 1.07))

    # ax2.plot(Gdatenums,GBy_moy,c="black",label="bg By field (Cornnerney et al. 2018)")
    ax2.plot(Gdatenums,GBy_ind,c="black",ls="-",label="$B_J$ + induced")
    # ax2.set_ylim(60.0,180.0)

    # ax3.plot(Gdatenums,GBz_moy,c="black",label="bg Bz field (Cornnerney et al. 2018)")
    ax3.plot(Gdatenums,GBz_ind,c="black",ls="-",label="$B_J$ + induced")
    # ax3.set_ylim(-530.0,-360.0)

    # ax4.plot(Gdatenums,GBtot_moy,c="black",label="bg Btot field (Cornnerney et al. 2018)")
    ax4.plot(Gdatenums,GBtot_ind,c="black",ls="-",label="$B_J$ + induced")
    # ax4.set_ylim(300.0,500.0)


plt.xlim(Gdatenums[0], Gdatenums[-1])

fig.tight_layout(rect=[0, 0, 1, 1])

plt.savefig("conf_2023_comparaison_B_field_nsim_"+mission+"_MAG_simul_background_B.png")