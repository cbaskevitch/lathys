from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

src_dir = "./"
ncfile1 = sys.argv[1] #b3 file

ncid1 = Dataset(ncfile1)
var_nc1 = ncid1.variables

B_mean = np.transpose(var_nc1["dinfo_B_mean"][:])
B_quad = np.transpose(var_nc1["dinfo_B_quad"][:])
V_mean = np.transpose(var_nc1["dinfo_v_mean"][:])
V_quad = np.transpose(var_nc1["dinfo_v_quad"][:])
e_tot = var_nc1["dinfo_etot"][:]
print(e_tot)
n_part = var_nc1["dinfo_n_part"][:]
e_therm = var_nc1["dinfo_etherm"][:]
e_mag = var_nc1["dinfo_mag"][:]

nb_diag = var_nc1["iter"][:][0]

dx = 300.0/nb_diag
times1= np.arange(0.0,300.0,dx)
B1 = []
Bqd = []
Vqd = []
n = []
V1 = []
etot1 = []
etherm1 = []


fig, ax = plt.subplots(3,1,figsize=(8,6),sharex=True)
ax[0].plot(times1[2:],B_quad[0][2:nb_diag],c="blue",label="Bx")
ax[1].plot(times1[2:],B_quad[1][2:nb_diag],c="red",label="By")
ax[2].plot(times1[2:],B_quad[2][2:nb_diag],c="green",label="Bz")

ax[2].set_xlabel('Temps [$\Omega_{SO^{++}}^{-1}$]')#,'fontsize',12,'fontweight','b');
ax[0].set_ylabel('$(<B_x-B_0>)^2$')#,'fontsize',12,'fontweight','b');
ax[1].set_ylabel('$(<B_y-B_0>)^2$')#,'fontsize',12,'fontweight','b');
ax[2].set_ylabel('$(<B_z-B_0>)^2$')#,'fontsize',12,'fontweight','b');

# ax[0].legend(loc="upper right")
# plt.title("B field squared deviation from the mean")
plt.savefig("b3_B_quad.png")
plt.close('all')

fig, ax = plt.subplots(3,1,figsize=(8,6),sharex=True)
ax[0].plot(times1,V_quad[0][:nb_diag],c="blue",label="Vx")
ax[1].plot(times1,V_quad[1][:nb_diag],c="red",label="Vy")
ax[2].plot(times1,V_quad[2][:nb_diag],c="green",label="Vz")

ax[2].set_xlabel('Temps [$\Omega_{SO^{++}}^{-1}$]')#,'fontsize',12,'fontweight','b');
ax[0].set_ylabel('$(<V_x-V_0>)^2$')#,'fontsize',12,'fontweight','b');
ax[1].set_ylabel('$(<V_y-V_0>)^2$')#,'fontsize',12,'fontweight','b');
ax[2].set_ylabel('$(<V_z-V_0>)^2$')#,'fontsize',12,'fontweight','b');
# ax[0].set_ylim(0.0026,0.0028)
# ax[1].set_ylim(0.0026,0.0028)
ax[2].set_ylim(0.00273,0.00274)
# ax.legend(loc="upper right")
# plt.title("Velocity squared deviation from the mean")
plt.savefig("b3_V_quad.png")
plt.close('all')


fig, ax = plt.subplots(figsize=(8,6))
ax.plot(times1,n_part[:nb_diag],c="blue")#,label="number of particles")

ax.set_xlabel('Temps [$\Omega_{SO^{++}}^{-1}$]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('Nombre de particules ')#,'fontsize',12,'fontweight','b');
# ax.legend(loc="upper right")
# plt.title("Number of particles in the simulation box")
plt.savefig("b3_n_part.png")
plt.close('all')

fig, ax = plt.subplots(3,1,figsize=(8,6),sharex=True)
ax[0].plot(times1[1:],B_mean[0][1:nb_diag],c="blue",label="Bx")
ax[1].plot(times1,B_mean[1][:nb_diag],c="red",label="By")
ax[2].plot(times1,B_mean[2][:nb_diag],c="green",label="Bz")

ax[2].set_xlabel('Temps [$\Omega_{SO^{++}}^{-1}$]')#,'fontsize',12,'fontweight','b');
ax[0].set_ylabel('$<B_x/B_0>$')#,'fontsize',12,'fontweight','b');
ax[1].set_ylabel('$<B_y/B_0>$')#,'fontsize',12,'fontweight','b');
ax[2].set_ylabel('$<B_z/B_0>$')#,'fontsize',12,'fontweight','b');
ax[0].set_ylim(0.1651,0.1653)
# ax[1].set_ylim(-0.3,-0.25)
# ax[2].set_ylim(-1,-0.9)
# ax.legend(loc="upper right")
# plt.title("Mean B field")
plt.savefig("b3_B_mean.png")
plt.close('all')

fig, ax = plt.subplots(3,1,figsize=(8,6),sharex=True)
ax[0].plot(times1,V_mean[0][:nb_diag],c="blue",label="Vx")
ax[1].plot(times1,V_mean[1][:nb_diag],c="red",label="Vy")
ax[2].plot(times1,V_mean[2][:nb_diag],c="green",label="Vz")

ax[2].set_xlabel('Temps [$\Omega_{SO^{++}}^{-1}$]')#,'fontsize',12,'fontweight','b');
ax[0].set_ylabel('$<V_x/V_A>$')#,'fontsize',12,'fontweight','b');
ax[1].set_ylabel('$<V_y/V_A>$')#,'fontsize',12,'fontweight','b');
ax[2].set_ylabel('$<V_z/V_A>$')#,'fontsize',12,'fontweight','b');
# ax[0].set_ylim(0.0027,0.0028)
# ax[1].set_ylim(0.0027,0.0028)
# ax[2].set_ylim(0.0027,0.0028)
# ax.legend(loc="upper right")
# plt.title("Mean velocity")
plt.savefig("b3_V_mean.png")
plt.close('all')

fig, ax = plt.subplots(figsize=(8,6))
ax.plot(times1,e_tot[:nb_diag],c="blue")#,label="Etot")

ax.set_xlabel('Temps [$\Omega_{SO^{++}}^{-1}$]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('$<E_{tot}>$')#,'fontsize',12,'fontweight','b');
# ax.set_ylim(1.14,1.15)
# ax.legend(loc="upper right")
# plt.title("Total energy")
plt.savefig("b3_etot_mean.png")
plt.close('all')

fig, ax = plt.subplots(figsize=(8,6))
ax.plot(times1,e_therm[:nb_diag],c="blue")#,label="E_therm")

ax.set_xlabel('Temps [$\Omega_{SO^{++}}^{-1}$]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('$<E_{therm}>$')#,'fontsize',12,'fontweight','b');
# ax.set_ylim(0.008,0.0085)
# ax.legend(loc="upper right")
# plt.title("Thermal energy")
plt.savefig("b3_etherm_mean.png")
plt.close('all')

fig, ax = plt.subplots(figsize=(8,6))
ax.plot(times1,e_mag[:nb_diag],c="blue")#,label="E_therm")
ax.set_xlabel('Temps [$\Omega_{SO^{++}}^{-1}$]')#,'fontsize',12,'fontweight','b');
ax.set_ylabel('$<E_{mag}>$')#,'fontsize',12,'fontweight','b');
# ax.set_ylim(0.008,0.0085)
# ax.legend(loc="upper right")
# plt.title("Thermal energy")
plt.savefig("b3_e_mag.png")
plt.close('all')
