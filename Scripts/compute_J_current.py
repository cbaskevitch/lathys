#####
# Compute J current by curl(B) = mu0 * J
# input : netcdef Magw file
# output : netcdf file containing the current and magnetic filed components
#####


from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os

# mu0 = 4.*np.pi*0.0000001

ncfile = sys.argv[1] # Magw file
ncid = Dataset(ncfile)
var_nc = ncid.variables

centr      = var_nc['s_centr'][:]
radius     = var_nc['r_planet'][:]
r_iono     = var_nc['r_iono'][:]
gs         = var_nc['gstep'][:]
Bx         = var_nc["Bx"][:]
By         = var_nc["By"][:]
Bz         = var_nc["Bz"][:]

nc = [len(Bx[0][0]),len(Bx[0]),len(Bx)]
gstep_invq = 1/(4.0*gs[:])

J = np.zeros((nc[2],nc[1],nc[0]))
Jx = np.zeros((nc[2],nc[1],nc[0]))
Jy = np.zeros((nc[2],nc[1],nc[0]))
Jz = np.zeros((nc[2],nc[1],nc[0]))

dJx = np.zeros((nc[2],nc[1],nc[0])) #Jxparalelle
dJy = np.zeros((nc[2],nc[1],nc[0])) #Jyparalelle
dJz = np.zeros((nc[2],nc[1],nc[0])) #Jzparalelle

Jxperp = np.zeros((nc[2],nc[1],nc[0])) #Jxperp
Jyperp = np.zeros((nc[2],nc[1],nc[0])) #Jyperp
Jzperp = np.zeros((nc[2],nc[1],nc[0])) #Jzperp

selector = np.zeros((8,3))
selector[:,0] = gstep_invq[0]*np.array([-1., 1.,-1., 1.,-1., 1.,-1., 1.])
selector[:,1] = gstep_invq[1]*np.array([-1.,-1., 1., 1.,-1.,-1., 1., 1.])
selector[:,2] = gstep_invq[2]*np.array([-1.,-1.,-1.,-1., 1., 1., 1., 1.])

curl_bx=0
curl_by=0
curl_bz=0
for ii in range(1,nc[0]):
    for jj in range(1,nc[1]):
        for kk in range(1,nc[2]):
            b_eightx = np.array([Bx[kk-1,jj-1,ii-1],Bx[kk-1,jj-1,ii], Bx[kk-1,jj,ii-1], Bx[kk-1,jj,ii], Bx[kk,jj-1,ii-1], Bx[kk,jj-1,ii], Bx[kk,jj,ii-1], Bx[kk,jj,ii]])#*1e-9
            b_eighty = np.array([By[kk-1,jj-1,ii-1],By[kk-1,jj-1,ii], By[kk-1,jj,ii-1], By[kk-1,jj,ii], By[kk,jj-1,ii-1], By[kk,jj-1,ii], By[kk,jj,ii-1], By[kk,jj,ii]])#*1e-9
            b_eightz = np.array([Bz[kk-1,jj-1,ii-1],Bz[kk-1,jj-1,ii], Bz[kk-1,jj,ii-1], Bz[kk-1,jj,ii], Bz[kk,jj-1,ii-1], Bz[kk,jj-1,ii], Bz[kk,jj,ii-1], Bz[kk,jj,ii]])#*1e-9
            # print(b_eightx)
            # b_eightx[:] = b_eightx[:]*1e-9
            # b_eighty[:] = b_eighty[:]*1e-9
            # b_eightz[:] = b_eightz[:]*1e-9

            curl_bx = np.sum(selector[:,1]*b_eightz[:]-selector[:,2]*b_eighty[:])
            curl_by = np.sum(selector[:,2]*b_eightx[:]-selector[:,0]*b_eightz[:])
            curl_bz = np.sum(selector[:,0]*b_eighty[:]-selector[:,1]*b_eightx[:])

            Jx[kk,jj,ii] = curl_bx#/mu0
            Jy[kk,jj,ii] = curl_by#/mu0
            Jz[kk,jj,ii] = curl_bz#/mu0
            J[kk,jj,ii] = np.sqrt(Jx[kk,jj,ii]**2 + Jy[kk,jj,ii]**2 + Jz[kk,jj,ii]**2)

            bix = 1/8*np.sum(b_eightx)
            biy = 1/8*np.sum(b_eighty)
            biz = 1/8*np.sum(b_eightz)

            bi_dot_J = bix*curl_bx + biy*curl_by + biz*curl_bz
            bi_dot_bi = bix**2 + biy**2 + biz**2
            dJx[kk,jj,ii] = bix * bi_dot_J/bi_dot_bi
            dJy[kk,jj,ii] = biy * bi_dot_J/bi_dot_bi
            dJz[kk,jj,ii] = biz * bi_dot_J/bi_dot_bi

            Jxperp[kk,jj,ii] = Jx[kk,jj,ii] - dJx[kk,jj,ii]
            Jyperp[kk,jj,ii] = Jy[kk,jj,ii] - dJy[kk,jj,ii]
            Jzperp[kk,jj,ii] = Jz[kk,jj,ii] - dJz[kk,jj,ii]
       

# for ii in range(0,nc[0]-1):
#     for jj in range(0,nc[1]-1):
#         for kk in range(0,nc[2]-1):
#             # Jx = d(Bz)/dy - d(By)/dz
#             Jx[kk,jj,ii] = 1/4*(Bz[kk,jj+1,ii] + Bz[kk,jj+1,ii+1]+Bz[kk+1,jj+1,ii+1]+Bz[kk+1,jj+1,ii]) - 1/4*(Bz[kk,jj,ii] + Bz[kk,jj,ii+1]+Bz[kk+1,jj,ii+1]+Bz[kk+1,jj,ii]) - (1/4*(By[kk+1,jj,ii] + By[kk+1,jj,ii+1]+By[kk+1,jj+1,ii+1]+By[kk+1,jj+1,ii]) - 1/4*(By[kk,jj,ii] + By[kk,jj,ii+1]+By[kk,jj+1,ii+1]+By[kk,jj+1,ii]))
#             # Jy = d(Bx)/dz - d(Bz)/dx
#             Jy[kk,jj,ii] = 1/4*(Bx[kk+1,jj,ii] + Bx[kk+1,jj,ii+1]+Bx[kk+1,jj+1,ii+1]+Bx[kk+1,jj+1,ii]) - 1/4*(Bx[kk,jj,ii] + Bx[kk,jj,ii+1]+Bx[kk,jj+1,ii+1]+Bx[kk,jj+1,ii]) - (1/4*(Bz[kk,jj,ii+1] + Bz[kk+1,jj,ii+1]+Bz[kk+1,jj+1,ii+1]+Bz[kk,jj+1,ii+1]) - 1/4*(Bz[kk,jj,ii] + Bz[kk+1,jj,ii]+Bz[kk+1,jj+1,ii]+Bz[kk,jj+1,ii]))
#             # Jz = d(By)/dx - d(Bx)/dy
#             Jz[kk,jj,ii] = 1/4*(By[kk,jj,ii+1] + By[kk+1,jj,ii+1]+By[kk+1,jj+1,ii+1]+By[kk,jj+1,ii+1]) - 1/4*(By[kk,jj,ii] + By[kk+1,jj,ii]+By[kk+1,jj+1,ii]+By[kk,jj+1,ii]) - (1/4*(Bx[kk,jj+1,ii] + Bx[kk,jj+1,ii+1]+Bx[kk+1,jj+1,ii+1]+Bx[kk+1,jj+1,ii]) - 1/4*(Bx[kk,jj,ii] + Bx[kk,jj,ii+1]+Bx[kk+1,jj,ii+1]+Bx[kk+1,jj,ii]))
            
#             bix = 1/8*(Bx[kk+1,jj+1,ii+1] + Bx[kk+1,jj+1,ii] + Bx[kk+1,jj,ii+1] + Bx[kk+1,jj,ii] + Bx[kk,jj+1,ii+1] + Bx[kk,jj+1,ii] + Bx[kk,jj,ii+1] + Bx[kk,jj,ii])
#             biy = 1/8*(By[kk+1,jj+1,ii+1] + By[kk+1,jj+1,ii] + By[kk+1,jj,ii+1] + By[kk+1,jj,ii] + By[kk,jj+1,ii+1] + By[kk,jj+1,ii] + By[kk,jj,ii+1] + By[kk,jj,ii])
#             biz = 1/8*(Bz[kk+1,jj+1,ii+1] + Bz[kk+1,jj+1,ii] + Bz[kk+1,jj,ii+1] + Bz[kk+1,jj,ii] + Bz[kk,jj+1,ii+1] + Bz[kk,jj+1,ii] + Bz[kk,jj,ii+1] + Bz[kk,jj,ii])

#             bi_dot_J = bix*Jx[kk,jj,ii] + biy*Jy[kk,jj,ii] + biz*Jz[kk,jj,ii]
#             bi_dot_bi = bix**2 + biy**2 + biz**2
#             dJx[kk,jj,ii] = bix * bi_dot_J/bi_dot_bi
#             dJy[kk,jj,ii] = biy * bi_dot_J/bi_dot_bi
#             dJz[kk,jj,ii] = biz * bi_dot_J/bi_dot_bi

#             Jxperp[kk,jj,ii] = Jx[kk,jj,ii] - dJx[kk,jj,ii]
#             Jyperp[kk,jj,ii] = Jy[kk,jj,ii] - dJy[kk,jj,ii]
#             Jzperp[kk,jj,ii] = Jz[kk,jj,ii] - dJz[kk,jj,ii]
#             J[kk,jj,ii] = np.sqrt(Jx[kk,jj,ii]**2 + Jy[kk,jj,ii]**2 + Jz[kk,jj,ii]**2)
       



nc_out = Dataset(sys.argv[1], "r+")

# size_x_out = nc_out.createDimension("size_x",152)
# size_y_out = nc_out.createDimension("size_y",306)
# size_z_out = nc_out.createDimension("size_z",466)

# out_Bx = nc_out.createVariable("Bx","f8",("size_z","size_y","size_x"))
# out_By = nc_out.createVariable("By","f8",("size_z","size_y","size_x"))
# out_Bz = nc_out.createVariable("Bz","f8",("size_z","size_y","size_x"))
# out_Bx.units = "nT"
# out_By.units = "nT"
# out_Bz.units = "nT"
# out_Bx[:,:,:] = Bx[:,:,:]
# out_By[:,:,:] = By[:,:,:]
# out_Bz[:,:,:] = Bz[:,:,:]

## Jtot
out_Jx = nc_out.createVariable("Jx","f4",("size_z","size_y","size_x"))
out_Jy = nc_out.createVariable("Jy","f4",("size_z","size_y","size_x"))
out_Jz = nc_out.createVariable("Jz","f4",("size_z","size_y","size_x"))
out_J  = nc_out.createVariable("J","f4",("size_z","size_y","size_x"))
# out_Jx.units = "A.m-2"
# out_Jy.units = "A.m-2"
# out_Jz.units = "A.m-2"
# out_J.units  = "A.m-2"
out_Jx[:,:,:] = Jx[:,:,:]
out_Jy[:,:,:] = Jy[:,:,:]
out_Jz[:,:,:] = Jz[:,:,:]
out_J[:,:,:]  = J[:,:,:]

## Jpara
out_dJx = nc_out.createVariable("Jxpara","f4",("size_z","size_y","size_x"))
out_dJy = nc_out.createVariable("Jypara","f4",("size_z","size_y","size_x"))
out_dJz = nc_out.createVariable("Jzpara","f4",("size_z","size_y","size_x"))
# out_Jx.units = "A.m-2"
# out_Jy.units = "A.m-2"
# out_Jz.units = "A.m-2"
out_dJx[:,:,:] = dJx[:,:,:]
out_dJy[:,:,:] = dJy[:,:,:]
out_dJz[:,:,:] = dJz[:,:,:]

## Jperp
out_Jxperp = nc_out.createVariable("Jxperp","f4",("size_z","size_y","size_x"))
out_Jyperp = nc_out.createVariable("Jyperp","f4",("size_z","size_y","size_x"))
out_Jzperp = nc_out.createVariable("Jzperp","f4",("size_z","size_y","size_x"))
# out_Jx.units = "A.m-2"
# out_Jy.units = "A.m-2"
# out_Jz.units = "A.m-2"
out_Jxperp[:,:,:] = Jxperp[:,:,:]
out_Jyperp[:,:,:] = Jyperp[:,:,:]
out_Jzperp[:,:,:] = Jzperp[:,:,:]

nc_out.close()


