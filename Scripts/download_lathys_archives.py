import sys, os

### Download results files of simulation (Read_moment_species, Thew, Magw, Elew and species files)
### @param src_dir : source directory where files are stored
### @param dest_dir : destination directory
### @param date : date of the simualtion
### @param time : time of the simulation

serveur = "cbaskevitch@ciclad.ipsl.jussieu.fr:"
# serveur = "cbaskevitch@camelot.ipsl.polytechnique.fr:"

if len(sys.argv) < 4:
	print("python download_lathys_res_files.py <path_to_source_directory> <path_to_dest_directory> nproc")

dir_src = sys.argv[1]+"/"
dir_dest = sys.argv[2]+"/"
#date = sys.argv[3]
#time = sys.argv[4]
nproc = int(sys.argv[3])
Np3 = 8

for j in range(1,Np3+1):
    cmd = "mkdir "+dir_dest+"/p3_"+str(j)
    print(cmd)
    os.system(cmd)

cmd = "mkdir "+dir_dest+"/Atm3 "+dir_dest+"/c3 "+dir_dest+"/Mom3 "+dir_dest+"/r "+dir_dest+"/Pro3 "
print(cmd)
os.system(cmd)

# for n in range(46,nproc):
#     if n < 10:
#         pstr="00"+str(n)
#     elif n < 100:
#         pstr="0"+str(n)
#     else:
#         pstr=str(n)
#     cmd = "scp "+serveur+dir_src+"/outputs/Atm3_"+pstr+"_* "+dir_dest+"/Atm3"#+date+"_t"+time+".dat "+dir_dest
#     print(cmd)
#     os.system(cmd)
# cmd = "tar -czvf "+dir_dest+"Atm3.tar.gz "+dir_dest+"Atm3"
# print(cmd)
# os.system(cmd)

# for n in range(54,nproc):
#     if n < 10:
#         pstr="00"+str(n)
#     elif n < 100:
#         pstr="0"+str(n)
#     else:
#         pstr=str(n)
#     cmd = "scp "+serveur+dir_src+"/outputs/c3_"+pstr+"_* "+dir_dest+"/c3"#+date+"_t"+time+".dat "+dir_dest
#     print(cmd)
#     os.system(cmd)
# cmd = "tar -czvf "+dir_dest+"c3.tar.gz "+dir_dest+"c3"
# print(cmd)
# os.system(cmd)

# for n in range(46,nproc):
#     if n < 10:
#         pstr="00"+str(n)
#     elif n < 100:
#         pstr="0"+str(n)
#     else:
#         pstr=str(n)
#     cmd = "scp "+serveur+dir_src+"/outputs/Mom3_"+pstr+"_* "+dir_dest+"/Mom3"#+date+"_t"+time+".dat "+dir_dest
#     print(cmd)
#     os.system(cmd)
# cmd = "tar -czvf "+dir_dest+"Mom3.tar.gz "+dir_dest+"Mom3"
# print(cmd)
# os.system(cmd)

# for n in range(0,nproc):
#     if n < 10:
#         pstr="00"+str(n)
#     elif n < 100:
#         pstr="0"+str(n)
#     else:
#         pstr=str(n)
#     for j in range(1,Np3+1):
#         if nproc*(j-1)/Np3 <= n and n < nproc*j/Np3:
#             cmd = "scp "+serveur+dir_src+"/outputs/p3_"+pstr+"_* "+dir_dest+"/p3_"+str(j)#+date+"_t"+time+".dat "+dir_dest
#             print(cmd)
#             os.system(cmd)
# for j in range(1,Np3+1):
#     cmd = "tar -czvf "+dir_dest+"p3_"+str(j)+".tar.gz "+dir_dest+"p3_"+str(j)
#     print(cmd)
#     os.system(cmd)

for n in range(0,nproc):
    if n < 10:
        pstr="00"+str(n)
    elif n < 100:
        pstr="0"+str(n)
    else:
        pstr=str(n)
    cmd = "scp "+serveur+dir_src+"/outputs/Pro3_"+pstr+"_* "+dir_dest+"/Pro3"#+date+"_t"+time+".dat "+dir_dest
    print(cmd)
    os.system(cmd)
cmd = "tar -czvf "+dir_dest+"Pro3.tar.gz "+dir_dest+"Pro3"
print(cmd)
os.system(cmd)

for n in range(0,nproc):
    if n < 10:
        pstr="00"+str(n)
    elif n < 100:
        pstr="0"+str(n)
    else:
        pstr=str(n)
    cmd = "scp "+serveur+dir_src+"/outputs/r_"+pstr+"_* "+dir_dest+"/r"
    print(cmd)
    os.system(cmd)
cmd = "tar -czvf "+dir_dest+"r.tar.gz "+dir_dest+"r"
print(cmd)
os.system(cmd)

cmd = "scp "+serveur+dir_src+"/infos "+dir_dest
print(cmd)
os.system(cmd)

cmd = "scp "+serveur+dir_src+"/quiet_plasma "+dir_dest
print(cmd)
os.system(cmd)

cmd = "scp "+serveur+dir_src+"/hyb3d_* "+dir_dest
print(cmd)
os.system(cmd)

cmd = "scp "+serveur+dir_src+"/sortie_europa "+dir_dest
print(cmd)
os.system(cmd)
#Read_moment_species










