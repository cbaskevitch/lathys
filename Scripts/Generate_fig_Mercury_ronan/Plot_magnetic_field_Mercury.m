%%%%%%%%%%%%%%%%%%%%%%
% Plot_magnetic_field_Mercury.m
%---------------------
% This routine reads the magnetic
% field file and plot the module
% and the B components in 
% the XY and XZ plane
%
% R. Modolo
% UVSQ-LATMOS
% ronan.modolo@latmos.ipsl.fr
% December 2011
%%%%%%%%%%%%%%%%%%%%%%

typefile = 'Magw_';
ncfile = [dirname typefile runname diagtime '.nc'];

ncid = netcdf.open(ncfile,'NC_NOWRITE');
% Get information about the contents of the file.
[numdims, numvars, numglobalatts, unlimdimID] = netcdf.inq(ncid);
nom_variable ='' ;

for i=0:numvars-1

[varname, xtype, dimids, numatts] = netcdf.inqVar(ncid,i);

nom_variable = char(nom_variable, varname);

end

planetname = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'planetname')));
centr      = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'s_centr')));
radius     = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'r_planet')));
gs=transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'gstep')));
nptot=transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'nptot')));
Bx         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Bx'));
By         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'By'));
Bz         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Bz'));
nrm = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'phys_mag'));
len =  netcdf.getVar(ncid,netcdf.inqVarID(ncid,'phys_length'));
%radius=radius/gs(1);
%centr=centr./gs;
nc = size(Bx);
netcdf.close(ncid);

Btot  = sqrt(Bx.^2+By.^2+Bz.^2);

%Bx = Bx*nrm*1.e9;  % values in nT
%By = By*nrm*1.e9;  % values in nT
%Bz = Bz*nrm*1.e9;  % values in nT
%Btot = Btot*nrm*1.e9; % values in nT

% maximum and minimum 
min_val = -50.; % nT
max_val = 50.;  % nT
min_valtot = 0.; % nT
max_valtot = 150.; % nT


% -- Creation of axis values centered on the planet ( nromalized to planet
% radius)
[X_XY,Y_XY] = meshgrid(0:gs(1):(nc(1)-1)*gs(1),0:gs(2):(nc(2)-1)*gs(2));
X_XY = X_XY'/radius-(centr(1)+0.5*gs(1))*ones(nc(1),nc(2))/radius;
Y_XY = Y_XY'/radius-(centr(2)+0.5*gs(2))*ones(nc(1),nc(2))/radius;

[X_XZ,Z_XZ] = meshgrid(0:gs(1):(nc(1)-1)*gs(1),0:gs(3):(nc(3)-1)*gs(3));
X_XZ = X_XZ'/radius-(centr(1)+0.5*gs(1))*ones(nc(1),nc(3))/radius;
Z_XZ = Z_XZ'/radius-(centr(3)+0.5*gs(3))*ones(nc(1),nc(3))/radius;

[Y_YZ,Z_YZ] = meshgrid(0:gs(2):(nc(2)-1)*gs(2),0:gs(3):(nc(3)-1)*gs(3));
Y_YZ = Y_YZ'/radius-(centr(2)+0.5*gs(2))*ones(nc(2),nc(3))/radius;
Z_YZ = Z_YZ'/radius-(centr(3)+0.5*gs(3))*ones(nc(2),nc(3))/radius;



% planet center in cell number (NB: cell number start at 1
icentr = fix(centr(1)/gs(1))+1;
jcentr = fix(centr(2)/gs(2))+1;
kcentr = fix(centr(3)/gs(3))+1;
iwake = icentr + fix(1.5*radius/gs(1));

Bx_XY      = zeros(nc(1),nc(2));
Bx_XY(:,:) = Bx(:,:,kcentr);
By_XY      = zeros(nc(1),nc(2));
By_XY(:,:) = By(:,:,kcentr);
Bz_XY      = zeros(nc(1),nc(2));
Bz_XY(:,:) = Bz(:,:,kcentr);
Btot_XY    = zeros(nc(1),nc(2));
Btot_XY(:,:) = Btot(:,:,kcentr);

Bx_XZ      = zeros(nc(1),nc(3));
Bx_XZ(:,:) = Bx(:,jcentr,:);
By_XZ      = zeros(nc(1),nc(3));
By_XZ(:,:)  = By(:,jcentr,:);
Bz_XZ      = zeros(nc(1),nc(3));
Bz_XZ(:,:)  = Bz(:,jcentr,:);
Btot_XZ    = zeros(nc(1),nc(3));
Btot_XZ(:,:) = Btot(:,jcentr,:);

Bx_YZ_term      = zeros(nc(2),nc(3));
Bx_YZ_term(:,:) = Bx(icentr,:,:);
By_YZ_term      = zeros(nc(2),nc(3));
By_YZ_term(:,:) = By(icentr,:,:);
Bz_YZ_term      = zeros(nc(2),nc(3));
Bz_YZ_term(:,:) = Bz(icentr,:,:);
Btot_YZ_term    = zeros(nc(2),nc(3));
Btot_YZ_term(:,:) = Btot(icentr,:,:);

Bx_YZ_wake      = zeros(nc(2),nc(3));
Bx_YZ_wake(:,:) = Bx(iwake,:,:);
By_YZ_wake      = zeros(nc(2),nc(3));
By_YZ_wake(:,:) = By(iwake,:,:);
Bz_YZ_wake      = zeros(nc(2),nc(3));
Bz_YZ_wake(:,:) = Bz(iwake,:,:);
Btot_YZ_wake    = zeros(nc(2),nc(3));
Btot_YZ_wake(:,:) = Btot(iwake,:,:);


Bx_1D = zeros(nc(1),1);
Bx_1D = Bx(:,jcentr,kcentr);
By_1D = zeros(nc(1),1);
By_1D = By(:,jcentr,kcentr);
Bz_1D = zeros(nc(1),1);
Bz_1D = Bz(:,jcentr,kcentr);
x = ([0:nc(1)-1]*gs(1)-centr(1))./radius;

savefile = [dirname runname '_1D_' diagtime '.mat'];
save(savefile,'x','Dn_1D','Dne_1D','Bx_1D','By_1D','Bz_1D');

%-- Bow shock parameter
% Extracted from Trotignon et al, PSS, 2006
xshock = ([1:0.1:nc(1)]*gs(1)-centr(1))/radius;
eps = 1.026;
L = 2.081;
x_F = 0.6;
yshock = NaN.*zeros(1,size(xshock,2));
for i=1:size(xshock,2)
  yshock(i) = pos_boundary(xshock(i),x_F,eps,L);
end

ind = find(isnan(yshock)==1);
yshock(ind(1)) = 0;
yshock = [yshock -yshock];
xshock = [xshock xshock];

%-- IMB parameter
% Extracted from Vignes et al, GRL, 2000
ximb = ([1:0.1:nc(1)]*gs(1)-centr(1))/radius;
eps = 0.9;
L = 0.96;
x_F = 0.78;
yimb = NaN.*zeros(1,size(ximb,2));
for i=1:size(ximb,2)
  yimb(i) = pos_boundary(ximb(i),x_F,eps,L);
end

ind = find(isnan(yimb)==1);
yimb(ind(1)) = 0;
yimb = [yimb -yimb];
ximb = [ximb ximb];

% planet drawing
theta = 2.*pi*[1:101]/100.;
xp = cos(theta);
yp = sin(theta);

%-- Figure 1 & 2 -- Bx
%**************************************************************************
figure(1);
colormap('redblue');
pcolor(X_XY,Y_XY,Bx_XY);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);
hold off;
titre = strcat('Bx [nT] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Bx_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f1','-dpng'); 

%==
figure(2);
colormap('redblue');
pcolor(X_XZ,Z_XZ,Bx_XZ);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);
hold off;
titre = strcat('Bx [nT] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Bx_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f2','-dpng'); 

%-- Figure 3 & 4 -- By
%**************************************************************************
figure(3);
pcolor(X_XY,Y_XY,By_XY);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);
hold off;
titre = strcat('By [nT] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'By_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f3','-dpng'); 

%==
figure(4);
pcolor(X_XZ,Z_XZ,By_XZ);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);
hold off;
titre = strcat('By [nT] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'By_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f4','-dpng'); 

%-- Figure 5 & 6 -- Bz
%**************************************************************************
figure(5);
pcolor(X_XY,Y_XY,Bz_XY);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);
hold off;
titre = strcat('Bz [nT] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Bz_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f5','-dpng'); 

%==
figure(6);
pcolor(X_XZ,Z_XZ,Bz_XZ);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);
hold off;
titre = strcat('Bz [nT] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Bz_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f6','-dpng'); 

%-- Figure 7 & 8 -- Btot
%**************************************************************************
figure(7);
colormap('jet');
pcolor(X_XY,Y_XY,Btot_XY);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);
hold off;
set(gca,'XLim',[-4. 4.]);
set(gca,'YLim',[-5. 5.]);
titre = strcat('Btot [nT] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Btot_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f7','-dpng'); 

%==
figure(8);
colormap('jet');
pcolor(X_XZ,Z_XZ,Btot_XZ);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
  %plot(-xshock,yshock,'k','linewidth',2);
  %plot(-ximb,yimb,'k--','linewidth',2);
hold off;
set(gca,'XLim',[-4. 4.]);
set(gca,'YLim',[-5. 5.]);
titre = strcat('Btot [nT] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Btot_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f8','-dpng'); 

%=========== figure in YZ plane ==============================
% Bx in X=0 and X=1.5Rm

% figure 9 & 10
figure(9);
pcolor(Y_YZ,Z_YZ,Bx_YZ_term);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
hold off;
titre = strcat('Bx [nT] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Bx_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f9','-dpng'); 


figure(10);
pcolor(Y_YZ,Z_YZ,Bx_YZ_wake);
shading flat;

titre = strcat('Bx [nT] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Bx_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f10','-dpng');


% By in X=0 and X=1.5Rm
% figure 11 & 12
figure(11);
pcolor(Y_YZ,Z_YZ,By_YZ_term);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
hold off;
titre = strcat('By [nT] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'By_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f11','-dpng'); 


figure(12);
pcolor(Y_YZ,Z_YZ,By_YZ_wake);
shading flat;

titre = strcat('By [nT] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'By_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f12','-dpng');

% Bz in X=0 and X=1.5Rm
% figure 13 & 14
figure(13);
pcolor(Y_YZ,Z_YZ,Bz_YZ_wake);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
hold off;
titre = strcat('Bz [nT] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Bz_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f13','-dpng'); 


figure(14);
pcolor(Y_YZ,Z_YZ,Bz_YZ_wake);
shading flat;

titre = strcat('Bz [nT] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Bz_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f14','-dpng');

% Btot in X=0 and X=1.5Rm
% figure 15 & 16
figure(15);
pcolor(Y_YZ,Z_YZ,Btot_YZ_wake);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
hold off;
titre = strcat('Btot [nT] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Btot_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f15','-dpng'); 


figure(16);
pcolor(Y_YZ,Z_YZ,Btot_YZ_wake);
shading flat;

titre = strcat('Btot [nT] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Btot_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f16','-dpng');
