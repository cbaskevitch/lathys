%%%%%%%%%%%%%%%%%%%%%%
% Plot_Bulk_speed_Mercury.m
%---------------------
% This routine reads the bulk speed
%  file and plot the module
% and the V components in 
% the XY and XZ plane
%
% R. Modolo
% UVSQ-LATMOS
% ronan.modolo@latmos.ipsl.fr
% December 2011
%%%%%%%%%%%%%%%%%%%%%%

typefile = 'Thew_';
ncfile = [dirname typefile runname diagtime '.nc'];

ncid = netcdf.open(ncfile,'NC_NOWRITE');
% Get information about the contents of the file.
[numdims, numvars, numglobalatts, unlimdimID] = netcdf.inq(ncid);
nom_variable ='' ;

for i=0:numvars-1

[varname, xtype, dimids, numatts] = netcdf.inqVar(ncid,i);

nom_variable = char(nom_variable, varname);

end

planetname = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'planetname')));
centr      = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'s_centr')));
radius     = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'r_planet')));
gs=transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'gstep')));
nptot=transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'nptot')));
Vx         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Ux'));
Vy         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Uy'));
Vz         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Uz'));
nrm = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'phys_speed'));
%radius=radius/gs(1);
%centr=centr./gs;
nc = size(Vx);
netcdf.close(ncid);

Vtot  = sqrt(Vx.^2+Vy.^2+Vz.^2);

%Vx = Vx*nrm;  % values in km/s
%Vy = Vy*nrm;  % values in km/s
%Vz = Vz*nrm;  % values in km/s
%Vtot = Vtot*nrm; % values in km/s

% maximum and minimum 
min_val = -100.; % km/s
max_val = 100.;  % km/s
min_valtot = 0.; % km/s
max_valtot = 600.; % km/s


% -- Creation of axis values centered on the planet ( nromalized to planet
% radius)
[X_XY,Y_XY] = meshgrid(0:gs(1):(nc(1)-1)*gs(1),0:gs(2):(nc(2)-1)*gs(2));
X_XY = X_XY'/radius-(centr(1)+0.5*gs(1))*ones(nc(1),nc(2))/radius;
Y_XY = Y_XY'/radius-(centr(2)+0.5*gs(2))*ones(nc(1),nc(2))/radius;

[X_XZ,Z_XZ] = meshgrid(0:gs(1):(nc(1)-1)*gs(1),0:gs(3):(nc(3)-1)*gs(3));
X_XZ = X_XZ'/radius-(centr(1)+0.5*gs(1))*ones(nc(1),nc(3))/radius;
Z_XZ = Z_XZ'/radius-(centr(3)+0.5*gs(3))*ones(nc(1),nc(3))/radius;

[Y_YZ,Z_YZ] = meshgrid(0:gs(2):(nc(2)-1)*gs(2),0:gs(3):(nc(3)-1)*gs(3));
Y_YZ = Y_YZ'/radius-(centr(2)+0.5*gs(2))*ones(nc(2),nc(3))/radius;
Z_YZ = Z_YZ'/radius-(centr(3)+0.5*gs(3))*ones(nc(2),nc(3))/radius;

% planet center in cell number (NB: cell number start at 1
icentr = fix(centr(1)/gs(1))+1;
jcentr = fix(centr(2)/gs(2))+1;
kcentr = fix(centr(3)/gs(3))+1;
iwake = icentr + fix(1.5*radius/gs(1));

Vx_XY      = zeros(nc(1),nc(2));
Vx_XY(:,:) = Vx(:,:,kcentr);
Vy_XY      = zeros(nc(1),nc(2));
Vy_XY(:,:) = Vy(:,:,kcentr);
Vz_XY      = zeros(nc(1),nc(2));
Vz_XY(:,:) = Vz(:,:,kcentr);
Vtot_XY    = zeros(nc(1),nc(2));
Vtot_XY(:,:) = Vtot(:,:,kcentr);

Vx_XZ      = zeros(nc(1),nc(3));
Vx_XZ(:,:) = Vx(:,jcentr,:);
Vy_XZ      = zeros(nc(1),nc(3));
Vy_XZ(:,:)  = Vy(:,jcentr,:);
Vz_XZ      = zeros(nc(1),nc(3));
Vz_XZ(:,:)  = Vz(:,jcentr,:);
Vtot_XZ    = zeros(nc(1),nc(3));
Vtot_XZ(:,:) = Vtot(:,jcentr,:);

Vx_YZ_term      = zeros(nc(2),nc(3));
Vx_YZ_term(:,:) = Vx(icentr,:,:);
Vy_YZ_term      = zeros(nc(2),nc(3));
Vy_YZ_term(:,:) = Vy(icentr,:,:);
Vz_YZ_term      = zeros(nc(2),nc(3));
Vz_YZ_term(:,:) = Vz(icentr,:,:);
Vtot_YZ_term    = zeros(nc(2),nc(3));
Vtot_YZ_term(:,:) = Vtot(icentr,:,:);

Vx_YZ_wake      = zeros(nc(2),nc(3));
Vx_YZ_wake(:,:) = Vx(iwake,:,:);
Vy_YZ_wake      = zeros(nc(2),nc(3));
Vy_YZ_wake(:,:) = Vy(iwake,:,:);
Vz_YZ_wake      = zeros(nc(2),nc(3));
Vz_YZ_wake(:,:) = Vz(iwake,:,:);
Vtot_YZ_wake    = zeros(nc(2),nc(3));
Vtot_YZ_wake(:,:) = Vtot(iwake,:,:);

% planet drawing
theta = 2.*pi*[1:101]/100.;
xp = cos(theta);
yp = sin(theta);

%-- Figure 1 & 2 -- Vx
%**************************************************************************
figure(1);
colormap('jet');
pcolor(X_XY,Y_XY,Vx_XY);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);
hold off;
titre = strcat('Vx [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Vx_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f1','-dpng'); 

%==
figure(2);
colormap('jet');
pcolor(X_XZ,Z_XZ,Vx_XZ);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Vx [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Vx_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f2','-dpng'); 

%-- Figure 3 & 4 -- Vy
%**************************************************************************
figure(3);
colormap('jet');
pcolor(X_XY,Y_XY,Vy_XY);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Vy [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Vy_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f3','-dpng'); 

%==
figure(4);
pcolor(X_XZ,Z_XZ,Vy_XZ);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Vy [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Vy_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f4','-dpng'); 

%-- Figure 5 & 6 -- Vz
%**************************************************************************
figure(5);
colormap('jet');
pcolor(X_XY,Y_XY,Vz_XY);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Vz [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Vz_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f5','-dpng'); 

%==
figure(6);
colormap('jet');
pcolor(X_XZ,Z_XZ,Vz_XZ);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
  %plot(-xshock,yshock,'k','linewidth',2);
  %plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Vz [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Vz_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f6','-dpng'); 

%-- Figure 7 & 8 -- Vtot
%**************************************************************************
figure(7);
colormap('jet');
pcolor(X_XY,Y_XY,Vtot_XY);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Vtot [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Vtot_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f7','-dpng'); 

%==
figure(8);
colormap('jet');
pcolor(X_XZ,Z_XZ,Vtot_XZ);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Vtot [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Vtot_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f8','-dpng'); 


%=========== figure in YZ plane ==============================
% Vx in X=0 and X=1.5Rm

% figure 9 & 10
figure(9);
colormap('jet');
pcolor(Y_YZ,Z_YZ,Vx_YZ_term);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
hold off;
titre = strcat('Vx [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Vx_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f9','-dpng'); 


figure(10);
colormap('jet');
pcolor(Y_YZ,Z_YZ,Vx_YZ_wake);
shading flat;

titre = strcat('Vx [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Vx_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f10','-dpng');


% Vy in X=0 and X=1.5Rm
% figure 11 & 12
figure(11);
colormap('jet');
pcolor(Y_YZ,Z_YZ,Vy_YZ_term);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
hold off;
titre = strcat('Vy [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Vy_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f11','-dpng'); 


figure(12);
colormap('jet');
pcolor(Y_YZ,Z_YZ,Vy_YZ_wake);
shading flat;

titre = strcat('Vy [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Vy_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f12','-dpng');

% Vz in X=0 and X=1.5Rm
% figure 13 & 14
figure(13);
colormap('jet');
pcolor(Y_YZ,Z_YZ,Vz_YZ_wake);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
hold off;
titre = strcat('Vz [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Vz_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f13','-dpng'); 


figure(14);
colormap('jet');
pcolor(Y_YZ,Z_YZ,Vz_YZ_wake);
shading flat;

titre = strcat('Vz [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Vz_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f14','-dpng');

% Vtot in X=0 and X=1.5Rm
% figure 15 & 16
figure(15);
colormap('jet');
pcolor(Y_YZ,Z_YZ,Vtot_YZ_wake);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
hold off;
titre = strcat('Vtot [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Vtot_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f15','-dpng'); 


figure(16);
colormap('jet');
pcolor(Y_YZ,Z_YZ,Vtot_YZ_wake);
shading flat;

titre = strcat('Vtot [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Vtot_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f16','-dpng');
