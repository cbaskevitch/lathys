%%%%%%%%%%%%%%%%%%%%%%
% Plot_electric_field_Mercury.m
%---------------------
% This routine reads the electric
% field file and plot the module
% and the E components in 
% the XY and XZ plane
%
% R. Modolo
% UVSQ-LATMOS
% ronan.modolo@latmos.ipsl.fr
% December 2011
%%%%%%%%%%%%%%%%%%%%%%

typefile = 'Elew_';
ncfile = [dirname typefile runname diagtime '.nc'];

ncid = netcdf.open(ncfile,'NC_NOWRITE');
% Get information about the contents of the file.
[numdims, numvars, numglobalatts, unlimdimID] = netcdf.inq(ncid);
nom_variable ='' ;

for i=0:numvars-1

[varname, xtype, dimids, numatts] = netcdf.inqVar(ncid,i);

nom_variable = char(nom_variable, varname);

end

planetname = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'planetname')));
centr      = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'s_centr')));
radius     = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'r_planet')));
gs=transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'gstep')));
nptot=transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'nptot')));
Ex         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Ex'));
Ey         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Ey'));
Ez         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Ez'));
nrm1 = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'phys_mag'));
nrm2 = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'phys_speed'));
%radius=radius/gs(1);
%centr=centr./gs;
nc = size(Ex);
netcdf.close(ncid);

Etot  = sqrt(Ex.^2+Ey.^2+Ez.^2);

%nrm = nrm1*nrm2*1.e3*1.e3;
%nrm = 1.e6;
nrm=1.
Ex = Ex*nrm;  % values in mV/m
Ey = Ey*nrm;  % values in mV/m
Ez = Ez*nrm;  % values in mV/m
Etot = Etot*nrm; % values in mV/m

% maximum and minimum 
min_val = -2.; % mV/m
max_val = 2.;  % mV/m
min_valtot = 0.; % mV/m
max_valtot = 10.; % mV/m


% -- Creation of axis values centered on the planet ( nromalized to planet
% radius)
[X_XY,Y_XY] = meshgrid(0:gs(1):(nc(1)-1)*gs(1),0:gs(2):(nc(2)-1)*gs(2));
X_XY = X_XY'/radius-(centr(1)+0.5*gs(1))*ones(nc(1),nc(2))/radius;
Y_XY = Y_XY'/radius-(centr(2)+0.5*gs(2))*ones(nc(1),nc(2))/radius;

[X_XZ,Z_XZ] = meshgrid(0:gs(1):(nc(1)-1)*gs(1),0:gs(3):(nc(3)-1)*gs(3));
X_XZ = X_XZ'/radius-(centr(1)+0.5*gs(1))*ones(nc(1),nc(3))/radius;
Z_XZ = Z_XZ'/radius-(centr(3)+0.5*gs(3))*ones(nc(1),nc(3))/radius;

[Y_YZ,Z_YZ] = meshgrid(0:gs(2):(nc(2)-1)*gs(2),0:gs(3):(nc(3)-1)*gs(3));
Y_YZ = Y_YZ'/radius-(centr(2)+0.5*gs(2))*ones(nc(2),nc(3))/radius;
Z_YZ = Z_YZ'/radius-(centr(3)+0.5*gs(3))*ones(nc(2),nc(3))/radius;

% planet center in cell number (NB: cell number start at 1
icentr = fix(centr(1)/gs(1))+1;
jcentr = fix(centr(2)/gs(2))+1;
kcentr = fix(centr(3)/gs(3))+1;
iwake = icentr + fix(1.5*radius/gs(1));

Ex_XY      = zeros(nc(1),nc(2));
Ex_XY(:,:) = Ex(:,:,kcentr);
Ey_XY      = zeros(nc(1),nc(2));
Ey_XY(:,:) = Ey(:,:,kcentr);
Ez_XY      = zeros(nc(1),nc(2));
Ez_XY(:,:) = Ez(:,:,kcentr);
Etot_XY    = zeros(nc(1),nc(2));
Etot_XY(:,:) = Etot(:,:,kcentr);

Ex_XZ      = zeros(nc(1),nc(3));
Ex_XZ(:,:) = Ex(:,jcentr,:);
Ey_XZ      = zeros(nc(1),nc(3));
Ey_XZ(:,:)  = Ey(:,jcentr,:);
Ez_XZ      = zeros(nc(1),nc(3));
Ez_XZ(:,:)  = Ez(:,jcentr,:);
Etot_XZ    = zeros(nc(1),nc(3));
Etot_XZ(:,:) = Etot(:,jcentr,:);

Ex_YZ_term      = zeros(nc(2),nc(3));
Ex_YZ_term(:,:) = Ex(icentr,:,:);
Ey_YZ_term      = zeros(nc(2),nc(3));
Ey_YZ_term(:,:) = Ey(icentr,:,:);
Ez_YZ_term      = zeros(nc(2),nc(3));
Ez_YZ_term(:,:) = Ez(icentr,:,:);
Etot_YZ_term    = zeros(nc(2),nc(3));
Etot_YZ_term(:,:) = Etot(icentr,:,:);

Ex_YZ_wake      = zeros(nc(2),nc(3));
Ex_YZ_wake(:,:) = Ex(iwake,:,:);
Ey_YZ_wake      = zeros(nc(2),nc(3));
Ey_YZ_wake(:,:) = Ey(iwake,:,:);
Ez_YZ_wake      = zeros(nc(2),nc(3));
Ez_YZ_wake(:,:) = Ez(iwake,:,:);
Etot_YZ_wake    = zeros(nc(2),nc(3));
Etot_YZ_wake(:,:) = Etot(iwake,:,:);

% planet drawing
theta = 2.*pi*[1:101]/100.;
xp = cos(theta);
yp = sin(theta);

%-- Figure 1 & 2 -- Ex
%**************************************************************************
figure(1);
pcolor(X_XY,Y_XY,Ex_XY);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
 % fill(xp,yp,'w');
%  plot(-xshock,yshock,'k','linewidth',2);
%  plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Ex [mV/m] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Ex_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f1','-dpng'); 

%==
figure(2);
pcolor(X_XZ,Z_XZ,Ex_XZ);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
%  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Ex [mV/m] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Ex_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f2','-dpng'); 

%-- Figure 3 & 4 -- Ey
%**************************************************************************
figure(3);
pcolor(X_XY,Y_XY,Ey_XY);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
 % fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Ey [mV/m] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Ey_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f3','-dpng'); 

%==
figure(4);
pcolor(X_XZ,Z_XZ,Ey_XZ);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
 % fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Ey [mV/m] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Ey_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f4','-dpng'); 

%-- Figure 5 & 6 -- Ez
%**************************************************************************
figure(5);
pcolor(X_XY,Y_XY,Ez_XY);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
 % fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Ez [mV/m] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Ez_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f5','-dpng'); 

%==
figure(6);
pcolor(X_XZ,Z_XZ,Ez_XZ);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
%  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Ez [mV/m] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Ez_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f6','-dpng'); 

%-- Figure 7 & 8 -- Etot
%**************************************************************************
figure(7);
pcolor(X_XY,Y_XY,Etot_XY);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
%  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Etot [mV/m] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Etot_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f7','-dpng'); 

%==
figure(8);
pcolor(X_XZ,Z_XZ,Etot_XZ);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
%  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Etot [mV/m] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Etot_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f8','-dpng'); 

%=========== figure in YZ plane ==============================
% Ex in X=0 and X=1.5Rm

% figure 9 & 10
figure(9);
pcolor(Y_YZ,Z_YZ,Ex_YZ_term);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
%  fill(xp,yp,'w');
hold off;
titre = strcat('Ex [mV/m] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Ex_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f9','-dpng'); 


figure(10);
pcolor(Y_YZ,Z_YZ,Ex_YZ_wake);
shading flat;

titre = strcat('Ex [mV/m] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Ex_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f10','-dpng');


% Ey in X=0 and X=1.5Rm
% figure 11 & 12
figure(11);
pcolor(Y_YZ,Z_YZ,Ey_YZ_term);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
%  fill(xp,yp,'w');
hold off;
titre = strcat('Ey [mV/m] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Ey_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f11','-dpng'); 


figure(12);
pcolor(Y_YZ,Z_YZ,Ey_YZ_wake);
shading flat;

titre = strcat('Ey [mV/m] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Ey_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f12','-dpng');

% Ez in X=0 and X=1.5Rm
% figure 13 & 14
figure(13);
pcolor(Y_YZ,Z_YZ,Ez_YZ_term);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
%  fill(xp,yp,'w');
hold off;
titre = strcat('Ez [mV/m] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Ez_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f13','-dpng'); 


figure(14);
pcolor(Y_YZ,Z_YZ,Ez_YZ_wake);
shading flat;

titre = strcat('Ez [mV/m] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Ez_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f14','-dpng');

% Etot in X=0 and X=1.5Rm
% figure 15 & 16
figure(15);
pcolor(Y_YZ,Z_YZ,Etot_YZ_term);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  %fill(xp,yp,'w');
hold off;
titre = strcat('Etot [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Etot_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f15','-dpng'); 


figure(16);
pcolor(Y_YZ,Z_YZ,Etot_YZ_wake);
shading flat;

titre = strcat('Etot [mV/m] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Etot_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f16','-dpng');
