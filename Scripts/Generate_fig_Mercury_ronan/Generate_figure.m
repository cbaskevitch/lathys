%%%%%%%%%%%%%%%%%%%%%%%%
% Generate_Figure.m
%----------------------
% This routine generate automatically
% pdf files of different quantities
% in XY and XZ plane
% Quantities are :
%    - Magnetic field (module and components)
%    - Electric field (module and components)
%    - Electron number density
%    - Bulk speed ( module and components)
%    - Ion density (for each species)
%    - Neutral density (for each species)
%   (- Production for each species)
%
% R. Modolo
% UVSQ - LATMOS
% ronan.modolo@latmos.ipsl.fr
% December 2011
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;

runname = '10_06_20_';
%dirname = 'D:\Mercury_Simulation\2020_02_12_Fatemi_test\';
%dirname='E:\Simu_Mercure\2020_03_02_Fatemi_E=0_inside_planet\';
%dirname = 'D:\Mercury_Simulation\2020_04_03_Sae_simu_H_Na\';
dirname = 'D:\Mercury_Simulation\2020_05_23_Sae_Messenger_comparison\';
%dirname='D:\Mercury_Simulation\2020_04_17_Sae_with_H_Na_large_Hswparticles\';
%dirname='E:\Simu_Mercure\2020_03_02_Fatemi_E=0_inside_planet\';
%dirname = 'C:\Users\modolo\Documents\MISSION\Bepi\Thomas_2020\';
diagtime = 't00100';

% %-- Electron number density
 Plot_density_ne_Mercury;

 %-- Plot ion density
 Plot_ion_density_Mercury
% 
% %-- Magnetic field
 Plot_magnetic_field_Mercury;
% 
% %-- Electric current
% Plot_current_Mercury;
% 
% 
% 
% % %-- Bulk speed
  Plot_Bulk_speed_Mercury;
% % 
% % 
% 
% % %-- Plot neutral
% %Plot_neutral_density_Mercury