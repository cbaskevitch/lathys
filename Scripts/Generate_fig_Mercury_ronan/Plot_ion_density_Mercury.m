%%%%%%%%%%%%%%%%%%%%%%
% Plot_Density_ne_mars.m
%---------------------
% This routine reads the density
%  file and plot the map in 
% the XY and XZ plane
%
% R. Modolo
% UVSQ-LATMOS
% ronan.modolo@latmos.ipsl.fr
% December 2011
%%%%%%%%%%%%%%%%%%%%%%

 filename = [dirname 'Read_moment_species_' runname diagtime '.dat'];
 fid = fopen(filename,'r');
 nb_species = fscanf(fid,'%i');
 species_name =  fgetl(fid)
 namearray = [];
 for i = 1:nb_species
     temp =  fgetl(fid);
     namearray = [namearray; temp];
 end
 fclose(fid);
%typefile = 'Opl_';
%ncfile = [dirname typefile runname diagtime '.nc'];

Dn_1D = zeros(nc(1),nb_species);

for nb=1:nb_species
 nom = namearray(nb,2:size(namearray,2))
 ind = strfind(nom,' ');
 nom = nom(1:ind(1)-1);
 ncfile = [dirname nom];

ncid = netcdf.open(ncfile,'NC_NOWRITE');
% Get information about the contents of the file.
[numdims, numvars, numglobalatts, unlimdimID] = netcdf.inq(ncid);
nom_variable ='' ;

for i=0:numvars-1

[varname, xtype, dimids, numatts] = netcdf.inqVar(ncid,i);

nom_variable = char(nom_variable, varname);

end

planetname = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'planetname')));
centr      = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'s_centr')));
radius     = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'r_planet')));
gs=transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'gstep')));
nptot=transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'nptot')));
Dn         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Density'));
Ux_ion         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Ux'));
Uy_ion         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Uy'));
Uz_ion         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Uz'));
nrm = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'phys_density'));
%radius=radius/gs(1);
%centr=centr./gs;
nc = size(Dn);
netcdf.close(ncid);
%Dn = Dn*nrm;  % values in cm^-3

Utot_ion = sqrt(Ux_ion.^2++Uy_ion.^2+Uz_ion.^2);

% maximum and minimum 
min_val = -2.; % log(cm-3)
max_val = 2.;  % log(sm-3)
min_valtot = 0.; % km/s
max_valtot = 600.; % km/s


% -- Creation of axis values centered on the planet ( nromalized to planet
% radius)
[X_XY,Y_XY] = meshgrid(0:gs(1):(nc(1)-1)*gs(1),0:gs(2):(nc(2)-1)*gs(2));
X_XY = -(X_XY'/radius-(centr(1)+0.5*gs(1))*ones(nc(1),nc(2))/radius);
Y_XY = -(Y_XY'/radius-(centr(2)+0.5*gs(2))*ones(nc(1),nc(2))/radius);

[X_XZ,Z_XZ] = meshgrid(0:gs(1):(nc(1)-1)*gs(1),0:gs(3):(nc(3)-1)*gs(3));
X_XZ = -(X_XZ'/radius-(centr(1)+0.5*gs(1))*ones(nc(1),nc(3))/radius);
Z_XZ = Z_XZ'/radius-(centr(3)+0.5*gs(3))*ones(nc(1),nc(3))/radius;

[Y_YZ,Z_YZ] = meshgrid(0:gs(2):(nc(2)-1)*gs(2),0:gs(3):(nc(3)-1)*gs(3));
Y_YZ = -(Y_YZ'/radius-(centr(2)+0.5*gs(2))*ones(nc(2),nc(3))/radius);
Z_YZ = Z_YZ'/radius-(centr(3)+0.5*gs(3))*ones(nc(2),nc(3))/radius;

% planet center in cell number (NB: cell number start at 1
icentr = fix(centr(1)/gs(1))+1;
jcentr = fix(centr(2)/gs(2))+1;
kcentr = fix(centr(3)/gs(3))+1;
iwake = icentr + fix(2.*radius/gs(1));

Dn_XY      = zeros(nc(1),nc(2));
Dn_XY(:,:) = Dn(:,:,kcentr);
Utot_XY_ion = zeros(nc(1),nc(2)); 
Utot_XY_ion = Utot_ion(:,:,kcentr);

Dn_XZ      = zeros(nc(1),nc(3));
Dn_XZ(:,:) = Dn(:,jcentr,:);
Utot_XZ_ion   = zeros(nc(1),nc(3));
Utot_XZ_ion(:,:) = Utot_ion(:,jcentr,:);

Dn_YZ_term    = zeros(nc(2),nc(3));
Dn_YZ_term(:,:) = Dn(icentr,:,:);

Dn_YZ_wake    = zeros(nc(2),nc(3));
Dn_YZ_wake(:,:) = Dn(iwake,:,:);

%if (nom(1:3) == 'Hsw')

Dn_1D(:,nb) = Dn(:,jcentr,kcentr);
x = ([0:nc(1)-1]*gs(1)-centr(1))./radius;
%end

% planet drawing
theta = 2.*pi*[1:101]/100.;
xp = cos(theta);
yp = sin(theta);

%-- Figure 1 & 2 -- Dn
%**************************************************************************
figure(1);
colormap('jet');
pcolor(X_XY,Y_XY,log10(Dn_XY));
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
 % fill(xp,yp,'w');
 %   plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);
hold off;
ind = strfind(nom,'_');
species = nom(1:ind(1)-1);
%species = 'Opl';

titre = strcat('Dn ',species,' log[cm-3] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');
set(gca,'XLim',[-3. 3.]);
set(gca,'YLim',[-3. 3.]);

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Dn_',species,'_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f1','-dpng'); 

%==
figure(2);
colormap('jet');
pcolor(X_XZ,Z_XZ,log10(Dn_XZ));
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
%  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Dn ',species,' log[cm-3] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');
set(gca,'XLim',[-3. 3.]);
set(gca,'YLim',[-3. 3.]);
c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Dn_',species,'_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f2','-dpng'); 

%=========== figure in YZ plane ==============================
% Dn in X=0 and X=1.5Rm

% figure 3 & 4
figure(3);
colormap('jet');
pcolor(Y_YZ,Z_YZ,log10(Dn_YZ_term));
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
%  fill(xp,yp,'w');
hold off;
titre = strcat('Dn ',species,' log[cm-3] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');
set(gca,'XLim',[-3. 3.]);
set(gca,'YLim',[-3. 3.]);
c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Dn_',species,'_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f3','-dpng'); 


 figure(4);
 colormap('jet');
 pcolor(Y_YZ,Z_YZ,log10(Dn_YZ_wake));
 shading flat;
% 
 titre = strcat('Dn ',species,' log[cm-3] time:',diagtime);
 title(titre,'fontsize',12,'fontweight','b');
 xlabel('Y [R_M]','fontsize',12,'fontweight','b');
 ylabel('Z [R_M]','fontsize',12,'fontweight','b');
 set(gca,'fontsize',12,'fontweight','b');
 
 c_limit = [min_val max_val];
 caxis(c_limit);
 
 h_bar = colorbar('location','Eastoutside');
 scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
 %set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
 set(gca,'DataAspectRatio',[1 1 1]);
 
 filename = strcat(dirname,'Dn_',species,'_YZ_wake_Mercury_',runname,diagtime,'.png');
 print(filename,'-f4','-dpng');

 %-- Figure 1 & 2 -- Dn
%**************************************************************************
figure(5);
colormap('jet');
pcolor(X_XY,Y_XY,Utot_XY_ion);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
 % fill(xp,yp,'w');
 %   plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);
hold off;
ind = strfind(nom,'_');
species = nom(1:ind(1)-1);
%species = 'Opl';

titre = strcat('Utot ',species,' [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');
set(gca,'XLim',[-3. 3.]);
set(gca,'YLim',[-3. 3.]);

c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Utot_',species,'_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f5','-dpng'); 

figure(6);
colormap('jet');
pcolor(X_XZ,Z_XZ,Utot_XZ_ion);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
%  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Utot ',species,' [km/s] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');
set(gca,'XLim',[-3. 3.]);
set(gca,'YLim',[-3. 3.]);
c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Utot_',species,'_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f6','-dpng'); 

end