%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot_density_exosphere.m
%---------------------------
% read density exosphere file
%
% R. Modolo
% LATMOS/UVSQ
% Jan 2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
clc;
clear figure;

filename = 'density_177_183_00000271.nc';
dirname = 'G:\Simu_Mercure\Test_loading_exosphere\';

ncfile = [dirname filename];

ncid = netcdf.open(ncfile,'NC_NOWRITE');
% Get information about the contents of the file.
[numdims, numvars, numglobalatts, unlimdimID] = netcdf.inq(ncid);
nom_variable ='' ;

for i=0:numvars-1

[varname, xtype, dimids, numatts] = netcdf.inqVar(ncid,i);

nom_variable = char(nom_variable, varname);

end

r_upp         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'r_upp'));
r_low         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'r_low'));
theta_upp         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'theta_upp'));
theta_low         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'theta_low'));
phi_upp         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'phi_upp'));
phi_low         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'phi_low'));
Weight         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Weight'));
dt = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'dt'));
Accumul_time_steps          = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Accumul_time_steps'));
name_species          = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'name_species'));
Weight = Weight/(dt*double(Accumul_time_steps));

densH = Weight(:,:,:,1);
densHe = Weight(:,:,:,2);
densNa = Weight(:,:,:,5);

nr = size(r_low,1);
ntheta = size(r_low,2);
nphi = size(r_low,3);

altitude = zeros(nr,1);
theta = zeros(ntheta,1);
phi = zeros(nphi,1);
altitude(:) = r_low(:,1,1);
theta(:) = theta_low(1,:,1);
phi(:) = phi_low(1,1,:);



ncx = 125;
ncy = 150;
dx = 2.;
radius_km = 2440.;

cwpi = 41.15;
rplanet = radius_km/cwpi;
density_H = zeros(ncx,ncy);
kcentr = 75;
jcentr = 75;
icentr = 62.5;
xcentr = icentr*dx;
ycentr = jcentr*dx;
zcentr = kcentr*dx;

altitude = (altitude*1.e-5)/cwpi ;

for i=1:ncx
    for j=1:ncy
%for i=29
%    for j=78

        ssk = (kcentr-1)*dx;
        ssj = (j-1)*dx;
        ssi = (i-1)*dx;
        radius = sqrt((ssk-zcentr)^2+(ssj-ycentr)^2+(ssi-xcentr)^2);
        if (radius < altitude(nr) & (radius > rplanet) )
            x_cdr = -(ssi-xcentr);
            y_cdr = -(ssj-ycentr);
            z_cdr = ssk-zcentr;
            
            tan_p = atan(y_cdr/x_cdr);
            tan_t = atan(sqrt((x_cdr^2+y_cdr^2)/z_cdr^2));
            
            if (z_cdr ==0) 
                tan_t = pi/2.;
            end
            if (z_cdr <0)
                tan_t = pi-tan_t;
            end
            
            if ((x_cdr>0) & (y_cdr<0)) 
                tan_p = pi*2+tan_p;
            end
            if ((x_cdr >0) & (y_cdr ==0))
                tan_p = 0;
            end
            if ((x_cdr < 0.) & (y_cdr < 0.)) 
                tan_p = pi+tan_p;
            end
          if ((x_cdr < 0.) & (y_cdr == 0.)) 
              tan_p = pi;
          end
          if ((x_cdr < 0.) & (y_cdr >0.)) 
              tan_p = pi+tan_p;
          end

          if ((x_cdr == 0.) & (y_cdr > 0.)) 
              tan_p = pi/2.;
          end
          if ((x_cdr == 0.) & (y_cdr < 0.)) 
              tan_p = pi*3./2.;
          end
          if ((x_cdr == 0.) & (y_cdr == 0.)) 
              tan_p = 0.;
          end
          n_alt = 2;
          n_phi = 2;
          n_theta = 1;
          diff_alt = radius-altitude(1);
          diff_phi = tan_p-phi(1);
          diff_theta = pi;
          
          find_alt = false;
          find_theta = false;
          find_phi = false;
            
          while ((find_alt == false) & (n_alt <= nr))
              if (abs(radius-altitude(n_alt)) < abs(diff_alt))
                  diff_alt = radius-altitude(n_alt);
                  n_alt = n_alt+1;
              else
                  find_alt = true;
              end
          end
          % phi
          while ((find_phi == false) & (n_phi <= nphi))
              if (abs(tan_p-phi(n_phi))<abs(diff_phi))
                  diff_phi = tan_p-phi(n_phi);
                  n_phi = n_phi+1;
              else
                  find_phi = true;
              end
          end
          % theta
          while ((find_theta == false) & (n_theta <=ntheta))
              if (abs(tan_t-theta(n_theta)) < abs(diff_theta))
                  diff_theta = tan_t - theta(n_theta);
                  n_theta = n_theta +1;
              else
                  find_theta = true;
              end
          end
             if (find_phi == false)
                 %X = sprintf(' pb phi %f %i %i',tan_p,i,j); 
                 %disp(X);
                 n_phi = 1;
             end
             if (find_theta == false)
                 X = sprintf(' pb theta %f %i %j',tan_t,i,j); 
                 disp(X);
                 n_theta = 1;
             end
             
             Vol0 = 1./3.*(r_upp(n_alt,n_theta,n_phi)^3-...
                 r_low(n_alt,n_theta,n_phi)^3) * ...
                 (phi_upp(n_alt,n_theta,n_phi) - phi_low(n_alt,n_theta,n_phi))*...
                 (cos(theta_low(n_alt,n_theta,n_phi))-cos(theta_upp(n_alt,n_theta,n_phi)));
             if (find_alt == true)
                 density_H(i,j) = densH(n_alt,n_theta,n_phi)/Vol0;
                 density_He(i,j) = densHe(n_alt,n_theta,n_phi)/Vol0;
                 density_Na(i,j) = densNa(n_alt,n_theta,n_phi)/Vol0;
             end
           
          end
        end
    end

        


%DnH_XY = 




