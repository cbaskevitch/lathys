%%%%%%%%%%%%%%%%%%%%%%
% Plot_Density_ne_Mercury.m
%---------------------
% This routine reads the density
%  file and plot the map in 
% the XY and XZ plane
%
% R. Modolo
% UVSQ-LATMOS
% ronan.modolo@latmos.ipsl.fr
% December 2011
%%%%%%%%%%%%%%%%%%%%%%

typefile = 'Thew_';
ncfile = [dirname typefile runname diagtime '.nc'];

ncid = netcdf.open(ncfile,'NC_NOWRITE');
% Get information about the contents of the file.
[numdims, numvars, numglobalatts, unlimdimID] = netcdf.inq(ncid);
nom_variable ='' ;

for i=0:numvars-1

[varname, xtype, dimids, numatts] = netcdf.inqVar(ncid,i);

nom_variable = char(nom_variable, varname);

end

planetname = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'planetname')));
centr      = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'s_centr')));
radius     = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'r_planet')));
gs=transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'gstep')));
nptot=transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'nptot')));
Dn         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Density'));
nrm = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'phys_density'));
nrm_len = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'phys_length'));
%radius=radius/gs(1);
%centr=centr./gs;
nc = size(Dn);
netcdf.close(ncid);

ind = find(Dn <=0);
Dn(ind) = NaN;
%Dn = Dn*nrm*1.e-6;  % values in cm^-3

% maximum and minimum 
min_val = -1.; % log(cm-3)
max_val = 2.5;  % log(sm-3)

% -- Creation of axis values centered on the planet ( nromalized to planet
% radius)
[X_XY,Y_XY] = meshgrid(0:gs(1):(nc(1)-1)*gs(1),0:gs(2):(nc(2)-1)*gs(2));
X_XY = X_XY'/radius-(centr(1)+0.5*gs(1))*ones(nc(1),nc(2))/radius;
Y_XY = Y_XY'/radius-(centr(2)+0.5*gs(2))*ones(nc(1),nc(2))/radius;

[X_XZ,Z_XZ] = meshgrid(0:gs(1):(nc(1)-1)*gs(1),0:gs(3):(nc(3)-1)*gs(3));
X_XZ = X_XZ'/radius-(centr(1)+0.5*gs(1))*ones(nc(1),nc(3))/radius;
Z_XZ = Z_XZ'/radius-(centr(3)+0.5*gs(3))*ones(nc(1),nc(3))/radius;

[Y_YZ,Z_YZ] = meshgrid(0:gs(2):(nc(2)-1)*gs(2),0:gs(3):(nc(3)-1)*gs(3));
Y_YZ = Y_YZ'/radius-(centr(2)+0.5*gs(2))*ones(nc(2),nc(3))/radius;
Z_YZ = Z_YZ'/radius-(centr(3)+0.5*gs(3))*ones(nc(2),nc(3))/radius;

% planet center in cell number (NB: cell number start at 1
icentr = fix(centr(1)/gs(1))+1;
jcentr = fix(centr(2)/gs(2))+1;
kcentr = fix(centr(3)/gs(3))+1;
iwake = icentr + fix(1.5*radius/gs(1));

Dn_XY      = zeros(nc(1),nc(2));
Dn_XY(:,:) = Dn(:,:,kcentr);

Dn_XZ      = zeros(nc(1),nc(3));
Dn_XZ(:,:) = Dn(:,jcentr,:);

Dn_YZ_term    = zeros(nc(2),nc(3));
Dn_YZ_term(:,:) = Dn(icentr,:,:);

Dn_YZ_wake    = zeros(nc(2),nc(3));
Dn_YZ_wake(:,:) = Dn(iwake,:,:);

Dne_1D = zeros(nc(1),1);
Dne_1D = Dn(:,jcentr,kcentr);
x = ([0:nc(1)-1]*gs(1)-centr(1))./radius;



%-- Bow shock parameter
% Extracted from Trotignon et al, PSS, 2006
xshock = ([1:0.1:nc(1)]*gs(1)-centr(1))/radius;
eps = 1.026;
L = 2.081;
x_F = 0.6;
yshock = NaN.*zeros(1,size(xshock,2));
for i=1:size(xshock,2)
  yshock(i) = pos_boundary(xshock(i),x_F,eps,L);
end

ind = find(isnan(yshock)==1);
yshock(ind(1)) = 0;
yshock = [yshock -yshock];
xshock = [xshock xshock];

%-- IMB parameter
% Extracted from Vignes et al, GRL, 2000
ximb = ([1:0.1:nc(1)]*gs(1)-centr(1))/radius;
eps = 0.9;
L = 0.96;
x_F = 0.78;
yimb = NaN.*zeros(1,size(ximb,2));
for i=1:size(ximb,2)
  yimb(i) = pos_boundary(ximb(i),x_F,eps,L);
end

ind = find(isnan(yimb)==1);
yimb(ind(1)) = 0;
yimb = [yimb -yimb];
ximb = [ximb ximb];


% planet drawing
theta = 2.*pi*[1:101]/100.;
xp = cos(theta);
yp = sin(theta);

%-- Figure 1 & 2 -- Dn
%**************************************************************************
figure(1);
pcolor(X_XY,Y_XY,log10(Dn_XY));
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  %fill(xp,yp,'w');
%    plot(-xshock,yshock,'k','linewidth',2);
%  plot(-ximb,yimb,'k--','linewidth',2);
hold off;
titre = strcat('Dn ne log[cm-3] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Dn_ne_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f1','-dpng');
%print(filename,'-f1','-dpng'); 

%==
figure(2);
pcolor(X_XZ,Z_XZ,log10(Dn_XZ));
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
 % fill(xp,yp,'w');
%  plot(-xshock,yshock,'k','linewidth',2);
%  plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Dn ne log[cm-3] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Dn_ne_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f2','-dpng'); 

%=========== figure in YZ plane ==============================
% Dn in X=0 and X=1.5Rm

% figure 3 & 4
figure(3);
pcolor(Y_YZ,Z_YZ,log10(Dn_YZ_term));
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
 % fill(xp,yp,'w');
hold off;
titre = strcat('Dn ne log[cm-3] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Dn_ne_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f3','-dpng'); 


figure(4);
pcolor(Y_YZ,Z_YZ,log10(Dn_YZ_wake));
shading flat;

titre = strcat('Dn ne log[cm-3] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Dn_ne_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f4','-dpng');
