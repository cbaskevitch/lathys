%%%%%%%%%%%%%%%%%%%%%%
% Plot_current_Mercury.m
%---------------------
% This routine reads the electri current
% field file and plot the module
% and the B components in 
% the XY and XZ plane
%
% R. Modolo
% UVSQ-LATMOS
% ronan.modolo@latmos.ipsl.fr
% December 2011
%%%%%%%%%%%%%%%%%%%%%%

typefile = 'Jcur_';
ncfile = [dirname typefile runname diagtime '.nc'];

ncid = netcdf.open(ncfile,'NC_NOWRITE');
% Get information about the contents of the file.
[numdims, numvars, numglobalatts, unlimdimID] = netcdf.inq(ncid);
nom_variable ='' ;

for i=0:numvars-1

[varname, xtype, dimids, numatts] = netcdf.inqVar(ncid,i);

nom_variable = char(nom_variable, varname);

end

planetname = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'planetname')));
centr      = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'s_centr')));
%radius     = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'r_planet')));
gs=transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'gstep')));
%nptot=transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'nptot')));
Jx         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Jx'));
Jy         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Jy'));
Jz         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Jz'));
%nrm = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'phys_mag'));
%len =  netcdf.getVar(ncid,netcdf.inqVarID(ncid,'phys_length'));
%radius=radius/gs(1);
%centr=centr./gs;
nc = size(Jx);
netcdf.close(ncid);

Jtot  = sqrt(Jx.^2+Jy.^2+Jz.^2);

%Jx = Jx*nrm*1.e9;  % values in nt
%Jy = Jy*nrm*1.e9;  % values in nt
%Jz = Jz*nrm*1.e9;  % values in nt
%Jtot = Jtot*nrm*1.e9; % values in nt

% maximum and minimum 
min_val = -150.; % nt
max_val = 150.;  % nt
min_valtot = 0.; % nt
max_valtot = 500.; % nt


% -- Creation of axis values centered on the planet ( nromalized to planet
% radius)
[X_XY,Y_XY] = meshgrid(0:gs(1):(nc(1)-1)*gs(1),0:gs(2):(nc(2)-1)*gs(2));
X_XY = X_XY'/radius-(centr(1)+0.5*gs(1))*ones(nc(1),nc(2))/radius;
Y_XY = Y_XY'/radius-(centr(2)+0.5*gs(2))*ones(nc(1),nc(2))/radius;

[X_XZ,Z_XZ] = meshgrid(0:gs(1):(nc(1)-1)*gs(1),0:gs(3):(nc(3)-1)*gs(3));
X_XZ = X_XZ'/radius-(centr(1)+0.5*gs(1))*ones(nc(1),nc(3))/radius;
Z_XZ = Z_XZ'/radius-(centr(3)+0.5*gs(3))*ones(nc(1),nc(3))/radius;

[Y_YZ,Z_YZ] = meshgrid(0:gs(2):(nc(2)-1)*gs(2),0:gs(3):(nc(3)-1)*gs(3));
Y_YZ = Y_YZ'/radius-(centr(2)+0.5*gs(2))*ones(nc(2),nc(3))/radius;
Z_YZ = Z_YZ'/radius-(centr(3)+0.5*gs(3))*ones(nc(2),nc(3))/radius;



% planet center in cell number (NB: cell number start at 1
icentr = fix(centr(1)/gs(1))+1;
jcentr = fix(centr(2)/gs(2))+1;
kcentr = fix(centr(3)/gs(3))+1;
iwake = icentr + fix(1.5*radius/gs(1));

Jx_XY      = zeros(nc(1),nc(2));
Jx_XY(:,:) = Jx(:,:,kcentr);
Jy_XY      = zeros(nc(1),nc(2));
Jy_XY(:,:) = Jy(:,:,kcentr);
Jz_XY      = zeros(nc(1),nc(2));
Jz_XY(:,:) = Jz(:,:,kcentr);
Jtot_XY    = zeros(nc(1),nc(2));
Jtot_XY(:,:) = Jtot(:,:,kcentr);

Jx_XZ      = zeros(nc(1),nc(3));
Jx_XZ(:,:) = Jx(:,jcentr,:);
Jy_XZ      = zeros(nc(1),nc(3));
Jy_XZ(:,:)  = Jy(:,jcentr,:);
Jz_XZ      = zeros(nc(1),nc(3));
Jz_XZ(:,:)  = Jz(:,jcentr,:);
Jtot_XZ    = zeros(nc(1),nc(3));
Jtot_XZ(:,:) = Jtot(:,jcentr,:);

Jx_YZ_term      = zeros(nc(2),nc(3));
Jx_YZ_term(:,:) = Jx(icentr,:,:);
Jy_YZ_term      = zeros(nc(2),nc(3));
Jy_YZ_term(:,:) = Jy(icentr,:,:);
Jz_YZ_term      = zeros(nc(2),nc(3));
Jz_YZ_term(:,:) = Jz(icentr,:,:);
Jtot_YZ_term    = zeros(nc(2),nc(3));
Jtot_YZ_term(:,:) = Jtot(icentr,:,:);

Jx_YZ_wake      = zeros(nc(2),nc(3));
Jx_YZ_wake(:,:) = Jx(iwake,:,:);
Jy_YZ_wake      = zeros(nc(2),nc(3));
Jy_YZ_wake(:,:) = Jy(iwake,:,:);
Jz_YZ_wake      = zeros(nc(2),nc(3));
Jz_YZ_wake(:,:) = Jz(iwake,:,:);
Jtot_YZ_wake    = zeros(nc(2),nc(3));
Jtot_YZ_wake(:,:) = Jtot(iwake,:,:);


%-- Bow shock parameter
% Extracted from Trotignon et al, PSS, 2006
xshock = ([1:0.1:nc(1)]*gs(1)-centr(1))/radius;
eps = 1.026;
L = 2.081;
x_F = 0.6;
yshock = NaN.*zeros(1,size(xshock,2));
for i=1:size(xshock,2)
  yshock(i) = pos_boundary(xshock(i),x_F,eps,L);
end

ind = find(isnan(yshock)==1);
yshock(ind(1)) = 0;
yshock = [yshock -yshock];
xshock = [xshock xshock];

%-- IMB parameter
% Extracted from Vignes et al, GRL, 2000
ximb = ([1:0.1:nc(1)]*gs(1)-centr(1))/radius;
eps = 0.9;
L = 0.96;
x_F = 0.78;
yimb = NaN.*zeros(1,size(ximb,2));
for i=1:size(ximb,2)
  yimb(i) = pos_boundary(ximb(i),x_F,eps,L);
end

ind = find(isnan(yimb)==1);
yimb(ind(1)) = 0;
yimb = [yimb -yimb];
ximb = [ximb ximb];

% planet drawing
theta = 2.*pi*[1:101]/100.;
xp = cos(theta);
yp = sin(theta);

colormap('redblue');
%-- Figure 1 & 2 -- Jx
%**************************************************************************
figure(1);
colormap('redblue');
pcolor(X_XY,Y_XY,Jx_XY);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);
hold off;
titre = strcat('Jx [nA/m^2] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Jx_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f1','-dpng'); 

%==
figure(2);
colormap('redblue');
pcolor(X_XZ,Z_XZ,Jx_XZ);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);
hold off;
titre = strcat('Jx [nA/m^2] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Jx_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f2','-dpng'); 

%-- Figure 3 & 4 -- Jy
%**************************************************************************
figure(3);
colormap('redblue');
pcolor(X_XY,Y_XY,Jy_XY);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);
hold off;
titre = strcat('Jy [nA/m^2] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Jy_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f3','-dpng'); 

%==
figure(4);
colormap('redblue');
pcolor(X_XZ,Z_XZ,Jy_XZ);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);
hold off;
titre = strcat('Jy [nA/m^2] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Jy_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f4','-dpng'); 

%-- Figure 5 & 6 -- Jz
%**************************************************************************
figure(5);
colormap('redblue');
pcolor(X_XY,Y_XY,Jz_XY);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);
hold off;
titre = strcat('Jz [nA/m^2] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [-max_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Jz_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f5','-dpng'); 

%==
figure(6);
colormap('redblue');
pcolor(X_XZ,Z_XZ,Jz_XZ);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);
hold off;
titre = strcat('Jz [nA/m^2] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [-max_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Jz_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f6','-dpng'); 

%-- Figure 7 & 8 -- Jtot
%**************************************************************************
figure(7);
colormap('redblue');
pcolor(X_XY,Y_XY,Jtot_XY);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);
hold off;
titre = strcat('Jtot [nA/m^2] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Jtot_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f7','-dpng'); 

%==
figure(8);
colormap('redblue');
pcolor(X_XZ,Z_XZ,Jtot_XZ);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
  %plot(-xshock,yshock,'k','linewidth',2);
  %plot(-ximb,yimb,'k--','linewidth',2);
hold off;
titre = strcat('Jtot [nA/m^2] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Jtot_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f8','-dpng'); 

%=========== figure in YZ plane ==============================
% Jx in X=0 and X=1.5Rm

% figure 9 & 10
figure(9);
colormap('redblue');
pcolor(Y_YZ,Z_YZ,Jx_YZ_term);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
hold off;
titre = strcat('Jx [nA/m^2] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Jx_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f9','-dpng'); 


figure(10);
colormap('redblue');
pcolor(Y_YZ,Z_YZ,Jx_YZ_wake);
shading flat;

titre = strcat('Jx [nA/m^2] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Jx_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f10','-dpng');


% Jy in X=0 and X=1.5Rm
% figure 11 & 12
figure(11);
colormap('redblue');
pcolor(Y_YZ,Z_YZ,Jy_YZ_term);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
hold off;
titre = strcat('Jy [nA/m^2] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Jy_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f11','-dpng'); 


figure(12);
colormap('redblue');
pcolor(Y_YZ,Z_YZ,Jy_YZ_wake);
shading flat;

titre = strcat('Jy [nA/m^2] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Jy_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f12','-dpng');

% Jz in X=0 and X=1.5Rm
% figure 13 & 14
figure(13);
colormap('redblue');
pcolor(Y_YZ,Z_YZ,Jz_YZ_term);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
hold off;
titre = strcat('Jz [nA/m^2] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [-max_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Jz_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f13','-dpng'); 


figure(14);
colormap('redblue');
pcolor(Y_YZ,Z_YZ,Jz_YZ_wake);
shading flat;

titre = strcat('Jz [nA/m^2] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [-max_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Jz_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f14','-dpng');

% Jtot in X=0 and X=1.5Rm
% figure 15 & 16
figure(15);
colormap('redblue');
pcolor(Y_YZ,Z_YZ,Jtot_YZ_wake);
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
hold off;
titre = strcat('Jtot [nA/m^2] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Jtot_YZ_terminator_Mercury_',runname,diagtime,'.png');
print(filename,'-f15','-dpng'); 


figure(16);
colormap('redblue');
pcolor(Y_YZ,Z_YZ,Jtot_YZ_wake);
shading flat;

titre = strcat('Jtot [nA/m^2] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');

c_limit = [min_valtot max_valtot];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
%set(h_bar, 'CLim', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Jtot_YZ_wake_Mercury_',runname,diagtime,'.png');
print(filename,'-f16','-dpng');
