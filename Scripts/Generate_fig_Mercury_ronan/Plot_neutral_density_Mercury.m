%%%%%%%%%%%%%%%%%%%%%%
% Plot_neutral_density_mars.m
%---------------------
% This routine reads the atmosphere
%  file and plot the map in 
% the XY and XZ plane
%
% R. Modolo
% UVSQ-LATMOS
% ronan.modolo@latmos.ipsl.fr
% December 2011
%%%%%%%%%%%%%%%%%%%%%%

typefile = 'Atmw_';
ncfile = [dirname typefile runname diagtime '.nc'];

ncid = netcdf.open(ncfile,'NC_NOWRITE');
% Get information about the contents of the file.
[numdims, numvars, numglobalatts, unlimdimID] = netcdf.inq(ncid);
nom_variable ='' ;

for i=0:numvars-1

[varname, xtype, dimids, numatts] = netcdf.inqVar(ncid,i);

nom_variable = char(nom_variable, varname);

end

planetname = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'planetname')));
centr      = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'s_centr')));
radius     = transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'r_planet')));
gs=transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'gstep')));
nptot=transpose(netcdf.getVar(ncid,netcdf.inqVarID(ncid,'nptot')));
Dn_H         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Den_H'));
Dn_He         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Den_He'));
Dn_Na         = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'Den_Na'));
nrm = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'phys_density'));
%radius=radius/gs(1);
%centr=centr./gs;
nc = size(Dn_H);
netcdf.close(ncid);

Dn_H = Dn_H*nrm*1.e-6;  % values in cm^-3
Dn_He = Dn_He*nrm*1.e-6;  % values in cm^-3
Dn_Na = Dn_Na*nrm*1.e-6;  % values in cm^-3

% maximum and minimum 
min_val = 2.; % log(cm-3)
max_val = 4.;  % log(sm-3)

% -- Creation of axis values centered on the planet ( nromalized to planet
% radius)
[X_XY,Y_XY] = meshgrid(0:gs(1):(nc(1)-1)*gs(1),0:gs(2):(nc(2)-1)*gs(2));
X_XY = X_XY'/radius-(centr(1)+0.5*gs(1))*ones(nc(1),nc(2))/radius;
Y_XY = Y_XY'/radius-(centr(2)+0.5*gs(2))*ones(nc(1),nc(2))/radius;

[X_XZ,Z_XZ] = meshgrid(0:gs(1):(nc(1)-1)*gs(1),0:gs(3):(nc(3)-1)*gs(3));
X_XZ = X_XZ'/radius-(centr(1)+0.5*gs(1))*ones(nc(1),nc(3))/radius;
Z_XZ = Z_XZ'/radius-(centr(3)+0.5*gs(3))*ones(nc(1),nc(3))/radius;

[Y_YZ,Z_YZ] = meshgrid(0:gs(2):(nc(2)-1)*gs(2),0:gs(3):(nc(3)-1)*gs(3));
Y_YZ = Y_YZ'/radius-(centr(2)+0.5*gs(1))*ones(nc(2),nc(3))/radius;
Z_YZ = Z_YZ'/radius-(centr(3)+0.5*gs(3))*ones(nc(2),nc(3))/radius;


% planet center in cell number (NB: cell number start at 1
icentr = fix(centr(1)/gs(1))+1;
jcentr = fix(centr(2)/gs(2))+1;
kcentr = fix(centr(3)/gs(3))+1;

DnH_XY      = zeros(nc(1),nc(2));
DnH_XY(:,:) = Dn_H(:,:,kcentr);
DnHe_XY      = zeros(nc(1),nc(2));
DnHe_XY(:,:) = Dn_He(:,:,kcentr);
DnNa_XY      = zeros(nc(1),nc(2));
DnNa_XY(:,:) = Dn_Na(:,:,kcentr);

DnH_XZ      = zeros(nc(1),nc(3));
DnH_XZ(:,:) = Dn_H(:,jcentr,:);
DnHe_XZ      = zeros(nc(1),nc(3));
DnHe_XZ(:,:) = Dn_He(:,jcentr,:);
DnNa_XZ      = zeros(nc(1),nc(3));
DnNa_XZ(:,:) = Dn_Na(:,jcentr,:);

DnH_YZ      = zeros(nc(2),nc(3));
DnH_YZ(:,:) = Dn_H(icentr,:,:);
DnHe_YZ      = zeros(nc(2),nc(3));
DnHe_YZ(:,:) = Dn_He(icentr,:,:);
DnNa_YZ      = zeros(nc(2),nc(3));
DnNa_YZ(:,:) = Dn_Na(icentr,:,:);

% planet drawing
theta = 2.*pi*[1:101]/100.;
xp = cos(theta);
yp = sin(theta);


%-- Figure 1 & 2 -- Dn  H
%**************************************************************************
figure(1);
colormap('jet');
pcolor(X_XY,Y_XY,log10(DnH_XY));
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
%  plot(-xshock,yshock,'k','linewidth',2);
%  plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Dn H log[cm-3] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');
set(gca,'XLim',[-2 2]);
set(gca,'YLim',[-2 2]);

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Dn_Hneutral_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f1','-dpng'); 

%==
figure(2);
colormap('jet');
pcolor(X_XZ,Z_XZ,log10(DnH_XZ));
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
%  plot(-xshock,yshock,'k','linewidth',2);
%  plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Dn H log[cm-3] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');
set(gca,'XLim',[-2 2]);
set(gca,'YLim',[-2 2]);

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Dn_Hneutral_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f2','-dpng'); 

%-- Figure 3 & 4 -- Dn  He
%**************************************************************************
min_val = 2.; % log(cm-3)
max_val = 6.;  % log(sm-3)


figure(3);
colormap('jet');
pcolor(X_XY,Y_XY,log10(DnHe_XY));
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Dn He log[cm-3] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');
set(gca,'XLim',[-2 2]);
set(gca,'YLim',[-2 2]);

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Dn_Heneutral_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f3','-dpng'); 

%==

figure(4);
colormap('jet');
pcolor(X_XZ,Z_XZ,log10(DnHe_XZ));
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
 % plot(-xshock,yshock,'k','linewidth',2);
 % plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Dn He log[cm-3] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');
set(gca,'XLim',[-2 2]);
set(gca,'YLim',[-2 2]);

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Dn_Heneutral_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f4','-dpng'); 

%-- Figure 5 & 6 -- Dn  Na
%**************************************************************************

min_val = 2.; % log(cm-3)
max_val = 4.;  % log(sm-3)
figure(5);
colormap('jet');
pcolor(X_XY,Y_XY,log10(DnNa_XY));
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
  %plot(-xshock,yshock,'k','linewidth',2);
  %plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Dn Na log[cm-3] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Y [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');
set(gca,'XLim',[-2 2]);
set(gca,'YLim',[-2 2]);

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Dn_Naneutral_XY_Mercury_',runname,diagtime,'.png');
print(filename,'-f5','-dpng'); 

%==
figure(6);
colormap('jet');
pcolor(X_XZ,Z_XZ,log10(DnNa_XZ));
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
  %plot(-xshock,yshock,'k','linewidth',2);
  %plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Dn Na log[cm-3] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('X [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');
set(gca,'XLim',[-2 2]);
set(gca,'YLim',[-2 2]);

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Dn_Naneutral_XZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f6','-dpng'); 
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(7);
colormap('jet');
pcolor(Y_YZ,Z_YZ,log10(DnH_YZ));
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
%  plot(-xshock,yshock,'k','linewidth',2);
%  plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Dn H log[cm-3] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');
set(gca,'XLim',[-2 2]);
set(gca,'YLim',[-2 2]);

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Dn_Hneutral_YZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f7','-dpng'); 

figure(8);
min_val = 2.; % log(cm-3)
max_val = 6.;  % log(sm-3)

colormap('jet');
pcolor(Y_YZ,Z_YZ,log10(DnHe_YZ));
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
%  plot(-xshock,yshock,'k','linewidth',2);
%  plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Dn He log[cm-3] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');
set(gca,'XLim',[-2 2]);
set(gca,'YLim',[-2 2]);

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Dn_Heneutral_YZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f8','-dpng'); 

figure(9);
min_val = 2.; % log(cm-3)
max_val = 4.;  % log(sm-3)
colormap('jet');
pcolor(Y_YZ,Z_YZ,log10(DnNa_YZ));
shading flat;
hold on;
  plot(xp,yp,'k','linewidth',2);
  fill(xp,yp,'w');
%  plot(-xshock,yshock,'k','linewidth',2);
%  plot(-ximb,yimb,'k--','linewidth',2);  
hold off;
titre = strcat('Dn Na log[cm-3] time:',diagtime);
title(titre,'fontsize',12,'fontweight','b');
xlabel('Y [R_M]','fontsize',12,'fontweight','b');
ylabel('Z [R_M]','fontsize',12,'fontweight','b');
set(gca,'fontsize',12,'fontweight','b');
set(gca,'XLim',[-2 2]);
set(gca,'YLim',[-2 2]);

c_limit = [min_val max_val];
caxis(c_limit);

h_bar = colorbar('location','Eastoutside');
scale_data = linspace( c_limit(1), c_limit(2), length( colormap ));
set(h_bar, 'Limits', c_limit,'fontsize',12,'fontweight','b'); 
set(gca,'DataAspectRatio',[1 1 1]);

filename = strcat(dirname,'Dn_Naneutral_YZ_Mercury_',runname,diagtime,'.png');
print(filename,'-f9','-dpng'); 

