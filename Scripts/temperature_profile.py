from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import math
import sys, os


ncfile = sys.argv[1]
ncid = Dataset(ncfile)
var_nc = ncid.variables

centr      = var_nc['s_centr'][:]
radius     = var_nc['r_planet'][:]
r_iono     = var_nc['r_iono'][:]
gs         = var_nc['gstep'][:]
phys_length = var_nc['phys_length'][:]
temp = var_nc["Temperature"][:]
nc = [len(temp[0][0]), len(temp[0]), len(temp)]

icentr = int(np.fix(centr[0]/gs[0]))
jcentr = int(np.fix(centr[1]/gs[1]))
kcentr = int(np.fix(centr[2]/gs[2]))

temp_X=temp[kcentr,jcentr,:] #profile selon x avec y=0, z=0
temp_Y=temp[kcentr,:,icentr] #profile selon y avec x=0, z=0
temp_Z=temp[:,jcentr,icentr] #profile selon z avec x=0, y=0
X=np.arange(0,nc[0]*gs[0],gs[0])/radius - centr[0]/radius
Y=np.arange(0,nc[1]*gs[1],gs[1])/radius - centr[1]/radius
Z=np.arange(0,nc[2]*gs[2],gs[2])/radius - centr[2]/radius



plt.plot(X[icentr:],temp_X[icentr:],c="b",label="temperature")
plt.legend(loc="upper right")
plt.xlabel("X [$R_E$]")
plt.ylabel("temperature [eV]")
plt.xlim(1,2.5)
plt.savefig("profile_X_temperature.png")

plt.close('all')
plt.plot(Y[jcentr:],temp_Y[jcentr:],c="b",label="temperature")
plt.legend(loc="upper right")
plt.xlabel("Y [$R_E$]")
plt.ylabel("temperature [eV]")
plt.xlim(1,2.5)
plt.savefig("profile_Y_temperature.png")

plt.close('all')
plt.plot(Z[kcentr:],temp_Z[kcentr:],c="b",label="temperature")
plt.legend(loc="upper right")
plt.xlabel("Z [$R_E$]")
plt.ylabel("temperature [eV]")
plt.xlim(1,2.5)
plt.savefig("profile_Z_temperature.png")