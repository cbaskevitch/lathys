from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st
import math
import sys, os

for i in range(0,64):
    if i<10:
        ncid = Dataset("p3_00"+str(i)+"_17_09_21_t00100.nc")
    else:
        ncid = Dataset("p3_0"+str(i)+"_17_09_21_t00100.nc")
    var_nc = ncid.variables
    pmass = var_nc["particule_mass"][:]
    pchar = var_nc["particule_char"][:]
    pqsm = pchar/pmass
    for p in range(0,len(pqsm)):
        if pqsm[p] != 1.0:
            print("proc : ",i," qsm : ",pqsm[p])